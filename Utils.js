import React from 'react';

import { nameIdText } from './reactadmin/Utils';
import { PageNumbers } from './store/enums';
import {
    DropDown, Input,
    ModalFactory,
    Pager,
    Tooltip,
} from './reactadmin/components';
import i18n from './i18n';

export * from './reactadmin/Utils';

export const setFilterHandles = (pageComponent, actions, reduxProp, isPaginate = true) => {
    const that = pageComponent;
    that.setState({
        where: [],
        whereObj: {},
        whereText: {},
    });
    that.reduxProp = reduxProp;
    that.isPaginate = isPaginate;

    if (!that.fetch) {
        that.fetch = ({ success, failure } = {}) => {
            const { dispatch, token } = that.props;
            const { where, } = that.state;
            dispatch(actions.setProp('list', []));
            dispatch(actions.fetch(token, where, '', false, success, failure));
        };
    }
    if (!that.fetchPage) {
        that.fetchPage = (pageNumber = 1) => {
            const { dispatch, token, [reduxProp]: { pageSize, }, } = that.props;
            const { where, } = that.state;
            dispatch(actions.setProp('list', []));
            dispatch(actions.setProp('pageNumber', pageNumber));
            dispatch(actions.setProp('totalRows', 0));

            const success = (resp) => {
                // console.log(resp);
                const { status, data } = resp;
                if (status === 200 && data.code === 200) {
                    const { items, totalRows } = data;
                    if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
                        const maxPage = Pager.getMaxPage(totalRows, pageSize);
                        that.fetchPage(maxPage);
                    }
                }
            };
            dispatch(actions.fetchPage({ token, where, pageNumber, pageSize, success, }));
        };
    }
    if (!that.pageChange) {
        that.pageChange = ({ newPage }) => {
            that.fetchPage(newPage);
        };
    }
    if (!that.fetchData) {
        that.fetchData = (newPage) => {
            if (that.isPaginate) {
                that.fetchPage(newPage);
            } else {
                that.fetch();
            }
        };
    }

    if (!that.filterText) {
        that.filterText = (label, def) => that.state.whereText[label] || def;
    }
    if (!that.filterTextChange) {
        that.filterTextChange = (name, value) => {
            that.setState({ whereText: { ...that.state.whereText, [name]: value, } });
        };
    }
    if (!that.filterValue) {
        that.filterValue = (label, def) => {
            const { whereObj } = that.state;
            return (whereObj[label] && whereObj[label].value) || def;
        };
    }
    if (!that.filterChange) {
        that.filterChange = ({ label, name, value, criteria, separator, type, group }) => {
            const { whereObj, } = that.state;
            const prop = label || name;
            let filter;
            if (whereObj[prop]) {
                filter = whereObj[prop];
            } else {
                filter = {};
            }

            if ((!value && value !== 0 && value !== false) || value === 'none') {
                filter = undefined;
            } else {
                filter.separator = separator;
                filter.name = name;
                filter.criteria = criteria;
                filter.type = type;
                filter.value = value || '';
                filter.group = group;
            }

            that.setState({
                whereObj: Object.assign({}, whereObj, { [prop]: filter })
            });
        };
    }
    if (!that.filterClear) {
        that.filterClear = () => {
            const { dispatch } = that.props;
            const where = [];
            const whereObj = {};
            const whereText = {};
            dispatch(actions.setProp('where', where));
            dispatch(actions.setProp('whereObj', whereObj));
            dispatch(actions.setProp('whereText', whereText));
            dispatch(actions.setProp('totalRows', 0));
            dispatch(actions.setProp('pageNumber', 1));
            dispatch(actions.setProp('list', []));
            that.setState({ where, whereObj, whereText }, () => that.fetchData(1));
        };
    }

    if (!that.loadWhere) {
        that.loadWhere = (callback) => {
            const { [reduxProp]: { where, whereObj, whereText, advancedSearch, } } = that.props;
            // that.state.where = where;
            // that.state.whereObj = whereObj;
            // that.state.whereText = whereText;
            that.setState({ where, whereObj, whereText, advancedSearch, }, callback);
        };
    }
    if (!that.saveWhere) {
        that.saveWhere = (callback) => {
            const { dispatch, } = that.props;
            const { whereObj, whereText, advancedSearch } = that.state;
            const where = Object.keys(whereObj)
                .map(key => (whereObj[key]))
                .filter(i => !!i);
            that.setState({ where });
            dispatch(actions.setProp('where', where));
            dispatch(actions.setProp('whereObj', whereObj));
            dispatch(actions.setProp('whereText', whereText));
            dispatch(actions.setProp('advancedSearch', advancedSearch));
            // that.state.where = where;
            that.setState({ where }, callback);
        };
    }
    if (!that.saveWhereObj) {
        that.saveWhereObj = (callback) => {
            const { dispatch, } = that.props;
            const { where, whereText, advancedSearch } = that.state;
            const whereObj = {};
            (where || []).forEach(w => {
                const prop = w.label || w.name;
                if (w.value) {
                    whereObj[prop] = w;
                }
            });
            that.setState({ whereObj });
            dispatch(actions.setProp('where', where));
            dispatch(actions.setProp('whereObj', whereObj));
            dispatch(actions.setProp('whereText', whereText));
            dispatch(actions.setProp('advancedSearch', advancedSearch));
            // that.state.whereObj = whereObj;
            that.setState({ whereObj }, callback);
        };
    }

    if (!that.searchClick) {
        that.searchClick = () => {
            const callback = () => that.fetchPage(1);
            if (that.state.advancedSearch) {
                that.saveWhereObj(callback);
            } else {
                that.saveWhere(callback);
            }
        };
    }
    if (!that.cleanClick) {
        that.cleanClick = () => {
            that.filterClear();
        };
    }
    if (!that.advancedClick) {
        that.advancedClick = () => {
            const advancedSearch = !that.state.advancedSearch;
            that.setState({
                advancedSearch,
                where: advancedSearch ? [{ criteria: '=', separator: 'and', value: '', }] : [],
                whereObj: {},
                whereText: {},
            });
        };
    }

    if (!that.initFetch) {
        that.initFetch = (args) => {
            that.loadWhere(() => that.fetch(args));
        };
    }
    if (!that.initFirstPage) {
        that.initFirstPage = () => {
            that.loadWhere(() => that.fetchPage(1));
        };
    }
    if (!that.initSamePage) {
        that.initSamePage = () => {
            that.loadWhere(() => that.fetchPage(that.props[reduxProp].pageNumber));
        };
    }

    if (!that.onKeyDownSimple) {
        that.onKeyDownSimple = (e) => {
            if (e.key === 'Enter') {
                that.searchClick();
            }
        };
    }
    if (!that.onKeyDownMixed) {
        that.onKeyDownMixed = (e) => {
            if (e.key === 'Enter' && (e.altKey)) {
                that.searchClick();
            }
        };
    }

    return that;
};
export const setItemHandles = ({ pageComponent, actions, itemName }) => {
    const that = pageComponent;
    that.modalNameRef = `${itemName}Modal`;
    const state = {};
    if (!that.state.item) {
        state.item = {};
    }
    if (!that.state.itemDisabled) {
        state.itemDisabled = false;
    }
    if (!that.state.deleting) {
        state.deleting = {};
    }
    that.setState(state);

    if (!that.newItemClick) {
        that.newItemClick = (e) => {
            e.preventDefault();
            that.setState({ item: {}, itemDisabled: false, });
            ModalFactory.show(that.modalNameRef);
        };
    }

    if (!that.selectItem) {
        that.selectItem = (item) => {
            // console.log(`${itemName} clicked: `, item);
            that.setState({ item: JSON.parse(JSON.stringify(item)) });
            ModalFactory.show(that.modalNameRef);
        };
    }

    if (!that.saveItem) {
        that.saveItem = (item) => {
            const { dispatch, token, } = that.props;
            const success = () => {
                ModalFactory.hide(that.modalNameRef);
                that.setState({ itemDisabled: false, });
                const page = item.id ? that.props[that.reduxProp].pageNumber : 1;
                that.fetchData(page);
            };
            const failure = () => that.setState({ itemDisabled: false });
            that.setState({ itemDisabled: true });
            if (!item.id) {
                dispatch(actions.create(token, item, success, failure));
            } else {
                dispatch(actions.update({ token, item, success, failure }));
            }
        };
    }

    if (!that.handleItemDelete) {
        that.handleItemDelete = (id) => {
            that.setState({
                confirmModalProps: {
                    title: i18n.t(`WishDelete${itemName}`),
                    description: i18n.t(`WishDelete${itemName}Description`),
                    buttons: [
                        {
                            className: 'btn-danger',
                            label: i18n.t('Ok'),
                            onClick: () => {
                                const deleting = (v) => that.setState({
                                    deleting: Object.assign({}, that.state.deleting, { [id]: v }),
                                });
                                const { dispatch, token, } = that.props;
                                deleting(true);
                                dispatch(actions.delete({
                                    token,
                                    item: { id },
                                    success: () => {
                                        ModalFactory.hide('confirmModal');
                                        // deleting(false);
                                        that.fetchData(that.props[that.reduxProp].pageNumber);
                                    },
                                    failure: () => deleting(false),
                                }));
                            },
                            disabled: () => that.state.deleting[id],
                        },
                        {
                            className: 'btn-default',
                            label: i18n.t('Close'),
                            onClick: () => ModalFactory.hide('confirmModal'),
                        },
                    ],
                },
            }, () => ModalFactory.show('confirmModal'));
        };
    }

    return that;
};

export const NameIDText = (item) => nameIdText(item, 'name', 'id');
export const EmployeeNamePropText = (item) => nameIdText(item, 'employee_name', 'id_employee');

export const renderListHeader = function ({
    textFilter, onFilterChange,
    pageSize, onPageSizeChange,
    orderBy, orderDesc, orderByList, onOrderChange,
}) {
    const pageNumbers = PageNumbers;
    return (
        <header className="panel-heading" style={{ marginTop: 10, }}>
            <div className="col-sm-3 pull-right p-l-none">
                <Tooltip className="balloon" tag="TooltipTablePageSize">
                    <DropDown
                        items={pageNumbers}
                        value={pageSize}
                        onChange={(e) => onPageSizeChange(e.target.value * 1)}
                        style={{ margin: 0 }}
                    />
                </Tooltip>
                {i18n.t('recordsByPage')}
            </div>
            <div className="col-sm-4 pull-right">
                <Tooltip className="balloon" tag="TooltipTableTextSearch">
                    <Input
                        clearable
                        placeholder={i18n.t('Search')}
                        onFieldChange={(e) => onFilterChange(e.target.value)}
                        value={textFilter || ''}
                    />
                </Tooltip>
            </div>
            {onOrderChange &&
                <div className="col-sm-3 pull-right p-l-none">
                    <Tooltip className="balloon" tag="TooltipTableOrderBy">
                        <DropDown
                            items={orderByList}
                            value={orderBy.id}
                            emptyOption={<option disabled>{i18n.t('OrderBy')}</option>}
                            onChange={(e) => {
                                const newOrder = orderByList
                                    .find(i => `${i.id}` === e.target.value);
                                onOrderChange(newOrder);
                            }}
                            style={{ margin: 0 }}
                        />
                    </Tooltip>
                    {/* {i18n.t('OrderBy')}: */}
                    <span className="m-l-sm m-r-sm">{i18n.t('DescendentOrder')}: </span>
                    <Tooltip tag="TooltipTableOrderDescendent">
                        <input
                            type="checkbox"
                            value={orderDesc || false}
                            onChange={(e) => onOrderChange(orderBy, e.target.checked)}
                        />
                    </Tooltip>
                </div>
            }
        </header>
    );
};
