/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Index
 */

import 'babel-polyfill';
import React, { Component } from 'react';
import ReactDom from 'react-dom';
import $ from 'jquery';
import 'react-drafts/dist/react-drafts.css';

import i18n from './i18n';
import { AnimatedSpinner, NotificationShower, } from './reactadmin/components';

/**
 * Componente raíz. Muestra el indicador de cargando mientras se solicita el perfil del usuario.
 *
 * @class IndexComponent
 * @extends {Component}
 */
class IndexComponent extends Component {
	state = {
		loaded: false,
	}
	componentWillMount() {
		i18n.on('loaded', () => {
			this.setState({ loaded: true });
		});
	}
	render() {
		let view = null;
		if (!this.state.loaded) {
			view = (
				<div
					style={{
						position: 'absolute',
						display: 'flex',
						height: '100%',
						width: '100%',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<div>
						<AnimatedSpinner show style={{ fontSize: '3em', }} />
					</div>
				</div>
			);
		} else {
			const { Root } = require('./containers/');

			view = <Root />;
		}

		return (
			<div style={{ height: '100%', width: '100%', }}>
				<NotificationShower />
				{view}
			</div>
		);
	}
}
ReactDom.render(
	<IndexComponent />,
	document.getElementById('wrapperContainer')
);
window.$ = window.jQuery = $;
$(document).on('show.bs.modal', '.modal', function () {
	const zIndex = 1040 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);
	setTimeout(() => {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});
