import { CommonReducers } from '../reactadmin/reducers/';
import { ReduxOutlet } from '../outlets/'; // Outlet reducers

const baseInitialState = {
	item: {},
	items: {},
	list: [],
	pageNumber: 1,
	pageSize: 10,
	totalRows: 0,
	where: [],
	whereObj: {},
	whereText: {},
	temp: [],
	tempNumber: 1,
	tempSize: 10,
	tempRows: 0,
	all: [],
	tempAll: [],
	fetchedList: false,
	isFetching: false,
	editing: -1,
	didInvalidate: false,
};

// outlets
// const boards = LocalReduxOutlet('board').makeReducer(initialState.boards);
// const pins = LocalReduxOutlet('pin').makeReducer(initialState.pins);
// const notes = LocalReduxOutlet('note').makeReducer(initialState.notes);
// const messages = LocalReduxOutlet('messages').makeReducer(initialState.messages);
// const emails = LocalReduxOutlet('emails').makeReducer(initialState.emails);

const workflows = ReduxOutlet('workflows', 'workflow').makeReducer(baseInitialState);
const workflowCriterias = ReduxOutlet('workflows', 'workflowCriteria')
	.makeReducer(baseInitialState);
const workflowSteps = ReduxOutlet('workflows', 'workflowstep').makeReducer(baseInitialState);
const workflowStepContent = ReduxOutlet('workflows', 'workflowStepContent')
	.makeReducer(baseInitialState);
const workflowVersions = ReduxOutlet('workflows', 'workflowVersion').makeReducer(baseInitialState);

const docs = ReduxOutlet('docs', 'doc').makeReducer(baseInitialState);
const contents = ReduxOutlet('contents', 'content').makeReducer(baseInitialState);
const languages = ReduxOutlet('languages', 'language').makeReducer(baseInitialState);
const systemTags = ReduxOutlet('languages', 'systemTag').makeReducer(baseInitialState);

const centers = ReduxOutlet('centers', 'center').makeReducer(baseInitialState);
const categories = ReduxOutlet('categories', 'category').makeReducer(baseInitialState);
const criterias = ReduxOutlet('criterias', 'criteria').makeReducer(baseInitialState);
const delegations = ReduxOutlet('delegations', 'delegation').makeReducer(baseInitialState);
const endCalls = ReduxOutlet('endcalls', 'endCall').makeReducer(baseInitialState);
const dashboard = ReduxOutlet('dashboard', 'dashboard').makeReducer(baseInitialState);

const transactions = ReduxOutlet('transactions', 'transaction').makeReducer(baseInitialState);
const transactionNotes = ReduxOutlet('transactions', 'transactionNote')
	.makeReducer(baseInitialState);
const vTransactions = ReduxOutlet('transactions', 'v_transaction').makeReducer(baseInitialState);
const vTransactionSteps = ReduxOutlet('transactions', 'v_transactionstep')
	.makeReducer(baseInitialState);
const alerts = ReduxOutlet('alerts', 'alert').makeReducer(baseInitialState);
const alertColumns = ReduxOutlet('alerts', 'alertColumn').makeReducer(baseInitialState);
const alertCriterias = ReduxOutlet('alerts', 'alertCriteria').makeReducer(baseInitialState);
const alertTriggereds = ReduxOutlet('alerts', 'alertTriggered').makeReducer(baseInitialState);
const alertTriggeredDatas = ReduxOutlet('alerts', 'alertTriggeredData')
	.makeReducer(baseInitialState);

const interfaces = ReduxOutlet('interfaces', 'interface').makeReducer(baseInitialState);
const interfaceBases = ReduxOutlet('interfaces', 'interfaceBase').makeReducer(baseInitialState);

export default {
	...CommonReducers,
	alerts,
	alertColumns,
	alertCriterias,
	alertTriggereds,
	alertTriggeredDatas,
	workflows,
	workflowCriterias,
	workflowSteps,
	workflowVersions,
	docs,
	centers,
	contents,
	criterias,
	dashboard,
	delegations,
	languages,
	workflowStepContent,
	categories,
	endCalls,
	systemTags,
	transactions,
	vTransactions,
	vTransactionSteps,
	transactionNotes,
	interfaces,
	interfaceBases,
};
