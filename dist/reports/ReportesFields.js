// <%@ Page Language="C#" AutoEventWireup="false" ContentType="text/javascript" %>
/* eslint-disable max-len, no-unused-vars */

const Dacs = window.Dacs = { Reportes: [], ReporteGroups: [] };
const reportServer = window.top.config.reportServer;
const i18n = window.top.i18n;
const HasRole = (permission) => window.top.user.hasPermissions(permission);
let reporte = {};
let campos = [];
const columnas = [];
function GetSource(ID) {
	let data = $.ajax({
		type: 'GET',
		url: `API/FormularioGetSource?ID_Group=${ID}`,
		async: false,
		dataType: 'json'
	}).responseText;

	data = data.replace(/Name/g, 'label');
	data = data.replace(/Value/g, 'codigo');
	data = jQuery.parseJSON(data);
	return data;
}

function SortByName(x, y) {
	if (x.nombre.toUpperCase() === y.nombre.toUpperCase()) {
		return 0;
	}
	return x.nombre.toUpperCase() > y.nombre.toUpperCase() ? 1 : -1;
}

function SortByName2(x, y) {
	if (x.name.toUpperCase() === y.name.toUpperCase()) {
		return 0;
	}
	return x.name.toUpperCase() > y.name.toUpperCase() ? 1 : -1;
}

reporte.NombreReporte = i18n.t('Transactions');
reporte.idGroup = 1;
reporte.url = `${reportServer}Transactions&rc:Parameters=false&rs:Command=Render`;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{
		codigo: 't.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		selected: true,
		orderby: true,
		orderdesc: true,
		orderbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 't.id_center @operador @valor',
		name: 'id_center',
		nombre: 'ID del centro',
		label: 'ID del centro',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.center_name) @operador lower(@valor)',
		name: 'center_name',
		nombre: 'Nombre del centro',
		label: 'Nombre del centro',
		type: 'textbox',
		selected: true,
		groupby: true,
		groupbyselected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_employee @operador @valor',
		name: 'id_employee',
		nombre: 'Tarjeta del empleado',
		label: 'Tarjeta del empleado',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(t.employee_name), lower(@valor)) @operador 0',
		name: 'employee_name',
		nombre: 'Nombre del empleado',
		label: 'Nombre del empleado',
		type: 'clob',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.manager_id @operador @valor',
		name: 'manager_id',
		nombre: 'Tarjeta del supervisor',
		label: 'Tarjeta del supervisor',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(t.manager_name), lower(@valor)) @operador 0',
		name: 'manager_name',
		nombre: 'Nombre del supervisor',
		label: 'Nombre del supervisor',
		type: 'clob',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_workflow @operador @valor',
		name: 'id_workflow',
		nombre: 'ID del flujo',
		label: 'ID del flujo',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.workflow_name) @operador lower(@valor)',
		name: 'workflow_name',
		nombre: 'Nombre del flujo',
		label: 'Nombre del flujo',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_workflow_version @operador @valor',
		name: 'id_workflow_version',
		nombre: 'ID de la versión de flujo',
		label: 'ID de la versión de flujo',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.version_id_version) @operador lower(@valor)',
		name: 'version_id_version',
		nombre: 'Versión de flujo',
		label: 'Versión de flujo',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_current_step @operador @valor',
		name: 'id_current_step',
		nombre: 'ID del paso actual',
		label: 'ID del paso actual',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.workflow_step_name) @operador lower(@valor)',
		name: 'workflow_step_name',
		nombre: 'Nombre del paso',
		label: 'Nombre del paso',
		type: 'textbox',
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(t.workflow_step_entry_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'workflow_step_entry_date',
		nombre: 'Fecha de entrada al paso',
		label: 'Fecha de entrada al paso',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.state @operador @valor',
		name: 'state',
		nombre: 'Estado de la transacción',
		label: 'Estado de la transacción',
		type: 'dropdown',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false,
			acItems: [
				{ codigo: 1, label: 'En curso', },
				{ codigo: 2, label: 'Completada', },
				{ codigo: 3, label: 'Cancelada', },
			],
		}
	},
	{
		codigo: 't.end_cause @operador @valor',
		name: 'end_cause',
		nombre: 'ID de la razón de fin',
		label: 'ID de la razón de fin',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.end_cause_name) @operador lower(@valor)',
		name: 'end_cause_name',
		nombre: 'Nombre de la razón de fin',
		label: 'Nombre de la razón de fin',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.end_id_employee @operador @valor',
		name: 'end_id_employee',
		nombre: 'Tarjeta del empleado que finalizó',
		label: 'Tarjeta del empleado que finalizó',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(t.end_employee_name), lower(@valor)) @operador 0',
		name: 'end_employee_name',
		nombre: 'Nombre del empleado que finalizó',
		label: 'Nombre del empleado que finalizó',
		type: 'clob',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_category @operador @valor',
		name: 'id_category',
		nombre: 'ID de la categoría',
		label: 'ID de la categoría',
		type: 'number',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.category_name) @operador lower(@valor)',
		name: 'category_name',
		nombre: 'Nombre de categoría',
		label: 'Nombre de categoría',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.id_customer) @operador lower(@valor)',
		name: 'id_customer',
		nombre: 'ID de cliente',
		label: 'ID de cliente',
		type: 'textbox',
		selected: false,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.ccu @operador @valor',
		name: 'ccu',
		nombre: 'CCU',
		label: 'CCU',
		type: 'number',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 't.ban @operador @valor',
		name: 'ban',
		nombre: 'BAN',
		label: 'BAN',
		type: 'number',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.document_type) @operador lower(@valor)',
		name: 'document_type',
		nombre: 'Tipo de documento',
		label: 'Tipo de documento',
		type: 'dropdown',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [{ codigo: 'C\xe9dula', label: 'C\xe9dula' }, { codigo: 'Pasaporte', label: 'Pasaporte' }],
		}
	},
	{
		codigo: 'lower(t.document) @operador lower(@valor)',
		name: 'document',
		nombre: 'Documento',
		label: 'Documento',
		type: 'textbox',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.customer_name) @operador lower(@valor)',
		name: 'customer_name',
		nombre: 'Nombre del cliente',
		label: 'Nombre del cliente',
		type: 'textbox',
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.customer_type) @operador lower(@valor)',
		name: 'customer_type',
		nombre: 'Tipo de cliente',
		label: 'Tipo de cliente',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.segment) @operador lower(@valor)',
		name: 'segment',
		nombre: 'Segmento',
		label: 'Segmento',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id_subscription @operador @valor',
		name: 'id_subscription',
		nombre: 'ID de subscripción',
		label: 'ID de subscripción',
		type: 'textbox',
		selected: false,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.producttype_description) @operador lower(@valor)',
		name: 'producttype_description',
		nombre: 'Producto',
		label: 'Producto',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.billingplan_description) @operador lower(@valor)',
		name: 'billingplan_description',
		nombre: 'Plan',
		label: 'Plan',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.subscriber_no @operador @valor',
		name: 'subscriber_no',
		nombre: 'Número de subscripción',
		label: 'Número de subscripción',
		type: 'number',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.type1) @operador lower(@valor)',
		name: 'type1',
		nombre: 'Tipo 1',
		label: 'Tipo 1',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.type2) @operador lower(@valor)',
		name: 'type2',
		nombre: 'Tipo 2',
		label: 'Tipo 2',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.type3) @operador lower(@valor)',
		name: 'type3',
		nombre: 'Tipo 3',
		label: 'Tipo 3',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.contact_case) @operador lower(@valor)',
		name: 'contact_case',
		nombre: 'Contacto del caso',
		label: 'Contacto del caso',
		type: 'textbox',
		selected: true,
		groupby: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(t.categories), lower(@valor)) @operador 0',
		name: 'categories',
		nombre: 'Categoría',
		label: 'Categoría',
		type: 'clob',
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.case_id) @operador lower(@valor)',
		name: 'case_id',
		nombre: 'Número de caso',
		label: 'Número de caso',
		type: 'textbox', // TODO: evaluate to change to number
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.case_fechacreacion) @operador lower(@valor)',
		name: 'case_fechacreacion',
		nombre: 'Fecha de creación del caso',
		label: 'Fecha de creación del caso',
		type: 'textbox',
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(t.case_sla) @operador lower(@valor)',
		name: 'case_sla',
		nombre: 'SLA del caso',
		label: 'SLA del caso',
		type: 'textbox',
		selected: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(t.start_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'start_date',
		nombre: 'Fecha de Inicio',
		label: 'Fecha de Inicio',
		type: 'datetime',
		subquery: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(t.end_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'end_date',
		nombre: 'Fecha de Fin',
		label: 'Fecha de Fin',
		type: 'datetime',
		subquery: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.duration @operador @valor',
		name: 'duration',
		nombre: 'Duración',
		label: 'Duración',
		type: 'textbox',
		subquery: false,
		value: '',
		selected: false,
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'lower(t.id_call) @operador lower(@valor)',
		name: 'id_call',
		nombre: 'ID de llamada',
		label: 'ID de llamada',
		type: 'textbox',
		subquery: false,
		value: '',
		selected: false,
		css: [{ name: 'width', value: '200px ' }],
		events: [],
		params: {
			isrequired: false,
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(t.note), lower(@valor)) @operador 0',
		name: 'note',
		nombre: 'Notas',
		label: 'Notas',
		type: 'clob',
		selected: false,
		css: [{ name: 'width', value: '400px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	// {
	// 	codigo: 't.caller_alt_phone @operador @valor',
	// 	name: 'caller_alt_phone',
	// 	nombre: 'Teléfono alternativo',
	// 	label: 'Teléfono alternativo',
	// 	type: 'textbox',
	// 	selected: false,
	// 	css: [{ name: 'width', value: '150px ' }],
	// 	events: {},
	// 	params: {
	// 		isrequired: false
	// 	}
	// },
	// {
	// 	codigo: 't.id_indicator @operador @valor',
	// 	name: 'id_indicator',
	// 	nombre: 'ID de indicador',
	// 	label: 'ID de indicador',
	// 	type: 'textbox',
	// 	subquery: false,
	// 	value: '',
	// 	selected: false,
	// 	css: [{ name: 'width', value: '200px ' }],
	// 	events: [],
	// 	params: {
	// 		isrequired: false,
	// 	}
	// },
	{
		codigo: 't.id in (select distinct tr.id_transaction from transaction_result tr where lower(tr.name) @operador lower(@valor))',
		name: 'variable_name',
		nombre: 'Nombre de variable',
		label: 'Nombre de variable',
		type: 'textbox',
		subquery: true,
		selected: true,
		orderby: false,
		nocolumn: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id in (select distinct tr.id_transaction from transaction_result tr where dbms_lob.@function(lower(tr.value), lower(@valor)) @operador 0)',
		name: 'variable_value',
		nombre: 'Valor de variable',
		label: 'Valor de variable',
		type: 'clob',
		subquery: true,
		selected: true,
		orderby: false,
		nocolumn: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: "t.id in (select distinct tr.id_transaction from transaction_result tr where dbms_lob.@function(lower(to_clob(tr.name) || '=' || tr.value), lower(@valor)) @operador 0)",
		name: 'variable_name_value',
		nombre: 'Nombre y Valor de variable',
		label: 'Nombre y Valor de variable',
		type: 'clob',
		subquery: true,
		selected: true,
		orderby: false,
		nocolumn: true,
		css: [{ name: 'width', value: '200px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id in (select distinct att.id_transaction from alert_triggered_transaction att where att.id_alert_triggered @operador @valor)',
		name: 'id_alert_triggered',
		nombre: 'ID de alerta disparada',
		label: 'ID de alerta disparada',
		type: 'number',
		nocolumn: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: 't.id in (select distinct att.id_transaction from alert_triggered_transaction att where att.id_alert_triggered in (select at.id from alert_triggered at where at.id_alert @operador @valor))',
		name: 'id_alert_triggered',
		nombre: 'ID de alerta',
		label: 'ID de alerta',
		type: 'number',
		nocolumn: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
	{
		codigo: "t.id in (select distinct att.id_transaction from alert_triggered_transaction att where att.id_alert_triggered in (select at.id from alert_triggered at where cast(trunc(at.start_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM')))",
		name: 'alert_triggered_start_date',
		nombre: 'Fecha de inicio de alerta disparada',
		label: 'Fecha de inicio de alerta disparada',
		type: 'datetime',
		nocolumn: true,
		css: [{ name: 'width', value: '150px ' }],
		events: {},
		params: {
			isrequired: false
		}
	},
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 1;
Dacs.Reportes.push(reporte);
Dacs.Reportes.selected = 0; //Tener el primero seleccionado por defecto.

//######################################################################################################
//######################################################################################################
//Reporte Log
//######################################################################################################
//######################################################################################################
reporte = {};
reporte.NombreReporte = 'Histórico';
reporte.idGroup = 1;
reporte.url = `${reportServer}Log&rc:Parameters=false&rs:Command=Render`;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{
		codigo: 'l.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		selected: true,
		orderby: true,
		orderdesc: true,
		orderbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(l.event_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'event_date',
		nombre: 'Fecha del evento',
		label: 'Fecha del evento',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'l.id_employee @operador @valor',
		name: 'id_employee',
		nombre: 'Tarjeta del empleado',
		label: 'Tarjeta del empleado',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(l.name) @operador lower(@valor)',
		name: 'name',
		nombre: 'Nombre del empleado',
		label: 'Nombre del empleado',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '200px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(l.table_name) @operador lower(@valor)',
		name: 'table_name',
		nombre: 'Tabla',
		label: 'Tabla',
		type: 'dropdown',
		selected: true,
		orderby: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [
				{ codigo: 'Alertas', label: 'Alertas' },
				{ codigo: 'Alertas disparadas', label: 'Alertas disparadas' },
				{ codigo: 'Bitácora de acceso', label: 'Bitácora de acceso' },
				{ codigo: 'CRM Casos', label: 'CRM Casos' },
				{ codigo: 'Categorías', label: 'Categorías' },
				{ codigo: 'Centros', label: 'Centros' },
				{ codigo: 'Columnas de alertas', label: 'Columnas de alertas' },
				{ codigo: 'Condiciones de criterios', label: 'Condiciones de criterios' },
				{ codigo: 'Configuraciones del sistema', label: 'Configuraciones del sistema' },
				{ codigo: 'Contenidos', label: 'Contenidos' },
				{ codigo: 'Contenidos de pasos de flujos', label: 'Contenidos de pasos de flujos' },
				{ codigo: 'Criterios', label: 'Criterios' },
				{ codigo: 'Criterios de alertas', label: 'Criterios de alertas' },
				{ codigo: 'Criterios de flujos', label: 'Criterios de flujos' },
				{ codigo: 'Delegaciones', label: 'Delegaciones' },
				{ codigo: 'Documentaciones', label: 'Documentaciones' },
				{ codigo: 'Empleados', label: 'Empleados' },
				{ codigo: 'Filtros de Reportes', label: 'Filtros de Reportes' },
				{ codigo: 'Flujos', label: 'Flujos' },
				{ codigo: 'Generación de Reportes', label: 'Generación de Reportes' },
				{ codigo: 'Idiomas por usuario', label: 'Idiomas por usuario' },
				{ codigo: 'Mapeo de campos de interfaces', label: 'Mapeo de campos de interfaces' },
				{ codigo: 'Opciones de pasos de flujos', label: 'Opciones de pasos de flujos' },
				{ codigo: 'Pasos de flujos', label: 'Pasos de flujos' },
				{ codigo: 'Pasos de transacciones', label: 'Pasos de transacciones' },
				{ codigo: 'Proceso de alertas', label: 'Proceso de alertas' },
				{ codigo: 'Razones de fin de llamada', label: 'Razones de fin de llamada' },
				{ codigo: 'SMS', label: 'SMS' },
				{ codigo: 'Servicios de interfaces', label: 'Servicios de interfaces' },
				{ codigo: 'Supervisores de centros', label: 'Supervisores de centros' },
				{ codigo: 'Transacciones', label: 'Transacciones' },
				{ codigo: 'Transacciones de alertas disparadas', label: 'Transacciones de alertas disparadas' },
				{ codigo: 'Variables de alertas disparadas', label: 'Variables de alertas disparadas' },
				{ codigo: 'Variables de transacciones', label: 'Variables de transacciones' },
			],
		}
	},
	{
		codigo: 'lower(l.action) @operador lower(@valor)',
		name: 'action',
		nombre: 'Acción',
		label: 'Acción',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'l.id_record @operador @valor',
		name: 'id_record',
		nombre: 'ID de registro',
		label: 'ID de registro',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'l.ip @operador @valor',
		name: 'ip',
		nombre: 'IP',
		label: 'IP',
		type: 'textbox',
		subquery: true,
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(l.before_value), lower(@valor)) @operador 0',
		name: 'before_value',
		nombre: 'Valor anterior',
		label: 'Valor anterior',
		type: 'clob',
		subquery: true,
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(l.after_value), lower(@valor)) @operador 0',
		name: 'after_value',
		nombre: 'Valor',
		label: 'Valor',
		type: 'clob',
		subquery: true,
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 2;
Dacs.Reportes.push(reporte);

//######################################################################################################
//######################################################################################################
//Reporte Flujos
//######################################################################################################
//######################################################################################################
reporte = {};
reporte.NombreReporte = 'Flujos';
reporte.url = `${reportServer}Workflows&rc:Parameters=false&rs:Command=Render`;
reporte.idGroup = 1;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{
		codigo: 'w.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		orderdesc: true,
		orderbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'w.id_center @operador @valor',
		name: 'id_center',
		nombre: 'ID del centro',
		label: 'ID del centro',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(w.center_name) @operador lower(@valor)',
		name: 'center_name',
		nombre: 'Nombre del centro',
		label: 'Nombre del centro',
		type: 'textbox',
		selected: true,
		orderby: true,
		groupby: true,
		groupbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(w.name) @operador lower(@valor)',
		name: 'name',
		nombre: 'Nombre del flujo',
		label: 'Nombre del flujo',
		type: 'textbox',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'w.main @operador @valor',
		name: 'is_main',
		nombre: 'Principal',
		label: 'Principal',
		type: 'dropdown',
		subquery: true,
		selected: true,
		orderby: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }],
		}
	},
	{
		codigo: 'w.deleted @operador @valor',
		name: 'is_deleted',
		nombre: 'Inhabilitado',
		label: 'Inhabilitado',
		type: 'dropdown',
		subquery: true,
		selected: true,
		orderby: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }],
		}
	},
	{
		codigo: 'lower(w.tags) @operador lower(@valor)',
		name: 'tags',
		nombre: 'Etiquetas',
		label: 'Etiquetas',
		type: 'textbox',
		subquery: true,
		selected: true,
		orderby: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'w.version_id @operador @valor',
		name: 'version_id',
		nombre: 'ID de la versión de flujo',
		label: 'ID de la versión de flujo',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(w.version_id_version) @operador lower(@valor)',
		name: 'version_id_version',
		nombre: 'Versión de flujo',
		label: 'Versión de flujo',
		type: 'textbox',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(w.docdate, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'docdate',
		nombre: 'Fecha de la versión',
		label: 'Fecha de la versión',
		type: 'datetime',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'w.active @operador @valor',
		name: 'version_status',
		nombre: 'Estado de la versión',
		label: 'Estado de la versión',
		type: 'dropdown',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [
				{ codigo: '0', label: 'Editable' },
				{ codigo: '1', label: 'Activa' },
				{ codigo: '2', label: 'Archivada' },
			],
		}
	},
	{
		codigo: 'w.step_count @operador @valor',
		name: 'step_count',
		nombre: 'Cantidad de pasos',
		label: 'Cantidad de pasos',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'w.transaction_count @operador @valor',
		name: 'transaction_count',
		nombre: 'Cantidad de transacciones',
		label: 'Cantidad de transacciones',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(w.notes), lower(@valor)) @operador 0',
		name: 'notes',
		nombre: 'Notas de la versión de Flujo',
		label: 'Notas de la versión de Flujo',
		type: 'clob',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		nombre: 'steps',
		name: 'steps',
		label: 'Pasos',
		type: 'number',
		nofilter: true,
		css: [],
		events: {},
		params: {
			isrequired: false,
		}
	},
	{
		nombre: 'options',
		name: 'options',
		label: 'Opciones',
		type: 'number',
		nofilter: true,
		css: [],
		events: {},
		params: {
			isrequired: false,
		}
	},
	{
		nombre: 'contents',
		name: 'contents',
		label: 'Contenidos',
		type: 'number',
		nofilter: true,
		css: [],
		events: {},
		params: {
			isrequired: false,
		}
	},
	{
		nombre: 'interface_fields',
		name: 'interface_fields',
		label: 'Mapeo de campos',
		type: 'number',
		nofilter: true,
		css: [],
		events: {},
		params: {
			isrequired: false,
		}
	},
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 3;
Dacs.Reportes.push(reporte);

//######################################################################################################
//######################################################################################################
//Reporte Centros
//######################################################################################################
//######################################################################################################
reporte = {};
reporte.NombreReporte = 'Centros';
reporte.url = `${reportServer}Centers&rc:Parameters=false&rs:Command=Render`;
reporte.idGroup = 1;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{// This is a hack, there should be a field tipe that only allows numbers.
		codigo: 'c.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		subquery: true,
		selected: true,
		orderby: true,
		orderbyselected: true,
		// orderdesc: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(c.name) @operador lower(@valor)',
		name: 'name',
		nombre: 'Nombre del centro',
		label: 'Nombre del centro',
		type: 'textbox',
		subquery: true,
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'c.no_employees @operador @valor',
		name: 'no_employees',
		nombre: 'Cantidad de empleados',
		label: 'Cantidad de empleados',
		type: 'number',
		selected: false,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'c.no_workflows @operador @valor',
		name: 'no_workflows',
		nombre: 'Cantidad de flujos',
		label: 'Cantidad de flujos',
		type: 'number',
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'c.no_published_workflows @operador @valor',
		name: 'no_published_workflows',
		nombre: 'Cantidad de flujos publicados',
		label: 'Cantidad de flujos publicados',
		type: 'number',
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'c.no_transactions @operador @valor',
		name: 'no_transactions',
		nombre: 'Cantidad de transacciones realizadas',
		label: 'Cantidad de transacciones realizadas',
		type: 'number',
		selected: true,
		orderby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	// TODO: agregar notas del centro
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 4;
Dacs.Reportes.push(reporte);

//######################################################################################################
//######################################################################################################
//Reporte Alertas
//######################################################################################################
//######################################################################################################
reporte = {};
reporte.NombreReporte = 'Alertas';
reporte.url = `${reportServer}Alerts&rc:Parameters=false&rs:Command=Render`;
reporte.idGroup = 1;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{
		codigo: 'a.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		selected: true,
		orderby: true,
		// orderdesc: true,
		orderbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.id_center @operador @valor',
		name: 'id_center',
		nombre: 'ID de centro',
		label: 'ID de centro',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.quantity @operador @valor',
		name: 'quantity',
		nombre: 'Cantidad de transacciones',
		label: 'Cantidad de transacciones',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.time_quantity @operador @valor',
		name: 'time_quantity',
		nombre: 'Cantidad de tiempo',
		label: 'Cantidad de tiempo',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.no_alert_triggereds @operador @valor',
		name: 'no_alert_triggereds',
		nombre: 'Cantidad de alertas disparadas',
		label: 'Cantidad de alertas disparadas',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.no_active_alert_triggereds @operador @valor',
		name: 'no_active_alert_triggereds',
		nombre: 'Cantidad de alertas disparadas activas',
		label: 'Cantidad de alertas disparadas activas',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.name) @operador lower(@valor)',
		name: 'name',
		nombre: 'Nombre de la alerta',
		label: 'Nombre de la alerta',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.center_name) @operador lower(@valor)',
		name: 'center_name',
		nombre: 'Nombre del centro',
		label: 'Nombre del centro',
		type: 'textbox',
		selected: true,
		groupby: true,
		groupbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.tags) @operador lower(@valor)',
		name: 'tags',
		nombre: 'Etiquetas',
		label: 'Etiquetas',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.note) @operador lower(@valor)',
		name: 'note',
		nombre: 'Notas de la alerta',
		label: 'Notas de la alerta',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.emails) @operador lower(@valor)',
		name: 'emails',
		nombre: 'Correos destinatarios',
		label: 'Correos destinatarios',
		type: 'textbox',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(a.email_subject) @operador lower(@valor)',
		name: 'email_subject',
		nombre: 'Asunto del correo',
		label: 'Asunto del correo',
		type: 'textbox',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'dbms_lob.@function(lower(a.email_text), lower(@valor)) @operador 0',
		name: 'email_text',
		nombre: 'Texto del correo',
		label: 'Texto del correo',
		type: 'clob',
		selected: false,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(a.docdate, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'docdate',
		nombre: 'Fecha de creación',
		label: 'Fecha de creación',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(a.last_activation, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'last_activation',
		nombre: 'Fecha de última activación',
		label: 'Fecha de última activación',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'a.active @operador @valor',
		name: 'active',
		nombre: 'Activa',
		label: 'Activa',
		type: 'dropdown',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }],
		}
	},
	{
		codigo: 'a.time_unit @operador @valor',
		name: 'time_unit',
		nombre: 'Unidad de tiempo',
		label: 'Unidad de tiempo',
		type: 'dropdown',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [
				{ codigo: '1', label: 'Minutos' },
				{ codigo: '2', label: 'Horas' },
				{ codigo: '3', label: 'D\xedas' },
				{ codigo: '4', label: 'Meses' },
			],
		}
	},
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 5;
Dacs.Reportes.push(reporte);

//######################################################################################################
//######################################################################################################
//Reporte Alertas
//######################################################################################################
//######################################################################################################
reporte = {};
reporte.NombreReporte = 'Alertas disparadas';
reporte.url = `${reportServer}AlertTriggereds&rc:Parameters=false&rs:Command=Render`;
reporte.idGroup = 1;
campos = [
	{ codigo: '(', nombre: '(', label: '(' },
	{ codigo: ')', nombre: ')', label: ')' },
	{
		codigo: 'at.id @operador @valor',
		name: 'id',
		nombre: 'ID',
		label: 'ID',
		type: 'number',
		selected: true,
		orderby: true,
		// orderdesc: true,
		orderbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'at.id_center @operador @valor',
		name: 'id_center',
		nombre: 'ID de centro',
		label: 'ID de centro',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.center_name) @operador lower(@valor)',
		name: 'center_name',
		nombre: 'Nombre de centro',
		label: 'Nombre de centro',
		type: 'textbox',
		selected: true,
		groupby: true,
		groupbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'at.id_alert @operador @valor',
		name: 'id_alert',
		nombre: 'ID de alerta',
		label: 'ID de alerta',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.alert_name) @operador lower(@valor)',
		name: 'alert_name',
		nombre: 'Nombre de alerta',
		label: 'Nombre de alerta',
		type: 'textbox',
		selected: true,
		groupby: true,
		groupbyselected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'at.state @operador @valor',
		name: 'state',
		nombre: 'Estado',
		label: 'Estado',
		type: 'dropdown',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false,
			acItems: [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }],
		}
	},
	{
		codigo: "cast(trunc(at.start_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'start_date',
		nombre: 'Fecha de inicio',
		label: 'Fecha de inicio',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(at.end_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'end_date',
		nombre: 'Fecha de fin',
		label: 'Fecha de fin',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: "cast(trunc(at.first_transaction_date, 'MI') as timestamp) @operador to_date(@valor, 'DD/MM/YYYY HH:MI AM') ",
		name: 'first_transaction_date',
		nombre: 'Fecha de la primera transacción',
		label: 'Fecha de la primera transacción',
		type: 'datetime',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'at.transactions_count @operador @valor',
		name: 'transactions_count',
		nombre: 'Cantidad de transacciones',
		label: 'Cantidad de transacciones',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'at.id_employee @operador @valor',
		name: 'id_employee',
		nombre: 'Tarjeta del emmpleado',
		label: 'Tarjeta del emmpleado',
		type: 'number',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.employee_name) @operador lower(@valor)',
		name: 'employee_name',
		nombre: 'Nombre del empleado',
		label: 'Nombre del empleado',
		type: 'textbox',
		selected: true,
		groupby: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.parent_ticket) @operador lower(@valor)',
		name: 'parent_ticket',
		nombre: 'Ticket padre',
		label: 'Ticket padre',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.action) @operador lower(@valor)',
		name: 'action',
		nombre: 'Acción',
		label: 'Acción',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
	{
		codigo: 'lower(at.description) @operador lower(@valor)',
		name: 'description',
		nombre: 'Descripción',
		label: 'Descripción',
		type: 'textbox',
		selected: true,
		value: '',
		css: [{ name: 'width', value: '150px ' }],
		events: [],
		params: {
			isrequired: false
		}
	},
];
campos = campos.sort(SortByName);
reporte.campos = campos;
reporte.id = 6;
Dacs.Reportes.push(reporte);


// //######################################################################################################
// //######################################################################################################
// //Reporte 4
// //######################################################################################################
// //######################################################################################################
// reporte = {};
// reporte.NombreReporte = 'Derecho de Operacion';
// reporte.url = `${reportServer}DOP&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 4;
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'DOP.ID_Zona @operador @valor ',
// 		nombre: 'ID_Zona',
// 		name: 'id_zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// ];
// campos = campos.sort(SortByName);
// reporte.campos = campos;
// reporte.id = 5;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'General';
// reporte.url = `${reportServer}CountByResponseInspeccion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Nombre @operador @valor)',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto IN (select ID from dacs.Puntos p where p.ID_Zona @operador @valor ) ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Punto in (select ID from DACS.Puntos p where p.ID_Ejecutivo @operador @valor)',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '250px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	}
// ];

// columnas = [
// 	{ label: '¿Punto abierto?', name: 'Punto_Abierto', selected: true, },
// 	{ label: 'Aire Acondicionado', name: 'Aire_Acondicionado', selected: true },
// 	{ label: 'Ayuda Ventas', name: 'AyudaVentas', selected: true },
// 	{ label: 'Canales HD', name: 'Canales_HD', selected: true },
// 	{ label: 'Cantidad de Empleados en el PDV', name: 'Cantidad_Empleados', selected: true },
// 	{ label: 'Claro TV instalado', name: 'Claro_TV', selected: true },
// 	{ label: 'Cobro de Facturas', name: 'Cobro_Facturas', selected: true },
// 	{ label: 'Copiadora', name: 'Copiadora', selected: true },
// 	{ label: 'Equipos zona experiencia', name: 'Equipos_Zona_Experiencia', selected: true },
// 	{ label: 'Especifique el método usado', name: 'Otro_Copiadora', selected: true },
// 	{ label: 'Higiene Organización PDV', name: 'Higiene_Organizacion_PDV', selected: true },
// 	{ label: 'Inventario', name: 'Inventario', selected: true },
// 	{ label: 'Inventario equipos', name: 'Inventario', selected: true },
// 	{ label: 'Inversor', name: 'Inversor', selected: true },
// 	{ label: 'Letrería', name: 'Letreria', selected: true },
// 	{ label: 'Luces techo', name: 'Luces_Techo', selected: true },
// 	{ label: 'Luces Vitrina', name: 'Luces_Vitrina', selected: true },
// 	{ label: 'Mobiliario', name: 'Mobiliario', selected: true },
// 	{ label: 'Objetivo Asignado', name: 'Objetivo_Asignado', selected: true },
// 	{ label: 'Papel de cobro de factura', name: 'Papel_Cobro_Facturas', selected: true },
// 	{ label: 'Pintura Exterior', name: 'Pintura_Exterior', selected: true },
// 	{ label: 'Pintura Interior', name: 'Pintura_Interior', selected: true },
// 	{ label: 'POP', name: 'POP', selected: true },
// 	{ label: 'POP de Pared', name: 'POP_Pared', selected: true },
// 	{ label: 'Porta Celulares', name: 'Porta_Celulares', selected: true },
// 	{ label: 'Porta Precios', name: 'Porta_Precios', selected: true },
// 	{ label: 'Recomendado para', name: 'Recomendado_Para', selected: true },
// 	{ label: 'Sistemas', name: 'Sistemas', selected: true },
// 	{ label: 'Tiene Claro Video?', name: 'Claro_Video', selected: true },
// 	{ label: 'TV', name: 'TV', selected: true },
// 	{ label: 'UPS', name: 'UPS', selected: true },
// 	{ label: 'Verifone', name: 'Verifone', selected: true },
// 	{ label: 'Volantes y/o Brochures de diferentes productos.', name: 'Volantes_Brochures', selected: true }
// ];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 32;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona';
// reporte.url = `${reportServer}CountByResponseInspeccion1Agrupacion&Agrupacion=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// columnas = [
// 	{ label: '¿Punto abierto?', name: 'Punto_Abierto', selected: true },
// 	{ label: 'Aire Acondicionado', name: 'Aire_Acondicionado', selected: true },
// 	{ label: 'Ayuda Ventas', name: 'AyudaVentas', selected: true },
// 	{ label: 'Canales HD', name: 'Canales_HD', selected: true },
// 	{ label: 'Cantidad de Empleados en el PDV', name: 'Cantidad_Empleados', selected: true },
// 	{ label: 'Claro TV instalado', name: 'Claro_TV', selected: true },
// 	{ label: 'Cobro de Facturas', name: 'Cobro_Facturas', selected: true },
// 	{ label: 'Copiadora', name: 'Copiadora', selected: true },
// 	{ label: 'Equipos zona experiencia', name: 'Equipos_Zona_Experiencia', selected: true },
// 	{ label: 'Especifique el método usado', name: 'Otro_Copiadora', selected: true },
// 	{ label: 'Higiene Organización PDV', name: 'Higiene_Organizacion_PDV', selected: true },
// 	{ label: 'Inventario', name: 'Inventario', selected: true },
// 	{ label: 'Inventario equipos', name: 'Inventario', selected: true },
// 	{ label: 'Inversor', name: 'Inversor', selected: true },
// 	{ label: 'Letrería', name: 'Letreria', selected: true },
// 	{ label: 'Luces techo', name: 'Luces_Techo', selected: true },
// 	{ label: 'Luces Vitrina', name: 'Luces_Vitrina', selected: true },
// 	{ label: 'Mobiliario', name: 'Mobiliario', selected: true },
// 	{ label: 'Objetivo Asignado', name: 'Objetivo_Asignado', selected: true },
// 	{ label: 'Papel de cobro de factura', name: 'Papel_Cobro_Facturas', selected: true },
// 	{ label: 'Pintura Exterior', name: 'Pintura_Exterior', selected: true },
// 	{ label: 'Pintura Interior', name: 'Pintura_Interior', selected: true },
// 	{ label: 'POP', name: 'POP', selected: true },
// 	{ label: 'POP de Pared', name: 'POP_Pared', selected: true },
// 	{ label: 'Porta Celulares', name: 'Porta_Celulares', selected: true },
// 	{ label: 'Porta Precios', name: 'Porta_Precios', selected: true },
// 	{ label: 'Recomendado para', name: 'Recomendado_Para', selected: true },
// 	{ label: 'Sistemas', name: 'Sistemas', selected: true },
// 	{ label: 'Tiene Claro Video?', name: 'Claro_Video', selected: true },
// 	{ label: 'TV', name: 'TV', selected: true },
// 	{ label: 'UPS', name: 'UPS', selected: true },
// 	{ label: 'Verifone', name: 'Verifone', selected: true },
// 	{ label: 'Volantes y/o Brochures de diferentes productos.', name: 'Volantes_Brochures', selected: true }
// ];


// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 33;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios';
// reporte.url = `${reportServer}CountByResponseInspeccion1Agrupacion&Agrupacion=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 34;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo';
// reporte.url = `${reportServer}CountByResponseInspeccion1Agrupacion&Agrupacion=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 35;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto';
// reporte.url = `${reportServer}CountByResponseInspeccion1Agrupacion&Agrupacion=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 36;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}CountByResponseInspeccion1Agrupacion&Agrupacion=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 37;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Socio';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 38;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 39;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2AgrupacionStack&Agrupacion=Zona&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 40;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 41;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Socios&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 42;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Socios&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 43;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Socios&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 44;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Socios&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 45;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 46;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 47;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 48;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 49;
// Dacs.Reportes.push(reporte);


// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 50;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 51;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 52;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 53;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 54;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 55;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 56;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseInspeccion2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 9;
// reporte.Pagina = 'Reportes.RespuestaSimilaresInpeccion';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 57;
// Dacs.Reportes.push(reporte);


// //-----------------------------------------------------------------------------------------------
// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'General';
// reporte.url = `${reportServer}CountByResponseAuditorias&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Nombre @operador @valor)',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto IN (select ID from dacs.Puntos p where p.ID_Zona @operador @valor ) ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Punto in (select ID from DACS.Puntos p where p.ID_Ejecutivo @operador @valor)',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '250px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	}
// ];

// columnas = [
// 	{ label: 'Estatus del Punto de Venta', name: 'Estatus_Punto', selected: true },
// 	{ label: 'Posee Letrero Exterior', name: 'Letrero_Exterior', selected: true },
// 	{ label: 'Posee Accesos y o Sistemas el PDV', name: 'Accesos_Sistemas', selected: true },
// 	{ label: 'Pintura del Local en Buen Estado?', name: 'Pintura_Local', selected: true },
// 	{ label: 'Mobiliario Cumple con Estandar de Marca:', name: 'MobiliarioEstandarizado', selected: true },
// 	{ label: 'Nombre del Representante', name: 'Nombre_Representante', selected: true },
// 	{ label: 'C\xe9dula del Representante', name: 'Cedula_Representante', selected: true },
// 	{ label: 'Tarjeta del Representante', name: 'Tarjeta_Representante', selected: true },
// 	{ label: 'Condici\xf3n del Representante', name: 'Condicion_Representante', selected: true },
// 	{ label: 'Recibe Comisi\xf3n por Ventas de cada Producto cuando logra Objetivo?', name: 'Comision', selected: true },
// 	{ label: '¿Representante uniformado?', name: 'Uniforme', selected: true },
// 	{ label: 'Tiempo Laborando en el PDV', name: 'TiempoLaborando', selected: true },
// 	{ label: 'Posee Televisor para Claro TV?', name: 'ClaroTV', selected: true },
// ];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 58;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona';
// reporte.url = `${reportServer}CountByResponseAuditorias1Agrupacion&Agrupacion=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';

// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 59;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios';
// reporte.url = `${reportServer}CountByResponseAuditorias1Agrupacion&Agrupacion=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 60;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo';
// reporte.url = `${reportServer}CountByResponseAuditorias1Agrupacion&Agrupacion=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 61;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto';
// reporte.url = `${reportServer}CountByResponseAuditorias1Agrupacion&Agrupacion=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 62;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}CountByResponseAuditorias1Agrupacion&Agrupacion=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 63;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Socio';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 64;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 65;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2AgrupacionStack&Agrupacion=Zona&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 66;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Zona-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2AgrupacionStack&Agrupacion=Zona&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 67;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Socios&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 68;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Socios&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 69;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Socios&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 70;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Socios-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Socios&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 71;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 72;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 73;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 74;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Ejecutivo-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Ejecutivo&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 75;
// Dacs.Reportes.push(reporte);


// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 76;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 77;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 78;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Tipo de Punto-Clasificaci\xf3n de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=TipoPunto&Agrupacion2=Clasificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 79;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Zona';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Zona&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 80;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Socios';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Socios&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 81;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Ejecutivo';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=Ejecutivo&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 82;
// Dacs.Reportes.push(reporte);

// //##########################################################
// reporte = {};
// reporte.NombreReporte = 'Clasificaci\xf3n de Punto-Tipo de Punto';
// reporte.url = `${reportServer}ReportViewer.aspx?%2fScripting%CountByResponseAuditorias2Agrupacion&Agrupacion=Clasificacion&Agrupacion2=TipoPunto&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 10;
// reporte.Pagina = 'Reportes.RespuestaSimilaresAuditoria';
// reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 83;
// Dacs.Reportes.push(reporte);


// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'Inventario';
// reporte.url = `${reportServer}Inventario&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 7;
// reporte.Pagina = 'Reportes.Inventario';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CodigoLocalidad @operador @valor',
// 		nombre: 'Codigo Localidad',
// 		name: 'CodigoLocalidad',
// 		label: 'Codigo Localidad',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Nombre_Socio @operador @valor',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'NombreTienda  @operador @valor',
// 		nombre: 'Nombre Tienda',
// 		name: 'NombreTienda',
// 		label: 'Nombre Tienda',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Clasificacion  @operador @valor',
// 		nombre: 'Clasificacion',
// 		name: 'Clasificacion_Punto',
// 		label: 'Clasificacion',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource(51);
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'Tipo_Local @operador @valor',
// 		nombre: 'Alquilado_Claro',
// 		name: 'Alquilado_Claro',
// 		label: 'Alquilado por Claro',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('Radio options');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'upper(Direccion) @operador upper(@valor)',
// 		nombre: 'Direccion',
// 		name: 'Direccion',
// 		label: 'Direccion',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Zona @operador @valor ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Ejecutivo @operador @valor',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario in (select ID from DACS.fn_empleados_tree_down(@valor))',
// 		nombre: 'ID_Usuario',
// 		name: 'ID_Usuario',
// 		label: 'Gestores reportes de',
// 		type: 'autocomplete',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=CadenaMando',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario @operador @valor',
// 		nombre: 'Auditor',
// 		name: 'Auditor',
// 		label: 'Auditor',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=Usuarios',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: ' UsuarioExterno @operador @valor',
// 		nombre: 'Tipo_Usuario',
// 		name: 'Tipo_Usuario',
// 		label: 'Tipo de Usuario',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'Interno' }, { codigo: '1', label: 'Externo' }]; }
// 		}
// 	}
// ];
// columnas = [

// 	{ label: 'GAMA', name: 'GAMA', selected: true },
// 	{ label: 'Propio', name: 'Propio', selected: true },
// 	{ label: 'No Aplica Propio', name: 'No_AplicaPropio', selected: true },
// 	{ label: 'Claro', name: 'Claro', selected: true },
// 	{ label: 'No Aplica Claro', name: 'No_AplicaClaro', selected: true }
// ];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// //reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 84;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'Resumen de Visitas';
// reporte.url = `${reportServer}ResumenVisitas&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 7;
// reporte.Pagina = 'Reportes.ResumenVisitas';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CodigoLocalidad @operador @valor',
// 		nombre: 'Codigo Localidad',
// 		name: 'CodigoLocalidad',
// 		label: 'Codigo Localidad',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Nombre_Socio @operador @valor',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'NombreTienda  @operador @valor',
// 		nombre: 'Nombre Tienda',
// 		name: 'NombreTienda',
// 		label: 'Nombre Tienda',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Clasificacion  @operador @valor',
// 		nombre: 'Clasificacion',
// 		name: 'Clasificacion_Punto',
// 		label: 'Clasificacion',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource(51);
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'Tipo_Local @operador @valor',
// 		nombre: 'Alquilado_Claro',
// 		name: 'Alquilado_Claro',
// 		label: 'Alquilado por Claro',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('Radio options');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'upper(Direccion) @operador upper(@valor)',
// 		nombre: 'Direccion',
// 		name: 'Direccion',
// 		label: 'Direccion',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Zona @operador @valor ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Ejecutivo @operador @valor',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario in (select ID from DACS.fn_empleados_tree_down(@valor))',
// 		nombre: 'ID_Usuario',
// 		name: 'ID_Usuario',
// 		label: 'Gestores reportes de',
// 		type: 'autocomplete',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=CadenaMando',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario @operador @valor',
// 		nombre: 'Auditor',
// 		name: 'Auditor',
// 		label: 'Auditor',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=Usuarios',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: ' UsuarioExterno @operador @valor',
// 		nombre: 'Tipo_Usuario',
// 		name: 'Tipo_Usuario',
// 		label: 'Tipo de Usuario',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'Interno' }, { codigo: '1', label: 'Externo' }]; }
// 		}
// 	}
// ];
// columnas = [

// 	{ label: 'Formal', name: 'Formal', selected: true },
// 	{ label: 'Sucursal', name: 'Sucursal', selected: true },
// 	{ label: 'Tercero', name: 'Tercero', selected: true },
// 	{ label: 'Cadenas', name: 'Cadenas', selected: true },
// 	{ label: 'AAA+', name: 'AAAPlus', selected: true },
// 	{ label: 'AAA', name: 'AAA', selected: true },
// 	{ label: 'DVD', name: 'DVD', selected: true },
// 	{ label: 'BBB', name: 'BBB', selected: true },
// 	{ label: 'Total', name: 'Total', selected: true }
// ];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// //reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 85;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'Cumplimiento Auditorias';
// reporte.url = `${reportServer}CumplimientoAuditorias&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 6;
// reporte.Pagina = 'Reportes.CumplimientoAuditorias';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CodigoLocalidad @operador @valor',
// 		nombre: 'Codigo Localidad',
// 		name: 'CodigoLocalidad',
// 		label: 'Codigo Localidad',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Nombre_Socio @operador @valor',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'NombreTienda  @operador @valor',
// 		nombre: 'Nombre Tienda',
// 		name: 'NombreTienda',
// 		label: 'Nombre Tienda',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Clasificacion  @operador @valor',
// 		nombre: 'Clasificacion',
// 		name: 'Clasificacion_Punto',
// 		label: 'Clasificacion',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource(51);
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'Tipo_Local @operador @valor',
// 		nombre: 'Alquilado_Claro',
// 		name: 'Alquilado_Claro',
// 		label: 'Alquilado por Claro',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('Radio options');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'upper(Direccion) @operador upper(@valor)',
// 		nombre: 'Direccion',
// 		name: 'Direccion',
// 		label: 'Direccion',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Zona @operador @valor ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Ejecutivo @operador @valor',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario in (select ID from DACS.fn_empleados_tree_down(@valor))',
// 		nombre: 'ID_Usuario',
// 		name: 'ID_Usuario',
// 		label: 'Gestores reportes de',
// 		type: 'autocomplete',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=CadenaMando',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario @operador @valor',
// 		nombre: 'Auditor',
// 		name: 'Auditor',
// 		label: 'Auditor',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=Usuarios',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: ' UsuarioExterno @operador @valor',
// 		nombre: 'Tipo_Usuario',
// 		name: 'Tipo_Usuario',
// 		label: 'Tipo de Usuario',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'Interno' }, { codigo: '1', label: 'Externo' }]; }
// 		}
// 	},
// 	{
// 		codigo: ' cast( Inicio as date ) @operador convert(date , @valor, 103) ',
// 		nombre: 'Fecha',
// 		name: 'Fecha',
// 		label: 'Fecha',
// 		type: 'date',
// 		subquery: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false
// 		}
// 	}
// ];
// columnas = [];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// //reporte.filtroResultados = true;
// reporte.columnas = columnas;
// reporte.campos = campos;
// reporte.id = 86;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'Cumplimiento Planificaci\xf3n';
// reporte.url = `${reportServer}CumplimientoPlanificacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 6;
// reporte.Pagina = 'Reportes.CumplimientoPlanificacion';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'ID_Socio in (select ID from DACS.Socios S where S.Codigo @operador @valor)',
// 		nombre: 'Codigo_Socio',
// 		name: 'Codigo_Socio',
// 		label: 'Codigo Socio',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CodigoLocalidad @operador @valor',
// 		nombre: 'Codigo Localidad',
// 		name: 'CodigoLocalidad',
// 		label: 'Codigo Localidad',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Nombre_Socio @operador @valor',
// 		nombre: 'Nombre_Socio',
// 		name: 'Nombre_Socio',
// 		label: 'Nombre Socio',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'NombreTienda  @operador @valor',
// 		nombre: 'Nombre Tienda',
// 		name: 'NombreTienda',
// 		label: 'Nombre Tienda',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo_Punto @operador @valor',
// 		nombre: 'Tipo_Punto',
// 		name: 'Tipo_Punto',
// 		label: 'Tipo de Punto',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TPP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Clasificacion  @operador @valor',
// 		nombre: 'Clasificacion',
// 		name: 'Clasificacion_Punto',
// 		label: 'Clasificacion',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource(51);
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'Tipo_Local @operador @valor',
// 		nombre: 'Alquilado_Claro',
// 		name: 'Alquilado_Claro',
// 		label: 'Alquilado por Claro',
// 		type: 'dropdown',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('Radio options');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'upper(Direccion) @operador upper(@valor)',
// 		nombre: 'Direccion',
// 		name: 'Direccion',
// 		label: 'Direccion',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'ID_Zona @operador @valor ',
// 		nombre: 'Zona',
// 		name: 'Zona',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	},
// 	{
// 		codigo: ' ID_Ejecutivo @operador @valor',
// 		nombre: 'Ejecutivo',
// 		name: 'Ejecutivo',
// 		label: 'Ejecutivo',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=USUARIOS',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario in (select ID from DACS.fn_empleados_tree_down(@valor))',
// 		nombre: 'ID_Usuario',
// 		name: 'ID_Usuario',
// 		label: 'Gestores reportes de',
// 		type: 'autocomplete',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=CadenaMando',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: 'ID_Punto in (select ID from DACS.Puntos p where p.Estado @operador @valor)',
// 		nombre: 'ID_Estado_Punto',
// 		name: 'ID_Estado_Punto',
// 		label: 'Estado del Punto',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ESP');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Tipo @operador @valor ',
// 		nombre: 'Tipo_Visita',
// 		name: 'Tipo_Visita',
// 		label: 'Tipo de Visita',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('TVisita');
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'ID_Usuario @operador @valor',
// 		nombre: 'Auditor',
// 		name: 'Auditor',
// 		label: 'Auditor',
// 		type: 'autocomplete',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px' }],
// 		events: {},
// 		params: {
// 			isrequired: false,
// 			minLength: 3,
// 			acSource: 'API/FormularioGetSource?ID_Group=Usuarios',
// 			acItems: ''
// 		}
// 	},
// 	{
// 		codigo: ' UsuarioExterno @operador @valor',
// 		nombre: 'Tipo_Usuario',
// 		name: 'Tipo_Usuario',
// 		label: 'Tipo de Usuario',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'Interno' }, { codigo: '1', label: 'Externo' }]; }
// 		}
// 	},
// 	{
// 		codigo: ' cast( Inicio as date ) @operador convert(date , @valor, 103) ',
// 		nombre: 'Fecha',
// 		name: 'Fecha',
// 		label: 'Fecha',
// 		type: 'date',
// 		subquery: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false
// 		}
// 	}
// ];
// columnas = [];
// campos = campos.sort(SortByName);
// reporte.campos = campos;
// reporte.id = 87;
// reporte.idGroup = 6;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'GPS';
// reporte.url = `${reportServer}EmpleadosUbicacion&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 8;
// reporte.Pagina = 'Reportes.GPS';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'e.Tarjeta @operador @valor',
// 		nombre: 'Tarjeta_Ejecutivo',
// 		name: 'Tarjeta_Ejecutivo',
// 		label: 'Tarjeta de Ejecutivo/ Promotor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'e.name @operador @valor',
// 		nombre: 'Nombre_Ejecutivo',
// 		name: 'Nombre_Ejecutivo',
// 		label: 'Nombre de Ejecutivo/ Promotor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'e.Telefono @operador @valor',
// 		nombre: 'Telefono',
// 		name: 'Telefono',
// 		label: 'Telefono',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'e.email @operador @valor',
// 		nombre: 'email',
// 		name: 'email',
// 		label: 'Email',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 's.Tarjeta @operador @valor',
// 		nombre: 'Tarjeta_Superior',
// 		name: 'Tarjeta_Superior',
// 		label: 'Tarjeta Supervisor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 's.name @operador @valor',
// 		nombre: 'Nombre_Superior',
// 		name: 'Nombre_Superior',
// 		label: 'Supervisor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'u.IMEI @operador @valor',
// 		nombre: 'IMEI',
// 		name: 'IMEI',
// 		label: 'IMEI',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Direccion @operador @valor',
// 		nombre: 'Direccion',
// 		name: 'Direccion',
// 		label: 'Ubicación (Dirección)',
// 		type: 'textbox',
// 		//subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: ' Online @operador @valor',
// 		nombre: 'Conectado',
// 		name: 'Online',
// 		label: 'Conectado',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }]; }
// 		}
// 	},
// 	{
// 		codigo: ' Wifi @operador @valor',
// 		nombre: 'Wifi',
// 		name: 'Wifi',
// 		label: 'WI FI',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() { return [{ codigo: '0', label: 'No' }, { codigo: '1', label: 'S\xed' }]; }
// 		}
// 	},
// 	{
// 		codigo: ' PowerSource @operador @valor',
// 		nombre: 'Fuente de Energía',
// 		name: 'PowerSource',
// 		label: 'Fuente de Energía',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return [{ codigo: 'Battery', label: 'Bateria' }, { codigo: 'Usb', label: 'Conectado a USB' },
// 				{ codigo: 'Ac', label: 'Corriente' }];
// 			}
// 		}
// 	},
// 	{
// 		codigo: 'Latitud  @operador @valor',
// 		nombre: 'Latitud',
// 		name: 'Latitud',
// 		label: 'Latitud',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Longitud  @operador @valor',
// 		nombre: 'Longitud',
// 		name: 'Longitud',
// 		label: 'Longitud',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: ' cast( Fecha as date ) @operador convert(date , @valor, 103) ',
// 		nombre: 'Fecha',
// 		name: 'Fecha',
// 		label: 'Fecha',
// 		type: 'date',
// 		selected: true,
// 		subquery: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: ' cast( Fecha as time ) @operador CAST(@valor AS TIME) ',
// 		nombre: 'Hora',
// 		name: 'Hora',
// 		label: 'Hora',
// 		type: 'time',
// 		selected: true,
// 		subquery: true,
// 		value: '',
// 		css: [{ name: 'width', value: '100px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CAST(Accuracy AS VARCHAR) @operador upper(@valor)',
// 		nombre: 'Accuracy',
// 		name: 'Accuracy',
// 		label: 'Rango de Error',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CAST(ChangePercent AS VARCHAR) @operador upper(@valor)',
// 		nombre: 'Porcentaje de Batería',
// 		name: 'ChangePercent',
// 		label: 'Porcentaje de Batería',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'CAST(Speed AS VARCHAR) @operador upper(@valor)',
// 		nombre: 'Velocidad',
// 		name: 'Speed',
// 		label: 'Velocidad',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '400px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	}
// ];
// columnas = [];
// campos = campos.sort(SortByName);
// //reporte.filtroResultados = true;
// reporte.campos = campos;
// reporte.id = 88;
// reporte.idGroup = 6;
// Dacs.Reportes.push(reporte);

// //###########################################################
// reporte = {};
// reporte.NombreReporte = 'Resumen GPS';
// reporte.url = `${reportServer}ResumenGPS&rc:Parameters=false&rs:Command=Render`;
// reporte.idGroup = 8;
// reporte.Pagina = 'Reportes.ResumenGPS';
// campos = [
// 	{ codigo: '(', nombre: '(', label: '(' },
// 	{ codigo: ')', nombre: ')', label: ')' },
// 	{
// 		codigo: 'e.Tarjeta @operador @valor',
// 		nombre: 'Tarjeta_Ejecutivo',
// 		name: 'Tarjeta_Ejecutivo',
// 		label: 'Tarjeta de Ejecutivo/ Promotor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'e.name @operador @valor',
// 		nombre: 'Nombre_Ejecutivo',
// 		name: 'Nombre_Ejecutivo',
// 		label: 'Nombre de Ejecutivo/ Promotor',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'e.Telefono @operador @valor',
// 		nombre: 'Telefono',
// 		name: 'Telefono',
// 		label: 'Telefono',
// 		type: 'textbox',
// 		subquery: true,
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'u.IMEI @operador @valor',
// 		nombre: 'IMEI',
// 		name: 'IMEI',
// 		label: 'IMEI',
// 		type: 'textbox',
// 		selected: true,
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: {},
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: ' cast( Fecha as date ) @operador convert(date , @valor, 103) ',
// 		nombre: 'Fecha',
// 		name: 'Fecha',
// 		label: 'Fecha',
// 		type: 'date',
// 		selected: true,
// 		subquery: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false
// 		}
// 	},
// 	{
// 		codigo: 'Tarjeta IN (SELECT ID_Ejecutivo FROM [DACS].[Puntos] WHERE ID_Zona =  @operador @valor) ',
// 		nombre: 'Zona',
// 		name: 'zonas',
// 		label: 'Zona',
// 		type: 'dropdown',
// 		subquery: true,
// 		selected: true,
// 		value: '',
// 		css: [{ name: 'width', value: '150px ' }],
// 		events: [],
// 		params: {
// 			isrequired: false,
// 			acItems() {
// 				return GetSource('ZONA');
// 			}
// 		}
// 	}
// ];
// columnas = [
// 	{ label: 'Tarjeta de Ejecutivo/ Promotor', name: 'Tarjeta_Ejecutivo', selected: true },
// 	{ label: 'Nombre de Ejecutivo/ Promotor', name: 'Nombre_Ejecutivo', selected: true },
// 	{ label: 'Telefono', name: 'Telefono', selected: true },
// 	{ label: 'IMEI', name: 'IMEI', selected: true },
// 	{ label: 'Fecha', name: 'Fecha', selected: true },
// 	{ label: 'Tiempo Recorrido', name: 'Tiempo_Recorrido', selected: true },
// 	{ label: 'Kilometraje Recorrido', name: 'Kilometraje_Recorrido', selected: true },
// 	{ label: 'Ubicaci\xf3n Inicial', name: 'Ubicacion_Inicial', selected: true },
// 	{ label: 'Latitud Inicial', name: 'Latitud_Inicial', selected: true },
// 	{ label: 'Longitud Inicial', name: 'Longitud_Inicial', selected: true },
// 	{ label: 'Hora Inicial', name: 'Hora_Inicial', selected: true },
// 	{ label: 'Ubicaci\xf3n Final', name: 'Ubicacion_Final', selected: true },
// 	{ label: 'Latitud Final', name: 'Latitud_Final', selected: true },
// 	{ label: 'Longitud Final', name: 'Longitud_Final', selected: true },
// 	{ label: 'Hora Final', name: 'Hora_Final', selected: true },
// 	{ label: 'Cantidad de veces que quedo sin conexi\xf3n', name: 'Cantidad_No_Conexion', selected: true },
// 	{ label: 'Hora Ultima Conexion', name: 'Hora_Ultima_Conexion', selected: true },
// 	{ label: 'Tiempo No Conexi\xf3n', name: 'Tiempo_No_Conexion', selected: true },
// 	{ label: 'Ultima Ubicacion antes de no conexi\xf3n', name: 'Ultima_Ubicacion', selected: true }
// ];
// columnas = columnas.sort(SortByName2);
// campos = campos.sort(SortByName);
// //reporte.filtroResultados = true;
// reporte.campos = campos;
// reporte.columnas = columnas;
// reporte.id = 89;
// reporte.idGroup = 6;
// Dacs.Reportes.push(reporte);

//##################################################################################
const Reportes = Dacs.Reportes;


window.Dacs.createGroups = function () {
	window.Dacs.ReporteGroups = [
		{ text: 'Lista de reportes', value: 1, childrens: [] },
		{ text: 'Log', value: 2, childrens: [] },
		{ text: 'Socios', value: 3, childrens: [] },
		{ text: 'Derecho de Operaciones', value: 4, childrens: [] },
		{ text: 'Construccion y Resconstruccion', value: 5, childrens: [] },
		{ text: 'Gestion de Ejecutivo Estadisticos', value: 6, childrens: [] },
		{ text: 'Gestion de Ejecutivo Sumarizado', value: 7, childrens: [] },
		{ text: 'Gestion de Ejecutivo Detallado', value: 8, childrens: [] },
		{ text: 'Puntos con Respuestas Similares Inspecci\xf3n', value: 9, childrens: [] },
		{ text: 'Puntos con Respuestas Similares Auditoria', value: 10, childrens: [] },
	];
	const ReporteGroups = window.Dacs.ReporteGroups;
	ReporteGroups.push({ text: 'Otros', value: ReporteGroups.length, childrens: [] });

	for (let i = 0; i < Reportes.length; i++) {
		const report = Reportes[i];
		const valido = !report.Pagina || HasRole(report.Pagina);
		let found = false;
		if (report.idGroup !== undefined && valido) {
			for (let j = 0; j < ReporteGroups.length; j++) {
				const group = ReporteGroups[j];
				if (report.idGroup === group.value) {
					group.childrens.push({ text: report.NombreReporte, value: i });
					found = true;
					break;
				}
			}
		}
		if (!found && report.url !== undefined && valido) {
			if (ReporteGroups.length > 0) {
				ReporteGroups[ReporteGroups.length - 1].childrens.push({ text: report.NombreReporte, value: i });
			}
		}
	}
};
//window.Dacs.createGroups();
