﻿/* eslint-disable no-var, max-len */
//Requiere Jquery y jQuery UI

/*  ModalFromArray
* Funcion que muestra un modal con las opciones de un array;
* El objeto que debe insertarse como referencia es el array y luego se pasan los parametros a) Titulo(String) b) Funcion Custom para ejecutar el valor seleccionado
*
*   Ejemplo:
*   var list = []; list.push({ 'text': 'Nombre a mostrar', 'value': '1' });
*   $(list).ModalFromArray('Seleccionar Reporte', function (selectedValue) { Reportes.selected = selectedValue; ReportSelected(selectedValue); });
*/
(function ($) {
    let selected;
    $.fn.extend({
        ModalFromArray(title, func) {
            const modal = $("<div style='background-color:#fff;'></div>");
            const m = $(modal).dialog({ autoOpen: false, /* minWidth: '500', minHeight: '500',*/ title, modal: true });
            const list = this;
            const ul = $('<ul class="ModalArrayList"></ul>');
            for (let i = 0; i < list.length; i++) {
                if (list[i].text !== 'Cierre de Periodo DO') {        //<---- Resolución para ALM1357
                    const li = $('<li></li>');
                    const element = $('<a></a>')
                        .text(list[i].text)
                        .attr('value', list[i].value)
                        .addClass('dmenu_option')
                        .css('font-weight', 'bold');
                    if (list[i].childrens !== undefined) {
                        element.css('text-decoration', 'none')
                            .css('color', 'black');
                    }

                    if (list[i].value !== undefined) {
                        element.click(list[i].value, (v) => {
                            func(v.data);
                            m.dialog('close');
                        });
                    }

                    ul.append(li.append(element));

                    if (list[i].childrens !== undefined) {
                        // element = $("<a></a>").text('-' + list[i].text).addClass("dmenu_option");
                        // element.attr('value', list[i].value);
                        // element.click(list[i].value, function (v) {
                        //     func(v.data);
                        //     m.dialog('close');
                        // });
                        // var li = $("<li class='children'></li>");
                        // ul.append(li.append(element));
                        var children = $("<div class='children'></div>");
                        for (let j = 0; j < list[i].childrens.length; j++) {
                            const item = list[i].childrens[j];
                            const child = $('<a></a>')
                                .text(`- ${item.text}`)
                                .addClass('dmenu_option')
                                .attr('value', item.value)
                                .css('display', 'block')
                                .click(item.value, (v) => {
                                    func(v.data);
                                    m.dialog('close');
                                });
                            children.append(child);
                        }
                        li.append(children);
                    }
                }
            }
            modal.append(ul);
            m.append(modal);
            m.dialog('open');
        }
    });
}(jQuery));


/*  FilterController
* Funcion que muestra un controlador de filtros a partir de un json de colunmas;
* Debe indicarse un DIV en el markup que contenga el control a generarse. Como parametro recibe: a) columnas en un Json  b) funcion custom para recibir el querystring
*
*   Ejemplo:
*   HTML:
*       <div id="myparamsPlugin"></div>
*   JS:
*
*
    var campos = [
        {
            codigo: "(",
            nombre: "("
        },
        {
            codigo: ")",
            nombre: ")"
        },
        {
            codigo: " upper(w.name) like '%@valor%'",
            nombre: "Nombre Flujo",
            name: "workflow",
            label: "workflow",
            type: "textbox",
            subquery: true,
            value: "",
            css: [{ name: "width", value: "150px " }],
            events: [],
            params: {
                isrequired: false
            }
        },
        {
            codigo: " i.Fecha_inicio @operador @valor",
            nombre: "Fecha inicio",
            name: "fecha_inicio",
            label: "Fecha inicio",
            type: "textbox",
            subquery: true,
            value: "",
            css: [{ name: "width", value: "150px " }],
            events: [],
            params: {
                isrequired: false
            }
        },
        {
            codigo: " i.Fecha_fin @operador @valor",
            nombre: "Fecha Fin",
            name: "fecha_fin",
            label: "Fecha Fin",
            type: "textbox",
            subquery: true,
            value: "",
            css: [{ name: "width", value: "150px " }],
            events: [],
            params: {
                isrequired: false
            }
        },
        {
            codigo: " upper((select dbo.fn_usuariocargo(ia.ID_Instancia, ia.usuariocargo))) like '%@valor%'",
            nombre: "usuario cargo",
            name: "usuariocargo",
            label: "usuario cargo",
            type: "textbox",
            subquery: true,
            value: "",
            css: [{ name: "width", value: "150px " }],
            events: [],
            params: {
                isrequired: false
            }
        }
    ];

*   $('#myparamsPlugin').FilterController(campos, function (query) { console.log(query); });
*/

(function ($) {
    $.fn.extend({
        FilterController(ptipo, pColumns, pfunc) {
            Columns = pColumns;
            func = pfunc;
            tipo = ptipo;
            objfila[1].params.acItems = pColumns;
            loadfiltros();
            const container = $(this);

            const obj = genObj(tablas);
            camposContainer = $('<div></div>', { id: 'divcampos' });
            camposContainer.append(obj);
            container.append(camposContainer);
        }
    });
    //Tipo guarda la referencia de que pantalla esta usando los filtros (Reportes, Flujos, Puntos, Etc).
    let func,
        tipo;
    var Columns = [],
        filters = [];
    var camposContainer = $('<div></div>', { id: 'divcampos' });

    var objfila = [
        {
            name: 'separador',
            label: 'Separador',
            type: 'dropdown',
            css: [{ name: 'width', value: '150px ' }],
            events: {},
            params: {
                isrequired: false,
                acItems: [
                    {
                        codigo: 'and',
                        nombre: 'And',
                        label: 'Y'
                    }, {
                        codigo: 'or',
                        nombre: 'Or',
                        label: 'O'
                    }]
            }
        },
        {
            name: 'campo',
            label: 'Campo',
            type: 'dropdown',
            css: [{ name: 'width', value: '150px ' }],
            events: [{ name: 'change', value: 'xchangex' }],
            params: {
                isrequired: false,
                acItems: []
            }
        },
        {
            name: 'criterio',
            label: 'Criterio',
            type: 'dropdown',
            isConfig: true,
            css: [{ name: 'width', value: '150px ' }],
            events: [],
            params: {
                isrequired: false,
                acItems: [
                    { codigo: '=', label: 'es igual a', },
                    { codigo: '!=', label: 'es diferente de', },
                    { codigo: '>', label: 'es mayor a' },
                    { codigo: '>=', label: 'es mayor o igual a' },
                    { codigo: '<', label: 'es menor a' },
                    { codigo: '<=', label: 'es menor o igual a' },
                    { codigo: 'like', label: 'contiene' }
                ]
            }
        },
        {
            name: 'valor',
            label: 'Valor',
            type: 'label',
            value: '',
            css: [{ name: 'width', value: '150px' }],
            events: [],
            params: {
                isrequired: false
            }
        }
    ];


    var tablas = {
        name: 'tbcampos',
        label: 'tb',
        type: 'grid',
        css: [],
        events: [],
        params: {
            gridColumns: objfila,
            isrequired: false,
            gridRows: 10,
            buttonadd: true,
            singleRow: true
        }
    };


    function loadfiltros() {
        const url = './API/Reportes/Get';
        //var data = { "idRep": $('#reportes option:selected').attr('data-idrep') };
        const data = { idRep };


        $.ajax({
            type: 'GET',
            url,
            data,
            success(data) {
                filters = data;
                return true;
            },
            dataType: 'JSON'
        });
    }

    function genObj(objectotran) {
        //parentType = typeof parentType !=== 'undefined' ? parentType : 'none';
        let ob = ''; const res = objectotran;
        if (res.type === 'button') {
            ob = $('<button />').button().text(res.label);
        } else if (res.type === 'textbox' || res.type === 'date' || res.type === 'datetime') {
            ob = $('<input>').attr({ type: 'text', value: res.value }).addClass('objectotran');
        } else if (res.type === 'time') {
            ob = $('<input>').attr({ type: 'time', value: res.value }).addClass('objectotran');
        } else if (res.type === 'radio') {
            ob = $('<input>').attr({ type: 'radio', value: res.value }).addClass('objectotran');
        } else if (res.type === 'checkbox') {
            ob = $('<input>').attr({ type: 'checkbox', value: res.value }).addClass('objectotran');
        } else if (res.type === 'dropdown') {
            ob = $('<select></select>').text(res.label).addClass('objectotran');

            //Si tiene los parametros definidos como una funcion; ejecturarla para obtener los valores.
            if (typeof res.params.acItems === 'function') { res.params.acItems = res.params.acItems(); }
            if (res.params.acItems) {
                for (let k = 0; k < res.params.acItems.length; k++) {
                    const option = $('<option />', { value: res.params.acItems[k].codigo, text: res.params.acItems[k].label });
                    option.attr('data', JSON.stringify(res.params.acItems[k]));
                    option.attr('index', JSON.stringify(k));
                    option.appendTo(ob);
                }
            }
        } else if (res.type === 'textarea') {
            ob = $('<textarea></textarea>').text(res.label).attr('rows', '').attr('cols', '').addClass('objectotran').val(res.value);
        } else if (res.type === 'autocomplete') {
            ob = $('<input>').attr({ type: 'text' }).addClass('objectotran');
            ob.autocomplete({
                minLength: res.params.minLength,
                source(request, response) {
                    $.ajax({
                        url: typeof (res.params.acSource) === 'function' ? res.params.acSource(ob, res) : res.params.acSource,
                        dataType: 'json',
                        data: {
                            q: request.term
                        },
                        success(data) {
                            response(data.slice(0, 12));
                        }
                    });
                },
                focus(event, ui) {
                    $(this).val(ui.item.Name);
                    $(this).attr('acValue', ui.item.Value);
                    return false;
                },
                select(event, ui) {
                    $(this).val($.trim(ui.item.Name));
                    $(this).attr('acValue', ui.item.Value);
                    return false;
                }
            }).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li></li>')
                    .data('item.autocomplete', item)
                    .append(`<a>${item.Name}</a>`)
                    .appendTo(ul);
            };

            //        autocomp = null;
        } else if (res.type === 'grid') {
            //grid
            ob = $('<div />').addClass('objectotran');
            const gridtbl = $('<table></table>');
            let row,
                celda;
            let btn;
            const head = $('<thead />');
            if (res.params.buttonadd && res.params.buttonadd === true) {
                row = $('<tr></tr>');
                btn = $('<button />').button().text('Seleccionar Filtro').css('width', '150px');
                celda = $('<td></td>').append(btn);
                row.append(celda);
                btn.click(() => {
                    //retorna el query en el parametro FILTER = 'QUERY'
                    const list = [];
                    for (let i = 0; i < filters.length; i++) {
                        list.push({ text: filters[i].NombreReporte, value: i });
                    }
                    $(list).ModalFromArray('Seleccionar Filtro', (selectedvalue) => { renderfilter(selectedvalue); });
                });

                btn = $('<button />').button().text('Guardar Filtro').css('width', '150px');
                celda = $('<td></td>').append(btn);
                row.append(celda);
                btn.click(() => {
                    savefilter();
                });

                btn = $('<button />').button().text('Agregar Filtro').css('width', '150px');
                celda = $('<td></td>').append(btn);
                row.append(celda);
                btn.click(() => {
                    gridAddRow(res, gridtbl);
                });
                btn = $('<button />').button().text('Eliminar Filtro').css('width', '150px');
                celda = $('<td></td>').append(btn);
                row.append(celda);
                btn.click(() => {
                    removeRow(gridtbl);
                });
                //Btn generar query
                btn = $('<button />').button().text('Generar').css('width', '150px').attr('id', 'adv_btn_generar');
                celda = $('<td></td>').append(btn);
                row.append(celda);
                btn.click(() => {
                    //retorna el query en el parametro FILTER = 'QUERY'
                    func(createQuery());
                });
                head.append(row);
            }
            row = $('<tr></tr>');
            for (let x = 0; x < res.params.gridColumns.length; x++) {
                celda = $('<td></td>').text(res.params.gridColumns[x].label);
                row.append(celda);
            }
            head.append(row);
            gridtbl.append(head);
            const body = $('<tbody />');
            gridtbl.append(body);
            gridAddRow(res, gridtbl);
            ob.append(gridtbl);
        } else if (res.type === 'label') {
            ob = $(`<span />${res.value}</span>`);
        } else if (res.type === 'iframe') {
            ob = $('<iframe />').attr('src', res.value);
        }
        if (res.params.isrequired === true && res.type !== 'label') {
            ob.attr('required', 'required');
        }
        ob.attr('id', res.name);
        ob.attr('field', res.name);
        if (res.type === 'date') {
            ob.datepicker({ dateFormat: 'dd/mm/yy' });
        } else if (res.type === 'datetime') {
            ob.datetimepicker({ dateFormat: 'dd/mm/yy', timeFormat: 'hh:mm tt', ampm: true });
        }
        for (let y = 0; y < res.css.length; y++) {
            ob.css(res.css[y].name, res.css[y].value);
        }
        for (let r = 0; r < res.events.length; r++) {
            if (res.events[r].value === 'xchangex') {
                //Condicion para hacerle bind del on-change del field campo;
                //lmpg

                ob.change(function () {
                    const ob = $('option:selected', this);
                    const data = eval(`(${ob.attr('data')})`);
                    const row = $(this).parent().parent();
                    data.name += row.attr('id');
                    const cell = row.find('.valor');
                    cell.children().remove();
                    if (data.nombre === '(' || data.nombre === ')') return;
                    const htmlobject = genObj(Columns[ob.attr('index')]);

                    FiltraCriterios(Columns[ob.attr('index')].type, row.attr('id'));

                    htmlobject.change(function () { $(this).parent().val($(this).val()); });
                    htmlobject.removeClass('objectotran');
                    htmlobject.attr('subquery', data.subquery);
                    cell.append(htmlobject);
                    cell.val(htmlobject.val());
                });
            } else {
                const handler = res.events[r].value;
                ob.bind(res.events[r].name, typeof handler === 'function' ? handler : new Function(handler));
            }
        }
        return ob;
    }

    function FiltraCriterios(parentType, idobj) {
        let items,
            dropdownCriterios = $(`#criterio${idobj}`);

        if (parentType !== 'none') {
            switch (parentType) {
                case 'dropdown':
                    items = [{ codigo: '=', nombre: 'es igual a' }, { codigo: '!=', nombre: 'es diferente de' }];
                    break;
                case 'date':
                case 'datetime':
                    items = [{ codigo: '=', nombre: 'es igual a' }, { codigo: '!=', nombre: 'es diferente de' }, { codigo: '>=', nombre: 'es mayor o igual a' }, { codigo: '>', nombre: 'es mayor a' }, { codigo: '<', nombre: 'es menor a' }, { codigo: '<=', nombre: 'es menor o igual a' }];
                    break;
                default:
                    items = [{ codigo: '=', nombre: 'es igual a' }, { codigo: '!=', nombre: 'es diferente de' }, { codigo: '>=', nombre: 'es mayor o igual a' }, { codigo: '>', nombre: 'es mayor a' }, { codigo: '<', nombre: 'es menor a' }, { codigo: '<=', nombre: 'es menor o igual a' }, { codigo: 'like', nombre: 'contiene' }];
                    break;
            }

            dropdownCriterios.find('option').remove().end();
            for (let k = 0; k < items.length; k++) {
                const option = $('<option />', { value: items[k].codigo, text: items[k].nombre });
                option.appendTo(dropdownCriterios);
            }
        }
    }


    function gridAddRow(res, gridtbl) {
        const row = $('<tr></tr>').addClass('gridrows');
        const body = gridtbl.children('tbody');
        row.attr('id', body.children().length);
        for (let x = 0; x < res.params.gridColumns.length; x++) {
            obj = genObj(res.params.gridColumns[x]);
            obj.attr('id', res.params.gridColumns[x].name + (body.children().length));
            obj.removeClass('objectotran');
            obj.addClass(`tds${body.children().length}`);
            obj.addClass(res.params.gridColumns[x].name);
            celda = $('<td></td>').append(obj);
            row.append(celda);
        }
        body.append(row);
        return row;
    }
    function removeRow(gridtbl) {
        const body = gridtbl.children('tbody');
        if (body.children().length > 1) {
            body.children('tr:last').remove();
        }
    }

    function renderfilter(index) {
        const data = JSON.parse(filters[index].JSONBLOB);

        const tbcampos = camposContainer.find('TABLE');
        tbcampos.children('tbody').children().remove();
        for (let i = 0; i < data.filtros.length; i++) {
            const record = data.filtros[i];
            const newrow = { params: { gridColumns: objfila } };
            const row = gridAddRow(newrow, tbcampos);
            row.find('.separador').val(record.separador);
            row.find('.campo').val(record.campo);
            row.find('.campo').change();
            row.find('.criterio').val(record.criterio);
            if (record.campo !== '(' && record.campo !== ')') {
                const valor = row.find('.valor');
                valor.val(record.valor);
                valor.children().val(record.valor);
            }
        }
        if (!data.colums.length > 0) {
            fillCons(data.colums, 'divcolreport');
        } else {
            $('.chkcolum').attr('checked', true);
        }
    }

    function savefilter() {
        const idrep = idRep || tipo;

        let id = 0;

        const name = prompt('Nombre del filtro', '');
        if (name === null) {
            return;
        }
        //Verifica si existe
        for (let i = 0; i < filters.length; i++) {
            //Si existe con ese nombre, reemplazarlo.
            if (filters[i].NombreReporte.toUpperCase() === name.toUpperCase()) id = filters[i].id;
        }


        const columnas = {};
        columnas.colums = {};//JSON.parse($("#hcolumnsssave").val());
        columnas.filtros = getData('').tbcampos;
        let datarep = JSON.stringify(columnas);


        const url = './API/Reportes/Post';
        const data = { NombreReporte: name, JSONBLOB: datarep, CodigoUsuario: '*', id, idrep };

        $.ajax({
            type: 'GET',
            url,
            data,
            success(/* data */) {
                loadfiltros();
                return true;
            },
            dataType: 'JSON'
        });

        loadfiltros();
        datarep = null;
    }
    function fillCons(resultado, divName) {
        const div = $(`#${divName}`);
        for (const campo in resultado) {
            if (resultado[campo] === true) {
                div.find(`#${campo.replace('Out', '')}`).attr('checked', false);
            } else {
                div.find(`#${campo.replace('Out', '')}`).attr('checked', true);
            }
        }
    }

    function getData(div, nomessage) {
        const data = {}; let valid = true;
        //var inputsobj = $("#" + div).find(".objectotran");
        const inputsobj = camposContainer.find('.objectotran');
        for (let i = 0; i < inputsobj.length; i++) {
            if (inputsobj[i].type === 'checkbox' && $(inputsobj[i]).is(':checked') === true) {
                data[$(inputsobj[i]).attr('field')] = 's';
            } else if (inputsobj[i].type === 'checkbox' && $(inputsobj[i]).is(':checked') === false) {
                data[$(inputsobj[i]).attr('field')] = 'n';
            } else if (inputsobj[i].type === 'select-one') {
                data[$(inputsobj[i]).attr('field')] = $(inputsobj[i]).attr('value');
            } else if (inputsobj[i].type === 'textarea') {
                data[$(inputsobj[i]).attr('field')] = $(inputsobj[i]).attr('value');
            } else if (inputsobj[i].tagName === 'SPAN') {
                data[$(inputsobj[i]).attr('field')] = $(inputsobj[i]).val();
            } else if (inputsobj[i].tagName === 'DIV') {
                const gridrows = $(inputsobj[i]).find('.gridrows');
                const aliasgrid = inputsobj[i].id;
                const gridvalues = [];
                let valuesrow = {};
                let rowsobject;
                for (let j = 0; j < gridrows.length; j++) {
                    rowsobject = gridrows.find(`.tds${j}`);
                    for (let k = 0; k < rowsobject.length; k++) {
                        if ((rowsobject[k].required === true && rowsobject[k].value === '' || rowsobject[k].acValue === '') && nomessage !== true) {
                            alert(`El campo ${rowsobject[k].id.substring(0, rowsobject[k].id.length - 1)} es requerido para ejecutar la transaccion. Favor verificar.`);
                            $(rowsobject[k]).focus();
                            valid = false;
                            break;
                        } else {
                            if (rowsobject[k].tagName === 'SPAN') {
                                //valuesrow[$(rowsobject[k]).attr('field')] = $(rowsobject[k]).find("input").attr("acValue");
                                //Check for autocomplete:
                                if ($(rowsobject[k]).find('input').attr('acValue') === undefined) {
                                    valuesrow[$(rowsobject[k]).attr('field')] = $(rowsobject[k]).val();
                                } else {
                                    valuesrow[$(rowsobject[k]).attr('field')] = $(rowsobject[k]).find('input').attr('acValue');
                                }
                            } else if (inputsobj[i].type === 'select-one') {
                                data[$(inputsobj[i]).attr('field')] = $(inputsobj[i]).attr('value');
                            } else if (inputsobj[i].type === 'checkbox' && $(inputsobj[i]).is(':checked') === true) {
                                data[$(inputsobj[i]).attr('field')] = 's';
                            } else if (inputsobj[i].type === 'checkbox' && $(inputsobj[i]).is(':checked') === false) {
                                data[$(inputsobj[i]).attr('field')] = 'n';
                            } else if ($(rowsobject[k]).attr('acValue') === undefined) {
                                valuesrow[$(rowsobject[k]).attr('field')] = rowsobject[k].value;
                            } else {
                                valuesrow[$(rowsobject[k]).attr('field')] = $(rowsobject[k]).attr('acValue');
                            }
                            valid = true;
                        }
                    }
                    gridvalues.push(valuesrow);
                    valuesrow = {};
                }
                data[aliasgrid] = gridvalues;
            } else if (((inputsobj[i].required === true && inputsobj[i].value === '') || inputsobj[i].acValue === '') && nomessage !== true) {
                alert(`El campo ${inputsobj[i].id} es requerido para ejecutar la transaccion. Favor verificar.`);
                $(inputsobj[i]).focus();
                valid = false;
                break;
            } else {
                if ($(inputsobj[i]).attr('acValue') === undefined) {
                    data[$(inputsobj[i]).attr('field')] = inputsobj[i].value;
                } else {
                    data[$(inputsobj[i]).attr('field')] = $(inputsobj[i]).attr('acValue');
                }
                valid = true;
            }
        }
        if (valid !== false) {
            // aliasgrid = null; gridrows = null; valuesrow = null; rowsobject = null; datpersontran = null;
            return data;
        }
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function CoerceToString(query) {
        let result = false,
            exceptions = ['telefono', 'rnc', 'tel1', 'tel2', 'Valor', 'Porcentaje'];

        for (let i = exceptions.length - 1; i >= 0; i--) {
            if (query.indexOf(exceptions[i]) > 0) { return true; }
        }

        return result;
    }

    function createQuery() {
        const reportData = getData('divcampos').tbcampos;
        let filter = '';

        for (let i = 0; i < reportData.length; i++) {
            const record = reportData[i];
            if (i > 0 && record.campo !== ')' && reportData[i - 1].campo !== '(') { filter += ` ${record.separador}`; }
            if (record.campo.indexOf('@operador') !== -1) {
                if (record.criterio === 'like' || CoerceToString(record.campo.toLowerCase())) {
                    //lmpg; agregando filtro like
                    filter += ` ${record.campo.replace(/@operador/g, record.criterio).replace(/@valor/g, `'%${record.valor}%'`)}`;
                } else {
                    if (!isNumber(record.valor)) {
                        record.valor = `'${record.valor}'`;
                    }

                    filter += ` ${record.campo.replace(/@operador/g, record.criterio).replace(/@valor/g, record.valor)}`;
                }
            } else if (record.campo.indexOf('@valor') === -1) {
                filter += ` ${record.campo}`;
                if (record.campo !== '(' && record.campo !== ')') {
                    filter += ` ${record.criterio}`;
                    filter += ` '${record.valor}'`;
                }
            } else {
                filter += ` ${record.campo.replace(/@valor/g, record.valor)}`;
            }
        }

        if (filter) filter = `where ${filter}`;
        const stringurl = `&FILTER= ${filter}`;
        return stringurl;
    }
}(jQuery));
