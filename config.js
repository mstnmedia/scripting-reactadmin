import CommonConfig from './reactadmin/config';

const config = { //Custom configs of each app
	...CommonConfig,
	...window.config,
};

export default config;
