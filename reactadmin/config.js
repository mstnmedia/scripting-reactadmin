const server = window.location.hostname;
const config = {
	backend: `http://${server}:8000/api`,
	apiURL: `http://${server}:5000`,
	serverURL: `http://${server}:5050/`,
	appId: '5765521282941492bd81ed90',
	cookieIdentifier: 'X-accesstoken-dev',
	TOKEN_COOKIE_KEY: 'X-STRATUM-accesstoken',
};

export default config;
