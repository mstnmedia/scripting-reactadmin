/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Dashboard
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
// import {
//     CircularGaugeComponent,
//     AxesDirective, AxisDirective,
//     RangeDirective, RangesDirective,
//     PointersDirective, PointerDirective,
// } from '@syncfusion/ej2-react-circulargauge';

import {
	Panel,
	Page, Col, Row,
	BarChart,
	Input,
	FormGroup,
	AnimatedSpinner, Fa,
	PieChart,
	DataTable,
	DropDown,
	Button,
	ModalFactory,
	Factory,
	PanelCollapsible,
} from '../components/';
import EasyPie from '../components/charts/EasyPie';
import i18n from '../../i18n';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import { CategoryTypes, ContentTypes, LanguagePrefix, } from '../../store/enums';
import { TransactionContentItem } from '../../scripting/Transactions';
import ReactSummernoteInput from '../components/ui/ReactSummernoteInput';
import { sortFunction, respErrorDetails } from '../Utils';

const actions = ReduxOutlet('dashboard', 'dashboard').actions;
const categoriesActions = ReduxOutlet('transactions', 'category').actions;
const centerActions = ReduxOutlet('centers', 'center').actions;
class Dashboard extends Component {

	state = {
		range: 'DD',
		groupBy: 'HH',
		type1: '',
		type2: '',
		from: this.getDateString(new Date()),
		to: '', //this.getDateString(new Date()),
		rangeNPS: 'DD',
		fromNPS: this.getDateString(new Date()),
		toNPS: this.getDateString((new Date()).setDate(new Date().getUTCDate() + 1)),
		timeFilter: '',
		loadingCategories: false,
		loadingByProductType: false,
		loadingHistoryByDay: false,
		loadingByCategory: false,
		loadingTimeAverage: false,
		loadingNPS: 0,
		loadingContent: false,
		notes: { id: 0, value: '' },
		notesDisabled: false,
		ranges: [
			{ value: 'DD', label: i18n.t('ByDay') },
			{ value: 'WW', label: i18n.t('ByWeek') },
			{ value: 'MM', label: i18n.t('ByMonth') },
			{ value: 'YY', label: i18n.t('ByYear') },
			{ value: 'CC', label: i18n.t('Custom') },
		],
	}
	componentWillMount() {
		this.refresh(this.props);
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), },
		];
		// window.Scripting_Dashboard = this;
	}
	componentWillReceiveProps(props) {
		if (!this.props.user.permissions.length && props.user.permissions.length) {
			// Ya ha cargado la información del usuario
			this.fetchAll(props);
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getCategoriesType1() {
		const { categories: { all, }, } = this.props;
		const categories = (all || [])
			.filter(i => i.id_type === CategoryTypes.TYPE1);
		return categories;
	}
	getCategoriesType2() {
		const { type1 } = this.state;
		const { categories: { all, }, } = this.props;
		const categories = (all || [])
			.filter(i => i.id_parent === (type1 * 1) && i.id_type === CategoryTypes.TYPE2)
			.map(i => ({ id: i.name, label: i.name }));
		return categories;
	}
	getByProductTypePath(separator = '_') {
		return `dashboard${separator}byProductType`;
	}
	getByProductType() {
		const { dashboard, } = this.props;
		const data = dashboard[this.getByProductTypePath()];
		return ((data && data.items) || [])
			.sort(sortFunction(['value', 'label'], [true, false]));
	}
	getHistoryByDayPath(separator = '_') {
		return `dashboard${separator}historyByDay`;
	}
	getHistoryByDay() {
		const { dashboard, } = this.props;
		const data = dashboard[this.getHistoryByDayPath()];
		return (data && data.items) || [];
	}
	getByCategoryPath(separator = '_') {
		return `dashboard${separator}byCategory`;
	}
	getByCategory() {
		const { dashboard, } = this.props;
		const data = dashboard[this.getByCategoryPath()];
		return (data && data.items) || [];
	}
	getTimeAveragePath(separator = '_') {
		return `dashboard${separator}timeAverage`;
	}
	getTimeAverage() {
		const { dashboard, } = this.props;
		const data = dashboard[this.getTimeAveragePath()];
		return (data && data.items) || [];
	}
	getTimeAverageRanges(item) {
		const green = '#8BC34A';
		const yellow = '#ffc333';
		const red = '#ea5a5a';
		const ranges = [
			{ start: 0, end: 0, color: red, },
			{ start: 0, end: 30, color: yellow, },
			{ start: 30, end: 60, color: green, },
			{ start: 60, end: 100, color: red, },
		];
		let value = 0;
		if (item) {
			value = item.value || 0;
			const objetive = item.objetive || 0;
			const margin = (30 / 100) * objetive;
			ranges[2] = { start: objetive - margin, end: objetive + margin, color: green, };
			ranges[1] = {
				start: 0,
				end: ranges[2].start,
				color: yellow,
			};
			ranges[3] = {
				start: ranges[2].end,
				end: Math.max(ranges[2].end + margin, value),
				color: red,
			};
		}
		const min = Math.min(ranges[0].start, value);
		const max = Math.max(ranges[ranges.length - 1].end, value);
		let range = {};
		let totalP = 0;
		ranges.forEach(({ start, end, }, i) => {
			const porcentage = Math.min(max === 0 ? 25 :
				Math.round(((end - start) / max) * 100), 100 - totalP);
			ranges[i].porcentage = porcentage;
			totalP += porcentage;
			if (start < value) {
				range = ranges[i];
			}
		});
		const porcentage = Math.round((value / max) * 100);
		return { ranges, range, value, min, max, green, yellow, red, porcentage };
	}
	getNPSPath(separator = '_') {
		return `dashboard${separator}nps`;
	}
	getNPS() {
		const { dashboard, } = this.props;
		const data = dashboard[this.getNPSPath()];
		return (data && data.items) || [];
	}
	getDateString(date) {
		const a = new Date(date);
		const y = a.getUTCFullYear();
		const m = a.getUTCMonth() + 1;
		const d = a.getUTCDate();
		return `${y}-${m < 10 ? `0${m}` : m}-${d < 10 ? `0${d}` : d}`;
	}
	getFilters() {
		const { from, to, } = this.state;
		const type = 'date';
		const filters = [];
		if (!from) return;
		filters.push({ name: 'start_date', criteria: '>=', value: from, type, });
		if (to) {
			filters.push({ name: 'end_date', criteria: '<=', value: to, type, });
		}
		return filters;
	}
	setLoadingNPS(value) {
		const loadingNPS = Math.max(this.state.loadingNPS + value, 0);
		this.handleChange('loadingNPS', loadingNPS);
	}

	goToTransactionCreate() {
		this.props.router.push('/data/transactions/create');
	}

	refresh(props) {
		this.fetchAll(props);
		this.fetchNPS(props);
	}
	fetchAll(props) {
		this.fetchCategories(props);
		this.fetchByProductType(props);
		this.fetchHistoryByDay(props);
		this.fetchByCategory(props);
		this.fetchTimeAverage(props);
		// this.fetchNPS(props);
		this.fetchCenter(props);
	}
	fetchCategories(props) {
		const { dispatch, token } = (props || this.props);
		const success = () => this.handleChange('loadingCategories', false);
		const failure = (resp) => {
			this.handleChange('loadingCategories', false);
			window.message.error(i18n.t('ErrorFetchingCategories'), 0, respErrorDetails(resp));
		};
		this.handleChange('loadingCategories', true);
		const where = [
			// { name: 'deleted', value: '0', },
		];
		dispatch(categoriesActions.fetchAll({ token, where, success, failure }));
	}
	fetchByProductType(props) {
		if (!this.hasCharts1(props)) return null;
		const { dispatch, token } = (props || this.props);
		const item = this.getFilters();
		if (item) {
			const { type1, type2, } = this.state;
			const params = { type1, type2, };
			this.handleChange('loadingByProductType', true);
			dispatch(actions.postToURL({
				token,
				route: this.getByProductTypePath('/'),
				path: this.getByProductTypePath(),
				item,
				params,
				success: () => this.handleChange('loadingByProductType', false),
				failure: (resp) => {
					this.handleChange('loadingByProductType', false);
					window.message.error(i18n.t('ErrorFetchingByProductType'), 0, respErrorDetails(resp));
				},
			}));
		}
	}
	fetchHistoryByDay(props) {
		if (!this.hasCharts1(props)) return null;
		const { dispatch, token } = (props || this.props);
		const item = this.getFilters();
		if (item) {
			const { type1, type2, groupBy } = this.state;
			const params = { type1, type2, groupBy, };
			this.handleChange('loadingHistoryByDay', true);
			dispatch(actions.postToURL({
				token,
				route: this.getHistoryByDayPath('/'),
				path: this.getHistoryByDayPath(),
				item,
				params,
				success: (resp) => {
					// console.log(resp);
					const items = resp.data.items || [];
					items.forEach(i => {
						const row = i;
						const Label = this.labelHistoryByDay(row.label);
						row.Label = Label;
					});
					this.handleChange('loadingHistoryByDay', false);
				},
				failure: (resp) => {
					this.handleChange('loadingHistoryByDay', false);
					window.message.error(i18n.t('ErrorFetchingHistoryByDay'), 0, respErrorDetails(resp));
				},
			}));
		}
	}
	fetchByCategory(props) {
		if (!this.hasCharts1(props)) return null;
		const { dispatch, token } = (props || this.props);
		const item = this.getFilters();
		if (item) {
			const { type1, type2, } = this.state;
			const params = { type1, type2, };
			this.handleChange('loadingByCategory', true);
			dispatch(actions.postToURL({
				token,
				route: this.getByCategoryPath('/'),
				path: this.getByCategoryPath(),
				item,
				params,
				success: () => this.handleChange('loadingByCategory', false),
				failure: (resp) => {
					this.handleChange('loadingByCategory', false);
					window.message.error(i18n.t('ErrorFetchingByCategory'), 0, respErrorDetails(resp));
				},
			}));
		}
	}
	fetchTimeAverage(props) {
		if (!this.hasCharts2(props)) return null;
		const { dispatch, token } = (props || this.props);
		const item = this.getFilters();
		if (item) {
			const { type1, } = this.state;
			const params = { type1, };
			this.handleChange('loadingTimeAverage', true);
			dispatch(actions.postToURL({
				token,
				route: this.getTimeAveragePath('/'),
				path: this.getTimeAveragePath(),
				item,
				params,
				success: () => this.handleChange('loadingTimeAverage', false),
				failure: (resp) => {
					this.handleChange('loadingTimeAverage', false);
					window.message.error(i18n.t('ErrorFetchingTimeAverage'), 0, respErrorDetails(resp));
				},
			}));
		}
	}
	fetchNPS(props) {
		const { dispatch, token } = (props || this.props);
		const { fromNPS: from, toNPS: to, } = this.state;
		this.setLoadingNPS(+1);
		dispatch(actions.postToURL({
			token,
			route: this.getNPSPath('/'),
			path: this.getNPSPath(),
			params: { from, to },
			success: () => this.setLoadingNPS(-1),
			failure: (resp) => {
				this.setLoadingNPS(-1);
				window.message.error(i18n.t('ErrorFetchingNPS'), 0, respErrorDetails(resp)
				);
			},
		}));
	}

	fetchCenter(props) {
		const { dispatch, token, user, } = (props || this.props);
		if (user.center) {
			const success = () => this.setState({ loadingContent: false });
			const failure = (resp) => {
				window.message.error(i18n.t('ErrorFetchingDashboardNotes'), 0, respErrorDetails(resp));
				this.setState({ loadingContent: false });
			};
			this.setState({ loadingContent: true, });
			dispatch(centerActions.setProp('item', {}));
			dispatch(centerActions.fetchOne(token, user.center.id, success, failure, false));
		}
	}
	canEditNotes() {
		return this.props.user.hasPermissions('centers.editnotes');
	}
	updateNotes(item) {
		if (this.canEditNotes()) {
			const { dispatch, token, } = this.props;
			this.setState({ notesDisabled: true });
			dispatch(centerActions.postToURL({
				token,
				route: 'center/updateNotes',
				item,
				success: (resp) => {
					this.setState({ notesDisabled: false, notes: {} });
					ModalFactory.hide('editModal');
					dispatch(centerActions.setProp('item', resp.data));
				},
				failure: (resp) => {
					window.message.error(i18n.t('ErrorUpdatingDashboardNotes'), 0, respErrorDetails(resp));
					this.setState({ notesDisabled: false });
				},
			}));
		}
	}

	handleChange(prop, value, callback) {
		this.setState({ [prop]: value }, callback);
	}
	handleRangeChange(range, callback) {
		const now = new Date();
		let from;
		let to = '';
		let fY;
		let tY;
		let fM;
		let tM;
		let fD;
		let tD;
		if (range !== 'CC') {
			let groupBy = 'HH';
			if (range === 'WW') {
				const day = now.getUTCDay();
				const diff = (now.getUTCDate() - day) + (day === 0 ? -6 : 1);
				from = this.getDateString(new Date(now.setDate(diff)));
				to = this.getDateString(new Date(now.setDate(diff + 7)));
				groupBy = 'IW';
			} else {
				fY = tY = now.getUTCFullYear();
				if (range === 'YY') {
					fM = tM = 0;
					tY = fY + 1;
					groupBy = 'MM';
				} else {
					fM = tM = now.getUTCMonth();
					if (range === 'MM') {
						tM = fM + 1;
					}
					groupBy = 'DD';
				}
				if (range === 'YY' || range === 'MM') {
					fD = tD = 1;
				} else {
					fD = now.getUTCDate();
					tD = fD + 1;
					groupBy = 'HH';
				}
				from = this.getDateString(new Date(fY, fM, fD));
				to = this.getDateString(new Date(tY, tM, tD));
			}
			callback({ from, groupBy, to, });
		}
	}
	handleType1Change(type1) {
		const type2 = '';
		this.setState({ type1, type2, }, () => {
			this.fetchByProductType();
			this.fetchHistoryByDay();
			this.fetchByCategory();
		});
	}
	handleType2Change(type2) {
		this.setState({ type2 }, () => {
			this.fetchByProductType();
			this.fetchHistoryByDay();
			this.fetchByCategory();
		});
	}

	hasCharts1(props) {
		return (props || this.props).user.hasPermissions('dashboard.charts1');
	}
	hasCharts2(props) {
		return (props || this.props).user.hasPermissions('dashboard.charts2');
	}
	hasTransactionPermission(props) {
		return (props || this.props).user.hasPermissions('transactions.add');
	}

	labelHistoryByDay(strLabel) {
		const date = new Date(strLabel);
		const { groupBy, } = this.state;
		const locale = (this.props.user.language || '')
			.replace(LanguagePrefix, '');
		switch (groupBy) {
			case 'HH':
				return date.toLocaleString(locale, { hour: 'numeric', hour12: true });
			case 'DD':
				return date.toLocaleString(locale, { day: 'numeric' });
			case 'IW':
				return date.toLocaleString(locale, { weekday: 'long' });
			case 'MM':
				return date.toLocaleString(locale, { month: 'short' });
			case 'YY':
				return date.toLocaleString(locale, { year: 'numeric' });
			default:
				break;
		}
	}

	schemaTimeAverage = {
		fields: {
			objetive: {
				label: i18n.t('Objetive'),
				format: (row) => `${row.objetive}`,
			},
			value: {
				label: i18n.t('Average'),
				format: (row) => `${row.value}`,
			},
			label: i18n.t('Name'),
			id: i18n.t('ID'),
		}
	}
	rowClassTimeAverage = (row, /*i, selected */) => {
		if (!row.value) {
			return 'none';
		}
		if (row.margin >= -30 && row.margin <= 30) {
			return 'success';
		}
		if (row.value < row.objetive) {
			return 'warning';
		}
		return 'danger';
	}
	schemaNPS = {
		fields: {
			nps: {
				label: i18n.t('NPS'),
				format: (row) => `${row.nps}`,
			},
			nombre: i18n.t('Name'),
			segmento: i18n.t('Center'),
		}
	}

	renderRanges(ranges, value, onChange, htmlFor) {
		return (
			<Col className="text-center">
				{ranges.map(range => (
					<label key={range.value} className="m-r-sm" htmlFor={htmlFor}>
						{range.label}
						<input
							type="radio"
							style={{ margin: '4px 5px 0px 5px' }}
							value={range.value}
							checked={value === range.value}
							onChange={onChange}
						/>
					</label>
				))}
			</Col>
		);
	}
	renderFilters() {
		if (!this.hasCharts1() && !this.hasCharts2() && !this.hasTransactionPermission()) return null;
		const {
			ranges, range, groupBy,
			type1, type2,
			from, to,
		} = this.state;
		const categoriesType1 = this.getCategoriesType1();
		const categoriesType2 = this.getCategoriesType2();
		return (
			<PanelCondensed>
				<Row
					style={{ borderBottom: '1px solid red', paddingBottom: 9, marginBottom: 10, }}
				>
					{this.renderRanges(ranges, range, (e) => {
						this.setState({ range: e.target.value });
						this.handleRangeChange(e.target.value,
							({ from: pFrom, groupBy: pGroupBy, }) => {
								this.handleChange('groupBy', pGroupBy);
								this.handleChange('from', pFrom);
								this.handleChange('to', '', () => this.fetchAll());
							}
						);
					}, 'byProductType')}
					{range === 'CC' && <Col>
						<FormGroup
							isValid={!!from}
							className="col-sm-4 col-md-3 col-lg-2 col-lg-offset-3 col-md-offset-1"
						>
							<label htmlFor="from">{i18n.t('FromDate')}:</label>
							<Input
								type="date" value={from}
								onFieldChange={(e) => this.handleChange(
									'from', e.target.value,
									() => this.fetchAll())
								}
							/>
						</FormGroup>
						<FormGroup className="col-sm-4 col-md-3 col-lg-2">
							<label htmlFor="to">{i18n.t('ToDate')}:</label>
							<Input
								type="date" value={to}
								onFieldChange={(e) => this.handleChange(
									'to', e.target.value,
									() => this.fetchAll())
								}
							/>
						</FormGroup>
						<FormGroup className="col-sm-4 col-md-3 col-lg-2">
							<label htmlFor="groupBy">{i18n.t('GroupBy')}:</label>
							<DropDown
								items={[
									{ value: 'HH', label: i18n.t('Hour') },
									{ value: 'DD', label: i18n.t('Day') },
									{ value: 'IW', label: i18n.t('Week') },
									{ value: 'MM', label: i18n.t('Month') },
									{ value: 'YY', label: i18n.t('Year') },
								]}
								value={groupBy}
								onChange={(e) => this.handleChange(
									'groupBy', e.target.value,
									() => this.fetchHistoryByDay())
								}
							/>
						</FormGroup>
					</Col>}
				</Row>
				<Col>
					<Col size={{ xs: 12, sm: 6, md: 3, }}>
						{this.hasCharts1() ? [
							<label key="1" htmlFor="type1">{i18n.t('DashboardType1')}:</label>,
							<DropDown
								key="2"
								items={categoriesType1}
								value={type1}
								onChange={(e) => this.handleType1Change(e.target.value)}
								emptyOption={<option value="">{i18n.t('AllType1')}</option>}
							/>
						] : null}
					</Col>
					<Col size={{ xs: 12, sm: 6, md: 3, }}>
						{this.hasCharts1() ? [
							<label key="1" htmlFor="type2">{i18n.t('DashboardType2')}:</label>,
							<DropDown
								key="2"
								items={categoriesType2}
								value={type2}
								onChange={(e) => this.handleType2Change(e.target.value)}
								emptyOption={<option value="">{i18n.t('AllType2')}</option>}
							/>
						] : null}
					</Col>
					<Col className="text-right" size={{ md: 6 }}>
						<label className="block" htmlFor="space">&zwnj;</label>
						{(this.hasCharts1() || this.hasCharts2()) ?
							<Button
								className="btn-info btn-sm_"
								icon="fa-refresh"
								label={i18n.t('ToUpdate')}
								onClick={() => this.refresh()}
							/>
							: null
						}
						{this.hasTransactionPermission() ?
							<Button
								className="btn-success btn-sm_"
								icon="fa-phone"
								label={i18n.t('StartTransaction')}
								onClick={() => this.goToTransactionCreate()}
							/>
							: null
						}
					</Col>
				</Col>
			</PanelCondensed>
		);
	}
	renderByProductType() {
		if (!this.hasCharts1()) return null;
		const loading = this.state.loadingByProductType;
		const items = this.getByProductType()/* .reverse() */;
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('ByProductTypeTitle')]}
			>
				<BarChart
					data={{
						labels: items.map(i => i.label),
						series: [
							items.map(i => i.value),
						]
					}} horizontal
					options={{
						// height: 255 + (!items.length ? 45 : 0),
						onlyInteger: true,
						axisX: {
							showGrid: false,
							showLabel: items.length,
							onlyInteger: true,
						},
						axisY: {
							offset: 70,
							onlyInteger: true,
							showGrid: false,
						}
					}}
				/>
				{items.map((item, i) => (
					<Col key={i} size={{ xs: 2 }} className="text-center m-b-sm">
						<span className="block" htmlFor="name">{item.label}</span>
						<span className="block" htmlFor="value">{item.value}</span>
					</Col>
				))}
			</PanelCondensed>
		);
	}
	renderHistoryByDay() {
		if (!this.hasCharts1()) return null;
		const loading = this.state.loadingHistoryByDay;
		const items = this.getHistoryByDay();
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('HistoryByDay')]}
			>
				<BarChart
					data={{
						labels: items.map(i => i.Label),
						series: [
							items.map(i => i.value),
						]
					}}
					options={{
						onlyInteger: true,
						axisX: {
							showGrid: false,
							onlyInteger: true,
						},
						axisY: {
							onlyInteger: true,
							showLabel: items.length,
							showGrid: false,
						}
					}}
				/>
				{items.map((item, i) => (
					<Col key={i} size={{ xs: 2 }} className="text-center m-b-sm">
						<span className="block" htmlFor="name">{item.Label}</span>
						<span className="block" htmlFor="value">{item.value}</span>
					</Col>
				))}
			</PanelCondensed>
		);
	}
	renderByCategory() {
		if (!this.hasCharts1()) return null;
		const loading = this.state.loadingByCategory;
		const items = this.getByCategory();
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('ByCategory')]}
			>
				<BarChart
					data={{
						labels: items.map(i => i.label),
						series: [
							items.map(i => i.value),
						]
					}} horizontal
					options={{
						onlyInteger: true,
						axisX: {
							showGrid: false,
							showLabel: items.length,
							onlyInteger: true,
						},
						axisY: {
							offset: 150,
							onlyInteger: true,
							showGrid: false,
						}
					}}
				/>
				{items.map((item, i) => (
					<Col key={i} size={{ xs: 2 }} className="text-center m-b-sm">
						<span className="block" htmlFor="name">{item.label}</span>
						<span className="block" htmlFor="value">{item.value}</span>
					</Col>
				))}
			</PanelCondensed>
		);
	}
	renderByCategoryPie() {
		if (!this.hasCharts1()) return null;
		const loading = this.state.loadingByCategory;
		const items = this.getByCategory();
		const labels = items.map(i => i.label);
		const series = items.map(i => i.value);
		// labels.push('Otro');
		// series.push(3);
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('ByCategory')]}
			>
				<PieChart
					donut legend
					width="100%"
					data={{ labels, series, }}
					options={{
						labelInterpolationFnc: (value, idx) => {
							const percentage = `${
								Math.round((series[idx] / series.reduce((a, b) => a + b)) * 100)
								}%`;
							return `${value} ${percentage}`;
						}
					}}
				/>
			</PanelCondensed>
		);
	}
	renderTimeAverage() {
		if (!this.hasCharts2()) return null;
		const { loadingTimeAverage: loading, } = this.state;
		const { user } = this.props;
		const items = this.getTimeAverage();
		// const {
		// 	ranges, range, value, /* min, */ max,
		// 	porcentage: valueP,
		// 	yellow, green, red,
		// } = this.getTimeAverageRanges(items[0]);
		// const userRows = items.filter(i => i.id === user.id);
		// const reportRows = items.filter(i => i.id !== user.id);
		const sortRows = (rows) => rows.sort(sortFunction(['value', 'label'], [true, false]));
		const userRows = [];
		const reportRows = [];
		const groupObjs = {};
		items.forEach((item) => {
			if (item.id === user.id) {
				userRows.push(item);
			} else if (item.id_parent === user.id) {
				reportRows.push(item);
			} else {
				let group = groupObjs[item.id];
				if (!group) {
					group = groupObjs[item.id] = [];
					group.items = 0;
				}
				group.name = item.label;
				group.items++;
				group.push(item);
				let manager = groupObjs[item.id_parent];
				if (!manager) {
					manager = groupObjs[item.id_parent] = [];
					manager.items = 0;
				}
				manager.push(item);
			}
		});
		const groups = Object.keys(groupObjs)
			.map(key => {
				groupObjs[key].key = key * 1;
				return groupObjs[key];
			})
			.filter(group => {
				const no = group.items;
				return no && group.items < group.length;
			})
			.sort(sortFunction('label'))
			.map((group) => {
				const rows = []
					.concat(sortRows(group.filter(a => a.id === group.key)))
					.concat(sortRows(group.filter(a => a.id !== group.key)))
					;
				rows.name = group.name;
				return rows;
			});
		this.groupsAHT = groups;
		this.groupObjsAHT = groupObjs;

		// const circleStyle = {
		// 	display: 'inline-block',
		// 	width: 100,
		// 	height: 100,
		// 	margin: '35px 15px',
		// 	borderRadius: 100,
		// 	opacity: 0.5,
		// };
		// const selectedStyle = {
		// 	opacity: 1,
		// 	boxShadow: '0 6px 20px 0 rgba(0, 0, 0, 0.3), 0 6px 20px 0 rgba(0, 0, 0, 0.3)',
		// };
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('TimeAverage')]}
			>
				{userRows.map(row => {
					const imageName = `aht-${this.rowClassTimeAverage(row)}`;
					return (
						<div key="1" className="text-center">
							<img
								role="presentation"
								className="img-responsive inline-block"
								style={{ height: 150, margin: 10, }}
								src={`/dist/images/${imageName}.png`}
							/>
						</div>
					);
				})}
				{userRows.length ?
					<DataTable
						key="2"
						schema={this.schemaTimeAverage}
						rows={sortRows(userRows)}
						rowClass={this.rowClassTimeAverage}
						tableClassName="table-bordered"
					/>
					: null
				}
				{reportRows.length ?
					<PanelCollapsible title={i18n.t('YourReports')}>
						<Row>
							<DataTable
								schema={this.schemaTimeAverage}
								tableClassName="table-bordered"
								rows={sortRows(reportRows)}
								rowClass={this.rowClassTimeAverage}
							/>
						</Row>
					</PanelCollapsible>
					: null
				}
				{groups.map((group, i) => (
					<PanelCollapsible
						key={i}
						title={i18n.t('ReportsAndDelegatingOf', { name: group.name })}
					>
						<Row>
							<DataTable
								schema={this.schemaTimeAverage}
								tableClassName="table-bordered"
								rows={group}
							/>
						</Row>
					</PanelCollapsible>
				))}
			</PanelCondensed >
		);
	}
	renderNPS() {
		if (!this.hasCharts2()) return null;
		const { loadingNPS: loading, ranges, rangeNPS, fromNPS, toNPS, } = this.state;
		const items = this.getNPS();
		const { user } = this.props;
		const sortRows = (rows) => rows.sort(sortFunction(['nps', 'nombre'], [true, false]));
		const userRows = [];
		const reportRows = [];
		const groupObjs = {};
		items.forEach((item) => {
			if (item.tarjeta_empleado === user.id) {
				userRows.push(item);
			} else if (item.tarjeta_supervisor === user.id) {
				reportRows.push(item);
			} else {
				let group = groupObjs[item.tarjeta_empleado];
				if (!group) {
					group = groupObjs[item.tarjeta_empleado] = [];
					group.items = 0;
				}
				group.name = item.nombre;
				group.items++;
				group.push(item);
				let manager = groupObjs[item.tarjeta_supervisor];
				if (!manager) {
					manager = groupObjs[item.tarjeta_supervisor] = [];
					manager.items = 0;
				}
				manager.push(item);
			}
		});
		const groups = Object.keys(groupObjs)
			.map(key => {
				groupObjs[key].key = key * 1;
				return groupObjs[key];
			})
			.filter(group => {
				const no = group.items;
				return no && group.items < group.length;
			})
			.sort(sortFunction('name'))
			.map((group) => {
				const rows = []
					.concat(sortRows(group.filter(a => a.tarjeta_empleado === group.key)))
					.concat(sortRows(group.filter(a => a.tarjeta_empleado !== group.key)))
					;
				rows.name = group.name;
				return rows;
			});
		this.groups = groups;
		this.groupObjs = groupObjs;
		return (
			<PanelCondensed
				title={[<AnimatedSpinner key show={loading} />, i18n.t('TitleNPS')]}
			>
				{this.renderRanges(ranges.filter(i => i.value !== 'YY'), rangeNPS, (e) => {
					this.setState({ rangeNPS: e.target.value });
					this.handleRangeChange(e.target.value,
						({ from: pFrom, to: pTo }) => {
							this.handleChange('fromNPS', pFrom);
							this.handleChange(
								'toNPS', pTo, () => this.fetchNPS()
							);
						}
					);
				}, 'nps')}
				{rangeNPS === 'CC' && <Col className="m-b-sm">
					<FormGroup isValid={!!fromNPS} className="col-sm-5 col-sm-offset-1">
						<label htmlFor="fromNPS">{i18n.t('FromDate')}:</label>
						<Input
							type="date" value={fromNPS}
							onFieldChange={(e) => this.handleChange(
								'fromNPS', e.target.value,
								() => this.fetchNPS())
							}
						/>
					</FormGroup>
					<FormGroup className="col-sm-5 m-b-sm">
						<label htmlFor="toNPS">{i18n.t('ToDate')}:</label>
						<Input
							type="date" value={toNPS}
							onFieldChange={(e) => this.handleChange(
								'toNPS', e.target.value,
								() => this.fetchNPS())
							}
						/>
					</FormGroup>
				</Col>}
				<div className="text-center">
					{userRows.map((item, i) => (
						<EasyPie
							key={i}
							theme="scripting_nps"
							barColor="#5fe4c3" trackColor="#ccc"
							size={150} fontSize={30} lineWidth={5}
							percent={item.nps}
							wrapperStyle={{ margin: 5, }}
						/>
					))}
				</div>
				{userRows.length ?
					<DataTable
						schema={this.schemaNPS}
						tableClassName="table-bordered"
						rows={sortRows(userRows)}
					/>
					: null
				}
				{reportRows.length ?
					<PanelCollapsible title={i18n.t('YourReports')}>
						<Row>
							<DataTable
								schema={this.schemaNPS}
								tableClassName="table-bordered"
								rows={sortRows(reportRows)}
							/>
						</Row>
					</PanelCollapsible>
					: null
				}
				{groups.map((group, i) => (
					<PanelCollapsible
						key={i}
						title={i18n.t('ReportsAndDelegatingOf', { name: group.name })}
					>
						<Row>
							<DataTable
								schema={this.schemaNPS}
								tableClassName="table-bordered"
								rows={group}
							/>
						</Row>
					</PanelCollapsible>
				))}
			</PanelCondensed>
		);
	}
	renderContent() {
		const { centers: { item }, user, } = this.props;
		const { loadingContent, } = this.state;
		return (user.center &&
			<Panel
				title={[
					<AnimatedSpinner key="1" show={loadingContent} />,
					i18n.t('DashboardNotes'),
					(this.canEditNotes() && <Fa
						key="3"
						icon="edit"
						onClick={() => {
							this.setState({
								notes: Object.assign({}, {
									id: item.id,
									value: item.notes,
								})
							});
							ModalFactory.show('editModal');
						}}
						style={{ position: 'absolute', right: 20, }}
					/>)
				]}
			>
				<TransactionContentItem
					notCached
					content={{ id_type: ContentTypes.HTML, value: item.notes }}
					results={[]}
					// containerStyle={{ marginRight: 100, }}
					separatorStyle={{ display: 'none' }}
				/>
			</Panel>
		);
	}
	render() {
		const { notes, notesDisabled, } = this.state;
		const noPadding = global.getBootstrapViewport().minWidth < 992;
		return (
			<Page>
				<Factory
					modalref="editModal" title={i18n.t('DashboardNotes')}
					factory={EditNotesModal} large canBeClose
					item={notes} disabled={notesDisabled}
					onUpdate={(newItem) => this.updateNotes(newItem)}
				/>
				<Col>
					{this.renderFilters()}
					<Row>
						<Col size="6" style={{ paddingRight: noPadding ? undefined : 5, }}>
							{this.renderByProductType()}
							{this.renderByCategory()}
							{this.renderTimeAverage()}
						</Col>
						<Col size="6" style={{ paddingLeft: noPadding ? undefined : 5, }}>
							{this.renderHistoryByDay()}
							{this.renderByCategoryPie()}
							{this.renderNPS()}
						</Col>
					</Row>
					{this.renderContent()}
				</Col>
			</Page>
		);
	}
}

const PanelCondensed = (props) => (
	<Panel
		marginBottomClass="m-b-sm"
		containerStyle={{ padding: '0 10px' }}
		{...props}
	/>
);

class EditNotesModal extends Component {

	state = {};

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}

	saveItem(e) {
		e.preventDefault();
		const item = this.getItem();
		this.props.onUpdate(item);
	}

	isDisabled() {
		return !!this.props.disabled;
	}

	render() {
		const { value } = this.getItem();
		return (
			<div>
				<FormGroup>
					<label className="control-label" htmlFor="notes">{i18n.t('Notes')}:</label>
					<ReactSummernoteInput
						ref={(ref) => {
							window.ReactSummernoteInput = this.ReactSummernoteInput = ref;
						}}
						value={value}
						onChange={(html) => this.handleChange('value', html)}
						disabled={this.isDisabled()}
					/>
				</FormGroup>

				<div className="">
					<hr />
					<button
						type="submit" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled()}
					>
						{i18n.t('SaveChanges')}
					</button>
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(Dashboard);
