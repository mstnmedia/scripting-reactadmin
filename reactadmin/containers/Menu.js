/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Menu
 */

import React, { Component } from 'react';
import Cookies from 'cookies-js';

const shallowCompare = require('react-addons-shallow-compare');

export default class Menu extends Component {
	constructor(props) {
		super(props);
		const { collapsed, } = getCookie();
		this.state = {
			open: false,
			collapsed,
		};
		this.toggleShowHide = this.toggleShowHide.bind(this);
		global.toggleMenu = this.toggleShowHide;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	toggleShowHide() {
		this.setState({ open: !this.state.open });
	}

	toggleCollapse() {
		const collapsed = !this.state.collapsed;
		this.setState({ collapsed, });
		setCookie({ collapsed, });
	}

	render() {
		const { collapsed, open } = this.state;
		let width = '';
		let classes = 'appMenu bg-black';
		let navClasses = 'nav-primary full';
		const screen = global.getBootstrapViewport();

		/* FIXME: when windows resizes between lg and xs screens this calc is unresponsive */
		if (screen.code === 'xs') {
			width = 'unset';
			if (open) {
				width = '100%';
			} else {
				classes += ' nav-xs';
				navClasses += ' hidden-xs';
			}
		} else {
			classes += ' nav-xs';
			navClasses += ' hidden-xs';
			if (collapsed) {
				width = '5px';
				navClasses += ' hidden-lg hidden-md hidden-sm';
			}
		}

		return (
			<aside className={classes} id='nav' style={{ width }}>
				<nav className={navClasses} style={styles.content}>
					<ul className='nav' style={styles.scrollable}>
						{this.props.children}

						{/* 
							<MenuItem
								link={'/'}
								icon='fa-desktop'
								color='bg-success'
								linkText='UI'
								currentPage={this.props.currentPage}
							>
								<SubMenuItem link={'/ui/general'} linkText={'General'} />
								<SubMenuItem link={'/ui/buttons'} linkText={'Buttons'} />
								<SubMenuItem link={'/ui/fontawesome'} linkText={'Font Awesome'} />
								<SubMenuItem link={'/ui/materialicons'} linkText={'Material Design Icons'} />
								<SubMenuItem link={'/ui/tables'} linkText={'Tables'} />
								<SubMenuItem link={'/ui/modals'} linkText={'Modals'} />
							</MenuItem>
							<MenuItem
								link={'/components'}
								//badgeCount={8}
								icon='fa-sitemap'
								color='bg-success'
								linkText='Components'
								currentPage={this.props.currentPage}
							/>
							<MenuItem
								link={'/'}
								icon='fa-database'
								color='bg-success'
								linkText='Data'
								currentPage={this.props.currentPage}
							>
								<SubMenuItem link={'/data/grid'} linkText={'Grid'} />
								<SubMenuItem link={'/data/forms'} linkText={'Forms'} />
							</MenuItem>
							<MenuItem
								link={'/forms'}
								icon='fa-tasks'
								color='bg-success'
								linkText='Forms'
								currentPage={this.props.currentPage}
							/>
							<MenuItem
								link={'/charts'}
								icon='fa-bar-chart-o'
								color='bg-success'
								linkText='Charts'
								currentPage={this.props.currentPage}
							/>
							<MenuItem
								link={'/'}
								icon='fa-rocket'
								color='bg-success'
								linkText='Apps'
								currentPage={this.props.currentPage}
							>
								<SubMenuItem link={'/apps/boards'} linkText={'Boards'} />
								<SubMenuItem link={'/apps/notes'} linkText={'Notes'} />
								<SubMenuItem link={'/apps/maps'} linkText={'Maps'} />
								<SubMenuItem link={'/apps/email'} linkText={'Email'} />
							</MenuItem> 
							<MenuItem
								link={'/widgets'}
								icon='fa-share-alt-square'
								color='bg-success'
								linkText='Widgets'
								currentPage={this.props.currentPage}
							/>

							<MenuItem
								link={'/analytics'}
								icon='fa-line-chart'
								color='bg-success'
								linkText='Analytics'
								currentPage={this.props.currentPage}
							/>
						*/}
					</ul>
				</nav>
				<div
					className="hidden-xs"
					style={{
						padding: 5,
						alignSelf: 'center',
						position: 'absolute',
						right: -15,
						color: 'rgb(119, 119, 119)',
						zIndex: 10,
					}}
					onClick={() => this.toggleCollapse()}
				>
					<i className={`fa fa-chevron-${collapsed ? 'right' : 'left'}`} />
				</div>
				{/* 
					<section className='vbox'>
						<section className='w-f scrollable' style={{ padding: 0 }}>
							<div className='slimScrollDiv'>
								<div
									className='slim-scroll'
									data-height='auto'
									data-disable-fade-out='true'
									data-distance='0'
									data-size='5px'
									data-color='#333333'
								>

								</div>
								<div className='slimScrollBar scrollBar' />
								<div className='slimScrollRail scrollRail' />
							</div>
						</section>
					</section>
				*/}
			</aside>
		);
	}
}

const styles = {
	content: {
		zIndex: 1500,
		// margin: 0,
		// overflow: 'hidden',
		// height: 'calc(100vh - 100px)',
	},
	// scrollable: {
	// 	height: '100%',
	// 	position: 'absolute',
	// 	overflowY: 'overlay',
	// },
};

function getCookie() {
	let state = Cookies.get('menuState');
	if (!state) {
		state = {
			collapsed: false,
			width: undefined,
		};
	} else {
		state = JSON.parse(state);
	}
	return state;
}
function setCookie(state) {
	Cookies.set('menuState', JSON.stringify(state), { expires: 100 });
}
