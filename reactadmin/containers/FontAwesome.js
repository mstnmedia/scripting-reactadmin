/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule FontAwesome
 */

import React, { Component } from 'react';
// import { Link, browserHistory } from 'react-router';

// import moment from 'moment';
// import ModalFactory from '../components/modals/factory';

import { faicons } from '../components/ui/faicons';
import { Col, Page, Fa, Panel, } from '../components/';

// const Factory = ModalFactory.modalFromFactory;
const shallowCompare = require('react-addons-shallow-compare');

class FontAwesome extends Component {
	state = {
		iconName: '',
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	mouseOut() {
		this.setState({ iconName: '' });
	}

	mouseOver(iconName) {
		console.log(iconName);
		this.setState({ iconName });
	}

	render() {
		return (
			<Page>
				<Col size="12">
					<Panel title="Font Awesome Icons">
						<div style={{ top: 120, right: 50, position: 'fixed' }}>
							<label htmlFor="iconName" className="label label-info" style={{ fontSize: 15 }}>
								{this.state.iconName}
							</label>
						</div>
						{Object.keys(faicons).sort().map(i =>
							<Col key={i} size="1" className="text-center">
								<Fa
									icon={i} size="2em"
									classes={'m-r-lg m-b-lg cursor-pointer'}
								// style={{ margin: 5 }}
								/>
								<label
									htmlFor="name"
									className="block"
									style={{ whiteSpace: 'nowrap' }}
								>{i}</label>
							</Col>
						)}
					</Panel>
				</Col>
			</Page>
		);
	}
}


export default FontAwesome;

