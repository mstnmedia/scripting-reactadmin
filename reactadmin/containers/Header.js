/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Header
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import i18n from '../../i18n';
import { mapStateToProps } from '../../outlets/';
// import Profile from '../components/widgets/Profile';

global.HeaderHeight = 100;
global.HeaderBottonBorder = { class: 'md', value: 20 };

class Header extends Component {

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	handleOnScreen() {
		$('#nav').toggleClass('nav-off-screen');
	}

	renderLogo() {
		return (
			<div className='headerLogo bg-danger'>
				<a
					className='btn btn-link visible-xs'
					onClick={() => global.toggleMenu()}
					data-toggle='class:nav-off-screen,open'
					data-target='#nav,html'
				>
					<i className='fa fa-bars' />
				</a>
				<div className='text-center hidden-xs'>
					<img width="90" src='/dist/images/claro-logo.png' alt={i18n.t('Logo')} />
				</div>
			</div>
		);
	}
	render() {
		const { route } = this.props;
		return (
			<div className="appHeader">
				{this.renderLogo()}
				<div className="headerBody" style={{ borderBottom: '5px solid #d7290f', }}>
					<h1 style={{ float: 'right' }}>
						<img
							className="m-r-xs"
							style={{ marginTop: -15 }}
							width='60'
							src='/dist/images/favicon.png'
							alt={i18n.t('Logo')} 
						/>
						{i18n.t('AppName')}
					</h1>
					<h3 className="m-b-none">{route.pageName}</h3>
					<small className="text-muted">{route.pageDescription}</small>
				</div>
				{/* <div className="headerProfile">
					<ul className="nav navbar-nav navbar-right nav-user">
						<Profile onLogout={this.props.onLogout} profile={this.props.user} />
					</ul>
				</div> */}
			</div>
		);
	}
}

export default connect(mapStateToProps, {})(Header);
