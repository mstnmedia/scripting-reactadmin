/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule App
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment';

import i18n from '../../i18n';
import Landing from './Landing';
import Header from './Header';
import { Menu } from '../../containers/';
import Signup from './Signup';

import { mapStateToProps } from '../../outlets/';
import { auth, signup, /* logout, isAuthenticated, fetchMe, */ } from '../../actions';
import { Fa } from '../components';
import { DateFormats } from '../../store/enums';

class App extends Component {
	componentWillMount() {
		i18n.on('loaded', () => {
			this.setState({ loaded: true });
		});
		window.moment = moment;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	renderBreadcrumb() {
		const { route } = this.props;
		const breadcrumbItems = global.getBreadcrumbItems
			? global.getBreadcrumbItems(route)
			: [{ label: i18n.t('Home'), to: '/' }];
		if (breadcrumbItems.loading) {
			return `${i18n.t('Loading')} ...`;
		}
		return breadcrumbItems.map((item, i) => {
			const { label, to, onClick, } = item;
			const linkProps = { to, onClick, };
			return (
				<div key={i} className="m-r-xs m-b-xs inline">
					<Link {...linkProps}>{label || item}</Link>
					{(i + 1) !== breadcrumbItems.length ? <span className="m-l-xs">/</span> : null}
				</div>
			);
		});
	}
	renderUserInfo() {
		const { user, } = this.props;
		const centerName = (user.center && user.center.name) || i18n.t('UserWithoutCenter');
		const userRole = user.role || i18n.t('UserWithoutRole');
		const lastLogin = (user.logged && moment(user.logged).format(DateFormats.DATE_TIME_Z)) || '';
		return (
			<div
				className="text-right"
				style={{ display: 'flex', flexDirection: 'row-reverse', }}
			>
				<a href={`${window.config.backend}/logout`} >
					<Fa
						className="fa-2x fa-sign-out m-l-sm -m-t-sm text-danger"
						title={i18n.t('Logout')}
					/>
				</a>
				<span
					title={`${i18n.t('Center')}: ${centerName}\n${
						i18n.t('UserRole')}: ${userRole}\n${
						i18n.t('UserLastLogin')}: ${lastLogin}`}
					style={{
						whiteSpace: 'nowrap',
						textOverflow: 'ellipsis',
						maxWidth: '100%',
						overflow: 'hidden',
					}}
				>{user.name}</span>
				<label className="m-r-xs" htmlFor="user">{i18n.t('User')}:</label>
			</div>
		);
	}
	render() {
		// eslint-disable-next-line no-shadow
		const { location, children,
			loggedIn, user,
			routes,
			//auth, signup, onLogout
		} = this.props;
		const { pathname } = location;
		const value = pathname; //.substring(1);

		if (!loggedIn) {
			if (value === 'signup') {
				return (
					<Signup onSignup={(email, companyName) => signup({ email, companyName, })} />
				);
			}
			return (
				<Landing onLogin={(email, password) => auth({ email, password, })} />
			);
		}
		const breadcrumb = this.renderBreadcrumb();
		const userInfo = this.renderUserInfo();
		const screen = global.getBootstrapViewport();
		return (
			<div className="appContainer">
				<Header user={user} routes={routes} route={routes[1]} />
				<div className="appBody">
					<Menu user={user} currentPage={value} />
					<div className="pagerContainer">
						<div
							className="appBreadcrumb"
							style={{
								flexDirection: 'row',
								display: (breadcrumb.length && screen.code !== 'xs') ? 'flex' : 'hidden'
							}}
						>
							<div className="" style={{ flex: 1 }}>
								{breadcrumb}
							</div>
							{userInfo}
						</div>
						{children}
					</div>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, { auth, signup })(App);
