/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Root
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router, hashHistory, } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import shallowCompare from 'react-addons-shallow-compare';

import configureStore from '../store/configureStore';

const store = configureStore();

const history = syncHistoryWithStore(hashHistory, store);

export default class Root extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	render() {
		return (
			<Provider store={store}>
				<Router history={history}>
					{this.props.children}
				</Router>
			</Provider>
		);
	}
}

global.getBootstrapViewport = () => {
	let code = '';
	let minWidth = 0;
	let maxWidth = 0;
	if ($(window).width() < 768) {
		// do something for small screens
		code = 'xs';
		maxWidth = 768;
	} else if ($(window).width() >= 768 && $(window).width() <= 992) {
		// do something for medium screens
		code = 'sm';
		minWidth = 768;
		maxWidth = 992;
	} else if ($(window).width() > 992 && $(window).width() <= 1200) {
		// do something for big screens
		code = 'md';
		minWidth = 992;
		maxWidth = 1200;
	} else {
		// do something for huge screens
		code = 'lg';
		minWidth = 1200;
		maxWidth = undefined;
	}
	return {
		code,
		minWidth,
		maxWidth,
	};
};
