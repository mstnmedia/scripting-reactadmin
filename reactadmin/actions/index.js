import request from 'axios';

import TYPES, {
	USER_SIGNUP,
	USER_ME_UPDATE,
	USER_AUTH,
	USER_IS_AUTHENTICATED,
	USER_LOGOUT,
	RECEIVE_USERS,
	RECEIVE_ME,
} from './types';
import config from '../../config'; // Custom configs of each app
// import { getCookie } from '../Utils';

const getAuthorizationHeader = (/* token */) => ({
	// Authorization: token,
	// Authorization: `Bearer ${getCookie(config.TOKEN_COOKIE_KEY)}`,
	//'x-access-token': token,
});

export * from './types';

export const API_URL = config.backend;

export function signup({ email, companyName, }) {
	const params = {
		email,
		companyName
	};
	return {
		type: USER_SIGNUP,
		promise: request.post(`${API_URL}/api/signup`, params)
	};
}

export function putMe({ me, token, }) {
	return {
		type: USER_ME_UPDATE,
		promise: request.post(`${API_URL}/api/users/me`, me, {
			headers: getAuthorizationHeader(token),
		})
	};
}

export function auth({ email, password, }) {
	const params = {
		email,
		password,
	};
	return {
		type: USER_AUTH,
		promise: request.post(`${API_URL}/authenticate`, params)
	};
}

export function logout() {
	return {
		type: USER_LOGOUT
	};
}

export function isAuthenticated() {
	return {
		type: USER_IS_AUTHENTICATED
	};
}

export function fetchUsers(token) {
	return {
		type: RECEIVE_USERS,
		promise: request.get(`${API_URL}'/api/users/`, {
			headers: getAuthorizationHeader(token),
		})
	};
}

export const fetchMe = function ({ /* token, */ success, failure, }) {
	return {
		type: RECEIVE_ME,
		promise: request.get(`${API_URL}/userInfo`, {
			// headers: getAuthorizationHeader(token),
			withCredentials: true,
		}).then(response => {
			try {
				if (success) success(response);
			} catch (error) {
				console.error(error);
			}
			return response;
		}).catch((error) => {
			// console.log(error);
			if (failure) failure(error);
			return error;
		})
	};
};

export default {
	...TYPES,
	auth,
	logout,
	fetchMe,
	fetchUsers,
	isAuthenticated,
	putMe,
	signup,
};
