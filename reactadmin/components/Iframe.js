/**
 * Based from https://codepen.io/micha149/pen/zBzLwJ
 */
import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import PropTypes from 'prop-types';

export default class Iframe extends React.Component {
	static propTypes = {
		htmlCode: PropTypes.string,
		urlSource: PropTypes.string,
	};
	constructor(props) {
		super(props);
		this.state = {
			iframeResize: (e) => {
				const doc = e.target.documentElement;
				if (doc) {
					const height = doc.offsetHeight;
					// $(iframe).height(`${height}px`);
					this.refs.iframe.setAttribute('height', height);
				}
			}
		};
	}

	componentDidMount() {
		this.updateIframe();
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	componentDidUpdate() {
		this.updateIframe();
	}

	updateIframe() {
		const { htmlCode, urlSource } = this.props;
		const iframe = this.refs.iframe;
		/* Clear the iframe */
		iframe.contentWindow.document.open();
		iframe.contentWindow.document.write('');
		iframe.contentWindow.document.close();

		if (urlSource || htmlCode) {
			if (urlSource) {
				iframe.src = urlSource;
			} else if (htmlCode) {
				iframe.contentDocument.body.innerHTML = htmlCode;
			}
			iframe.contentWindow.addEventListener('load', this.state.iframeResize.bind(this));
			iframe.contentWindow.addEventListener('resize', this.state.iframeResize.bind(this));
		}
	}

	render() {
		return (
			<iframe
				ref="iframe"
				style={{ border: 'none', width: '100%', ...this.props.style }}
			/>
		);
	}
}
