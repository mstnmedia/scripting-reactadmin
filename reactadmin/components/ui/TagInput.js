
import React, { Component } from 'react';
import { isFunction, replaceSpaces } from '../../Utils';

/**
 * @augments {Component<{ onChange: function, value: string, suggestions: [] }, {arraysf: number}>}
 */
export class TagInput extends Component {
    state = {
        input: '',
    };
    getTags() {
        return (this.props.value || '')
            .split(',')
            .filter(i => !!i)
            // .map(i => ({ id: i, text: i }))
            ;
    }

    handleChange(tags) {
        const { onChange, disabled } = this.props;
        if (disabled) return;
        this.setState({ tags });
        if (isFunction(onChange)) {
            const value = tags.join();
            onChange({ value, tags });
        }
    }

    handleInputKeyDown(e) {
        const value = e.target.value;
        const { input } = this.state;
        const tags = this.getTags();
        if (e.keyCode === 188) e.preventDefault();
        if (value && [13, 32, 188].indexOf(e.keyCode) !== -1) {
            this.setState({ input: '' });
            this.handleChange([...tags, value]);
        } else if (e.keyCode === 8 && tags.length && !input.length) {
            this.handleChange(tags.slice(0, tags.length - 1));
        }
        if (isFunction(this.props.onKeyDown)) {
            this.props.onKeyDown(e);
        }
    }
    handleRemoveItem(index, e) {
        e.preventDefault();
        const tags = this.getTags();
        this.handleChange(tags.filter((item, i) => i !== index));
    }

    render() {
        const { input } = this.state;
        const {
            required, disabled, readOnly,
            placeholder,
            inline
        } = this.props;
        const tags = this.getTags();
        const canEdit = !inline && !readOnly && !disabled;
        return (
            <div
                className={`tag-input ${inline ? 'inline' : ''}`}
                onClick={() => canEdit && this.refs.input.focus()}
                style={{ position: 'relative' }}
            >
                <ul
                    className={!inline ? 'form-control' : ''}
                    style={{
                        ...styles.list,
                        cursor: canEdit ? 'text' : 'unset',
                        minHeight: !inline ? undefined : 'unset',
                        marginBottom: !inline ? undefined : 'unset',
                    }}
                >
                    {canEdit && <i
                        key="icon" className="fa fa-tag text-muted"
                        style={{
                            margin: canEdit ? '5px 5px 5px 0' : undefined,
                            top: '15%',
                            alignSelf: 'center',
                        }}
                    />}
                    {tags.map((item, i) =>
                        <li
                            key={i}
                            style={{ ...styles.item, cursor: canEdit ? 'pointer' : 'unset' }}
                        >
                            {item}
                            {canEdit &&
                                <span
                                    style={styles.itemDel}
                                    onClick={(e) => canEdit && this.handleRemoveItem(i, e)}
                                >&times;</span>
                            }
                        </li>
                    )}
                    {canEdit &&
                        <div
                            className="inline"
                            style={{
                                position: 'relative',
                                maxWidth: '100%',
                                padding: '6px 0',
                                minHeight: 16,
                            }}
                        >
                            <input
                                ref="input"
                                value={input}
                                onChange={(e) => this.setState({
                                    input: replaceSpaces(e.target.value)
                                })}
                                onBlur={(e) => {
                                    const event = e;
                                    event.keyCode = 13;
                                    this.handleInputKeyDown(event);
                                }}
                                onKeyDown={(e) => this.handleInputKeyDown(e)}
                                required={required && !tags.length}
                                {...{ disabled, placeholder }}
                                maxLength={60}
                                style={{
                                    ...styles.control,
                                    position: 'absolute',
                                    width: 'calc(100% + 5px)',
                                    padding: '0',
                                    marginLeft: 2,
                                }}
                            />
                            <span
                                style={{ whiteSpace: 'nowrap', visibility: 'hidden', }}
                            >{input || placeholder || 'AnyText'}</span>
                        </div>
                    }
                </ul>
            </div>
        );
    }
}
const styles = {
    list: {
        padding: '0px 6px',
        height: '100%',
        minHeight: '34px',
        wordBreak: 'break-word',
    },
    item: {
        display: 'inline-block',
        margin: '2px 2px',
        padding: '2px 3px',
        border: '1px solid gray',
        borderRadius: '25%',
    },
    itemDel: {
        cursor: 'pointer',
        marginLeft: 2,
    },
    control: {
        outline: 'none',
        border: 'none',
        padding: '6px 5px',
        background: 'transparent',
    },
};
