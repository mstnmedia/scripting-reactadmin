
import React, { Component } from 'react';

/**
 * @augments {Component<{ className: string,  }, {a: number}>}
 */
export class NotificationShower extends Component {
    state = {
        messages: [],
        messagesObj: {},
    }
    componentWillMount() {
        window.message = this;
    }

    getMessages() {
        const list = this.state.messages.filter(i => !i.hidden);
        return list;
    }

    closeAll() {
        this.getMessages()
            .reverse()
            .map(message => this.closeMessage(message.id));
    }
    closeMessage(id) {
        $(`#${id}`)
            .slideUp()
            .animate({ 'margin-bottom': 0, }, 500, 'swing', () => {
                this.state.messagesObj[id].hidden = true;
                this.update();
            });
    }

    addRow(className, text, timeout, rows) {
        const message = {
            id: (new Date() * 1),
            className,
            text,
            rows,
            collapsed: false,
            hidden: false,
        };
        this.state.messages.push(message);
        this.state.messagesObj[message.id] = message;
        this.update();
        if (timeout > 0) {
            setTimeout(() => this.closeMessage(message.id), timeout);
        }
    }
    error(text, timeout, rows) { this.addRow('danger', text, timeout, rows); }
    info(text, timeout, rows) { this.addRow('info', text, timeout, rows); }
    success(text, timeout, rows) {
        let auxTO = timeout;
        if (!auxTO) {
            auxTO = 3000;
        }
        this.addRow('success', text, auxTO, rows);
    }
    warn(text, timeout, rows) { this.addRow('warning', text, timeout, rows); }

    update() {
        this.setState({ updateProp: new Date(), });
    }

    render() {
        const messages = this.getMessages();
        return (
            <div className="messageContainer">
                {messages.length ?
                    <div
                        onClick={() => this.closeAll()}
                        style={{ position: 'absolute', top: -18, right: 10 }}
                    >
                        <i
                            className="fa fa-angle-double-right text-muted"
                            style={{ fontSize: '150%' }}
                        />
                    </div>
                    : null
                }
                {messages.map((item) => {
                    const message = item;
                    return (
                        <div
                            id={message.id} key={message.id}
                            className={`alert alert-${message.className}`}
                            style={{ display: 'flex', padding: 10, }}
                        >
                            <div style={{ flex: 1, margin: 0 }}>
                                <span
                                    htmlFor="text"
                                    className="block"
                                    style={{ fontWeight: 'bold', }}
                                >{message.text}</span>
                                {!message.collapsed && message.rows
                                    && message.rows.map((row, i) => (
                                        <span
                                            key={i} htmlFor="row" className="block" 
                                            style={{ whiteSpace: 'pre-wrap', wordBreak: 'break-word', }}
                                        >{row}</span>
                                    ))
                                }
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    margin: '-10px -5px -10px 5px',
                                    justifyContent: 'space-between',
                                }}
                            >
                                <button
                                    type="button"
                                    className="close"
                                    onClick={() => this.closeMessage(message.id)}
                                >×</button>
                                {message.rows && message.rows.length && <button
                                    type="button"
                                    className="close"
                                    onClick={() => {
                                        message.collapsed = !message.collapsed;
                                        this.update();
                                    }}
                                >
                                    {message.collapsed ? '▾' : '▴'}
                                </button>}
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}
