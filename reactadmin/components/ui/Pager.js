import React, { PureComponent } from 'react';
import i18n from '../../../i18n';

/**
 * @augments {Component<{ currentPage: number, itemsPerPage: number, totalItems: number, },
 * { paginationRange: number, onPageChange: func, classes: string }>}
 */
export class Pager extends PureComponent {
	static getPageItems({ items, pageSize, pageNumber, }) {
		const pageNo = pageNumber - 1;
		const startIndex = pageNo * pageSize;
		const endIndex = (startIndex + pageSize);
		return (items || []).slice(startIndex, endIndex);
	}
	static getMaxPage(totalItems, itemsPerPage) {
		return Math.ceil(totalItems / itemsPerPage);
	}

	getPaginationRange(paginationRange) {
		return Math.max(paginationRange || 0, 11);
	}
	getPagesArray(currentPage, itemsPerPage, totalItems, _paginationRange) {
		const paginationRange = this.getPaginationRange(_paginationRange);
		const pages = [];
		const totalPages = Math.ceil(totalItems / itemsPerPage);
		const halfWay = Math.ceil(paginationRange / 2);
		let position;

		if (currentPage <= halfWay) {
			position = 'start';
		} else if ((totalPages - halfWay) < currentPage) {
			position = 'end';
		} else {
			position = 'middle';
		}

		const ellipsesNeeded = paginationRange < totalPages;
		let i = 1;
		while (i <= totalPages && i <= paginationRange) {
			const pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);

			const openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
			const closingEllipsesNeeded = (
				i === paginationRange - 1 &&
				(position === 'middle' || position === 'start')
			);
			if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
				pages.push('...');
			} else {
				pages.push(pageNumber);
			}
			i++;
		}
		return pages;
	}

	canGoBack() {
		const { currentPage, } = this.props;
		return this.canNavigateTo(currentPage - 1);
	}
	canGoFore() {
		const { currentPage, } = this.props;
		return this.canNavigateTo(currentPage + 1);
	}
	canNavigateTo(pageNumber) {
		const { currentPage, itemsPerPage, totalItems, } = this.props;
		if (pageNumber !== currentPage &&
			!isNaN(pageNumber * 1) &&
			pageNumber >= 1 &&
			pageNumber <= Math.ceil(totalItems / itemsPerPage)) {
			return true;
		}
		return false;
	}

	previousClick(event) {
		event.preventDefault();
		const { currentPage, } = this.props;
		this.pageChange((currentPage - 1), event);
	}
	pageChange(newPage, event) {
		event.preventDefault();
		const { currentPage, itemsPerPage, totalItems, onPageChange, } = this.props;
		if (onPageChange && this.canNavigateTo(newPage)) {
			onPageChange({
				event,
				currentPage,
				itemsPerPage,
				totalItems,
				newPage,
			});
		}
	}
	nextClick(event) {
		event.preventDefault();
		const { currentPage, } = this.props;
		this.pageChange((currentPage + 1), event);
	}

	renderPages() {
		const { currentPage, itemsPerPage, totalItems, paginationRange } = this.props;
		const getClassName = (page) => {
			let className = 'paginate_button page';
			if (page === currentPage) {
				className += ' active';
			} else if (page === '...') {
				className += ' disabled';
			}
			return className;
		};

		const pages = this.getPagesArray(currentPage, itemsPerPage, totalItems, paginationRange);
		return pages.map((page, i) => (
			<li
				key={`page-key-${page}-${i}`}
				className={getClassName(page)}
				aria-controls="DataTables_Table_1"
				tabIndex="0"
			>
				<a
					href={`/page/${page}`}
					onClick={(e) => this.pageChange(page, e)}
				>{page}</a>
			</li>
		));
	}
	render() {
		const { currentPage, itemsPerPage, totalItems, classes, } = this.props;
		const totalText = totalItems ?
			`${currentPage ? ` ${i18n.t('of')} ` : ''}${totalItems} ${i18n.t('records')}`
			: null;
		let toValue = currentPage * itemsPerPage;
		if (totalItems) {
			toValue = Math.min(toValue, totalItems);
		}
		const pageText = currentPage ?
			`${i18n.t('Showing')} ${((currentPage - 1) * itemsPerPage) + 1} `
			+ `${i18n.t('to')} ${toValue}`
			: null;

		return (
			<div className={`tbl-footer ${classes}`}>
				<div>
					<div className="text-right m-t-lg">
						{pageText}
						{totalText}
					</div>
				</div>
				<div>
					<div
						id="DataTables_Table_1_paginate"
						className="dataTables_paginate paging_simple_numbers text-right"
					>
						<ul className="pagination" style={{ margin: 5 }}>
							<li
								id="DataTables_Table_1_previous"
								className={`paginate_button previous ${!this.canGoBack() ? 'disabled' : ''}`}
								aria-controls="DataTables_Table_1"
								tabIndex="0"
							>
								<a
									href="/page/previous"
									onClick={(e) => this.previousClick(e)}
								>{i18n.t('Previous')}</a>
							</li>

							{this.renderPages()}

							<li
								id="DataTables_Table_1_next"
								className={`paginate_button next ${!this.canGoFore() ? 'disabled' : ''}`}
								aria-controls="DataTables_Table_1"
								tabIndex="0"
							>
								<a
									href="/page/next"
									onClick={(e) => this.nextClick(e)}
								>{i18n.t('Next')}</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}
const calculatePageNumber = (i, currentPage, paginationRange, totalPages) => {
	const halfWay = Math.ceil(paginationRange / 2);
	if (i === paginationRange) {
		return totalPages;
	} else if (i === 1) {
		return i;
	} else if (paginationRange < totalPages) {
		if (totalPages - halfWay < currentPage) {
			return (totalPages - paginationRange) + i;
		} else if (halfWay < currentPage) {
			return (currentPage - halfWay) + i;
		}
		return i;
	}
	return i;
};

export default Pager;
