import React, { Component } from 'react';
import { EditorState, } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { convertToHTML, } from 'draft-convert';
import { stateFromHTML } from 'draft-js-import-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import { isFunction } from '../../Utils';

export default class ReactDraftWysiwyg extends Component {
    state = {
        html: '',
        editorState: EditorState.createEmpty(),
    }

    render() {
        const editorState = EditorState.createWithContent(stateFromHTML(this.props.value));
        return (
            <div>
                <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={(newEditorState) => {
                        const { onFieldChange } = this.props;
                        const html = convertToHTML(newEditorState.getCurrentContent());
                        if (isFunction(onFieldChange)) {
                            onFieldChange(html);
                        }
                        this.setState({ html, editorState: newEditorState, });
                    }}
                />
            </div>
        );
    }
}
