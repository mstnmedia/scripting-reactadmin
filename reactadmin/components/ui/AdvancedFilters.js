
import React, { Component } from 'react';
import { DropDown, Fa } from '.';
import i18n from '../../../i18n';
import { sortFunction, isFunction } from '../../Utils';
import { Input } from './Input';
import {
    FieldErrorStyles,
    NumericBooleanOptions
} from '../../../store/enums';
import { Button } from './Button';
import { TagInput } from './TagInput';
import RemoteAutocomplete from '../../../scripting/RemoteAutocomplete';

/**
 * @augments {Component<{ fields: {}, where: [], whereObj: {} }, {arraysf: number}>}
 */
export class AdvancedFilters extends Component {
    state = {
        separator: 'and',
        updaterProp: new Date(),
    };
    componentWillMount() {
        // window.Scripting_AdvancedFilters = this;
    }
    getFields() {
        return this.props.fields || {};
    }
    getField(name) {
        return this.getFields()[name] || {};
    }
    getWheres() {
        return this.props.where || [];
    }
    getWheresObj() {
        return this.props.whereObj || {};
    }
    getFieldOptions() {
        const fields = this.getFields();
        return Object.keys(fields)
            .map(key => {
                const { label } = this.getField(key);
                return { value: key, label };
            })
            .sort(sortFunction('label'));
    }
    getCriteriaOptions(where) {
        const equals = { value: '=', label: i18n.t('EqualsTo') };
        const notEquals = { value: '!=', label: i18n.t('NotEqualsTo') };
        const contains = { value: 'like', label: i18n.t('Contains') };
        const fieldType = this.getField(where.name).type;
        switch (fieldType) {
            case 'number':
            case 'date':
            case 'datetime':
                return [equals, notEquals, '>', '<', '>=', '<='];
            case 'text':
            case 'clob':
                return [equals, notEquals, contains];
            case 'boolean':
            case 'dropdown':
            case 'autocomplete':
                return [equals, notEquals];
            default:
                return [equals, notEquals, contains, '>', '<', '>=', '<='];
        }
    }

    handleAddWhere() {
        const { separator } = this.state;
        const wheres = this.getWheres();
        wheres.push({ criteria: '=', separator, value: '', });
        this.refresh();
    }
    handleFieldChange(where, e) {
        const value = e.target.value;
        const { type, name } = this.getField(value);
        // const whereType = ['date', 'datetime', 'clob']
        //     .indexOf(type) > -1 ? type : undefined;
        if (where.type !== type || type === 'autocomplete') {
            this.handleWhereChange(where, 'value', undefined);
        }
        this.handleWhereChange(where, 'group', undefined);
        this.handleWhereChange(where, 'name', name || value);
        this.handleWhereChange(where, 'type', type);
        if (this.getCriteriaOptions(where).indexOf(where.criteria) === -1) {
            this.handleWhereChange(where, 'criteria', '=');
        }
    }
    handleSeparatorChange(e) {
        const wheres = this.getWheres();
        wheres.forEach(w => {
            const where = w;
            where.separator = e.target.value;
        });
        this.setState({ separator: e.target.value });
    }
    handleWhereChange(where, prop, value) {
        const aux = where;
        aux[prop] = value;
        this.refresh();
    }
    handleClearWheres() {
        const wheres = this.getWheres();
        while (wheres.length) {
            wheres.pop();
        }
        this.refresh();
    }
    handleRemoveWhere(where, idx) {
        const wheres = this.getWheres();
        wheres.splice(idx, 1);
        this.refresh();
    }

    refresh() {
        this.setState({ updaterProp: new Date() });
    }

    renderField(where) {
        const options = this.getFieldOptions();
        return (
            <DropDown
                emptyOption="SelectDisabled"
                style={!where.name ? FieldErrorStyles : undefined}
                items={options}
                value={where.name}
                onChange={(e) => this.handleFieldChange(where, e)}
            />
        );
    }
    renderCriteria(where) {
        const options = this.getCriteriaOptions(where);
        return (
            <DropDown
                items={options}
                value={where.criteria}
                onChange={(e) => this.handleWhereChange(where, 'criteria', e.target.value)}
            />
        );
    }
    renderValue(where) {
        const field = this.getField(where.name);
        const hasError = !where.value
            && ['text', 'textarea', 'clob', 'email', 'tags'].indexOf(field.type) === -1;
        const props = {
            className: 'form-control',
            style: hasError ? FieldErrorStyles : undefined,
            type: field.type,
            name: field.name,
            placeholder: field.label,
            value: where.value,
            onFieldChange: (e) => this.handleWhereChange(where, 'value', e.target.value),
            onKeyDown: field.onKeyDown,
            // required: true,
            // disabled: this.isFieldDisabled(),
        };

        switch (field.type) { /* eslint-disable no-fallthrough */
            case 'datetime':
            case 'datetime-local':
            case 'date':
            case 'time':
            case 'number':
            case 'text':
            case 'clob':
            case 'email':
            case 'hidden':
                if (field.type === 'datetime') {
                    props.type = 'datetime-local';
                } else if (field.type === 'clob') {
                    props.type = 'text';
                }
                return (
                    <Input {...props} />
                );
            case 'boolean':
            case 'dropdown':
                props.onChange = props.onFieldChange;
                delete props.onFieldChange;
                if (field.type === 'boolean') {
                    props.items = NumericBooleanOptions();
                } else {
                    props.items = field.items || [];
                }
                return (
                    <DropDown
                        emptyOption="SelectDisabled"
                        {...props}
                    />
                );
            case 'tags':
                return (
                    <TagInput
                        value={props.value}
                        placeholder={props.placeholder}
                        onChange={({ tags, value }) => {
                            this.handleWhereChange(where, 'value', tags.length ? value : '');
                            this.handleWhereChange(where, 'group', tags.map(i => ({
                                name: 'tags',
                                criteria: 'like',
                                value: i
                            })));
                        }}
                        onKeyDown={props.onKeyDown}
                    />
                );
            case 'autocomplete':
                return (
                    <RemoteAutocomplete
                        inputStyle={props.style}
                        url={field.url}
                        fetchProps={field.fetchProps}
                        // value={this.filterText('category_name') || ''}
                        getItemValue={field.getItemValue}
                        onFieldChange={(/*e , text */) => {
                            // this.filterTextChange('category_name', text);
                            this.handleWhereChange(where, 'value', undefined);
                        }}
                        onSelect={(text, item) => {
                            this.handleWhereChange(where, 'value', item[field.asignProp || 'id']);
                        }}
                        onKeyDown={props.onKeyDown}
                    />
                );
            default:
                return <Input disabled />;/* (
                    <div>{i18n.t('FieldValueIsMissing')}</div>
                ); */
        }/* eslint-enable no-fallthrough */
    }
    renderRow(where, idx) {
        const field = this.getField(where.name);
        const valueControl = this.renderValue(where);
        let valueCell = null;
        if (isFunction(field.renderValueCell)) {
            valueCell = field.renderValueCell({ field, valueControl });
        } else {
            valueCell = <td /* style={{ position: 'relative' }} */>{valueControl}</td>;
        }
        return (
            <tr key={idx}>
                <td>{this.renderField(where)}</td>
                <td>{this.renderCriteria(where)}</td>
                {valueCell}
                <td className="text-center">
                    <Button
                        type="button"
                        className="btn-danger btn-xs inline-block"
                        style={{ marginLeft: 5, }}
                        label={<Fa icon="remove" />}
                        onClick={() => this.handleRemoveWhere(where, idx)}
                    />
                </td>
            </tr>
        );
    }
    render() {
        const { separator } = this.state;
        const wheres = this.getWheres();
        return (
            <div className="-table-responsive">
                <div className="text-center">
                    <label className="m-r-sm" htmlFor="filter">
                        <input
                            type="radio"
                            value="and"
                            checked={separator === 'and'}
                            onChange={(e) => this.handleSeparatorChange(e)}
                        />
                        {i18n.t('All')}
                    </label>
                    <label className="m-r-sm" htmlFor="filter">
                        <input
                            type="radio"
                            value="or"
                            checked={separator === 'or'}
                            onChange={(e) => this.handleSeparatorChange(e)}
                        />
                        {i18n.t('Anyone')}
                    </label>
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>{i18n.t('Field')}</th>
                            <th>{i18n.t('CriteriaOperator')}</th>
                            <th>{i18n.t('Value')}</th>
                            {wheres.length ? <th className="text-center" style={{ maxWidth: 60 }}>
                                <Button
                                    type="button"
                                    className="btn-danger btn-xs m-"
                                    style={{ marginLeft: 5, }}
                                    label={[
                                        <Fa key="1" className="m-r-xs" icon="remove" />,
                                        i18n.t('DeleteAll')
                                    ]}
                                    onClick={() => this.handleClearWheres()}
                                />
                            </th> : null}
                        </tr>
                    </thead>
                    <tbody>
                        {wheres.map((w, idx) => this.renderRow(w, idx))}
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan="3" className="text-center">
                                <button
                                    type="button" className="btn btn-info m-t-sm"
                                    onClick={(e) => this.handleAddWhere(e)}
                                >{i18n.t('Add')}</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}
