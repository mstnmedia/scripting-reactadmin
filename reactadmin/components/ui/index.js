/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModules UI
 */

import React from 'react';
import i18n from '../../../i18n';
import { isFunction } from '../../Utils';
import { Input } from './Input';

export * from './ActionLink';
export * from './AdvancedFilters';
export * from './AnimatedSpinner';
export * from './BigAnimatedSpinner';
export * from './Button';
export * from './ButtonGroup';
export * from './DropDownButton';
export * from './Input';
export * from './NotificationShower';
export * from './NumericInput';
export * from './DateTimeInput';
export * from './Layout';
export * from './Pager';
export * from './TagInput';
export * from './TextArea';

export const Alert = ({ color, persistant, title, onClose, className, children }) => (
    <div className={`alert ${className} ${color}`}>
        {!persistant ?
            <button
                type="button"
                onClick={onClose}
                className="close"
                data-dismiss="alert"
            >×</button>
            : null
        }
        {title ? <h4>{title}</h4> : null}
        {children}
    </div>
);

export const DropDown = ({
    className, style,
    items, emptyOption,
    onChange, value, disabled,
    editable,
    ...rest,
}) => {
    let emptyControl = null;
    const emptyType = typeof (emptyOption);
    if (emptyType === 'function') {
        emptyControl = emptyControl();
    } else if (emptyOption === true) {
        emptyControl = <option value="" />;
    } else if (emptyType === 'string') {
        const emptyLabel = emptyOption;
        if (emptyLabel === 'SelectDisabled') {
            emptyControl = <option value="" disabled={!!value}>{i18n.t('Select')}</option>;
        } else {
            emptyControl = <option value="">{emptyLabel}</option>;
        }
    } else if (emptyType === 'number') {
        const emptyLabel = emptyOption;
        emptyControl = <option value="">{emptyLabel}</option>;
    } else {
        emptyControl = emptyOption || null;
    }
    const mergedStyles = {
        margin: '2px 2px 2px 0',
        ...style,
    };
    const props = {
        className: className || 'form-control',
        style: mergedStyles,
        onChange,
        value: value || '',
        disabled,
        ...rest,
    };
    const options = (items || []).map((item) => {
        const optionValue = item.value || item.id || item.key || item;
        const optionLabel = item.label || item.name || item;
        const optionDisabled = isFunction(item.disabled) ? item.disabled() : item.disabled;
        return (item.hidden ? null :
            <option
                key={optionValue}
                value={optionValue}
                disabled={optionDisabled}
            >{optionLabel}</option>
        );
    });
    if (editable) {
        const list = new Date().toISOString();
        return (
            <figure>
                <input list={list} {...props} />
                <datalist id={list}>{options}</datalist>
            </figure>
        );
    }
    return (
        <select {...props}>
            {emptyControl}
            {options}
        </select >
    );
};

export const InputRadio = ({
    name, items, onFieldChange, value = '', disabled,
    divProps, spanProps, inputProps, labelProps, onKeyDown,
}) => {
    const options = (items || []).map((item, i) => {
        const optionValue = item.value || item.id || item.key || item;
        const optionLabel = item.label || item.name || item;
        const optionDisabled = disabled ||
            (isFunction(item.disabled) ? item.disabled() : item.disabled);
        return (item.hidden ? null :
            <span key={i} {...spanProps}>
                <Input
                    {...{ name, onKeyDown, ...inputProps, }}
                    type="radio"
                    value={optionValue}
                    onFieldChange={(e) => onFieldChange(e)}
                    checked={optionValue === value}
                    disabled={optionDisabled}
                />
                <label htmlFor={optionValue} {...labelProps}>{optionLabel}</label>
            </span>
        );
    });

    return (
        <div {...divProps}>
            {options}
        </div>
    );
};

export const FormGroup = ({ isValid, className, ...rest }) => {
    let classes = className || 'form-group';
    if (isValid !== undefined && !isValid) classes += ' has-error';
    return (
        <div className={classes} {...rest} />
    );
};
export const Fa = ({ icon = '', size, color, style, className, onClick, ...rest }) => {
    const cursor = isFunction(onClick) ? 'pointer' : undefined;
    return (
        <i
            className={`fa${icon ? ` fa-${icon}` : ''}${className ? ` ${className}` : ''}`}
            style={{ fontSize: size, color, cursor, ...style }}
            onClick={onClick}
            {...rest}
        />
    );
};

export const I = ({ icon, size, color, classes, style }) => {
    const lClasses = `material-icons ${classes}`;
    const lStyle = Object.assign({},
        {
            fontSize: size || 24,
            verticalAlign: 'middle',
            color: color || '#757575'
        },
        style);
    return (
        <i className={lClasses} style={lStyle}>{icon}</i>
    );
};

export const IStack = ({ icon, size, color, bg, width, height, classes }) => {
    const lStyle = {
        backgroundColor: bg || '#757575',
        width,
        height
    };

    return (
        <div className={`iconStack ${classes}`}>
            <span style={lStyle}>
                <I icon={icon} size={size} color={color} style={{ lineHeight: `${height}px` }} />
            </span>
        </div>
    );
};

export const Label = ({ color, classes, title }) => (
    <span className={`label ${color} ${classes}`}>
        {title}
    </span>
);
export const H4 = ({ children, ...rest }) => (
    <h4 {...rest}>{children}</h4>
);

export const PageHead = ({ title, subtitle, children, classes }) => (
    <div className={`page-head ${classes}`}>
        <h3 dangerouslySetInnerHTML={{ __html: title }} />
        <span className="sub-title">{subtitle}</span>
        {children}
    </div>
);

export const Panel = ({
    title, children, scrollHeight,
    className, height, containerStyle,
    contentStyle, marginBottomClass,
}) => { // eslint-disable-line max-len
    let baseStyle = {};
    if (height) {
        baseStyle = { height, overflow: 'hidden' };
    }
    const mBC = marginBottomClass || 'm-b-md';
    return (
        <section
            className={`panel panel-default ${mBC}`}
            style={{
                ...(scrollHeight ? Object.assign({}, baseStyle, { paddingBottom: 30 }) : baseStyle),
                ...containerStyle,
            }}
        >
            {title ?
                <header className="panel-heading -text-uc" style={{ position: 'relative', }}>
                    {title}
                    <hr style={{ borderTop: '1px solid red' }} />
                </header>
                : null
            }
            <section
                className={`panel-body ${className}`}
                style={{
                    ...(scrollHeight ? { height: scrollHeight, overflowY: 'scroll' } : {}),
                    ...contentStyle,
                }}
            >
                {children}
            </section>
        </section>
    );
};
export const PanelCollapsible = ({
    className, title,
    children, footer,
    height, scrollHeight,
    containerStyle, contentStyle,
    startOpened,
}) => {
    const keyPanel = Math.floor(Math.random() * 100000);
    return (
        <div
            className="panel panel-default m-b-md"
            style={{
                ...containerStyle,
                ...(height ? { height, overflow: 'hidden' } : {}),
                paddingBottom: scrollHeight ? 30 : undefined,
            }}
        >
            <div
                className="panel-heading -text-uc"
                data-toggle="collapse"
                data-target={`#collapsable-${keyPanel}`}
            >
                <div className="panel-title-">
                    <i className="fa fa-plus m-r-sm" />
                    {title}
                    <hr style={{ marginTop: 5, marginBottom: 5 }} />
                </div>
            </div>
            <div
                id={`collapsable-${keyPanel}`}
                className={`panel-collapse ${startOpened ? 'in' : 'collapse'}`}
            >
                <div
                    className={`panel-body ${className}`}
                    style={{
                        ...contentStyle,
                        height: startOpened ? 'auto' : undefined,
                        ...(scrollHeight ? { height: scrollHeight, overflowY: 'scroll' } : {})
                    }}
                >
                    {children}
                </div>
            </div>
            {footer ?
                <div className="panel-footer">
                    {footer}
                </div>
                : null
            }
        </div>

    );
};

export const SearchBox = ({ val, onChange }) => (
    <form className="search-content" action="#" method="post">
        <div className={'iconic-input'}>
            <i className="fa fa-search" />
            <input
                type="text"
                className="form-control"
                name="keyword"
                onChange={onChange}
                value={val}
                placeholder={`${i18n.t('ToSearch')}...`}
            />
        </div>
    </form>
);

export const Table = ({ data, classes }) => (
    <table className={`table ${classes}`}>
        <thead>
            <tr>
                {Object.keys(data[0]).map((key, i) => <th key={`row-${key}-${i}`}>{key}</th>)}
            </tr>
        </thead>
        <tbody>
            {data.map((row, j) => <tr key={`r-${j}`}>
                {Object.keys(row).map((column, k) => (
                    <td key={`col-${column}-${k}`}>{row[column]}</td>
                ))}
            </tr>)}
        </tbody>
    </table>
);
export const Tooltip = ({ position = 'down', tag, children, system, ...rest, }) => {
    const title = tag ? i18n.t(tag) : undefined;
    const props = system
        ? { title }
        : { 'data-balloon': title || undefined, 'data-balloon-pos': position };
    return (
        <div
            className="inline"
            {...props}
            {...rest}
        >
            {children}
        </div>
    );
};

export default {
    Alert,
    DropDown,
    Fa,
    I,
    IStack,
    Label,
    PageHead,
    Panel,
    SearchBox,
    Table,
    Tooltip,
};
