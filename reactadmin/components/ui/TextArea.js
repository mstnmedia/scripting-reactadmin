import React, { Component } from 'react';

class TextArea extends Component {
	constructor(props) {
		super(props);
		this.state = {
			valid: true
		};
	}

	handleChange(e) {
		// validate based on format

		this.validate(e);
		if (this.props.onFieldChange) {
			this.props.onFieldChange(e, this.state.valid);
		}
	}

	isValid() {
		return this.state.valid;
	}

	validate(e) {
		const val = e.currentTarget.value;
		if (this.props.required) {
			if (isEmpty(val)) {
				this.setState({ valid: false });
				return;
			}
			this.setState({ valid: this.validateFormat(this.props.format, val) });
		} else {
			this.setState({ valid: this.validateFormat(this.props.format, val) });
		}
	}

	validateLength(val, minLength, maxLength) {
		return true;
	}

	validateFormat(format, val) {
		switch (format) {
			case 'email':
				return validations.isEmail('', val);
			default:
				return true;
		}
	}

	render() {
		const {
			onFieldChange, // eslint-disable-line no-unused-vars
			style,
			value,
			...rest
		} = this.props;

		return (
			<textarea
				className="form-control"
				onChange={(e) => this.handleChange(e)}
				style={{ minHeight: 34, /* height: '100%', */ resize: 'vertical', ...style, }}
				value={value || ''}
				{...rest}
			/>
		);
	}
}

// validations

const isExisty = (value) => (
	value !== null && value !== undefined
);

const isEmpty = (value) => (
	value === ''
);

const validations = {
	isDefaultRequiredValue: (values, value) => (
		value === undefined || value === ''
	),
	isExisty: (values, value) => (
		isExisty(value)
	),
	matchRegexp: (values, value, regexp) => (
		!isExisty(value) || isEmpty(value) || regexp.test(value)
	),
	isUndefined: (values, value) => (
		value === undefined
	),
	isEmptyString: (values, value) => (
		isEmpty(value)
	),
	isEmail: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			// eslint-disable-next-line max-len
			/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
		)
	),
	isUrl: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			// eslint-disable-next-line max-len
			/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
		)
	),
	isTrue: (values, value) => (
		value === true
	),
	isFalse: (values, value) => (
		value === false
	),
	isNumeric: (values, value) => {
		if (typeof value === 'number') {
			return true;
		}
		return validations.matchRegexp(values, value, /^[-+]?(?:\d*[.])?\d+$/);
	},
	isAlpha: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z]+$/i)
	),
	isAlphanumeric: (values, value) => (
		validations.matchRegexp(values, value, /^[0-9A-Z]+$/i)
	),
	isInt: (values, value) => (
		validations.matchRegexp(values, value, /^(?:[-+]?(?:0|[1-9]\d*))$/)
	),
	isFloat: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			/^(?:[-+]?(?:\d+))?(?:\.\d*)?(?:[eE][\+\-]?(?:\d+))?$/
		)
	),
	isWords: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z\s]+$/i)
	),
	isSpecialWords: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z\s\u00C0-\u017F]+$/i)
	),
	isLength: (values, value, length) => (
		!isExisty(value) || isEmpty(value) || value.length === length
	),
	equals: (values, value, eql) => (
		!isExisty(value) || isEmpty(value) || value === eql
	),
	equalsField: (values, value, field) => (
		value === values[field]
	),
	maxLength: (values, value, length) => (
		!isExisty(value) || value.length <= length
	),
	minLength: (values, value, length) => (
		!isExisty(value) || isEmpty(value) || value.length >= length
	)
};


export { TextArea };
