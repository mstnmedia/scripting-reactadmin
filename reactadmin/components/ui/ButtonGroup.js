import React, { Component } from 'react';

export class ButtonGroup extends Component {
	state = {
		active: undefined,
	}
	componentWillMount() {
		this.setState({ active: this.props.items.active });
	}
	componentWillReceiveProps(props) {
		this.setState({ active: props.items.active });
	}

	handleClick(e) {
		e.preventDefault();
		const { items } = this.props;
		const { values } = items;
		if (e.target.value !== this.state.active) {
			this.setState({ active: e.target.value });
			this.props.onChange(values[e.target.value]);
		}
	}

	render() {
		const classNames = {};
		const {
			className,
			items: { values },
			disabled,
		} = this.props;
		const { active } = this.state;
		Object.keys(values).map((name) => {
			classNames[name] = 'btn btn-default';
			return true;
		});
		classNames[active] += ' active';

		let containerClass = 'btn-group';
		if (className) {
			containerClass += ` ${className}`;
		}
		return (
			<div id="toggle" className={containerClass} aria-label="Select Item">
				{Object.keys(values).map((name) => (
					<button
						key={name}
						className={classNames[name]} type="button" value={name}
						onClick={(e) => this.handleClick(e)}
						disabled={active !== name && disabled}
					>
						{name}
					</button>
				))}
			</div>
		);
	}
}
export default ButtonGroup;
