import React, { Component } from 'react';
import { Input } from './Input';
import {
    isFunction,
    isNumber,
    replaceNonDigits,
    replaceNonInteger
} from '../../Utils';

/**
 * @augments {Component<{ className: string, natural: boolean, integer: boolean,  }, {a: number}>}
 */
export class NumericInput extends Component {
    onFieldChange(event, valid) {
        const e = event;
        const {
            min, max,
            integer,
            natural,
            onFieldChange
        } = this.props;
        let value = e.target.value;
        if (natural) {
            value = replaceNonDigits(value);
            value = parseInt(value || 0, 10);
            if (value < 0) {
                value *= -1;
            }
        } else if (integer) {
            value = replaceNonInteger(value);
            value = parseInt(value, 10);
        }
        if (isNumber(min) && value < min) {
            value = min;
        } else if (isNumber(max) && value > max) {
            value = max;
        }
        e.target.value = value;
        if (isFunction(onFieldChange)) {
            onFieldChange(e, valid);
        }
    }

    render() {
        const {
            type, onFieldChange, integer, //eslint-disable-line no-unused-vars
            natural, min,
            ...rest,
        } = this.props;
        return (
            <Input
                type="number"
                onFieldChange={(e, valid) => this.onFieldChange(e, valid)}
                min={natural ? Math.max(min || 1, 1) : min}
                {...rest}
            />
        );
    }
}
