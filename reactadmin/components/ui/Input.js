/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Input
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isFunction } from '../../Utils';

/**
 * Input component that supports format validation, icons and error messages
 */

class Input extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			valid: true
		};
	}

	handleChange(event) {
		const e = event;
		const {
			onFieldChange,
			notWhiteSpaces,
		} = this.props;
		let value = (e.target.value || '').trimLeft();
		if (notWhiteSpaces) {
			value = value.replace(/\s/g, '');
		}
		e.target.value = value;
		// validate based on format
		this.validate(e);
		if (isFunction(onFieldChange)) {
			onFieldChange(e, this.state.valid);
		}
	}

	isValid() {
		return this.state.valid;
	}

	validate(e) {
		const val = e.currentTarget.value;
		let valid = true;
		if (this.props.required) {
			if (isEmpty(val)) {
				valid = false;
			} else {
				valid = this.validateFormat(this.props.format, val);
			}
		} else {
			valid = this.validateFormat(this.props.format, val);
		}
		this.setState({ valid });
	}

	validateLength(val, minLength, maxLength) {
		return true;
	}

	validateFormat(format, val) {
		switch (format) {
			case 'email':
				return validations.isEmail('', val);
			case 'number':
				return validations.isEmail('', val);
			default:
				return true;
		}
	}

	render() {
		const icon = this.props.icon ? <i className={this.props.icon} /> : null;
		const type = this.props.format || this.props.type || 'text';

		const error = this.state.valid ?
			null
			: <p className="help-block text-left">{errorMessage}</p>;

		const {
			errorMessage = 'required',
			classes,
			containerStyle,
			onChange, onFieldChange, notWhiteSpaces, // eslint-disable-line no-unused-vars
			rounded,
			clearable, clearableStyle, onClear,
			value,
			...rest
		} = this.props;
		const clearableControl = value && clearable && (
			<span
				onClick={onClear || ((e) => this.handleChange(e))}
				style={{
					position: 'absolute',
					float: 'right',
					top: '15%',
					right: 5,
					fontSize: '1.5em',
					...clearableStyle,
				}}
			>×</span>
		);
		let className = '';
		if (type !== 'checkbox') {
			className += 'form-control';
		}
		if (rounded) {
			className += ' rounded';
		}
		// if (type === 'date' && value) {
		// 	const date = new Date(value);
		// 	if (!isNaN(date.getFullYear())) {
		// 		value = date.toISOString().split('T')[0];
		// 	}
		// }
		return (
			<div className={`${classes}`} style={containerStyle}>
				<div
					className={`${this.props.icon ? 'iconic-input' : ''}`}
					style={{ position: 'relative' }}
				>
					{icon}
					<input
						ref="editor"
						type={type} autoComplete="off"
						className={className}
						value={value || ''}
						onChange={e => this.handleChange(e)}
						{...rest}
					/>
					{clearableControl || null}
					{error || null}
				</div>
			</div>
		);
	}
}

// validations

const isExisty = (value) => (
	value !== null && value !== undefined
);

const isEmpty = (value) => (
	value === ''
);

const validations = {
	isDefaultRequiredValue: (values, value) => (
		value === undefined || value === ''
	),
	isExisty: (values, value) => (
		isExisty(value)
	),
	matchRegexp: (values, value, regexp) => (
		!isExisty(value) || isEmpty(value) || regexp.test(value)
	),
	isUndefined: (values, value) => (
		value === undefined
	),
	isEmptyString: (values, value) => (
		isEmpty(value)
	),
	isEmail: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			// eslint-disable-next-line
			/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
		)
	),
	isUrl: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			// eslint-disable-next-line
			/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
		)
	),
	isTrue: (values, value) => (
		value === true
	),
	isFalse: (values, value) => (
		value === false
	),
	isNumeric: (values, value) => {
		if (typeof value === 'number') {
			return true;
		}
		return validations.matchRegexp(values, value, /^[-+]?(?:\d*[.])?\d+$/);
	},
	isAlpha: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z]+$/i)
	),
	isAlphanumeric: (values, value) => (
		validations.matchRegexp(values, value, /^[0-9A-Z]+$/i)
	),
	isInt: (values, value) => (
		validations.matchRegexp(values, value, /^(?:[-+]?(?:0|[1-9]\d*))$/)
	),
	isFloat: (values, value) => (
		validations.matchRegexp(
			values,
			value,
			// eslint-disable-next-line
			/^(?:[-+]?(?:\d+))?(?:\.\d*)?(?:[eE][\+\-]?(?:\d+))?$/
		)
	),
	isWords: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z\s]+$/i)
	),
	isSpecialWords: (values, value) => (
		validations.matchRegexp(values, value, /^[A-Z\s\u00C0-\u017F]+$/i)
	),
	isLength: (values, value, length) => (
		!isExisty(value) || isEmpty(value) || value.length === length
	),
	equals: (values, value, eql) => (
		!isExisty(value) || isEmpty(value) || value === eql
	),
	equalsField: (values, value, field) => (
		value === values[field]
	),
	maxLength: (values, value, length) => (
		!isExisty(value) || value.length <= length
	),
	minLength: (values, value, length) => (
		!isExisty(value) || isEmpty(value) || value.length >= length
	)
};

Input.propTypes = {
	icon: PropTypes.string,
	format: PropTypes.string,
	errorMessage: PropTypes.string,
	onFieldChange: PropTypes.func,
	required: PropTypes.bool,
	classes: PropTypes.string,
	rounded: PropTypes.bool
};

Input.defaultProps = {
	rounded: false
};

export { Input };
export default Input;
