/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Layout
 */

import React from 'react';

export const Row = ({ children, classes, mason, ...rest }) => (
	<div className={`row ${classes || ''} ${mason ? 'mason_row_3' : ''}`} {...rest}>
		{children}
	</div>
);

export const Col = ({ children, size = '12', offset, className, style }) => {
	let colSize = '';
	if (typeof size === 'object') {
		colSize = Object.keys(size).map(k => `col-${k}-${size[k]} `).join(' ');
	} else {
		colSize = `col-md-${size}`;
	}

	let offsetSize = '';
	if (typeof offset === 'object') {
		offsetSize = Object.keys(offset).map(k => `col-${k}-offset-${offset[k]} `).join(' ');
	} else {
		offsetSize = `col-md-offset-${offset}`;
	}

	return (
		<div className={`${className || ''} ${colSize} ${offsetSize}`} style={style}>
			{children}
		</div>
	);
};

export const Page = ({ children, ...rest }) => (
	// <section className="vbox" style={height ? { height } : {}}>
	// 	<section className="scrollable">
	<div className="appPage full" {...rest}>
		{children}
	</div>
	// 	</section>
	// </section>
);

export const Scroller = ({ children, height }) => (
	<div className="scrollable" style={{ height }}>
		{children}
	</div>
);
export const H3 = ({ children, ...rest }) => (
	<h3 {...rest}>
		{children}
	</h3>
);

export default {
	Page,
	Row,
	Col,
	Scroller,
};
