import React, { Component } from 'react';
import { Input } from './Input';
import {
    isFunction,
} from '../../Utils';

/**
 * @augments {Component<{ className: string,  }, {}>}
 */
export class DateTimeInput extends Component {
    onFieldChange(event, valid) {
        const e = event;
        const {
            onFieldChange,
        } = this.props;
        let value = e.target.value;
        value += ':00';
        e.target.value = value;
        if (isFunction(onFieldChange)) {
            onFieldChange(e, valid);
        }
    }

    render() {
        const {
            type, onFieldChange, //eslint-disable-line no-unused-vars
            ...rest,
        } = this.props;
        return (
            <Input
                {...rest}
                type="datetime-local"
                onFieldChange={(e, valid) => this.onFieldChange(e, valid)}
            />
        );
    }
}
