
import React, { PureComponent } from 'react';

/**
 * @augments {Component<{ show: boolean,  }, {a: number}>}
 */
export class AnimatedSpinner extends PureComponent {
    render() {
        const { show, style, ...rest } = this.props;
        return (
            <i
                className="fa fa-spinner fa-spin"
                style={{
                    marginRight: 5,
                    display: show ? undefined : 'none',
                    ...style
                }}
                {...rest}
            />
        );
    }
}
