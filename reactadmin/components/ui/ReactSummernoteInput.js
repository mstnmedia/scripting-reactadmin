import React, { Component } from 'react';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'react-summernote/lang/summernote-es-ES'; // you can import any other locale

require('codemirror/mode/htmlmixed/htmlmixed');
// require('codemirror/theme/monokai.css');

// // Import bootstrap(v3 or v4) dependencies
// import 'bootstrap/js/modal';
// import 'bootstrap/js/dropdown';
// import 'bootstrap/js/tooltip';
// import 'bootstrap/dist/css/bootstrap.css';

export default class ReactSummernoteInput extends Component {
    onChange(/* ...args */) {
        // console.log('onChange', args);
        alert('sdfsdf');
    }

    render() {
        const {
            value,
            options,
            disabled,
            ...rest,
        } = this.props;

        let codemirrorV;
        let restOptionsV;
        if (options) {
            const { codemirror, ...restOptions } = options;
            codemirrorV = codemirror;
            restOptionsV = restOptions;
        }
        return (
            //null
            <ReactSummernote
                value={value || ''}
                disabled={!!disabled}
                options={{
                    lang: 'es-ES',
                    codemirror: {
                        // theme: 'monokai',
                        mode: 'htmlmixed',
                        onChange: this.onChange,
                        lineNumbers: true,
                        // foldGutter: true,
                        // prettifyHtml: true,
                        // gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
                        ...codemirrorV,
                    },
                    // prettifyHtml: true,
                    // tabSize: 4,
                    // indentWithTabs: true,
                    // lineNumbers: true,
                    // height: 350,
                    readOnly: false,
                    disable: false,
                    minHeight: 50,
                    maxHeight: '70vh',
                    dialogsInBody: true,
                    airMode: false,
                    toolbar: [
                        ['style', ['style']],
                        ['style', ['bold', 'underline', 'strikethrough']],
                        ['font', ['superscript', 'subscript', 'clear']],
                        ['fontname', ['fontname', 'fontsize', 'color']],
                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                        ['insert', ['linkDialogShow', 'table'
                        /*, 'unlink', 'picture', 'video', 'table', 'hr' */]],
                        ['view', ['help', 'fullscreen', 'codeview']]
                    ],
                    popover: {
                        link: [],
                        // image: [],
                        // air: []
                    },
                    /*
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        [, []]
                        You can compose a toolbar with pre-shipped buttons.
                        Font Style
                            color: set foreground and background color
                            forecolor: set foreground color
                            backcolor: set background color
                            bold: toggle font weight
                            italic: toggle italic
                            underline: toggle underline
                            strikethrough: toggle strikethrough
                            superscript: toggle superscript
                            subscript: toggle subscript
                            clear: clear font style
                        Paragraph style
                            style: format selected block
                            ol: toggle ordered list
                            ul: toggle unordered list
                            paragraph: dropdown for paragraph align
                            height: set line height
                        Misc
                            fullscreen: toggle fullscreen editing mode
                            codeview: toggle wysiwyg and html editing mode
                            undo: undo
                            redo: redo
                            help: open help dialog
                    */
                    ...restOptionsV,
                    // ...options,
                }}
                onChange={this.onChange}
                {...rest}
            />
        );
    }
}
