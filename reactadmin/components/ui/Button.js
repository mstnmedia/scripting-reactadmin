
import React, { PureComponent } from 'react';

/**
 * @augments {PureComponent<{ className: string,  }, {a: number}>}
 */
export class Button extends PureComponent {
    render() {
        const {
            funcRef,
            type,
            icon,
            label,
            size,
            color,
            className,
            rounded,
            onClick,
            style,
            tooltip,
            tooltipPos,
            disabled,
            ...rest,
        } = this.props;
        const buttonIcon = icon ? <i className={`${label ? 'm-r-xs' : ''} fa ${icon}`} /> : null;
        const buttonLabel = label ? <span className="text">{label}</span> : null;
        return (
            <button
                ref={(_this) => funcRef && funcRef(_this)}
                type={type || 'button'}
                className={`btn ${className} ${rounded ? 'btn-rounded' : ''} ${color} ${size}`}
                style={{ margin: 2, ...style }}
                data-balloon={tooltip}
                data-balloon-pos={tooltipPos || 'down'}
                onClick={onClick}
                disabled={disabled}
                {...rest}
            >
                {buttonIcon}{buttonLabel}
            </button>
        );
    }
}
