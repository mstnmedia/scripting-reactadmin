
import React, { Component } from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { isFunction } from '../../Utils';

const KeyCodes = { comma: 188, enter: 13, };
const delimiters = [KeyCodes.comma, KeyCodes.enter];

/**
 * @augments {Component<{ onChange: function, value: string, suggestions: [] }, {arraysf: number}>}
 */
export class TagInput extends Component {

    constructor(props) {
        super(props);

        // this.state = {
        //     tags: [{ id: 'Thailand', text: 'Thailand' }, { id: 'India', text: 'India' }],
        // };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);
    }

    state = {
        updaterProp: new Date(),
    };
    componentWillMount() {
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);

        // window.Scripting_TagInput = this;
    }
    getTags() {
        const { value } = this.props;
        return (value || '')
            .split(',')
            .filter(i => !!i)
            .map(i => ({ id: i, text: i }));
    }
    getSuggestions() {
        const { suggestions } = this.props;
        return suggestions || [];
    }

    handleChange(tags) {
        this.setState({ tags });
        const { onChange } = this.props;
        if (isFunction(onChange)) {
            const value = tags.join();
            onChange(value);
        }
    }
    handleAddition(tag) {
        this.handleChange([...this.state.tags, tag]);
    }
    handleDrag(tag, currPos, newPos) {
        const tags = [...this.state.tags];
        const newTags = tags.slice();

        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);

        this.handleChange(newTags);
    }
    handleDelete(i) {
        this.handleChange(this.state.tags.filter((tag, index) => index !== i));
    }
    handleTagClick(index) {
        // console.log(`The tag at index ${index} was clicked`);
    }

    render() {
        const tags = this.getTags();
        const suggestions = this.getSuggestions();
        return (
            <div>
                <ReactTags
                    tags={tags}
                    suggestions={suggestions}
                    delimiters={delimiters}
                    handleDelete={this.handleDelete}
                    handleAddition={this.handleAddition}
                    handleDrag={this.handleDrag}
                    handleTagClick={this.handleTagClick}
                />
            </div>
        );
    }
}
