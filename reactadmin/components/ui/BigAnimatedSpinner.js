
import React, { Component } from 'react';
import { AnimatedSpinner } from './AnimatedSpinner';

/**
 * @augments {Component<{ containerStyle: object, spinnerStyle: object, }, {a: number}>}
 */
export class BigAnimatedSpinner extends Component {
    render() {
        const { containerStyle, spinnerStyle, ...rest } = this.props;
        return (
            <div
                style={{
                    // position: 'absolute',
                    position: 'relative',
                    display: 'flex',
                    height: '100%',
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    ...containerStyle,
                }}
            >
                <div {...rest}>
                    <AnimatedSpinner show style={{ fontSize: '3em', ...spinnerStyle }} />
                </div>
            </div>
        );
    }
}
