import React, { Component } from 'react';
import ReactDrafts from 'react-drafts';
import { isFunction } from '../../Utils';

export default class ReactDraftsInput extends Component {

	onBlur() {
		this.editor.save().then((html) => {
			if (isFunction(this.props.onFieldChange)) {
				this.props.onFieldChange(html);
			}
		});
	}

	editorRef(editor) {
		this.editor = editor;
		if (isFunction(this.props.editorRef)) {
			this.props.editorRef(editor);
		}
	}
	render() {
		const {
			value,
			editorRef, // eslint-disable-line no-unused-vars
			...rest,
		} = this.props;
		return (
			<ReactDrafts
				ref={(editor) => this.editorRef(editor)}
				content={value}
				exportTo="html"
				onBlur={this.onBlur.bind(this)}
				onFileUpload={() => { }}
				{...rest}
			/>
		);
	}
}
