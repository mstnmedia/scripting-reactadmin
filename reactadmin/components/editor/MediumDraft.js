// import PropTypes from 'prop-types';

import React from 'react';
import {
	EditorState,
	convertToRaw,
	convertFromRaw,
	KeyBindingUtil,
	Modifier,
	// AtomicBlockUtils,
} from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';

// import 'draft-js/dist/Draft.css';
// import 'hint.css/hint.min.css';
import 'medium-draft/dist/medium-draft.css';
// import 'medium-draft/lib/components/addbutton.scss';
// import './components/toolbar.scss';
// import './components/blocks/text.scss';
// import './components/blocks/atomic.scss';
// import './components/blocks/blockquotecaption.scss';
// import './components/blocks/caption.scss';
// import './components/blocks/todo.scss';
// import './components/blocks/image.scss';

import {
	Editor,
	StringToTypeMap,
	Block,
	keyBindingFn,
	// createEditorState,
	addNewBlockAt,
	beforeInput,
	getCurrentBlock,
	ImageSideButton,
	rendererFn,
	HANDLED,
	NOT_HANDLED,
	AtomicBlock
} from 'medium-draft';

import {
	setRenderOptions,
	blockToHTML,
	entityToHTML,
	styleToHTML,
} from 'medium-draft/lib/exporter';

import { EmbedSideButton } from './EmbedSideButton';
import { SeparatorSideButton } from './SeparatorSideButton';
import { AtomicEmbedComponent, AtomicSeparatorComponent } from './AtomicClasses';
import { TextArea } from '../ui';
import i18n from '../../../i18n';

const { hasCommandModifier } = KeyBindingUtil;

const DQUOTE_START = '“';
const DQUOTE_END = '”';
const SQUOTE_START = '‘';
const SQUOTE_END = '’';

const newTypeMap = StringToTypeMap;
newTypeMap['2.'] = Block.OL;

const newBlockToHTML = (block) => {
	const blockType = block.type;
	if (blockType === Block.ATOMIC) {
		if (block.text === 'E') {
			return {
				start: '<figure class="md-block-atomic md-block-atomic-embed">',
				end: '</figure>',
			};
		} else if (block.text === '-') {
			return <div className="md-block-atomic md-block-atomic-break"><hr /></div >;
		}
	}
	return blockToHTML(block);
};

const newEntityToHTML = (entity, originalText) => {
	if (entity.type === 'embed') {
		return (
			<div>
				<a
					className="embedly-card"
					href={entity.data.url}
					data-card-controls="0"
					data-card-theme="dark"
				>Embedded ― {entity.data.url}
				</a>
			</div>
		);
	}
	return entityToHTML(entity, originalText);
};

const handleBeforeInput = (editorState, str, onChange) => {
	if (str === '"' || str === '\'') {
		const currentBlock = getCurrentBlock(editorState);
		const selectionState = editorState.getSelection();
		const contentState = editorState.getCurrentContent();
		const text = currentBlock.getText();
		const len = text.length;
		if (selectionState.getAnchorOffset() === 0) {
			onChange(EditorState.push(editorState,
				Modifier.insertText(contentState, selectionState,
					(str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
			return HANDLED;
		} else if (len > 0) {
			const lastChar = text[len - 1];
			if (lastChar !== ' ') {
				onChange(EditorState.push(editorState,
					Modifier.insertText(contentState, selectionState,
						(str === '"' ? DQUOTE_END : SQUOTE_END)), 'transpose-characters'));
			} else {
				onChange(EditorState.push(editorState,
					Modifier.insertText(contentState, selectionState,
						(str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
			}
			return HANDLED;
		}
	}
	return beforeInput(editorState, str, onChange, newTypeMap);
};

class MediumDraft extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			editorState: this.props.editorState,
			editorEnabled: true,
			placeholder: this.props.placeholder ? this.props.placeholder : 'Write here...',
			editHTML: false,
			html: '',
		};
		//console.log('state ', this.state);
		this.onChange = (editorState, callback = null) => {
			if (this.state.editorEnabled) {
				this.setState({ editorState }, () => {
					if (callback) {
						callback();
					}
					if (this.props.onFieldChange) {
						this.props.onFieldChange(editorState);
					}
				});
			}
		};

		this.sideButtons = [
			// { title: 'Image', component: ImageSideButton, },
			// { title: 'Embed', component: EmbedSideButton, },
			// { title: 'Separator', component: SeparatorSideButton, }
		];

		this.exporter = setRenderOptions({
			styleToHTML,
			blockToHTML: newBlockToHTML,
			entityToHTML: newEntityToHTML,
		});

		this.getEditorState = () => this.props.editorState;

		this.logData = this.logData.bind(this);
		this.renderHTML = this.renderHTML.bind(this);
		this.toggleEdit = this.toggleEdit.bind(this);
		this.fetchData = this.fetchData.bind(this);
		this.loadSavedData = this.loadSavedData.bind(this);
		this.keyBinding = this.keyBinding.bind(this);
		this.handleKeyCommand = this.handleKeyCommand.bind(this);
		this.handleDroppedFiles = this.handleDroppedFiles.bind(this);
		this.handleReturn = this.handleReturn.bind(this);
	}

	componentDidMount() {
		//setTimeout(this.fetchData, 1000);
	}

	componentWillReceiveProps(props) {
		this.setState({
			editorEnabled: !props.disabled,
			editorState: props.editorState,
			html: this.exporter(props.editorState.getCurrentContent()),
		});
	}

	keyBinding(e) {
		if (hasCommandModifier(e)) {
			if (e.which === 83) {  /* Key S */
				return 'editor-save';
			}
			// else if (e.which === 74 /* Key J */) {
			//  return 'do-nothing';
			//}
		}
		if (e.altKey === true) {
			if (e.shiftKey === true) {
				switch (e.which) {
					/* Alt + Shift + L */
					case 76: return 'load-saved-data';
					/* Key E */
					case 69: return 'toggle-edit-mode';
					default: break;
				}
			}
			if (e.which === 72 /* Key H */) {
				return 'toggleinline:HIGHLIGHT';
			}
		}
		return keyBindingFn(e);
	}

	handleKeyCommand(command) {
		if (command === 'editor-save') {
			window.localStorage.editor = JSON.stringify(
				convertToRaw(this.props.editorState.getCurrentContent())
			);
			window.ga('send', 'event', 'draftjs', command);
			return true;
		} else if (command === 'load-saved-data') {
			this.loadSavedData();
			return true;
		} else if (command === 'toggle-edit-mode') {
			this.toggleEdit();
		}
		return false;
	}

	fetchData() {
		return;
		// window.ga('send', 'event', 'draftjs', 'load-data', 'ajax');
		// this.setState({
		// 	placeholder: 'Loading...',
		// });
		// const req = new XMLHttpRequest();
		// req.open('GET', 'data.json', true);
		// req.onreadystatechange = () => {
		// 	if (req.readyState === 4) {
		// 		const data = JSON.parse(req.responseText);
		// 		this.setState({
		// 			editorState: createEditorState(data),
		// 			placeholder: 'Write here...'
		// 		}, () => {
		// 			this.m_editor.focus();
		// 		});
		// 		window.ga('send', 'event', 'draftjs', 'data-success');
		// 	}
		// };
		// req.send();
	}

	logData(e) {
		e.preventDefault();
		const currentContent = this.props.editorState.getCurrentContent();
		const es = convertToRaw(currentContent);
		// console.log(es);
		// console.log(this.props.editorState.getSelection().toJS());
		window.ga('send', 'event', 'draftjs', 'log-data');
	}

	loadSavedData() {
		const data = window.localStorage.getItem('editor');
		if (data === null) {
			return;
		}
		try {
			const blockData = JSON.parse(data);
			// console.log(blockData);
			this.onChange(
				EditorState.push(this.props.editorState, convertFromRaw(blockData)),
				this.m_editor.focus
			);
		} catch (e) {
			// console.log(e);
		}
		window.ga('send', 'event', 'draftjs', 'load-data', 'localstorage');
	}

	toggleEdit(e) {
		e.preventDefault();
		this.setState({
			editorEnabled: !this.state.editorEnabled
		}, () => {
			window.ga('send', 'event', 'draftjs', 'toggle-edit', `${this.state.editorEnabled}`);
		});
	}

	handleDroppedFiles(selection, files) {
		window.ga('send', 'event', 'draftjs', 'filesdropped', `${files.length} files`);
		const file = files[0];
		if (file.type.indexOf('image/') === 0) {
			// eslint-disable-next-line no-undef
			const src = URL.createObjectURL(file);
			this.onChange(addNewBlockAt(
				this.props.editorState,
				selection.getAnchorKey(),
				Block.IMAGE, {
					src,
				}
			));
			return HANDLED;
		}
		return NOT_HANDLED;
	}

	handleReturn(/* e */) {
		// const currentBlock = getCurrentBlock(this.props.editorState);
		// var text = currentBlock.getText();
		return NOT_HANDLED;
	}

	handleEditHTMLChange(e) {
		const editHTML = e.target.checked;
		const { html, editorState } = this.state;
		this.setState({ editHTML });
		if (editHTML) {
			this.setState({ html: this.exporter(editorState.getCurrentContent()) });
		} else {
			const newState = this.handleHTMLChange(html);
			this.onChange(newState);
		}
	}
	handleHTMLChange(html) {
		const editorState = EditorState.createWithContent(stateFromHTML(html));
		this.setState({ html, editorState });
		return editorState;
	}

	renderHTML(e) {
		e.preventDefault();
		const currentContent = this.props.editorState.getCurrentContent();
		const eHTML = this.exporter(currentContent);
		// console.log('stateToHTML: ', eHTML);

		const newWin = window.open(
			`${window.location.pathname}/dist/rendered.html`,
			'windowName', `height=${window.screen.height},width=${window.screen.wdith}`);
		newWin.onload = () => newWin.postMessage(eHTML, window.location.origin);
	}

	rendererFn(setEditorState, getEditorState) {
		const atomicRenderers = {
			embed: AtomicEmbedComponent,
			separator: AtomicSeparatorComponent,
		};
		const rFnOld = rendererFn(setEditorState, getEditorState);
		const rFnNew = (contentBlock) => {
			const type = contentBlock.getType();
			switch (type) {
				case Block.ATOMIC:
					return {
						component: AtomicBlock,
						editable: false,
						props: {
							components: atomicRenderers,
							getEditorState,
						},
					};
				default: return rFnOld(contentBlock);
			}
		};
		return rFnNew;
	}

	render() {
		const { editorState, editorEnabled, editHTML, html, } = this.state;
		return (
			<div>
				<div className="text-right">
					<input
						type="checkbox"
						checked={editHTML} style={{ margin: '3px', }}
						onChange={(e) => this.handleEditHTMLChange(e)}
					/>
					<span htmlFor="editHTML">{i18n.t('EditHTML')}</span>
				</div>
				{editHTML ?
					<TextArea
						ref={(e) => { this.textArea = e; }}
						className="form-control" name="html"
						style={{ height: '100%', resize: 'vertical' }}
						value={html || ''}
						onFieldChange={(e) => this.handleHTMLChange(e.target.value)}
						rows="8"
					/>
					:
					<div style={{ border: '1px solid #cccccc', borderColor: '#d9d9d9', borderRadius: 3 }}>
						<Editor
							ref={(e) => { this.m_editor = e; }}
							editorState={editorState}
							onChange={this.onChange}
							editorEnabled={editorEnabled}
							handleDroppedFiles={this.handleDroppedFiles}
							handleKeyCommand={this.handleKeyCommand}
							placeholder={this.state.placeholder}
							keyBindingFn={this.keyBinding}
							beforeInput={handleBeforeInput}
							handleReturn={this.handleReturn}
							sideButtons={this.sideButtons}
							rendererFn={this.rendererFn}
						/>
					</div>
				}
			</div>
			/* <div>
				<div className="editor-action">
					<button onClick={this.logData}>Log State</button>
					<button onClick={this.renderHTML}>Render HTML</button>
					<button onClick={this.toggleEdit}>Toggle Edit</button>
				</div> 
			</div>*/
		);
	}
}

window.ga = function (...args) {
	// console.log(args);
};

export default MediumDraft;
