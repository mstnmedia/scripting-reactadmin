import React from 'react';
import PropTypes from 'prop-types';

export class AtomicEmbedComponent extends React.Component {

	static propTypes = {
		data: PropTypes.object.isRequired,
	}

	constructor(props) {
		super(props);

		this.state = {
			showIframe: false,
		};

		this.enablePreview = this.enablePreview.bind(this);
	}

	componentDidMount() {
		this.renderEmbedly();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
			this.renderEmbedly();
		}
	}

	getScript() {
		const script = document.createElement('script');
		script.async = 1;
		script.src = '//cdn.embedly.com/widgets/platform.js';
		script.onload = () => {
			window.embedly();
		};
		document.body.appendChild(script);
	}

	enablePreview() {
		this.setState({
			showIframe: true,
		});
	}

	renderEmbedly() {
		if (window.embedly) {
			window.embedly();
		} else {
			this.getScript();
		}
	}

	render() {
		const { url } = this.props.data;
		const innerHTML = `<div><a class="embedly-card" href="${url}" data-card-controls="0" data-card-theme="dark">Embedded ― ${url}</a></div>`; // eslint-disable-line max-len
		return (
			<div className="md-block-atomic-embed">
				<div dangerouslySetInnerHTML={{ __html: innerHTML }} />
			</div>
		);
	}
}

export const AtomicSeparatorComponent = (/* props */) => (
	<hr />
);

export const AtomicBlock = (props) => {
	const { blockProps, block } = props;
	const content = blockProps.getEditorState().getCurrentContent();
	const entity = content.getEntity(block.getEntityAt(0));
	const data = entity.getData();
	const type = entity.getType();
	if (blockProps.components[type]) {
		const AtComponent = blockProps.components[type];
		return (
			<div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
				<AtComponent data={data} />
			</div>
		);
	}
	return <p>Block of type <b>{type}</b> is not supported.</p>;
};
