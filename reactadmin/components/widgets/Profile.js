/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Profile
 */

import React, { Component } from 'react';
import { Link } from 'react-router';

import i18n from '../../../i18n';

class Profile extends Component {
	render() {
		const logoutURL = `${window.config.backend}/logout`;
		return (
			<li className="dropdown">
				<a
					href=""
					className="dropdown-toggle"
					data-toggle="dropdown"
					style={{
						minWidth: 60,
						padding: 0,
						margin: 10,
					}}
				>
					<span className="thumb-sm avatar pull-left">
						<img alt="" src={this.props.profile ? this.props.profile.avatar : ''} />
					</span>
					<b className="caret" />
				</a>
				<ul className="dropdown-menu animated fadeInRight">
					<span className="arrow top" />
					<li>
						<Link to="/profile">Profile</Link>
					</li>
					<li>
						<Link to="/account">Account</Link>
					</li>
					<li>
						<Link to="/settings">Settings</Link>
					</li>
					<li className="divider" />
					<li>
						<a href={logoutURL} >
							{i18n.t('Logout')}
						</a>
					</li>
				</ul>
			</li>
		);
	}
}
export default Profile;
