/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Menu
 */

import React, { Component } from 'react';
import { Link } from 'react-router';
import i18n from '../../../i18n';

export default class SubMenuItem extends Component {
	render() {
		const {
			badgeText,
			tooltip,
			link, linkText,
		} = this.props;
		const badge = badgeText ?
			<b className='badge bg-danger pull-right'>{badgeText}</b>
			: null;

		let title = '';
		const tooltipProps = {};
		if (tooltip) {
			title = (tooltip.tag ? i18n.t(tooltip.tag) : undefined) || undefined;
			if (title) {
				tooltipProps['data-balloon'] = title;
				tooltipProps['data-balloon-pos'] = tooltip.position;
			}
		}
		return (
			<li {...tooltipProps}>
				{' '}
				<Link to={link}>
					{' '}
					<i className='fa fa-angle-right' />
					{badge}
					<span>{linkText}</span>
					{' '}
				</Link>
				{' '}
			</li>
		);
	}
}
