import React from 'react';

export default class DateDiff extends React.Component {

	static calc = ({ from, to, }) => {
		function addZ(n) {
			return (n > -10 && n < 10 ? '0' : '') + n;
		}
		const fromDate = from ? new Date(from) : new Date();
		const toDate = to ? new Date(to) : new Date();
		let res = toDate - fromDate;
		const ms = res % 1000;
		res = (res - ms) / 1000;
		const secs = res % 60;
		res = (res - secs) / 60;
		let mins = res % 60;
		let hrs = (res - mins) / 60;
		if (res < 0) {
			hrs = 0;
			mins = 0;
		}

		return `${addZ(hrs)}:${addZ(mins)}:${addZ(secs)}`;
	}
	static defaultText = '00:00:00';
	state = {
		dateDiff: '00:00:00',
	};

	componentDidMount() {
		this.callSetTimeout = this.callSetTimeout.bind(this);
		if (this.props.realTime) {
			this.callSetTimeout();
		}
	}
	componentWillUnmount() {
		this.unmounting = true;
	}
	callSetTimeout() {
		if (!this.unmounting) {
			if (this.props.from || this.props.to) {
				this.setState({
					dateDiff: DateDiff.calc({
						from: this.props.from,
						to: this.props.to
					})
				});
			}
			setTimeout(this.callSetTimeout, 1000);
		}
	}

	render() {
		const {
			container: Container,
			from, to,
			realTime, //eslint-disable-line no-unused-vars
			...rest
		} = this.props;
		const children = (from || to) ? this.state.dateDiff : DateDiff.defaultText;
		if (Container) {
			return <Container {...{ ...rest, children, }} />;
		}
		return <div {...rest}>{children}</div>;
		// switch (container) {
		// 	case 'h3':
		// 		return <h3 {...{ ...rest }}>{children}</h3>;
		// 	case 'h4':
		// 		return <h4 {...{ ...rest }}>{children}</h4>;
		// 	default:
		// 		return children;
		// }
	}
}
