/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule MenuLink
 */

import React from 'react';
import { Link } from 'react-router';
import i18n from '../../../i18n';

export default class MenuItem extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			open: false
		};
	}
	getItemClass() {
		const link = (this.props.link || '').toLowerCase();
		const currentPage = (this.props.currentPage || '').toLowerCase();
		if (link !== currentPage) {
			return link !== '/' && currentPage.indexOf(link) >= 0 ? 'active' : '';
		}
		return link === currentPage;
	}

	toggleOpen(e) {
		e.preventDefault();
		this.setState({ open: !this.state.open });
	}

	renderSubMenu() {
		if (!this.props.children) return null;
		return (
			<ul className="nav lt" style={this.state.open ? { display: 'block' } : {}}>
				{this.props.children}
			</ul>
		);
	}
	render() {
		const {
			icon, badgeCount,
			tooltip,
			link, linkText, children,
		} = this.props;
		const iconClasses = `m-l-md fa icon ${icon}`;
		const itemClass = this.getItemClass();
		const badge = badgeCount ?
			<b className="badge bg-danger">{badgeCount}</b>
			: null;

		let title = '';
		const tooltipProps = {};
		if (tooltip) {
			title = (tooltip.tag ? i18n.t(tooltip.tag) : undefined) || undefined;
			if (title) {
				tooltipProps['data-balloon'] = title;
				tooltipProps['data-balloon-pos'] = tooltip.position;
			}
		}
		if (children) {
			return (
				<li className={itemClass} {...tooltipProps}>
					<a className={itemClass} href={link} onClick={(e) => this.toggleOpen(e)}>
						<i className={iconClasses} />
						{badge}
						<span>{linkText}</span>
					</a>
					{this.renderSubMenu()}
				</li>
			);
		}
		return (
			<li className={itemClass} {...tooltipProps}>
				<Link className={itemClass} to={link}>
					<i className={iconClasses} />
					{badge}
					<span>{linkText}</span>
				</Link>
				{this.renderSubMenu()}
			</li>
		);
	}
}
