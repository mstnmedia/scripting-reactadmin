/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataTable
 */

import React, { PureComponent } from 'react';
import i18n from '../../../i18n';
import DataRow from './Row';
import Cell from './Cell';
import { Pager } from '../../components/';
import { isFunction } from '../../Utils';

export default class DataTable extends PureComponent {
	getHeaderKeys() {
		const { fields } = this.props.schema;
		return Object.keys(fields)
			.filter(key => !!fields[key])
			.reverse();
	}
	getEmptyRow(emptyRow) {
		const { loading } = this.props;
		let row = emptyRow;
		if (row === true || loading) {
			row = ({ columnNo }) => (
				<tr><td colSpan={columnNo}>{loading ? i18n.t('Loading') : i18n.t('NoResults')}.</td></tr>
			);
		}
		if (isFunction(row)) {
			return row({
				columnNo: this.getHeaderKeys().length + 1,
			});
		}
		return row;
	}
	getRowKey(row, i) {
		const { rowKey } = this.props;
		return isFunction(rowKey) ?
			rowKey(row, i) || i :
			row.id || i;
	}
	getRowClass(row, i, selected) {
		const { rowClass } = this.props;
		return isFunction(rowClass)
			? rowClass(row, i, selected)
			: (row === selected && 'success') || '';
	}
	getRowDisabled(row, rows) {
		const { rowDisabled } = this.props;
		switch (typeof (rowDisabled)) {
			case 'boolean':
				return rowDisabled;
			case 'function':
				return rowDisabled(row, rows);
			default:
				return false;
		}
	}
	getTableHeaders() {
		const { schema } = this.props;
		const headerKeys = this.getHeaderKeys();
		return headerKeys.map((key, i) => {
			const field = schema.fields[key];
			return (
				<th
					key={`header-key-${key}-${i}`} key={key}
					style={{
						textAlign: (field &&
							['datetime', 'datetimez'].indexOf((field.type || '').toLowerCase()) > -1)
							? 'center' : undefined
					}}
				>
					{field.label || field || key}
				</th>
			);
		});
	}

	handleRowDelete(row, i, e) {
		e.preventDefault();
		e.stopPropagation();
		this.props.onDelete(this.getRowKey(row, i), { row, i, e });
	}
	handleRowEdit(row, e) {
		e.preventDefault();
		e.stopPropagation();
		this.props.onEdit(row);
	}
	handleRowClick(row, e) {
		const { link, rows, } = this.props;
		if (link) link(row, e, rows);
	}

	canDelete() {
		const { canDelete, onDelete, } = this.props;
		let showDelete;
		if (isFunction(onDelete)) {
			if (isFunction(canDelete)) {
				showDelete = true;
			} else {
				showDelete = canDelete === undefined || canDelete !== false;
			}
		} else {
			showDelete = false;
		}
		return showDelete;
	}

	renderRowEditControls(row, i) {
		const { canDelete, onDelete, onEdit } = this.props;
		let showDelete;
		if (isFunction(onDelete)) {
			if (isFunction(canDelete)) {
				showDelete = canDelete(row);
			} else {
				showDelete = canDelete === undefined || canDelete !== false;
			}
		} else {
			showDelete = false;
		}
		const showEdit = isFunction(onEdit);
		const showCell = showDelete || showEdit;
		return (showCell &&
			<td style={{ justifyContent: 'space-between', }}>
				{showDelete &&
					// <Tooltip position="left" tag={this.props.deleteTooltip || 'TooltipTableDelete'}>
					<a
						href="#deleteRow" title={this.props.deleteTooltip || i18n.t('TooltipTableDelete')}
						onClick={(e) => this.handleRowDelete(row, i, e)}
					>
						<i className="fa text-muted fa-trash-o" />
					</a>
					// </Tooltip>
				}
				{showEdit &&
					// <Tooltip position="left" tag={this.props.editTooltip || 'TooltipTableEdit'}>
					<a
						href="#editRow"
						title={this.props.editTooltip || i18n.t('TooltipTableEdit')}
						onClick={(e) => this.handleRowEdit(row, e)}
					>
						<i className="fa text-muted fa-edit" />
					</a>
					// </Tooltip>
				}
			</td>
		);
	}
	renderRows() {
		const {
			rows, schema, link, selected,
			renderRow, emptyRow, loading, cellStyle,
		} = this.props;
		if (rows.length === 0 && (emptyRow || loading)) {
			return this.getEmptyRow(emptyRow);
		}
		const headerKeys = this.getHeaderKeys();
		return rows.map((row, iR) => {
			const cells = headerKeys.map((key, iC) =>
				<Cell
					key={`cell-key-${key}-${iC}`}
					schemaName={schema.name}
					field={schema.fields[key]}
					cellType={schema.fields[key].type || 'string'}
					value={row[key] || ''}
					{...{ row, rows, iR, iC, cellStyle, }}
				/>
			);
			const disabled = this.getRowDisabled(row, rows);
			const rowProps = {
				key: this.getRowKey(row, iR),
				className: this.getRowClass(row, iR, selected),
				style: { cursor: link && !disabled ? 'pointer' : undefined },
				cells,
				editControls: this.renderRowEditControls(row, iR),
				onClick: (e) => this.handleRowClick(row, e, rows),
				disabled,
				rows,
			};

			const useRenderRowProp = isFunction(renderRow);
			if (useRenderRowProp) {
				return renderRow({
					...rowProps,
					headerKeys,
					row,
					schema,
					selected,
				});
			}
			return (
				<DataRow {...rowProps} />
			);
		});
	}

	renderAddButton() {
		if (isFunction(this.props.renderAddButton)) {
			return this.props.renderAddButton();
		}
		if (isFunction(this.props.onAdd)) {
			return (
				<span className="text-muted m-l-sm pull-right">
					<div
						className="inline"
						data-balloon={this.props.addTooltip}
						data-balloon-pos="left"
					>
						<a
							href="#/OpenModal" className="btn btn-success btn-xs m-t-xs m-b-xs"
							onClick={(e) => this.props.onAdd(e)}
						>
							<i className="fa fa-plus" /> {i18n.t('Add')}
						</a>
					</div>
				</span>
			);
		}
		return null;
	}
	renderHeader() {
		const addButton = this.renderAddButton();
		if (isFunction(this.props.renderHeader)) {
			return this.props.renderHeader({
				addButton,
			});
		}
		return (
			<header className="panel-heading">
				{addButton}
			</header>
		);
	}

	renderPagination() {
		const {
			pageSize: itemsPerPage,
			pageNumber,
			totalRows: totalItems,
			onPageChange,
		} = this.props;
		const currentPage = pageNumber || 1;
		if (itemsPerPage) {
			return (
				<Pager {...{ currentPage, itemsPerPage, totalItems, onPageChange }} />
			);
		}
		return null;
	}
	render() {
		const {
			hover, notStriped, condensed,
			onEdit,
			tableClassName,
		} = this.props;

		let tableClass = 'table';
		tableClass += hover ? ' table-hover' : '';
		tableClass += condensed ? ' table-condensed' : '';
		tableClass += notStriped ? '' : ' table-striped';
		tableClass += tableClassName ? ` ${tableClassName || ''}` : '';

		const showDelete = this.canDelete() ? 30 : 0;
		const showEdit = isFunction(onEdit) ? 30 : 0;
		const showCell = !!showDelete || !!showEdit;
		return (
			<section>
				{this.renderHeader()}
				<div className="table-responsive">
					<table className={tableClass}>
						<thead>
							<tr>
								{this.getTableHeaders()}
								{showCell && <th style={{ width: showDelete + showEdit, }} />}
							</tr>
						</thead>
						<tbody>
							{this.renderRows()}
						</tbody>
					</table>
				</div>
				<footer className="">
					{this.renderPagination()}
				</footer>
			</section>
		);
	}
}
