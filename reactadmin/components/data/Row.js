/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Row
 */

import React, { PureComponent } from 'react';

export default class Row extends PureComponent {

	render() {
		const {
			cells, editControls,
			...rest
		} = this.props;
		return (
			<tr {...rest}>
				{cells}
				{editControls}
			</tr>
		);
	}
}
