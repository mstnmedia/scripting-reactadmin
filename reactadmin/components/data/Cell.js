/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Cell
 */

import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import i18n from '../../../i18n';
import { DateFormats } from '../../../store/enums';
import { isFunction } from '../../Utils';

export default class Cell extends PureComponent {
	render() {
		const { schemaName, value: cellValue, cellType, field, row, rows, iR, iC, } = this.props;
		const { format, coalesce, } = field;
		const cellStyle = Object.assign({}, this.props.cellStyle, field.cellStyle);
		let value = cellValue;
		if (format && typeof (format) === 'function') {
			value = format(row, rows);
		}
		let control = null;
		let styles = {};
		switch (cellType.toLowerCase()) {
			case 'id':
				control = (
					<Link className="btn btn-rounded btn-sm btn-dark" to={`/data/${schemaName}/${value}`}>
						{value || coalesce}
					</Link>
				);
				break;
			case 'reference':
				control = (
					<a className="btn btn-rounded btn-sm btn-info" href={`/data/${schemaName}/${value}`}>
						{value || coalesce}
					</a>
				);
				break;
			case 'array': {
				let buttonlabel = i18n.t('ViewRelations');
				let buttonColor = 'btn-default';

				if (value.length !== 0) {
					buttonColor = 'btn-primary';
					buttonlabel = `${i18n.t('ViewRelations')} (${value.length})`;
				}
				control = (
					<a className={`btn btn-rounded btn-sm ${buttonColor}`}>{buttonlabel}</a>
				);
				break;
			}
			case 'date': {
				styles = { whiteSpace: 'nowrap' };
				control = ((value && moment(value).format(DateFormats.DATE)) || coalesce);
				break;
			}
			case 'datetime':
			case 'datetimez': {
				if (value) {
					const dateFormats = (
						cellType.toLowerCase() === 'datetime'
							? DateFormats.DATE_TIME
							: DateFormats.DATE_TIME_Z
					).split(new RegExp('(?<=^\\S+)\\s'));
					control = (
						<div style={{ textAlign: 'center' }}>
							<span key="1" style={{ whiteSpace: 'nowrap' }}>
								{moment(value).format(dateFormats[0])}
							</span> <span key="2" style={{ whiteSpace: 'nowrap' }}>
								{moment(value).format(dateFormats[1])}
							</span>
						</div>
					);
				} else {
					value = coalesce;
				}
				break;
			}
			case 'boolean':
				control = (
					value ? i18n.t('Yes') : i18n.t('No')
				);
				break;
			default:
				control = (value || coalesce);
				break;
		}
		Object.assign(styles, isFunction(cellStyle) ? cellStyle(row, iR, iC) : cellStyle);
		return (
			<td style={styles}>{control}</td>);
	}
}
