/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule PieChart
 */

import React from 'react';
import ReactChartist from 'react-chartist';
// import Legend from 'chartist-plugin-legend';
import FillDonut from 'chartist-plugin-fill-donut';

export default class PieChart extends React.Component {

	render() {
		const {
			data,
			options,
			legend,
			donut, fillDonut,
			fillDonutOptions,
			...rest,
		} = this.props;

		let pluginOptions = {};
		const plugins = [];
		// if (legend) {
		// 	plugins.push(Legend({
		// 		legendNames: data.labels,
		// 	}));
		// }
		if (fillDonut) {
			data.labels = ['', '', ''];
			data.series = [20, 20, 20, 20];
			pluginOptions = {
				donut: true,
				donutWidth: 20,
				startAngle: 210,
				total: 90,
				showLabel: true,
			};

			const FillDonutOptions = fillDonutOptions || {
				items: [
					{ //Item 1
						content: '<i class="fa fa-tachometer"></i>',
						position: 'bottom',
						offsetY: 10,
						offsetX: -2
					},
					{ //Item 2
						content: '<h3>160<span class="small">mph</span></h3>'
					},
					{ //Item 3
						content: '<h3>All goes well</h3>',
						position: 'top',
						offsetY: 10,
						offsetX: -2
					}
				],
			};
			plugins.push(FillDonut(FillDonutOptions));
		}
		const chartOptions = {
			height: '300px',
			fullWidth: true,
			donut: donut || false,
			// labelInterpolationFnc: (value) => (
			// 	`${Math.round((value / data.series.reduce((a, b) => a + b)) * 100)}%`
			// ),
			plugins,
			...pluginOptions,
			...options,
		};

		return (
			<ReactChartist
				type="Pie" className={'ct-chart'}
				data={data}
				options={chartOptions}
				{...rest}
			/>
		);
	}
}
