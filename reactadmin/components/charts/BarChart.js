/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule BarChart
 */

import React from 'react';
import ReactChartist from 'react-chartist';
// import BarLabels from 'chartist-plugin-barlabels';
// import ChartistTooltip from 'chartist-plugin-tooltips-updated';

export default class BarChart extends React.Component {

	render() {
		const {
			horizontal: isHorizontal,
			options,
			data,
			barlabels,
		} = this.props;
		const horizontal = isHorizontal || false;
		const plugins = [];
		if (barlabels) {
			plugins.push(
				// BarLabels
				// ChartistTooltip(),
			);
		}
		const charOptions = {
			low: 0,
			height: '300px',
			stackBars: false,
			horizontalBars: horizontal,
			fullWidth: true,
			// chartPadding: {
			// 	right: 50,
			// 	top: 0
			// },
			plugins,
			axisY: {
				labelInterpolationFnc(value) {
					return value;
				}
			},
			...options,
		};

		return (
			<ReactChartist
				type="Bar"
				className={'ct-chart'}
				data={data}
				options={charOptions}
			/>
		);
	}
}
