/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Login
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { auth } from '../../../actions';

import /* ReduxOutlet, */ { mapStateToProps } from '../../../outlets/';

import { Input } from '../ui/';

class Login extends Component {
	state = {
			email: '',
			password: '',
			error: '',
			endpoint: ''
		}
	handleChange(prop, e) {
		this.setState({ [prop]: e.target.value });
	}

	handleLogin(e) {
		e.preventDefault();
		const { auth } = this.props; // eslint-disable-line no-shadow
		// this.props.onLogin(this.state.email, this.state.password);
		auth({
			email: this.state.email,
			password: this.state.password,
			endpoint: this.state.endpoint,
		});
	}
	handleForgot(e) {
		e.preventDefault();
		const { dispatch, forgot } = this.props;
		// this.props.onLogin(this.state.email, this.state.password);
		forgot({
			email: this.state.email,
			password: this.state.password,
			dispatch,
		});
	}

	render() {
		return (
			<section className="wrapper-md animated fadeInUp">
				<form role="form">
					<select 
						name="account" 
						className="form-control m-b"
					>
						<option>select an endpoint</option>
						<option>localhost</option>
					</select>
					<div className="form-group">
						<Input
							ref={(ref) => (this.emailRef = ref)}
							autoComplete={'off'}
							format="email"
							icon="fa fa-user"
							required
							errorMessage="Please verify your email"
							placeholder="email"
							value={this.state.email}
							onFieldChange={(e) => this.handleChange('email', e)}
						/>
					</div>
					<div className="form-group">
						<Input 
							// ref="loginRef" 
							ref={(ref) => (this.passwordRef = ref)}
							autoComplete={'off'} 
							format="password" 
							icon="fa fa-lock" 
							required 
							errorMessage="Password is required" 
							placeholder="password" 
							value={this.state.password} 
							onFieldChange={(e) => this.handleChange('password', e)} 
						/>
					</div>
					<p 
						className="help-block"
					>
						{this.props.errorMessage ? this.props.errorMessage.message : ''}
					</p>
					<div className="form-group">
						<button 
							type="button" 
							onClick={(e) => this.handleLogin(e)} 
							className="btn btn-info btn-block w-pad"
						>
							Login
						</button>
					</div>
					<div className="form-group">
						<p>{this.state.error}</p>
					</div>
					<div className="text-center">
						<a 
							href=""
							onClick={(e) => this.handleForgot(e)}
							className="w-login"
						>Forgot your password?</a>
					</div>
				</form>
			</section>
		);
	}
}

export default connect(mapStateToProps, { auth })(Login);
