/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule Modal
 */

import React, { PureComponent } from 'react';
import { isFunction, isBoolean } from '../../Utils';
import i18n from '../../../i18n';

export class Factory extends PureComponent {
	render() {
		const {
			modalref,
			canBeClose, onClosing,
			large, small, factory,
			item, backdrop, keyboard,
			//state, parent,
		} = this.props;
		// const {
		// 	onCreate, onUpdate, onDelete,
		// 	StartEdit, CancelEdit, IsValid,
		// } = parent;

		let size = large ? 'modal-lg' : 'modal-md';
		if (small) {
			size = 'modal-sm';
		}

		const ModalComponent = factory;
		/* let footer = null;

		if (this.props.footer === 'default') {
			footer = (
				<div className="modal-footer">
					<div id="botones_Mantenimiento">
						<button
							type="button" className="btn btn-danger"
							style={{ display: onDelete && state !== 'add' ? 'block' : 'none' }}
							onClick={onDelete}
						>Eliminar</button>
						<button
							type="button" className="btn btn-primary"
							style={{ display: state === 'list' && StartEdit ? 'block' : 'none' }}
							onClick={StartEdit}
						>Editar</button>
						<button
							type="button" className="btn btn-primary"
							style={{ display: state === 'edit' ? 'block' : 'none' }}
							onClick={CancelEdit}
						>Cancelar Edición</button>
						<button
							type="button" className="btn btn-primary"
							style={{ display: state !== 'add' && StartEdit ? 'block' : 'none' }}
							onClick={onUpdate}
							disabled={state !== 'edit' || (IsValid && IsValid())}
						>Guardar Cambios</button>
						<button
							type="button" className="btn btn-primary"
							style={{ display: state === 'add' && onCreate ? 'block' : 'none' }}
							onClick={onCreate}
							disabled={IsValid && IsValid()}
						>Guardar Registro</button>
						<button type="button" className="btn btn-primary" data-dismiss="modal">Salir</button>

					</div>
				</div>
			);
		} */
		if (onClosing) {
			if (isFunction(onClosing)) {
				$(`#${modalref}`).on('hidden.bs.modal', onClosing);
			} else if (isBoolean(onClosing)) {
				$(`#${modalref}`).on('hidden.bs.modal', () => {
					if (item.modified) {
						const result = confirm(i18n.t('DiscardChangesInModal'));
						return result;
					}
				});
			}
		}
		return (
			<div
				className="modal fade"
				id={modalref}
				tabIndex="-1"
				role="dialog"
				aria-labelledby="myModalLabel"
				aria-hidden="true"
				data-backdrop={backdrop || 'static'}
				data-keyboard={keyboard || 'false'}
			>
				<div className={`modal-dialog ${size}`}>
					<div className="modal-content">
						<div className="modal-header">
							<button
								type="button"
								className="_text-muted close"
								data-dismiss="modal"
								aria-hidden="true"
								style={{ fontSize: '2.4em', }}
							>
								×
							</button>
							<h4 className="modal-title">{this.props.title}</h4>
						</div>
						<div className="modal-body">
							<ModalComponent {...this.props} />
						</div>
						{/* footer */}
					</div>
				</div>
			</div>
		);
	}
}

export const ModalFactory = {
	show(modalRef) {
		$(`#${modalRef}`).modal({});
		$(`#${modalRef}`).animate({ scrollTop: 0 }, 400);

		$(`#${modalRef}`).find('.modal-dialog').css({
			height: 'auto',
			'max-height': '100%'
		});
	},
	hide(modalRef) {
		$(`#${modalRef}`).modal('hide');
	},
	modalFromFactory: Factory
};
export default ModalFactory;
