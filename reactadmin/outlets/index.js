/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule ReduxOutlet
 *
 * ReduxOutlet is an abstracted mini-store that contains action creators,
 * action consts and a reducer. It is instantiated with
 * a object type and initial state. The object type maps to a REST type.
 *
 */

import request from 'axios'; /* for promise based requests */
import i18n from '../../i18n';
import { 
	isFunction, isString, 
	// getCookie, 
	respErrorDetails,
} from '../Utils';

//request.defaults.withCredentials = true;
const getAuthorizationHeader = (/* token */) => ({
	// Authorization: token,
	// Authorization: `Bearer ${getCookie(window.config.TOKEN_COOKIE_KEY)}`,
	// Cookie: `${window.config.TOKEN_COOKIE_KEY}=${getCookie(window.config.TOKEN_COOKIE_KEY)}`,
	// 'Access-Control-Allow-Origin': '*',
	// 'Content-Type': 'application/json',
	//'x-access-token': token,
});
const getRequestConfig = ({ headers, ...rest, }) => ({
	headers: {
		...getAuthorizationHeader({}),
		...headers,
	},
	// mode: 'no-cors',
    // credentials: 'include',
	withCredentials: true,
	...rest,
});

export const CommonMapStateToProps = (state) => ({
	loggedIn: state.app.loggedIn,
	token: state.app.token,
	user: state.user,
	app: state.app,
});

export const CommonReduxOutlet = ({ service, schema, /* appid, */ config }) => {
	const SERVER_URL = config.backend;

	const SCHEMA = schema.toUpperCase();
	return ({
		SERVER_URL,
		SCHEMA,
		[`RECEIVE_${SCHEMA}`]: `RECEIVE_${SCHEMA}`,
		[`RECEIVE_ONE_${SCHEMA}`]: `RECEIVE_ONE_${SCHEMA}`,
		[`RECEIVE_TEMP_${SCHEMA}`]: `RECEIVE_TEMP_${SCHEMA}`,
		[`NEW_${SCHEMA}`]: `NEW_${SCHEMA}`,
		[`UPDATE_${SCHEMA}`]: `UPDATE_${SCHEMA}`,
		[`DELETE_${SCHEMA}`]: `DELETE_${SCHEMA}`,
		[`SET_SELECTED_${SCHEMA}`]: `SET_SELECTED_${SCHEMA}`,
		[`SET_EDITING_${SCHEMA}`]: `SET_EDITING_${SCHEMA}`,
		[`SET_PROP_${SCHEMA}`]: `SET_PROP_${SCHEMA}`,

		actions: {

			/*  will fetch if data not present */
			fetchIfNeeded: (list) => (dispatch, getState) => {
				if (shouldFetch(getState(), list)) {
					return dispatch(this.fetch);
				}
			},

			/* lets look for the id in items */
			fetchItemIfNeeded: (token, id, items, fetch, select) => (dispatch/* , getState */) => {
				if (!contains(id, items)) {
					return dispatch(fetch(token, { id }));
				}
				return dispatch(select);
			},

			// setSelected: (id) => (
			// 	{ type: `SET_SELECTED_${SCHEMA}`, id }
			// ),
			setSelected: (item) => (
				{ type: `SET_SELECTED_${SCHEMA}`, item }
			),

			setEditing: (id) => (
				{ type: `SET_EDITING_${SCHEMA}`, id }
			),

			setProp: (prop, value) => (
				{ type: `SET_PROP_${SCHEMA}`, prop, value }
			),
			updateState: (prop, value) => (dispatch) => {
				dispatch({ type: `SET_PROP_${SCHEMA}`, prop, value });
				return Promise.resolve();
			},
			updateStateObj: (newState) => (dispatch) => {
				dispatch({ type: `SET_PROP_OBJ_${SCHEMA}`, newState });
				return Promise.resolve();
			},

			fetch: (token, where, populate = '', temp = false, success, failure, showMessage) => ({
				type: `RECEIVE${temp ? '_TEMP' : ''}_${SCHEMA}`,
				promise: request.get(`${SERVER_URL}/${service}/${schema}`, getRequestConfig({
					params: { where: JSON.stringify(where), }
				})).then(response => {
					try {
						if (success) success(response);
					} catch (error) {
						console.error(error);
					}
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredFetchingRows'), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),

			fetchPage: ({
				/* token, */ where, pageNumber, pageSize, temp = false,
				success, failure, showMessage, orderBy,
			}) => ({
				type: `RECEIVE_PAGE${temp ? '_TEMP' : ''}_${SCHEMA}`,
				promise: request.get(`${SERVER_URL}/${service}/${schema}`, getRequestConfig({
					params: { where: JSON.stringify(where), pageNumber, pageSize, orderBy, }
				})).then(response => {
					try {
						if (success) success(response);
					} catch (error) {
						console.error(error);
					}
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredFetchingRows'), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),

			fetchAll: ({ /* token, */ where, temp = false, success, failure, showMessage }) => ({
				type: `RECEIVE_ALL${temp ? '_TEMP' : ''}_${SCHEMA}`,
				promise: request.get(`${SERVER_URL}/${service}/${schema}/getAll`, getRequestConfig({
					params: { where: JSON.stringify(where), }
				})).then(response => {
					try {
						if (success) success(response);
					} catch (error) {
						console.error(error);
					}
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredFetchingRows'), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),

			fetchOne: (token, id, success, failure, showMessage) => ({
				type: `RECEIVE_ONE_${SCHEMA}`,
				promise: request.get(`${SERVER_URL}/${service}/${schema}/${id}`, getRequestConfig({
				})).then(response => {
					try {
						if (success) success(response);
					} catch (error) {
						console.error(error);
					}
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredFetchingRow'), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),

			createChild: (token, item, parentActions, parentId, success, failure, showMessage, params) =>
				((dispatch) => {
					// console.log('creating child');
					request.post(`${SERVER_URL}/${service}/${schema}`, item, getRequestConfig({
						params,
					})).then(response => {
						if (showMessage !== false) {
							const newItem = (response.data && response.data.items && response.data.items[0]);
							window.message.success(i18n.t('CreatedSuccessfully', newItem || {}));
						}
						dispatch(parentActions.fetchOne(token, parentId));
						if (success) success(response.data);
						return response;
					}).catch((error) => {
						console.error(error);
						if (showMessage !== false) {
							window.message.error(i18n.t('AnErrorHasOccurredCreating', item), 0,
								respErrorDetails(error));
						}
						if (failure) failure(error);
						return error;
					});
				}
				),

			create: (token, item, success, failure, showMessage, params) => ({
				type: `NEW_${SCHEMA}`,
				promise: request.post(`${SERVER_URL}/${service}/${schema}`, item, getRequestConfig({
					params,
				})).then(response => {
					if (showMessage !== false) {
						const newItem = (response.data && response.data.items && response.data.items[0]);
						window.message.success(i18n.t('CreatedSuccessfully', newItem || {}));
					}
					if (success) success(response);
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredCreating', item), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),
			updateChild: (token, item, parentActions, parentId, success, failure, showMessage) => (
				(dispatch/* , getState */) => {
					// console.error('creating child');
					request.put(`${SERVER_URL}/${service}/${schema}/${item.id}`, item, getRequestConfig({
					})).then(response => {
						if (showMessage !== false) {
							window.message.success(i18n.t('UpdatedSuccessfully', item));
						}
						dispatch(parentActions.fetchOne(token, parentId));
						if (success) success(response.data);
						return response;
					}).catch((error) => {
						console.error(error);
						if (showMessage !== false) {
							window.message.error(i18n.t('AnErrorHasOccurredUpdating', item), 0,
								respErrorDetails(error));
						}
						if (failure) failure(error);
						return error;
					});
				}
			),
			update: ({ /* token, */ item, success, failure, showMessage }) => ({
				type: `UPDATE_${SCHEMA}`,
				promise: request.put(`${SERVER_URL}/${service}/${schema}/${item.id}`, item,
					getRequestConfig({})
				).then(response => {
					if (showMessage !== false) {
						window.message.success(i18n.t('UpdatedSuccessfully', item));
					}
					if (success) success(response);
					return response;
				}).catch((error) => {
					console.error(error);
					if (showMessage !== false) {
						window.message.error(i18n.t('AnErrorHasOccurredUpdating', item), 0,
							respErrorDetails(error));
					}
					if (failure) failure(error);
					return error;
				})
			}),
			deleteChild: (token, item, parentActions, parentId, success, failure, showMessage) => (
				(dispatch) => {
					// console.log('creating child');
					request.delete(`${SERVER_URL}/${service}/${schema}/${item.id}`, getRequestConfig({
					})).then(response => {
						if (showMessage !== false) {
							window.message.success(i18n.t('DeletedSuccessfully', item));
						}
						dispatch(parentActions.fetchOne(token, parentId));
						if (success) success(response.data);
						return response;
					}).catch((resp) => {
						console.error(resp);
						if (showMessage !== false) {
							window.message.error(
								i18n.t('AnErrorHasOccurredDeleting', item), 0, respErrorDetails(resp)
							);
						}
						if (failure) failure(resp);
						return resp;
					});
				}
			),
			delete: ({ /* token, */ item, success, failure, showMessage, }) => ({
				type: `DELETE_${SCHEMA}`,
				promise: request.delete(`${SERVER_URL}/${service}/${schema}/${item.id}`, getRequestConfig({
				})).then(response => {
					if (showMessage !== false) {
						window.message.success(i18n.t('DeletedSuccessfully', item));
					}
					if (success) success(response);
					return response;
				}).catch((resp) => {
					console.error(resp);
					if (showMessage !== false) {
						window.message.error(
							i18n.t('AnErrorHasOccurredDeleting', item), 0, respErrorDetails(resp)
						);
					}
					if (failure) failure(resp);
					return resp;
				})
			}),
			postToURL: ({ /* token, */ route, path, item, params, success, failure, errorMessage }) => {
				let lRoute = `/${service}`;
				lRoute += route && route.indexOf('/') === 0 ? '' : '/';
				lRoute += route || '';
				return {
					type: 'POST_TO_URL',
					store: path || route.replace(/\//g, '_'),
					promise: request.post(`${SERVER_URL}${lRoute}`, item || {}, getRequestConfig({
						params,
					})).then(response => {
						if (success) success(response);
						return response;
					}).catch((resp) => {
						console.error(resp);
						if (errorMessage) {
							if (isFunction(errorMessage)) {
								errorMessage(resp);
							} else if (isString(errorMessage)) {
								window.message.error(errorMessage, 0, respErrorDetails(resp));
							} else {
								window.message.error(i18n.t('AnErrorHasOccurredInAction'), 0,
									respErrorDetails(resp));
							}
						}
						if (failure) failure(resp);
						return resp;
					})
				};
			}
		},

		// reducer
		makeReducer: (initialState) => (state = initialState, action) => {
			if (action && action.res) {
				if (action.res.status === 401 && !window.endedSession) {
					window.message.error(i18n.t('EndedSession'));
					window.endedSession = true;
					location.replace(config.portalURL);
				} else if (action.res.status === 403) {
					window.message.error(i18n.t('UserNotAuthorized'));
				}
			}
			switch (action.type) {
				case `RECEIVE_${SCHEMA}`:
					return Object.assign({}, state, {
						fetchedList: true,
						list: action.res.data.items
					});
				case `RECEIVE_TEMP_${SCHEMA}`:
					return Object.assign({}, state, {
						temp: action.res.data.items,
					});
				case `RECEIVE_PAGE_${SCHEMA}`:
					return Object.assign({}, state, {
						fetchedList: true,
						list: action.res.data.items,
						pageNumber: action.res.data.pageNumber,
						pageSize: action.res.data.pageSize,
						totalRows: action.res.data.totalRows,
					});
				case `RECEIVE_PAGE_TEMP_${SCHEMA}`:
					return Object.assign({}, state, {
						temp: action.res.data.items,
						tempNumber: action.res.data.pageNumber,
						tempSize: action.res.data.pageSize,
						tempRows: action.res.data.totalRows,
					});
				case `RECEIVE_ALL_${SCHEMA}`:
					return Object.assign({}, state, {
						fetchedList: true,
						all: action.res.data.items
					});
				case `RECEIVE_ALL_TEMP_${SCHEMA}`:
					return Object.assign({}, state, {
						tempAll: action.res.data.items,
					});
				case `RECEIVE_ONE_${SCHEMA}`: {
					const statusOK = action.res.status === 200;
					return Object.assign({}, state, {
						item: statusOK ? action.res.data.items[0] : {},
						items: statusOK ? mergeitem(state.items, action.res.data.items[0]) : state.items,
						editing: action.itemId
					});
				}
				case `NEW_${SCHEMA}`:
					if (action.res.status !== 200) return state;
					return Object.assign({}, state, {
						list: state.list.concat(action.res.data.items)
					});
				case `UPDATE_${SCHEMA}`:
					if (action.res.status !== 200) return state;
					return Object.assign({}, state, {
						item: action.res.data.items[0],
						// list: state.list.concat(action.res.data.items),
					});
				case `DELETE_${SCHEMA}`:
					if (action.res.status !== 200) return state;
					return Object.assign({}, state, {
						list: state.list.filter(entry => entry.id !== action.res.data.objId)
					});
				case `SET_SELECTED_${SCHEMA}`:
					return Object.assign({}, state, {
						// item: state.items[action.id]
						item: action.item,
					});
				case `SET_EDITING_${SCHEMA}`:
					return Object.assign({}, state, {
						editing: action.id
					});
				case `SET_PROP_${SCHEMA}`:
					return Object.assign({}, state, {
						[action.prop]: action.value
					});
				case `SET_PROP_OBJ_${SCHEMA}`:
					return Object.assign({}, state, action.newState);
				case 'POST_TO_URL': {
					const { store } = action;
					return Object.assign({}, state, {
						[store]: action.res.data
					});
				}
				default:
					return state;
			}
		}
	});
};

function shouldFetch(state, items) {
	if (items.isFetching) {
		return false;
	}
	if (items.length === 0) {
		return true;
	}
	return items.didInvalidate;
}

function contains(id, items) {
	return items.hasOwnProperty(id);
}

/* mutations */
function mergeitem(items, item) {
	return Object.assign({}, items, {
		[item.id]: item
	});
}

export default CommonReduxOutlet;
