/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule app
 */

import Cookies from 'cookies-js';
import config from '../../config';  //Custom configs of each app
import {
	USER_AUTH, USER_AUTH_REQUEST, USER_IS_AUTHENTICATED, USER_LOGOUT
} from '../actions';

const initialState = checkCookie();

function getBearerToken(token) {
	return `Bearer ${token}`;
}

function setCookie(expiresIn, token) {
	const cookieExpirationDate = Date.now() + expiresIn;
	Cookies.set(config.TOKEN_COOKIE_KEY, token, { expires: cookieExpirationDate });
}

function checkCookie() {
	return {
		loggedIn: true,
		token: getBearerToken(Cookies.get(config.TOKEN_COOKIE_KEY)),
		selectedAppIndex: 0,
		selectedSchemaIndex: 0,
		selectedBlockIndex: 0,
		createdApp: {}
	};
}

function destroyCookie() {
	Cookies.expire(config.TOKEN_COOKIE_KEY);
}

export default function app(state = initialState, action) {
	switch (action.type) {
		case USER_IS_AUTHENTICATED:
			return Object.assign({}, state, checkCookie());
		case USER_AUTH_REQUEST:
			return state;
		case USER_AUTH:
			setCookie(action.res.data.expires, action.res.data.token);
			return Object.assign({}, state, {
				loggedIn: true,
				token: getBearerToken(action.res.data.token),
			});
		case USER_LOGOUT:
			destroyCookie();
			return Object.assign({}, state, { loggedIn: false, token: '' });
		default:
			return state;
	}
}
