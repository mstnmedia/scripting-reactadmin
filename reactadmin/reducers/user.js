/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule user
 */

import {
	RECEIVE_ME, USER_SIGNUP, USER_ME_UPDATE
} from '../actions';

const initialState = {
	_id: ' ',
	name: ' ',
	avatar: '/dist/images/img3.jpg',
	username: '@adminuser',
	email: ' ',
	permissions: [],
	permissionsObj: {},
	receivePermissions() {
		this.permissionsObj = {};
		this.permissions.forEach(i => (
			this.permissionsObj[i] = true
		));
	},
	hasPermissions(...args) {
		if (this.isMaster()) return true;
		for (let i = 0; i < args.length; i++) {
			const permission = args[i];
			if (!this.permissionsObj[permission]) {
				return false;
			}
		}
		return true;
	},
	isMaster() {
		return this.permissionsObj['scripting.master'];
	},
	removePermissions(...args) {
		for (let i = 0; i < args.length; i++) {
			const permission = args[i];
			this.permissionsObj[permission] = false;
		}
		return true;
	},
	addPermissions(...args) {
		for (let i = 0; i < args.length; i++) {
			const permission = args[i];
			this.permissionsObj[permission] = true;
		}
		return true;
	},
};

export default function user(state = initialState, action) {
	switch (action.type) {
		case RECEIVE_ME: {
			const newState = Object.assign({}, state, action.res.data);
			newState.receivePermissions();
			window.user = newState;
			return newState;
		}
		case USER_SIGNUP:
			return state;
		case USER_ME_UPDATE: {
			const newState = Object.assign({}, state, action.res.data);
			newState.receivePermissions();
			return newState;
		}
		default:
			return state;
	}
}
