/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule reducers
 */

import { routerStateReducer } from 'redux-react-router';

// custom reducers
import app from './app';
import user from './user';

export const CommonReducers = {
	router: routerStateReducer,
	app,
	user,
};

export default CommonReducers;
