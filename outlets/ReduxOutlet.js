import request from 'axios'; /* for promise based requests */
import { CommonReduxOutlet, CommonMapStateToProps } from '../reactadmin/outlets/';
import config from '../config';  //Custom configs of each app
// import { getCookie } from '../reactadmin/Utils';

// const getAuthorizationHeader = (/* token */) => ({
// 	// Authorization: token,
	// Authorization: `Bearer ${getCookie(config.TOKEN_COOKIE_KEY)}`,
// 	//'x-access-token': token,
// });

export const mapStateToProps = (state) => {
	const commonProps = CommonMapStateToProps(state);
	const {
		alerts,
		alertsColumns,
		alertsCriterias,
		alertTriggereds,
		alertTriggeredDatas,
		docs,
		centers,
		contents,
		criterias,
		dashboard,
		delegations,
		languages,
		workflows,
		workflowCriterias,
		workflowSteps: steps,
		workflowVersions: versions,
		workflowStepContent: stepContent,
		categories,
		endCalls,
		systemTags,
		transactions,
		vTransactions,
		vTransactionSteps,
		transactionNotes,
		interfaces,
		interfaceBases,
	} = state;
	return {
		...commonProps,
		alerts,
		alertsColumns,
		alertsCriterias,
		alertTriggereds,
		alertTriggeredDatas,
		categories,
		docs,
		centers,
		contents,
		criterias,
		dashboard,
		delegations,
		endCalls,
		interfaces,
		interfaceBases,
		languages,
		steps,
		stepContent,
		systemTags,
		transactions,
		transactionNotes,
		versions,
		vTransactions,
		vTransactionSteps,
		workflows,
		workflowCriterias,
	};
};

const CustomReduxOutlet = ({ service, schema, commonOutlet }) => {
	// const gotConfig = config || window.config;
	const SERVER_URL = commonOutlet.SERVER_URL;
	const SCHEMA = commonOutlet.SCHEMA;
	let customTypes = {};
	let customActions = {};
	let customMakeReducer = null;
	switch (service) {
		case 'workflows': {
			customTypes = {
				[`PUBLISH_${SCHEMA}`]: `PUBLISH_${SCHEMA}`
			};
			customActions = {
				publish: (token, publishConfig, success, failure) => (
					{
						type: `PUBLISH_${SCHEMA}`,
						promise: request.post(`${SERVER_URL}/${service}/${schema}/publish`, publishConfig, {
							// headers: getAuthorizationHeader(token),
							withCredentials: true,
						})
							.then(response => {
								if (success) success(response.data);
								return response;
							}).catch((error) => {
								// console.log(error);
								if (failure) failure(error);
								return error;
							})
					}
				),
			};
			customMakeReducer = (initialState) => (state = initialState, action) => {
				switch (action.type) {
					case `PUBLISH_${SCHEMA}`:
						return Object.assign({}, state, {
							workflowPublished: true,
						});
					default:
						return commonOutlet.makeReducer(initialState)(state, action);
				}
			};
			break;
		}
		default:
			break;
	}

	return ({
		commonOutlet,
		...customTypes,
		actions: {
			...commonOutlet.actions,
			...customActions,
		},
		// reducer
		makeReducer: customMakeReducer || commonOutlet.makeReducer,
	});
};
export const ReduxOutlet = (service, schema) => {
	const commonOutlet = CommonReduxOutlet({ service, schema, config });
	const customizedOutlet = CustomReduxOutlet({ service, schema, commonOutlet });
	return {
		...customizedOutlet,
	};
};

export default ReduxOutlet;
