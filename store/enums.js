import i18n from '../i18n';

export const TransactionStates = {
	RUNNING: 1,
	COMPLETED: 2,
	CANCELED: 3,
};
export const TransactionStateNames = () => ({
	[TransactionStates.RUNNING]: i18n.t('TransactionStateRunning'),
	[TransactionStates.COMPLETED]: i18n.t('TransactionStateCompleted'),
	[TransactionStates.CANCELED]: i18n.t('TransactionStateCanceled'),
});

export const CategoryTypes = {
	TYPE1: 'type1',
	TYPE2: 'type2',
	TYPE3: 'type3',
	ACTION: 'action',
};
export const CategoryTypeNames = () => ({
	[CategoryTypes.TYPE1]: i18n.t('CategoryType1'),
	[CategoryTypes.TYPE2]: i18n.t('CategoryType2'),
	[CategoryTypes.TYPE3]: i18n.t('CategoryType3'),
	[CategoryTypes.ACTION]: i18n.t('CategoryTypeAction'),
});
export const CategoryTypeOptions = () => [
	{ value: CategoryTypes.TYPE1, label: i18n.t('CategoryType1') },
	{ value: CategoryTypes.TYPE2, label: i18n.t('CategoryType2') },
	{ value: CategoryTypes.TYPE3, label: i18n.t('CategoryType3') },
	{ value: CategoryTypes.ACTION, label: i18n.t('CategoryTypeAction') },
];

export const CustomerSearchTypes = {
	ADVANCED: 'advanced',
	BAN_ACCOUNT_NO: 'ban_account_no',
	CLARO_VIDEO_EMAIL: 'claro_video_email',
	CLARO_VIDEO_IDENTITY_CARD: 'claro_video_identity_card',
	CLARO_VIDEO_NO: 'claro_video_no',
	CONTACT_NAME: 'contact_name',
	CRM_TICKET_NO: 'crm_ticket_no',
	CUSTOMER_EMAIL: 'customer_email',
	CUSTOMER_ID: 'customer_id',
	CUSTOMER_NAME: 'customer_name',
	IDENTITY_CARD: 'identity_card_id',
	IMEI: 'imei',
	ORDER_NO: 'order_no',
	PASSPORT: 'passport_id',
	PHONE: 'phone_number',
	SAAM_TICKET_NO: 'saam_ticket_no',
	SIM_CARD: 'simcard',
	SMART_CARD: 'smart_card',
	GUID_PARCIAL: 'guid_parcial',
	STB_NO: 'stb_no',
	TAX_ID: 'tax_id',
	// TICKET_NO: 'ticket_no',
};
export const CustomerSearchTypeNames = () => ({
	[CustomerSearchTypes.ADVANCED]: i18n.t('CustomerSearchAdv'),
	[CustomerSearchTypes.BAN_ACCOUNT_NO]: i18n.t('BanAccountNo'),
	[CustomerSearchTypes.CLARO_VIDEO_EMAIL]: i18n.t('ClaroVideoEmail'),
	[CustomerSearchTypes.CLARO_VIDEO_IDENTITY_CARD]: i18n.t('ClaroVideoIdentityCard'),
	[CustomerSearchTypes.CLARO_VIDEO_NO]: i18n.t('ClaroVideoNo'),
	[CustomerSearchTypes.CONTACT_NAME]: i18n.t('ContactNameLastName'),
	[CustomerSearchTypes.CRM_TICKET_NO]: i18n.t('CRMTicketNo'),
	[CustomerSearchTypes.CUSTOMER_EMAIL]: i18n.t('CustomerEmail'),
	[CustomerSearchTypes.CUSTOMER_ID]: i18n.t('CustomerID'),
	[CustomerSearchTypes.CUSTOMER_NAME]: i18n.t('CustomerName'),
	[CustomerSearchTypes.IDENTITY_CARD]: i18n.t('IdentityCard'),
	[CustomerSearchTypes.IMEI]: i18n.t('IMEI'),
	[CustomerSearchTypes.ORDER_NO]: i18n.t('OrderNo'),
	[CustomerSearchTypes.PASSPORT]: i18n.t('Passport'),
	[CustomerSearchTypes.PHONE]: i18n.t('Telephone'),
	[CustomerSearchTypes.SAAM_TICKET_NO]: i18n.t('SAAMTicketNo'),
	[CustomerSearchTypes.SIM_CARD]: i18n.t('SIMCardNo'),
	[CustomerSearchTypes.SMART_CARD]: i18n.t('SmartCard'),
	[CustomerSearchTypes.GUID_PARCIAL]: i18n.t('GuidParcial'),
	[CustomerSearchTypes.STB_NO]: i18n.t('STBNo'),
	[CustomerSearchTypes.TAX_ID]: i18n.t('TaxID'),
	// [CustomerSearchTypes.TICKET_NO]: i18n.t('TicketNo'),
});

export const TableTemplateStart = '{{table=';
export const TableTemplateColumnNameSplitter = '@';
export const TableTemplateColumnsSplitter = '||';
export const TableTemplateEnd = '}}';
export const TransactionResultListTemplate = '{#}';

export const ContentTypes = {
	HTML: 1,
	IMAGE: 2,
	FILE: 3,
};
export const ContentTypeNames = () => ({
	[ContentTypes.HTML]: i18n.t('HTML'),
	[ContentTypes.IMAGE]: i18n.t('Image'),
	[ContentTypes.FILE]: i18n.t('File'),
});
export const ContentTypeOptions = () => [
	{ value: ContentTypes.HTML, label: i18n.t('HTML') },
	{ value: ContentTypes.IMAGE, label: i18n.t('Image') },
	{ value: ContentTypes.FILE, label: i18n.t('File') },
];

export const WorkflowStepTypes = {
	UNDEFINED: -1,
	REGULAR: 1,
	WORKFLOW: 2,
	EXTERNAL: 3,
};
export const WorkflowStepTypeNames = () => ({
	[WorkflowStepTypes.REGULAR]: i18n.t('Regular'),
	[WorkflowStepTypes.WORKFLOW]: i18n.t('Workflow'),
	[WorkflowStepTypes.EXTERNAL]: i18n.t('ExternalSystem'),
});
export const WorkflowStepOptionTypes = {
	STEP: 1,
	VALUE: 2,
};

export const MapInterfaceFieldFrom = {
	FIXED: 'FIXED',
	USER: 'USER',
	// INTERFACE: 1,
};

export const Interfaces = {
	SCRIPTING: {
		CurrentTime: 105,
		EmployeeInfo: 106,
		TransactionInfo: 107,

		CustomerInfoBase: -5,
		SubscriptionInfoBase: -4,
		CurrentTimeBase: -3,
		EmployeeInfoBase: -2,
		TransactionInfoBase: -1,
		TransactionInfoName: 'transaction_info',
		SubscriptionInfoName: 'subscription_info',
	},
	OMS: {
		CustomerInfo: 1001,
		ServiceInfo: 1000,
		Orders: 2300,
	},
	CRM: {
		CustomerSearch: 2000,
		CustomerInfo: {
			Name: 'customer_info'
		},
		CustomerSubscriptions: 2050,
		CaseHistories: 5000,
		Cases: 2100,
		Notes: 2110,
		Interactions: 2200,
	},
	ENSAMBLE: {
		SubscriptionByPhone: 3000,
		SubscriptionsByCustomer: 3100,
	},
};
export const InterfaceFieldTypes = {
	TEXT: 1,
	NUMBER: 2,
	OPTIONS: 3,
	EMAIL: 4,
	CHECKBOX: 5,
	DATE: 6,
	DATETIME: 7,
	RADIO: 8,
	AUTOCOMPLETE: 9,
	TEXTAREA: 10,
	LINK: 11,
	HIDDEN: 12,
	TIME: 13,
	BOOLEAN: 14,
	TAGS: 15,
	HTML: 16,
	TABLE: 17,
	PASSWORD: 18,
};
export const InterfaceFieldTypeNames = () => ({
	[InterfaceFieldTypes.TEXT]: i18n.t('TEXT'),
	[InterfaceFieldTypes.NUMBER]: i18n.t('NUMBER'),
	[InterfaceFieldTypes.OPTIONS]: i18n.t('OPTIONS'),
	[InterfaceFieldTypes.EMAIL]: i18n.t('EMAIL'),
	[InterfaceFieldTypes.CHECKBOX]: i18n.t('CHECKBOX'),
	[InterfaceFieldTypes.DATE]: i18n.t('DATE'),
	[InterfaceFieldTypes.DATETIME]: i18n.t('DATETIME'),
	[InterfaceFieldTypes.RADIO]: i18n.t('RADIO'),
	[InterfaceFieldTypes.TIME]: i18n.t('TIME'),
	[InterfaceFieldTypes.AUTOCOMPLETE]: i18n.t('AUTOCOMPLETE'),
	[InterfaceFieldTypes.TEXTAREA]: i18n.t('TEXTAREA'),
	[InterfaceFieldTypes.LINK]: i18n.t('LINK'),
	[InterfaceFieldTypes.HIDDEN]: i18n.t('HIDDEN'),
	[InterfaceFieldTypes.BOOLEAN]: i18n.t('BOOLEAN'),
	[InterfaceFieldTypes.TAGS]: i18n.t('TAGS'),
	[InterfaceFieldTypes.HTML]: i18n.t('HTML'),
	[InterfaceFieldTypes.TABLE]: i18n.t('TABLE'),
	[InterfaceFieldTypes.PASSWORD]: i18n.t('PASSWORD'),
});
export const InterfaceFieldTypesHTML = {
	[InterfaceFieldTypes.TEXT]: 'TEXT',
	[InterfaceFieldTypes.NUMBER]: 'NUMBER',
	[InterfaceFieldTypes.EMAIL]: 'EMAIL',
	[InterfaceFieldTypes.CHECKBOX]: 'CHECKBOX',
	[InterfaceFieldTypes.DATE]: 'DATE',
	[InterfaceFieldTypes.DATETIME]: 'DATETIME-LOCAL',
	[InterfaceFieldTypes.RADIO]: 'RADIO',
	[InterfaceFieldTypes.TIME]: 'TIME',
	[InterfaceFieldTypes.HIDDEN]: 'HIDDEN',
	[InterfaceFieldTypes.PASSWORD]: 'PASSWORD',
};
export const InterfaceFieldBooleanOptions = () => [
	{ value: 'false', label: i18n.t('No') },
	{ value: 'true', label: i18n.t('Yes') },
];

export const NumericBooleanOptions = () => [
	{ value: '0', label: i18n.t('No') },
	{ value: '1', label: i18n.t('Yes') },
];

export const DateFormats = {
	ISO_DATE: 'YYYY-MM-DD',
	ISO_DATE_TIME: 'YYYY-MM-DDTHH:mm:ss',
	DATE: 'DD-MM-YYYY',
	DATE_TIME: 'DD-MM-YYYY HH:mm',
	DATE_TIME_Z: 'DD/MM/YY hh:mm:ss A',
	DDMMYYYY: 'DD-MM-YYYY',
};

export const ID_GREETING_CONTENT = -1;
export const ID_END_TRANSACTION_CONTENT = -2;

export const PageNumbers = [5, 10, 15, 25, 50, 100];
export const CustomersPageSize = 5;
export const SubcriptionsPageSize = 5;
export const CasesPageSize = 5;
export const NotesPageSize = 5;
export const InteractionsPageSize = 5;
export const OrdersPageSize = 5;
export const TransactionResultsPageSize = 10;

export const filterActions = () => ({
	[i18n.t('ToSearch')]: 'search',
	[i18n.t('ToClean')]: 'clean',
});
export const filterAdvActions = (adv) => ({
	...(filterActions()),
	[i18n.t(adv ? 'SimpleSearch' : 'AdvancedSearch')]: 'advanced',
});

export const LanguagePrefix = 'language_';
export const LanguageNames = {
	[`${LanguagePrefix}es`]: 'Spanish',
	[`${LanguagePrefix}en`]: 'English',
};
export const LanguageOptions = [
	{ value: `${LanguagePrefix}es`, label: i18n.t('Spanish') },
	{ value: `${LanguagePrefix}en`, label: i18n.t('English') },
];

export const TimeUnitOptions = () => [
	{ value: '1', label: i18n.t('Minute') },
	{ value: '2', label: i18n.t('Hour') },
	{ value: '3', label: i18n.t('Day') },
	{ value: '4', label: i18n.t('Month') },
	// { value: '5', label: i18n.t('Year') },
];

export const FieldErrorStyles = {
	borderColor: '#b94a48',
    boxShadow: 'inset 0 1px 1px rgba(0, 0, 0, 0.075)',
	outline: '0 none',
};

const enums = {
	ContentTypes,
	ContentTypeNames,
	CustomerSearchTypes,
	CustomerSearchTypeNames,
	DateFormats,
	ID_GREETING_CONTENT,
	InteractionsPageSize,
	Interfaces,
	InterfaceFieldBooleanOptions,
	InterfaceFieldTypes,
	InterfaceFieldTypesHTML,
	InterfaceFieldTypeNames,
	PageNumbers,
	CustomersPageSize,
	SubcriptionsPageSize,
	CasesPageSize,
	LanguageNames,
	LanguageOptions,
	MapInterfaceFieldFrom,
	NotesPageSize,
	NumericBooleanOptions,
	TransactionResultsPageSize,
	TransactionStates,
	TransactionStateNames,
	WorkflowStepTypes,
	WorkflowStepTypeNames,
	WorkflowStepOptionTypes,
};
window.enums = enums;
export default enums;
