import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	Input,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de un idioma por usuario.
 *
 * @class LanguageModal
 * @extends {Component}
 */
class LanguageModal extends Component {

	state = {};

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('language.add') || user.hasPermissions('language.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.id_employee && item.name;
	}

	render() {
		const { id,
			id_employee: employeeID,
			employee_name: employeeName,
			name,
			language_name: languageName,
		} = this.getItem();
		return (
			<div>
				{id && <FormGroup isValid={!!id}>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input type="number" className="form-control" value={id || ''} disabled />
				</FormGroup>}

				<FormGroup isValid={!!employeeID}>
					<label className="control-label inline-block" htmlFor="employee">
						{i18n.t('Employee')}:
					</label>
					{employeeID ? <span> {employeeID}</span> : null}
					<Tooltip className="balloon" tag="TooltipUserLanguageUserField">
						<RemoteAutocomplete
							url={`${window.config.backend}/employees/employee`}
							value={employeeName || ''}
							getItemValue={(item) => NameIDText(item)}
							onFieldChange={(e, text) => {
								this.handleChange('id_employee', 0);
								this.handleChange('employee_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_employee', item.id);
								this.handleChange('employee_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_employee) {
									this.handleChange('employee_name', '');
								}
							}}
							disabled={employeeID === -1 || this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Language')}:</label>
					{/* <DropDown
						emptyOption="SelectDisabled"
						items={LanguageOptions}
						value={name}
						onChange={(e) => this.handleChange('name', e.target.value)}
						disabled={this.isFieldDisabled()}
					/> */}
					<Tooltip className="balloon" tag="TooltipUserLanguageNameField">
						<RemoteAutocomplete
							url={`${window.config.backend}/languages/systemTag`}
							value={languageName || ''}
							getItemValue={(item) => item.label}
							fetchProps={['label', 'id']}
							onFieldChange={(e, text) => {
								this.handleChange('name', '');
								this.handleChange('language_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('name', item.name);
								this.handleChange('language_name', item.label);
							}}
							onBlur={() => {
								if (!this.getItem().name) {
									this.handleChange('language_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>

				<div className="">
					<label htmlFor="note">{i18n.t('LanguageModalNote')}</label>
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(LanguageModal);
