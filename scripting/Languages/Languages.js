import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	PanelCollapsible,
	ButtonGroup,
	Tooltip,
} from '../../reactadmin/components/';
import {
	filterActions,
} from '../../store/enums';
import { setFilterHandles, NameIDText, } from '../../Utils';
import i18n from '../../i18n';
import { LanguageModal, } from './';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { DialogModal } from '../DialogModal';

const dataSchema = {
	fields: {
		language_name: i18n.t('Language'),
		employee_name: {
			label: i18n.t('User'),
			format: (row) => `${row.employee_name || ''}`,
		},
		id: i18n.t('ID'),
	}
};
const actions = ReduxOutlet('languages', 'language').actions;

/**
 * Componente que controla la pantalla del mantenimiento de idiomas por usuario.
 *
 * @class Languages
 * @extends {Component}
 */
class Languages extends Component {
	state = {
		language: {},
		languageDisabled: false,
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'languages', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('LanguagesByUser'), },
		];
		// window.Scripting_Languages = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	setItemDisabled(value) {
		this.setState({ stepDisabled: value });
	}

	cleanClick() {
		this.filterClear();
	}
	newItemClick(e) {
		e.preventDefault();
		this.setState({ language: {} });
		ModalFactory.show('languageModal');
	}

	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteLanguage'),
				description: i18n.t('WishDeleteLanguageDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, languages: { pageNumber, }, } = this.props;
							dispatch(actions.delete({
								token,
								item: { id },
								success: () => {
									ModalFactory.hide('confirmModal');
									this.fetchPage(pageNumber);
								},
							}));
						},
						disabled: () => this.state.languageDisabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleItemChange(item) {
		const { dispatch, token, languages: { pageNumber, } } = this.props;
		const success = () => {
			const page = item.id ? pageNumber : 1;
			this.itemChanged(page);
		};
		const failure = () => this.setItemDisabled(false);
		this.setItemDisabled(true);
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}

	canDelete(row) {
		const { user, } = this.props;
		return row.id !== -1 && user.hasPermissions('language.delete');
	}
	itemChanged(page) {
		ModalFactory.hide('languageModal');
		this.setItemDisabled(false);
		this.fetchPage(page);
	}

	selectItem(item) {
		// console.log('language clicked: ', item);
		this.setState({ language: Object.assign({}, item) });
		ModalFactory.show('languageModal');
	}

	renderFilters() {
		const userID = this.filterValue('id_employee');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				<div className="form-group col-sm-4">
					<label htmlFor="user">{i18n.t('User')}:</label>
					{userID ? <span> {userID}</span> : null}
					<RemoteAutocomplete
						url={`${window.config.backend}/employees/employee`}
						getItemValue={(item) => NameIDText(item)}
						value={this.filterText('employee_name') || ''}
						onFieldChange={(e, text) => {
							this.filterTextChange('employee_name', text);
							this.filterChange({ name: 'id_employee', value: undefined });
						}}
						onSelect={(text, item) => {
							this.filterTextChange('employee_name', item.name);
							this.filterChange({ name: 'id_employee', value: item.id });
						}}
						onBlur={() => {
							if (!this.filterValue('id_employee')) {
								this.filterTextChange('employee_name', '');
							}
						}}
						onKeyDown={this.onKeyDownMixed}
					/>
				</div>
				<div className="form-group col-sm-4">
					<label htmlFor="name">{i18n.t('Language')}:</label>
					<RemoteAutocomplete
						url={`${window.config.backend}/languages/systemTag`}
						value={this.filterText('name') || ''}
						onFieldChange={(e, text) => {
							this.filterTextChange('name', text);
							this.filterChange({ name: 'name', value: undefined });
						}}
						onSelect={(text, item) => {
							this.filterTextChange('name', text);
							this.filterChange({ name: 'name', value: item.name });
						}}
						onBlur={() => {
							if (!this.filterValue('name')) {
								this.filterTextChange('name', '');
							}
						}}
						onKeyDown={this.onKeyDownMixed}
					/>
				</div>

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			languages: { list, pageNumber, pageSize, totalRows, },
			user,
		} = this.props;
		const { language, languageDisabled, confirmModalProps, } = this.state;
		const languages = list || [];

		const canAdd = user.hasPermissions('language.add');
		return (
			<Page>
				<Factory
					modalref="languageModal" title={i18n.t('LanguageInfo', language)}
					factory={LanguageModal}
					item={language} disabled={languageDisabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={dataSchema}
							rows={languages}
							addTooltip={i18n.t('TooltipTableAdd', {
								screen: i18n.t('LanguagesByUser')
							})}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={(row) => this.canDelete(row)}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Languages);
