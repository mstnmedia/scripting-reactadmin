import Languages from './Languages';
import LanguageModal from './LanguageModal';
import LanguageConfigModal from './LanguageConfigModal';
import SystemTags from './SystemTags';

export {
    Languages,
    LanguageModal,
    LanguageConfigModal,
    SystemTags,
};
