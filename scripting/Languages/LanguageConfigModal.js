import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	Input, FormGroup, Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import { LanguagePrefix } from '../../store/enums';

/**
 * Componente que controla el formulario de los detalles de un idioma del sistema.
 *
 * @class LanguageConfigModal
 * @extends {Component}
 */
class LanguageConfigModal extends Component {

	state = {};

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				item.tags = {};
				const config = {
					name: item.name,
					label: item.label,
					value: JSON.stringify(item),
				};
				this.props.onCreate(config);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('language.add') || user.hasPermissions('language.edit');
	}
	isFieldDisabled() {
		const item = this.getItem();
		if ([`${LanguagePrefix}es`, `${LanguagePrefix}en`].indexOf(item.name) !== -1) {
			return true;
		}
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name
			&& item.label
			&& this.notDuplicated();
	}
	notDuplicated() {
		const item = this.getItem();
		const { systemTags: { list } } = this.props;
		const languages = list || [];
		return !languages.find(i => i.id !== item.id && i.name === item.name);
	}

	render() {
		const item = this.getItem();
		const id = item.id;
		const name = (item.name || '').replace(LanguagePrefix, '');
		const label = item.label;
		return (
			<div>
				<FormGroup isValid={!!name && this.notDuplicated()}>
					<label className="control-label" htmlFor="name">{i18n.t('LanguageCode')}:</label>
					<Tooltip className="balloon" tag="TooltipLanguageCodeField">
						<Input
							value={name || ''} placeholder="es"
							onFieldChange={(e) => {
								this.handleChange('name', `${LanguagePrefix}${e.target.value || ''}`);
							}}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!label}>
					<label className="control-label" htmlFor="label">{i18n.t('Name')}:</label>
					<Tooltip className="balloon" tag="TooltipLanguageNameField">
						<Input
							value={label || ''}
							onFieldChange={(e) => this.handleChange('label', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						onClick={() => {
							this.handleChange('name', item.json.name);
							this.handleChange('label', item.json.label);
						}}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(LanguageConfigModal);
