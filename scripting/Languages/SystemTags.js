import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Page,
	ModalFactory,
	Input, TextArea,
	Button,
	Fa,
	Factory,
	AnimatedSpinner,
	Tooltip,
} from '../../reactadmin/components/';
import { setFilterHandles, } from '../../Utils';
import i18n from '../../i18n';
import LanguageConfigModal from './LanguageConfigModal';
import { containsIgnoringCaseAndAccents } from '../../reactadmin/Utils';
import { DialogModal } from '../DialogModal';
import { LanguagePrefix } from '../../store/enums';

const actions = ReduxOutlet('languages', 'systemTag').actions;
/**
 * Componente que controla la pantalla del mantenimiento de idiomas del sistema.
 *
 * @class SystemTags
 * @extends {Component}
 */
class SystemTags extends Component {
	state = {
		textFilter: '',
		languages: [],
		item: {},
		itemDisabled: {},
		loading: false,
		updateProp: new Date(),
	}
	componentWillMount() {
		const setLoading = (value) => this.setState({ loading: value });
		const success = (resp) => {
			const languages = resp.data.items || [];
			languages.forEach(item => this.handleItemReset(item));
			this.setState({ languages });
			setLoading(false);
		};
		const failure = () => setLoading(false);
		setLoading(true);
		setFilterHandles(this, actions, 'systemTags')
			.initFetch({ success, failure });
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('SystemTags'), },
		];
		// window.Scripting_SystemTags = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getLanguages() {
		return this.state.languages || [];
	}
	getTags() {
		const { textFilter } = this.state;
		return i18n.tags
			.filter(i => !textFilter ||
				containsIgnoringCaseAndAccents(i, textFilter)
			);
	}

	disabling(id, value) {
		this.setState({
			itemDisabled: Object.assign({}, this.state.itemDisabled, { [id]: value }),
		});
	}

	handleNameClick(item, e) {
		e.preventDefault();
		this.setState({ item });
		ModalFactory.show('languageModal');
	}
	handleItemChange(item) {
		const { dispatch, token } = this.props;
		const success = (resp) => {
			ModalFactory.hide('languageModal');
			this.setState({ item: {}, });
			this.disabling(item.id, false);
			const newItem = this.handleItemReset(resp.data.items[0]);
			const languages = this.getLanguages();
			if (!item.id) {
				languages.push(newItem);
			} else {
				for (let i = 0; i < languages.length; i++) {
					if (languages[i].id === item.id) {
						languages[i] = newItem;
						break;
					}
				}
			}
			this.setState({ languages });
		};
		const failure = () => this.disabling(item.id, false);
		this.disabling(item.id, true);
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			const config = Object.assign({}, item, {
				value: JSON.stringify(item.json),
				hasChanges: undefined,
				json: undefined,
			});
			dispatch(actions.update({ token, item: config, success, failure }));
		}
	}
	handleItemReset(item) {
		const lang = item;
		try {
			lang.json = JSON.parse(lang.value);
		} catch (error) {
			console.error('Error parsing lang value: ', lang, error);
			lang.json = {};
		}
		lang.hasChanges = undefined;
		this.setState({ updateProp: new Date() });
		return lang;
	}
	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteSystemTag'),
				description: i18n.t('WishDeleteSystemTagDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, } = this.props;
							this.disabling(id, true);
							const success = () => {
								ModalFactory.hide('confirmModal');
								const languages = this.getLanguages();
								const idx = languages.findIndex(i => i.id === id);
								languages.splice(idx, 1);
								this.setState({ languages, });
								this.disabling(id, false);
							};
							const failure = () => this.disabling(id, false);
							dispatch(actions.delete({ token, item: { id }, success, failure, }));
						},
						disabled: () => this.state.itemDisabled[id],
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('language.add') || user.hasPermissions('language.edit');
	}
	isFieldDisabled(lang) {
		return this.state.itemDisabled[lang.id] || !this.isAllowedUser();
	}

	canDelete(row) {
		return this.props.user.hasPermissions('language.delete')
			&& [`${LanguagePrefix}es`, `${LanguagePrefix}en`].indexOf(row.name) === -1;
	}
	canEdit() {
		return this.props.user.hasPermissions('language.edit');
	}

	renderActions(lang) {
		const canEdit = this.canEdit();
		return (
			<div className="pull-right">
				{canEdit && lang.hasChanges &&
					<Tooltip system tag="TooltipLanguageSave">
						<Button
							type="button"
							className="btn-success btn-xs inline-block"
							style={{ marginLeft: 5, }}
							label={<Fa icon="save" />}
							// tooltip={i18n.t('SaveChanges')}
							onClick={() => this.handleItemChange(lang)}
							disabled={this.state.itemDisabled[lang.id]}
						/>
					</Tooltip>}
				{lang.hasChanges &&
					<Tooltip system tag="TooltipLanguageUndo">
						<Button
							type="button"
							className="btn-info btn-xs inline-block"
							style={{ marginLeft: 5, }}
							label={<Fa icon="undo" />}
							// tooltip={i18n.t('DiscardChanges')}
							onClick={() => this.handleItemReset(lang)}
							disabled={this.state.itemDisabled[lang.id]}
						/>
					</Tooltip>}
				{this.canDelete(lang) &&
					<Tooltip system tag="TooltipLanguageDelete">
						<Button
							type="button"
							className="btn-danger btn-xs inline-block"
							style={{ marginLeft: 5, }}
							label={<Fa icon="trash" />}
							// tooltip={i18n.t('Delete')}
							onClick={() => this.handleItemDelete(lang.id)}
							disabled={this.state.itemDisabled[lang.id]}
						/>
					</Tooltip>}
			</div>
		);
	}
	renderHeader() {
		const { user } = this.props;
		const { textFilter } = this.state;
		const canAdd = user.hasPermissions('language.add');
		return (
			<header className="panel-heading">
				<div className="-form-group col-sm-4 col-md-3">
					<label className="block" htmlFor="filter">{i18n.t('Search')}:</label>
					<Input
						clearable
						placeholder={i18n.t('Name')}
						onFieldChange={(e) => this.setState({ textFilter: e.target.value })}
						value={textFilter || ''}
					/>
				</div>
				{canAdd &&
					<span className="text-muted m-l-sm pull-right">
						<div
							className="inline"
							data-balloon={i18n.t('TooltipAddSystemTag')}
							data-balloon-pos="left"
						>
							<a
								href="#AddField" className="btn btn-success btn-xs m-t-xs"
								onClick={(e) => {
									e.preventDefault();
									this.setState({ item: {} });
									ModalFactory.show('languageModal');
								}}
							>
								<i className="fa fa-plus" /> {i18n.t('Add')}
							</a>
						</div>
					</span>
				}
			</header>
		);
	}
	render() {
		const {
			item, itemDisabled,
			loading,
			confirmModalProps,
		} = this.state;
		const tags = this.getTags();
		const languages = this.getLanguages();
		const canEdit = this.canEdit();
		return (
			<Page>
				<Factory
					modalref="languageModal" title={i18n.t('Language', item)}
					factory={LanguageConfigModal}
					item={item} disabled={itemDisabled[item.id]}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				// onUpdate={() => ModalFactory.hide('languageModal')}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<div className="col-xs-12" style={{ height: '100%', }}>
					<div className="panel panel-default" style={{ height: '99%', overflowY: 'auto', }}>
						<div
							className="panel-body"
							style={{ height: '100%', padding: 0, display: 'flex', flexDirection: 'column', }}
						>
							{this.renderHeader()}
							<div className="table-responsive">
								<table className="table table-striped table-header-fixed">
									<thead>
										<tr>
											<th>{i18n.t('Name')}</th>
											{languages.map(lang => {
												const name = (lang.name || '').replace(LanguagePrefix, '');
												return (
													<th key={lang.id}>
														<Button
															className="btn-success btn-xs inline-block"
															icon="fa-pencil"
															label={`${lang.label} (${name})`}
															onClick={(e) => this.handleNameClick(lang, e)}
															disabled={!canEdit}
														/>
														{this.renderActions(lang)}
													</th>
												);
											})}
											{loading && <th className="text-center">{i18n.t('Loading')}</th>}
										</tr>
									</thead>
									<tbody>
										{tags.map(tag => (
											<tr key={tag}>
												<td>{tag}</td>
												{languages.map((language) => {
													const lang = language;
													const value = lang.json.tags[tag];
													return (
														<td key={lang.id}>
															{canEdit ?
																<TextArea
																	value={value || ''} rows="1"
																	onFieldChange={(e) => {
																		const keys = lang.json.tags;
																		lang.hasChanges = true;
																		keys[tag] = e.target.value;
																		this.setState({ updateProp: new Date() });
																	}}
																	disabled={this.isFieldDisabled(lang)}
																/>
																:
																<span>{value}</span>
															}
														</td>
													);
												})}
												{loading && <td className="text-center">
													<AnimatedSpinner show={loading} />
												</td>}
											</tr>
										))}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</Page >
		);
	}
}

export default connect(mapStateToProps)(SystemTags);
