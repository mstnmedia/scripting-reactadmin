/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	PanelCollapsible,
	Input,
	ButtonGroup,
	DropDown,
	AdvancedFilters,
	Tooltip,
} from '../../reactadmin/components/';
import { AlertTriggeredModal, CloseMultipleModal, } from './';
import i18n from '../../i18n';
import { setFilterHandles, NameIDText, } from '../../Utils';
import { filterAdvActions, NumericBooleanOptions, DateFormats, } from '../../store/enums';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('alerts', 'alertTriggered').actions;

/**
 * Componente que controla la pantalla del mantenimiento de alertas disparadas.
 *
 * @class AlertTriggereds
 * @extends {Component}
 */
class AlertTriggereds extends Component {
	state = {
		item: { data: [], },
		closeMultiple: {},
	}
	componentWillMount() {
		// setItemHandles({ pageComponent: this, actions, itemName: 'AlertTriggered' });
		setFilterHandles(this, actions, 'alertTriggereds', false)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('AlertTriggereds'), },
		];
		// window.Scripting_AlertTriggereds = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getRows() {
		return this.props.alertTriggereds.list || [];
	}

	setItemDisabled(value) {
		let itemDisabled = (this.itemDisabled || 0) + (value ? 1 : -1);
		if (this.itemDisabled < 0) itemDisabled = 0;
		// console.log('itemDisabled', itemDisabled);
		this.itemDisabled = itemDisabled;
	}
	itemDisabled = 0;

	newItemClick(e) {
		e.preventDefault();
		this.setState({ item: { data: [], } });
		ModalFactory.show('alertTriggeredModal');
	}

	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteAlertTriggered'),
				description: i18n.t('WishDeleteAlertTriggeredDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, alertTriggereds: { pageNumber, }, } = this.props;
							dispatch(actions.delete({
								token,
								item: { id },
								success: () => {
									ModalFactory.hide('confirmModal');
									this.fetchPage(pageNumber);
								},
							}));
						},
						disabled: () => this.itemDisabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleItemChange(item) {
		const { dispatch, token, alertTriggereds: { pageNumber } } = this.props;
		const success = () => {
			const page = item.id ? pageNumber : 1;
			this.fetchPage(page);
			ModalFactory.hide('alertTriggeredModal');
			this.setItemDisabled(false);
		};
		const failure = () => this.setItemDisabled(false);
		this.setItemDisabled(true);
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}

	selectItem(item) {
		// console.log('alertTriggered clicked: ', item);
		this.setState({ item: JSON.parse(JSON.stringify(item)) });
		ModalFactory.show('alertTriggeredModal');
	}

	closeMultiple(data) {
		const { dispatch, token, alertTriggereds: { pageNumber } } = this.props;
		const rows = this.getRows().filter(i => i.checked);
		const callback = (i) => {
			this.setItemDisabled(false);
			if (i === rows.length - 1) {
				this.fetchPage(pageNumber);
				this.setState({ closingMultiple: false, });
				ModalFactory.hide('CloseMultipleModal');
			}
		};
		for (let i = 0; i < rows.length; i++) {
			const row = rows[i];
			const item = Object.assign({}, row, data, { checked: undefined, });
			this.setItemDisabled(true);
			dispatch(actions.update({
				token,
				item,
				success: () => callback(i),
				failure: () => callback(i),
			}));
		}
	}
	checkedRows(value) {
		const rows = this.getRows().filter(i => i.state);
		rows.map((row, i) => (rows[i].checked = value));
		this.updateRows();
	}
	updateRows() {
		this.setState({ updateProp: new Date() });
	}

	tableSchema() {
		const isMaster = this.props.user.isMaster();
		return {
			fields: {
				transactions_count: i18n.t('TransactionsCount'),
				first_transaction_date: {
					type: 'DateTimeZ',
					label: i18n.t('FirstTransactionDate'),
				},
				state: {
					type: 'Boolean',
					label: i18n.t('ActiveAlert'),
				},
				end_date: {
					type: 'DateTimeZ',
					label: i18n.t('EndDate'),
				},
				start_date: {
					type: 'DateTimeZ',
					label: i18n.t('TriggeredDate'),
				},
				alert_name: i18n.t('Alert'),
				center_name: isMaster ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
				selectAll: this.state.closingMultiple ? {
					label: (<span>
						{i18n.t('SelectAll')} <br />
						<input
							type="checkbox"
							onChange={(e) => this.checkedRows(e.target.checked)}
						/>
					</span>
					),
					format: (aux) => {
						const row = aux;
						return (
							<input
								type="checkbox"
								checked={row.checked}
								onChange={(e) => {
									row.checked = e.target.checked;
									this.updateRows();
								}}
							/>
						);
					},
				} : undefined,
			}
		};
	}

	renderHeader() {
		const canAdd = this.props.user.hasPermissions('alerts.add');
		const canEdit = this.props.user.hasPermissions('alerts.edit');
		const { closingMultiple, } = this.state;
		const rows = this.getRows().filter(i => i.checked);
		let btn1 = null;
		let btn2 = null;
		if (closingMultiple) {
			btn1 = rows.length
				? (
					<div
						className="inline"
						data-balloon={i18n.t('TooltipTriggeredCloseOpen')}
						data-balloon-pos="left"
					>
						<a
							href="#/CloseMultiple" className="btn btn-success btn-xs m-r-sm"
							onClick={(e) => {
								e.preventDefault();
								this.setState({
									closeMultiple: {
										end_date: moment(new Date())
											.format(DateFormats.ISO_DATE_TIME.replace(':ss', '')),
									},
								});
								ModalFactory.show('CloseMultipleModal');
							}}
						>
							<i className="fa fa-plus-" /> {i18n.t('Close')}
						</a>
					</div>
				)
				: null;
			btn2 = (
				<div
					className="inline"
					data-balloon={i18n.t('TooltipTriggeredCloseCancel')}
					data-balloon-pos="left"
				>
					<a
						href="#/Cancel" className="btn btn-danger btn-xs m-r-sm"
						onClick={(e) => {
							e.preventDefault();
							this.checkedRows(false);
							this.setState({ closingMultiple: false, });
						}}
					>
						<i className="fa fa-close-" /> {i18n.t('Cancel')}
					</a>
				</div>
			);
		} else {
			btn1 = !canEdit ? null : (
				<div
					className="inline"
					data-balloon={i18n.t('TooltipTriggeredCloseMultiple')}
					data-balloon-pos="left"
				>
					<a
						href="#/CloseMultiple" className="btn btn-info btn-xs m-r-sm"
						onClick={(e) => {
							e.preventDefault();
							this.checkedRows(false);
							this.setState({ closingMultiple: true, });
						}}
					>
						<i className="fa fa-close" /> {i18n.t('CloseMultiple')}
					</a>
				</div>
			);
			btn2 = !canAdd ? null : (
				<div
					className="inline"
					data-balloon={i18n.t('TooltipTableAdd', { screen: i18n.t('AlertTriggereds') })}
					data-balloon-pos="left"
				>
					<a
						href="#/OpenModal" className="btn btn-success btn-xs m-r-sm"
						onClick={(e) => this.newItemClick(e)}
					>
						<i className="fa fa-plus" /> {i18n.t('Add')}
					</a>
				</div>
			);
		}

		return (
			<header className="panel-heading text-right">
				{btn1}
				{btn2}
			</header>
		);
	}
	renderFilters() {
		const { user, } = this.props;
		const { where, whereObj, advancedSearch } = this.state;
		const isMaster = user.isMaster();
		const centerID = this.filterValue('id_center');
		const alertID = this.filterValue('id_alert');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group col-md-3">
							<label htmlFor="id">{i18n.t('ID')}:</label>
							<Input
								type="number" className="form-control"
								value={this.filterValue('id', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'id',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>

						<div className="form-group col-md-3">
							<label htmlFor="from">{i18n.t('FromDate')}:</label>
							<Input
								type="date" className="form-control"
								value={this.filterValue('from', '')}
								onFieldChange={(e) => this.filterChange({
									label: 'from',
									type: 'date',
									name: 'start_date',
									criteria: '>=',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="to">{i18n.t('ToDate')}:</label>
							<Input
								type="date" className="form-control"
								value={this.filterValue('to', '')}
								onFieldChange={(e) => this.filterChange({
									type: 'date',
									label: 'to',
									name: 'start_date',
									value: e.target.value,
									criteria: '<=',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{isMaster && <div className="form-group col-md-3">
							<label htmlFor="center">{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/centers/center`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('center_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('center_name', text);
									this.filterChange({ name: 'id_center', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('center_name', item.name);
									this.filterChange({ name: 'id_center', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_center')) {
										this.filterTextChange('center_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>}
						<div className="form-group col-md-3">
							<label htmlFor="alert">{i18n.t('Alert')}:</label>
							{alertID ? <span> {alertID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/alerts/alert`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('alert_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('alert_name', text);
									this.filterChange({ name: 'id_alert', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('alert_name', item.name);
									this.filterChange({ name: 'id_alert', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_alert')) {
										this.filterTextChange('alert_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="state">{i18n.t('ActiveAlert')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('state', '')}
								items={NumericBooleanOptions()}
								onChange={(e) => this.filterChange({
									name: 'state',
									value: e.target.value
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
					</div>
					:
					<AdvancedFilters
						{...{ where, whereObj }}
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
							transactions_count: { type: 'number', label: i18n.t('TransactionsCount'), onKeyDown: this.onKeyDownSimple, },
							center_name: { type: 'text', label: i18n.t('CenterName'), onKeyDown: this.onKeyDownSimple, },
							alert_name: { type: 'text', label: i18n.t('AlertName'), onKeyDown: this.onKeyDownSimple, },
							start_date: { type: 'datetime', label: i18n.t('StartDate'), onKeyDown: this.onKeyDownSimple, },
							end_date: { type: 'datetime', label: i18n.t('EndDate'), onKeyDown: this.onKeyDownSimple, },
							first_transaction_date: { type: 'datetime', label: i18n.t('FirstTransactionDate'), onKeyDown: this.onKeyDownSimple, },
							id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
							id_alert: { type: 'autocomplete', label: i18n.t('Alert'), url: `${window.config.backend}/alerts/alert`, onKeyDown: this.onKeyDownMixed, },
							state: { type: 'dropdown', label: i18n.t('ActiveAlert'), items: NumericBooleanOptions(), onKeyDown: this.onKeyDownSimple },
						}}/* eslint-enable max-len */
					/>
				}

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			alertTriggereds: { pageNumber, pageSize, totalRows, },
			user,
		} = this.props;
		const {
			item,
			confirmModalProps,
			closingMultiple, closeMultiple,
		} = this.state;
		const rows = this.getRows().filter(i => !closingMultiple || i.state);

		const canDelete = user.hasPermissions('alerts.delete') && !closingMultiple;
		const tableProps = {
			rows,
			schema: this.tableSchema(),
			...(closingMultiple ? {} : { pageSize, link: (row) => this.selectItem(row), }),
			pageNumber,
			totalRows,
			onPageChange: (data) => this.pageChange(data),
			canDelete,
			onDelete: (id) => this.handleItemDelete(id),
			renderHeader: (data) => this.renderHeader(data),
		};
		return (
			<Page>
				<Factory
					modalref="alertTriggeredModal" title={i18n.t('AlertTriggeredInfo', item)}
					factory={AlertTriggeredModal}
					item={item} disabled={this.itemDisabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				/>
				<Factory
					modalref="CloseMultipleModal" title={i18n.t('CloseMultipleInfo')}
					factory={CloseMultipleModal}
					item={closeMultiple} disabled={this.itemDisabled}
					onCreate={(data) => this.closeMultiple(data)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable {...tableProps} />
					</Panel>
				</Col>
			</Page>
		);
	}
}
export default connect(mapStateToProps)(AlertTriggereds);
