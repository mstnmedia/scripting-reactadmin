/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
    Panel,
    Col,
    Page,
    ModalFactory,
    DataTable,
    Factory,
    PanelCollapsible,
    Input,
    ButtonGroup,
    DropDown,
    AdvancedFilters,
    TagInput,
    Tooltip,
} from '../../reactadmin/components/';
import { AlertModal } from './';
import i18n from '../../i18n';
import { setFilterHandles, NameIDText, } from '../../Utils';
import { NumericBooleanOptions, filterAdvActions, TimeUnitOptions } from '../../store/enums';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('alerts', 'alert').actions;
const centerActions = ReduxOutlet('centers', 'center').actions;

/**
 * Componente que controla la pantalla del mantenimiento de alertas.
 *
 * @class Alerts
 * @extends {Component}
 */
class Alerts extends Component {
    state = {
        item: { columns: [], criterias: [] },
        itemDisabled: false,
    }
    componentWillMount() {
        setFilterHandles(this, actions, 'alerts', true)
            .initFirstPage();
        global.getBreadcrumbItems = () => [
            { label: i18n.t('Home'), to: '/' },
            { label: i18n.t('Alerts'), },
        ];
        // window.Scripting_Alerts = this;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    setItemDisabled(value) {
        this.setState({ itemDisabled: value });
    }

    fetchCenters() {
        const { dispatch, token } = this.props;
        dispatch(centerActions.fetchAll({ token }));
    }

    newItemClick(e) {
        e.preventDefault();
        this.setState({ item: { columns: [], criterias: [] } });
        ModalFactory.show('alertModal');
    }

    handleItemDelete(id) {
        this.setState({
            confirmModalProps: {
                title: i18n.t('WishDeleteAlert'),
                description: i18n.t('WishDeleteAlertDescription'),
                buttons: [
                    {
                        className: 'btn-danger',
                        label: i18n.t('Ok'),
                        onClick: () => {
                            const { dispatch, token, alerts: { pageNumber, }, } = this.props;
                            dispatch(actions.delete({
                                token,
                                item: { id },
                                success: () => {
                                    ModalFactory.hide('confirmModal');
                                    this.fetchPage(pageNumber);
                                },
                            }));
                        },
                        disabled: () => this.state.itemDisabled,
                    },
                    {
                        className: 'btn-default',
                        label: i18n.t('Close'),
                        onClick: () => ModalFactory.hide('confirmModal'),
                    },
                ],
            },
        }, () => ModalFactory.show('confirmModal'));
    }
    handleItemChange(item) {
        const { dispatch, token, alerts: { pageNumber } } = this.props;
        const success = () => {
            const page = item.id ? pageNumber : 1;
            this.itemChanged(page);
        };
        const failure = () => this.setItemDisabled(false);
        this.setItemDisabled(true);
        if (!item.id) {
            dispatch(actions.create(token, item, success, failure));
        } else {
            dispatch(actions.update({ token, item, success, failure }));
        }
    }

    itemChanged(page) {
        ModalFactory.hide('alertModal');
        this.setItemDisabled(false);
        this.fetchPage(page);
    }

    selectItem(item) {
        // console.log('alert clicked: ', item);
        this.setState({ item: JSON.parse(JSON.stringify(item)) });
        ModalFactory.show('alertModal');
    }

    tableSchema() {
        const isMaster = this.props.user.isMaster();
        const dataSchema = {
            fields: {
                last_activation: {
                    type: 'DateTimeZ',
                    label: i18n.t('LastActivationDate'),
                },
                active: {
                    type: 'Boolean',
                    label: i18n.t('ActiveAlert'),
                },
                tags: {
                    label: i18n.t('Tags'),
                    format: (row) => (<TagInput inline value={row.tags} />)
                },
                name: i18n.t('Name'),
                center_name: isMaster ? i18n.t('Center') : undefined,
                id: i18n.t('ID'),
            }
        };
        return dataSchema;
    }

    renderFilters() {
        const { where, whereObj, advancedSearch } = this.state;
        const isMaster = this.props.user.isMaster();
        const centerID = this.filterValue('id_center');
        return (
            <PanelCollapsible
                title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
                contentStyle={{ padding: 5 }}
            >
                {!advancedSearch ?
                    <div>
                        <div className="form-group col-md-3">
                            <label htmlFor="id">{i18n.t('ID')}:</label>
                            <Input
                                type="number" className="form-control"
                                value={this.filterValue('id', '')}
                                onFieldChange={(e) => this.filterChange({
                                    name: 'id',
                                    value: e.target.value,
                                })}
                                onKeyDown={this.onKeyDownSimple}
                            />
                        </div>
                        {isMaster && <div className="form-group col-md-3">
                            <label htmlFor="center">{i18n.t('Center')}:</label>
                            {centerID ? <span> {centerID}</span> : null}
                            <RemoteAutocomplete
                                url={`${window.config.backend}/centers/center`}
                                getItemValue={(item) => NameIDText(item)}
                                value={this.filterText('center_name') || ''}
                                onFieldChange={(e, text) => {
                                    this.filterTextChange('center_name', text);
                                    this.filterChange({ name: 'id_center', value: undefined });
                                }}
                                onSelect={(text, item) => {
                                    this.filterTextChange('center_name', item.name);
                                    this.filterChange({ name: 'id_center', value: item.id });
                                }}
                                onBlur={() => {
                                    if (!this.filterValue('id_center')) {
                                        this.filterTextChange('center_name', '');
                                    }
                                }}
                                onKeyDown={this.onKeyDownMixed}
                            />
                        </div>}
                        <div className="form-group col-md-3">
                            <label htmlFor="name">{i18n.t('Name')}:</label>
                            <Input
                                type="text" className="form-control"
                                value={this.filterValue('name', '')}
                                onFieldChange={(e) => this.filterChange({
                                    name: 'name',
                                    value: e.target.value,
                                    criteria: 'like',
                                })}
                                onKeyDown={this.onKeyDownSimple}
                            />
                        </div>
                        <div className="form-group col-md-3">
                            <label htmlFor="tags">{i18n.t('Tags')}:</label>
                            <TagInput
                                value={this.filterValue('tags', '')} placeholder={i18n.t('Tags')}
                                onChange={({ tags, value }) => this.filterChange({
                                    label: 'tags',
                                    value: tags.length ? value : undefined,
                                    group: tags.map(i => ({
                                        name: 'tags',
                                        criteria: 'like',
                                        value: i
                                    })),
                                })}
                                onKeyDown={this.onKeyDownMixed}
                            />
                        </div>
                        <div className="form-group col-md-3">
                            <label htmlFor="active">{i18n.t('ActiveAlert')}:</label>
                            <DropDown
                                emptyOption
                                value={this.filterValue('active', '')}
                                items={NumericBooleanOptions()}
                                onChange={(e) => this.filterChange({
                                    name: 'active',
                                    value: e.target.value
                                })}
                                onKeyDown={this.onKeyDownSimple}
                            />
                        </div>
                    </div>
                    :
                    <AdvancedFilters
                        {...{ where, whereObj }}
                        fields={{/* eslint-disable max-len */
                            id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
                            time_quantity: { type: 'number', label: i18n.t('TimeQuantity'), onKeyDown: this.onKeyDownSimple, },
                            quantity: { type: 'number', label: i18n.t('Quantity'), onKeyDown: this.onKeyDownSimple, },
                            center_name: { type: 'text', label: i18n.t('CenterName'), onKeyDown: this.onKeyDownSimple, },
                            name: { type: 'text', label: i18n.t('Name'), onKeyDown: this.onKeyDownSimple, },
                            emails: { type: 'text', label: i18n.t('Emails'), onKeyDown: this.onKeyDownSimple, },
                            email_subject: { type: 'text', label: i18n.t('EmailSubject'), onKeyDown: this.onKeyDownSimple, },
                            tags: { type: 'tags', label: i18n.t('Tags'), onKeyDown: this.onKeyDownMixed, },
                            note: { type: 'clob', label: i18n.t('Notes'), onKeyDown: this.onKeyDownSimple, },
                            email_text: { type: 'clob', label: i18n.t('EmailText'), onKeyDown: this.onKeyDownSimple, },
                            docdate: { type: 'datetime', label: i18n.t('CreationDate'), onKeyDown: this.onKeyDownSimple, },
                            last_activation: { type: 'datetime', label: i18n.t('LastActivationDate'), onKeyDown: this.onKeyDownSimple, },
                            id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
                            active: { type: 'dropdown', label: i18n.t('ActiveAlert'), items: NumericBooleanOptions(), onKeyDown: this.onKeyDownSimple },
                            time_unit: { type: 'dropdown', label: i18n.t('TimeUnit'), items: TimeUnitOptions(), onKeyDown: this.onKeyDownSimple },
                        }}/* eslint-enable max-len */
                    />
                }
                <div className="col-xs-12 inline-block">
                    <hr style={{ marginTop: 5, marginBottom: 5 }} />
                    <ButtonGroup
                        items={{ values: filterAdvActions(advancedSearch), }}
                        onChange={(action) => this[`${action}Click`]()}
                    />
                </div>
            </PanelCollapsible>
        );
    }
    render() {
        const { alerts: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
        const { item, itemDisabled, confirmModalProps, } = this.state;
        const rows = list || [];

        const canAdd = user.hasPermissions('alerts.add');
        const canDelete = user.hasPermissions('alerts.delete');
        return (
            <Page>
                <Factory
                    modalref="alertModal" title={i18n.t('AlertInfo', item)}
                    factory={AlertModal} large
                    item={item} disabled={itemDisabled}
                    onCreate={(newItem) => this.handleItemChange(newItem)}
                    onUpdate={(newItem) => this.handleItemChange(newItem)}
                />
                <DialogModal
                    modalref="confirmModal"
                    {...confirmModalProps}
                />
                <Col>
                    {this.renderFilters()}
                    <Panel>
                        <DataTable
                            {...{ rows, pageSize, pageNumber, totalRows }}
                            onPageChange={(data) => this.pageChange(data)}
                            schema={this.tableSchema()}
                            onAdd={canAdd && ((e) => this.newItemClick(e))}
                            addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Alerts') })}
                            canDelete={canDelete}
                            onDelete={(id) => this.handleItemDelete(id)}
                            link={(row) => this.selectItem(row)}
                        />
                    </Panel>
                </Col>
            </Page>
        );
    }
}
export default connect(mapStateToProps)(Alerts);
