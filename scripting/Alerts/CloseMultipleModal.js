import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	FormGroup,
	Input,
	TextArea,
} from '../../reactadmin/components';
import i18n from '../../i18n';

/**
 * Componente que controla el formulario para cerrar multiples alertas.
 *
 * @class CloseMultipleModal
 * @extends {Component}
 */
class CloseMultipleModal extends Component {

	state = {
		name: '',
		value: '',
	}
	componentWillReceiveProps(props) {
		if (this.props.item !== props.item) {
			this.setState({ name: '', value: '', });
		}
		// window.Scripting_CloseMultipleModal = this;
	}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}
	handleDataChange(prop, value) {
		this.setState({ [prop]: value });
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			item.state = 0;
			this.props.onCreate(item);
		}
	}
	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('alerts.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.end_date;
	}

	render() {
		const { end_date: endDate, description, parent_ticket: parentTicket, action, } = this.getItem();
		return (
			<form>
				<FormGroup isValid={!!endDate}>
					<label className="control-label" htmlFor="endDate">{i18n.t('EndDate')}:</label>
					<Input
						type="datetime-local"
						value={endDate}
						onFieldChange={(e) => this.handleChange('end_date', e.target.value)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="ticket">{i18n.t('ParentTicket')}:</label>
					<Input
						value={parentTicket} placeholder={i18n.t('ParentTicket')}
						onFieldChange={(e) => this.handleChange('parent_ticket', e.target.value)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="action">{i18n.t('Action')}:</label>
					<Input
						value={action} placeholder={i18n.t('Action')}
						onFieldChange={(e) => this.handleChange('action', e.target.value)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="code">{i18n.t('Description')}:</label>
					<TextArea
						name="description"
						style={{ height: '100%', resize: 'vertical', }}
						placeholder={i18n.t('Description')}
						value={description}
						onFieldChange={(e) => this.handleChange('description', e.target.value)}
						rows="10"
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>{i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(CloseMultipleModal);
