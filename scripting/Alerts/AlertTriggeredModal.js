import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Row, Col,
	DataTable,
	FormGroup,
	Input,
	DropDown,
	Button,
	NumericInput,
	TextArea,
	Tooltip,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { NumericBooleanOptions } from '../../store/enums';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de una alerta disparada.
 *
 * @class AlertTriggeredModal
 * @extends {Component}
 */
class AlertTriggeredModal extends Component {
	state = {
		name: '',
		value: '',
	}
	componentWillReceiveProps(props) {
		if (this.props.item !== props.item) {
			this.setState({ name: '', value: '', });
		}
		// window.Scripting_AlertTriggeredModal = this;
	}

	getItem(props) {
		return (props || this.props).item || {};
	}
	getDatas() {
		return (this.getItem().data || [])
			.map((i, idx) => Object.assign({}, i, { idx }));
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}
	handleDataChange(prop, value) {
		this.setState({ [prop]: value });
	}

	addData() {
		const { name, value } = this.state;
		const { id, id_center: centerID, data, } = this.getItem();
		if (name) {
			const newData = {
				id_center: centerID,
				id_alert_triggered: id,
				name,
				value,
			};
			data.push(newData);
			this.setState({ name: '', value: '', });
		}
	}
	deleteData(idx) {
		if (idx === -1) return;
		const { data } = this.getItem();
		data.splice(idx, 1);
		this.setState({});
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				item.id_employee = this.props.user.id;
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}
	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('alerts.add', 'alerts.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const isMaster = this.props.user.isMaster();
		const item = this.getItem();
		return (!isMaster || item.id_center)
			&& item.state !== undefined
			&& item.alert_name
			&& item.start_date
			&& ((item.state * 1) || item.end_date)
			&& item.first_transaction_date
			&& item.transactions_count
			&& item.data && item.data.length;
	}

	renderAddData() {
		const { name, value, } = this.state;
		const item = this.getItem();
		const canEdit = !item.id ? this.props.user.hasPermissions('alerts.add')
			: this.props.user.hasPermissions('alerts.edit');
		return (canEdit &&
			<div className={`m-t-md ${this.isFieldDisabled() ? 'hidden' : ''}`}>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={item.data.length || !!name}>
							<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								value={name || ''}
								onFieldChange={(e) => {
									this.handleDataChange('name', e.target.value);
								}}
							/>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<label className="control-label" htmlFor="value">{i18n.t('Value')}:</label>
						<Input
							value={value || ''}
							onFieldChange={(e) => {
								this.handleDataChange('value', e.target.value);
							}}
						/>
					</Col>
				</Row>
				<Row classes="text-right">
					<Col>
						<Button
							className="btn btn-info m-t-sm"
							label={i18n.t('Add')}
							onClick={(e) => this.addData(e)}
							disabled={this.isDisabled() || !name}
						/>
					</Col>
				</Row>
			</div>
		);
	}
	render() {
		const {
			id,
			id_center: centerID, center_name: centerName,
			id_alert: alertID, alert_name: alertName,
			start_date: startDate, end_date: endDate,
			first_transaction_date: firstTransactionDate,
			description, parent_ticket: parentTicket, action,
			state, transactions_count: transactionsCount,
		} = this.getItem();
		const datas = this.getDatas();

		const { user, } = this.props;
		const isMaster = user.isMaster();
		const canDelete = !this.isDisabled() && user.hasPermissions('alerts.delete');
		return (
			<form>
				<FormGroup isValid={state !== undefined}>
					<label className="control-label" htmlFor="active">{i18n.t('ActiveAlert')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredActiveField">
						<DropDown
							emptyOption="SelectDisabled"
							items={NumericBooleanOptions()}
							value={`${state}` || ''}
							onChange={(e) => this.handleChange('state', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				{id && <FormGroup>
					<label className="control-label" htmlFor="name">{i18n.t('ID')}:</label>
					<Input type="number" value={id || ''} disabled={this.isFieldDisabled()} />
				</FormGroup>}

				{isMaster && <FormGroup>
					<label
						className="control-label inline-block"
						htmlFor="center"
					>{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							getItemValue={(item) => NameIDText(item)}
							value={centerName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
								if (item.id !== centerID) {
									this.handleChange('id_alert', 0);
									this.handleChange('alert_name', '');
								}
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={id || this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>}
				<FormGroup isValid={!!alertID}>
					<label className="control-label inline-block" htmlFor="alert">{i18n.t('Alert')}:</label>
					{alertID ? <span> {alertID}</span> : null}
					<Tooltip className="balloon" tag="TooltipTriggeredAlertField">
						<RemoteAutocomplete
							url={`${window.config.backend}/alerts/alert`}
							getItemValue={(item) => NameIDText(item)}
							value={alertName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_alert', 0);
								this.handleChange('alert_name', text);
							}}
							preWhere={[
								...(centerID ? [{ name: 'id_center', value: centerID }] : []),
							]}
							onSelect={(text, item) => {
								this.handleChange('id_alert', item.id);
								this.handleChange('alert_name', item.name);
								this.handleChange('id_center', item.id_center);
								this.handleChange('center_name', item.center_name);
							}}
							onBlur={() => {
								if (!this.getItem().id_alert) {
									this.handleChange('alert_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>


				<FormGroup isValid={!!startDate}>
					<label className="control-label" htmlFor="startDate">{i18n.t('StartDate')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredStartField">
						<Input
							type="datetime-local"
							value={startDate || ''}
							onFieldChange={(e) => {
								this.handleChange('start_date', e.target.value);
								if (endDate < startDate) this.handleChange('end_date', startDate);
							}}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={(state * 1) || !!endDate}>
					<label className="control-label" htmlFor="endDate">{i18n.t('EndDate')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredEndField">
						<Input
							type="datetime-local"
							value={endDate || ''}
							onFieldChange={(e) => {
								this.handleChange('end_date', e.target.value);
								if (endDate < startDate) this.handleChange('end_date', startDate);
							}}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!firstTransactionDate}>
					<label className="control-label" htmlFor="firstTransactionDate">
						{i18n.t('FirstTransactionDate')}:
					</label>
					<Tooltip className="balloon" tag="TooltipTriggeredFirstField">
						<Input
							type="datetime-local"
							value={firstTransactionDate || ''}
							onFieldChange={(e) => this.handleChange('first_transaction_date', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				<FormGroup isValid={!!transactionsCount}>
					<label className="control-label" htmlFor="transactions_count">
						{i18n.t('TransactionsCount')}:
					</label>
					<Tooltip className="balloon" tag="TooltipTriggeredCountField">
						<NumericInput
							natural
							value={transactionsCount || ''}
							onFieldChange={(e) => this.handleChange('transactions_count', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="ticket">{i18n.t('ParentTicket')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredParentField">
						<Input
							value={parentTicket || ''} placeholder={i18n.t('ParentTicket')}
							onFieldChange={(e) => this.handleChange('parent_ticket', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="action">{i18n.t('Action')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredActionField">
						<Input
							value={action || ''} placeholder={i18n.t('Action')}
							onFieldChange={(e) => this.handleChange('action', e.target.value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="code">{i18n.t('Description')}:</label>
					<Tooltip className="balloon" tag="TooltipTriggeredDescriptionField">
						<TextArea
							name="description"
							style={{ height: '100%', resize: 'vertical', }}
							placeholder={i18n.t('Description')}
							value={description || ''}
							onFieldChange={(e) => this.handleChange('description', e.target.value)}
							rows="8"
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				<FormGroup isValid={datas.length}>
					<Tooltip tag="TooltipTriggeredDataField">
						<label className="control-label" htmlFor="data" style={{ marginBottom: -10 }}>
							{i18n.t('Data')}:
						</label>
					</Tooltip>
					<DataTable
						emptyRow
						schema={{
							fields: {
								value: i18n.t('Value'),
								name: i18n.t('Name'),
								id: i18n.t('ID'),
							},
						}}
						rows={datas}
						rowKey={(row) => row.idx}
						canDelete={canDelete}
						onDelete={(idx) => this.deleteData(idx)}
						rowKey={(row) => row.idx}
					/>
				</FormGroup>
				{this.renderAddData()}

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(AlertTriggeredModal);
