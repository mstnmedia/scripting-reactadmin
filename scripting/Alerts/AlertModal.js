import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Input,
	NumericInput,
	TextArea,
	DropDown,
	Col, Row,
	DataTable,
	TagInput,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { TimeUnitOptions } from '../../store/enums';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';
import ReactSummernoteInput from '../../reactadmin/components/ui/ReactSummernoteInput';

/**
 * Componente que controla el formulario de los detalles de una alerta.
 *
 * @class AlertModal
 * @extends {Component}
 */
class AlertModal extends Component {

	state = {}

	getItem(props) {
		return (props || this.props).item || {};
	}
	getColumns() {
		return (this.getItem().columns || [])
			.map((i, idx) => Object.assign({}, i, { idx }));
	}
	getCriterias() {
		return (this.getItem().criterias || [])
			.map((i, idx) => Object.assign({}, i, { idx }));
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}
	handleStateChange(prop, value) {
		this.setState({ [prop]: value });
	}

	addColumn() {
		const { columnName: name, } = this.state;
		const { id, columns, } = this.getItem();
		const column = {
			id_alert: id,
			column: name,
		};
		columns.push(column);
		this.setState({ columnName: '' });
	}
	deleteColumn(idx) {
		if (idx === -1) return;
		const { columns, } = this.getItem();
		columns.splice(idx, 1);
		this.setState({});
	}

	addCriteria() {
		const { criteriaName: column, criteriaValue: value, } = this.state;
		const { id, criterias, } = this.getItem();
		const criteria = {
			id_alert: id,
			column,
			value,
		};
		criterias.push(criteria);
		this.setState({ criteriaName: '', criteriaValue: '', });
	}
	deleteCriteria(idx) {
		if (idx === -1) return;
		const { criterias, } = this.getItem();
		criterias.splice(idx, 1);
		this.setState({});
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}
	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('alerts.add') || user.hasPermissions('alerts.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const isMaster = this.props.user.isMaster();
		const item = this.getItem();
		return (!isMaster || item.id_center)
			&& item.name
			&& (!item.active || (
				item.quantity
				&& item.time_unit && item.time_quantity
				&& item.emails && item.email_text && item.email_subject
				&& item.columns && item.columns.length
			));
	}

	renderAddColumn() {
		const { columnName, } = this.state;
		return (
			<div className={`m-t-md ${this.isFieldDisabled() ? 'hidden' : ''}`}>
				<Row>
					<Col size={{ xs: 12 }}>
						<label className="block" htmlFor="name">{i18n.t('Name')}:</label>
						<Tooltip className="balloon" tag="TooltipAlertColumnField">
							<Input
								notWhiteSpaces
								type="text" className="form-control"
								value={columnName || ''}
								onFieldChange={(e) => this.setState({ columnName: e.target.value })}
							/>
						</Tooltip>
					</Col>
				</Row>
				<Row>
					<Col className="text-right">
						<button
							type="button" className="btn btn-info btn-sm m-t-sm"
							onClick={(e) => this.addColumn(e)}
							disabled={this.isDisabled() || !columnName}
						>{i18n.t('Add')}</button>
					</Col>
				</Row>
			</div>
		);
	}
	renderAddCriteria() {
		const { criteriaName, criteriaValue } = this.state;
		return (
			<div className={`m-t-md ${this.isFieldDisabled() ? 'hidden' : ''}`}>
				<Row>
					<Col size={{ sm: 6 }}>
						<label className="block" htmlFor="name">{i18n.t('Name')}:</label>
						<Tooltip className="balloon" tag="TooltipAlertCriteriaNameField">
							<Input
								notWhiteSpaces
								type="text" className="form-control"
								value={criteriaName || ''}
								onFieldChange={(e) => this.setState({ criteriaName: e.target.value })}
							/>
						</Tooltip>
					</Col>
					<Col size={{ sm: 6 }}>
						<label className="block" htmlFor="value">{i18n.t('Value')}:</label>
						<Tooltip className="balloon" tag="TooltipAlertCriteriaValueField">
							<Input
								type="text" className="form-control"
								value={criteriaValue || ''}
								onFieldChange={(e) => this.setState({ criteriaValue: e.target.value })}
							/>
						</Tooltip>
					</Col>
				</Row>
				<Row>
					<Col className="text-right">
						<button
							type="button" className="btn btn-info btn-sm m-t-sm"
							onClick={(e) => this.addCriteria(e)}
							disabled={this.isDisabled() || !criteriaName}
						>{i18n.t('Add')}</button>
					</Col>
				</Row>
			</div>
		);
	}
	render() {
		const { user, } = this.props;
		const { id,
			id_center: centerID,
			center_name: centerName,
			name, tags, note,
			docdate: createdDate, last_activation: lastActivation,
			quantity, time_unit: timeUnit, time_quantity: timeQuantity,
			active, emails, email_subject: emailSubject, email_text: emailText,
		} = this.getItem();

		const docdate = (createdDate || ''); //.split('T')[0];
		const lastActivationDate = (lastActivation || ''); //.split('T')[0];
		const columns = this.getColumns();
		const criterias = this.getCriterias();
		const isMaster = user.isMaster();
		const canDelete = !this.isDisabled() && user.hasPermissions('alerts.delete');
		return (
			<form>
				<Row>
					{id && <Col size={{ sm: 6 }}>
						<FormGroup isValid={!!id}>
							<label className="control-label" htmlFor="name">{i18n.t('ID')}:</label>
							<Input type="number" className="form-control" value={id} disabled />
						</FormGroup>
					</Col>}
					{isMaster && <Col size={{ sm: 6 }}>
						<FormGroup isValid={!!centerID}>
							<label
								className="control-label inline-block"
								htmlFor="center"
							>{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<Tooltip className="balloon" tag="TooltipCenterField">
								<RemoteAutocomplete
									url={`${window.config.backend}/centers/center`}
									getItemValue={(item) => NameIDText(item)}
									value={centerName || ''}
									onFieldChange={(e, text) => {
										this.handleChange('id_center', 0);
										this.handleChange('center_name', text);
									}}
									onSelect={(text, item) => {
										this.handleChange('id_center', item.id);
										this.handleChange('center_name', item.name);
									}}
									disabled={id || this.isFieldDisabled()} required
								/>
							</Tooltip>
						</FormGroup>
					</Col>}
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!!name}>
							<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								type="text" className="form-control"
								value={name || ''} placeholder={i18n.t('Name')}
								onFieldChange={(e) => {
									this.handleChange('name', e.target.value);
								}}
								disabled={this.isFieldDisabled()} required
							/>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="tags">{i18n.t('Tags')}:</label>
							<Tooltip className="balloon" tag="TooltipTagField">
								<TagInput
									value={tags} placeholder={i18n.t('Tags')}
									onChange={({ value }) => this.handleChange('tags', value)}
									disabled={this.isFieldDisabled()} required
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="docdate">{i18n.t('CreationDate')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertCreationField">
								<Input
									type="datetime-local" className="form-control"
									value={docdate || ''} disabled
								/>
							</Tooltip>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="lastActivation">
								{i18n.t('LastActivationDate')}:
							</label>
							<Tooltip className="balloon" tag="TooltipAlertActivationField">
								<Input
									type="datetime-local" className="form-control"
									value={lastActivationDate || ''} disabled
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="active">{i18n.t('ActiveAlert')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertActiveField">
								<Input
									type="checkbox"
									className="inline" checked={!!active}
									containerStyle={{ margin: 5, }}
									onFieldChange={(e) => this.handleChange('active', e.target.checked ? 1 : 0)}
									disabled={this.isFieldDisabled()}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!active || !!timeUnit}>
							<label className="control-label" htmlFor="timeUnit">{i18n.t('TimeUnit')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertUnitField">
								<DropDown
									emptyOption={<option disabled={!!timeUnit}>{i18n.t('Select')}</option>}
									items={TimeUnitOptions()}
									value={timeUnit || ''}
									onChange={(e) => this.handleChange('time_unit', e.target.value)}
									disabled={this.isFieldDisabled()}
									required={!!active}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!active || !!timeQuantity}>
							<label className="control-label" htmlFor="timeQuantity">
								{i18n.t('TimeQuantity')}:
							</label>
							<Tooltip className="balloon" tag="TooltipAlertTimeField">
								<NumericInput
									natural
									value={timeQuantity || ''}
									onFieldChange={(e) => this.handleChange('time_quantity', e.target.value * 1)}
									disabled={this.isFieldDisabled()}
									required={!!active}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!active || !!quantity}>
							<label className="control-label" htmlFor="quantity">{i18n.t('Quantity')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertQuantityField">
								<NumericInput
									natural
									value={quantity || ''}
									onFieldChange={(e) => this.handleChange('quantity', e.target.value * 1)}
									disabled={this.isFieldDisabled()}
									required={!!active}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="notes">{i18n.t('Notes')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertNotesField">
								<TextArea
									className="form-control" value={note || ''}
									onFieldChange={(e) => {
										this.handleChange('note', e.target.value);
									}}
									disabled={this.isFieldDisabled()}
									required={!!active}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!active || !!emails}>
							<label className="control-label" htmlFor="emails">{i18n.t('Emails')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertEmailField">
								<TagInput
									value={emails} placeholder={i18n.t('Emails')}
									onChange={({ value }) => this.handleChange('emails', value)}
									disabled={this.isFieldDisabled()}
									required={!!active}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={!active || !!emailSubject}>
							<label className="control-label" htmlFor="subject">{i18n.t('EmailSubject')}:</label>
							<Tooltip className="balloon" tag="TooltipAlertSubjectField">
								<Input
									value={emailSubject} placeholder={i18n.t('EmailSubject')}
									onFieldChange={(e) => {
										this.handleChange('email_subject', e.target.value);
									}}
									disabled={this.isFieldDisabled()}
								/>
							</Tooltip>
						</FormGroup>
					</Col>
				</Row>

				<Row>
					<Col size={{ sm: 12 }}>
						<FormGroup isValid={!active || !!emailText}>
							<Tooltip position="top" tag="TooltipAlertTextField">
								<label className="control-label" htmlFor="notes">{i18n.t('EmailText')}:</label>
							</Tooltip>
							<ReactSummernoteInput
								value={emailText}
								onChange={(html) => this.handleChange('email_text', html)}
								disabled={this.isFieldDisabled()}
							/>
						</FormGroup>
					</Col>
				</Row>

				<Row>
					<Col size={{ sm: 6 }}>
						<FormGroup isValid={active && columns.length}>
							<label className="control-label" htmlFor="columns" style={{ marginBottom: -10 }}>
								{active && columns.length ? '' : '* '}
								{i18n.t('AlertColumns')}:
							</label>
							<DataTable
								emptyRow
								schema={{
									fields: {
										column: i18n.t('Name'),
										id: i18n.t('ID'),
									},
								}}
								rows={columns}
								rowKey={(row) => row.idx}
								canDelete={canDelete}
								onDelete={(idx) => this.deleteColumn(idx)}
							/>
							{this.renderAddColumn()}
						</FormGroup>
					</Col>
					<Col size={{ sm: 6 }}>
						<FormGroup>
							<label className="control-label" htmlFor="criterias" style={{ marginBottom: -10 }}>
								{i18n.t('AlertCriterias')}:
							</label>
							<DataTable
								emptyRow
								schema={{
									fields: {
										value: i18n.t('Value'),
										column: i18n.t('Name'),
										id: i18n.t('ID'),
									},
								}}
								rows={criterias}
								rowKey={(row) => row.idx}
								canDelete={canDelete}
								onDelete={(idx) => this.deleteCriteria(idx)}
							/>
							{this.renderAddCriteria()}
						</FormGroup>
					</Col>
				</Row>
				<div>
					<hr />
					{
						this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(AlertModal);
