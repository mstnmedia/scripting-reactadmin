import React, { Component } from 'react';
import i18n from '../i18n';

/**
 * Componente que muestra un modal de confirmación con una descripción de lo que se le solicita.
 *
 * @export
 * @class ConfirmModal
 * @extends {Component}
 */
export class ConfirmModal extends Component {

	getDescription() {
		return this.props.description || '';
	}
	getButtons() {
		const { onClickOk, onClickCancel, disabled } = this.props;
		return [
			{ label: i18n.t('Agree'), onClick: onClickOk, className: 'btn-info', disabled, },
			{ label: i18n.t('Cancel'), onClick: onClickCancel, className: 'btn-danger', disabled },
		];
	}
	render() {
		return (
			<div role="form">
				<div className="form-group">{this.getDescription()}</div>
				<div className="">
					<hr />
					{this.getButtons().map(({ label, className, ...rest }, i) => (
						<button
							key={i}
							type="button"
							className={`btn ${className}`}
							{...rest}
						>{label}</button>
					))}
				</div>
			</div>
		);
	}
}
export default ConfirmModal;
