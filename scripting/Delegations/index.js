import Delegations from './Delegations';
import DelegationModal from './DelegationModal';

export {
    Delegations,
    DelegationModal,
};
