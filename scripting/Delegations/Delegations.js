import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	Input,
	PanelCollapsible,
	ButtonGroup,
	Tooltip,
} from '../../reactadmin/components/';
import {
	filterActions,
} from '../../store/enums';
import { setFilterHandles, } from '../../Utils';
import i18n from '../../i18n';
import { DelegationModal, } from './';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('delegations', 'delegation').actions;

/**
 * Componente que controla la pantalla del mantenimiento de delegaciones.
 *
 * @class Delegations
 * @extends {Component}
 */
class Delegations extends Component {
	state = {
		item: {},
		itemDisabled: false,
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'delegations', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Delegations'), },
		];
		// window.Scripting_Delegations = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	setItemDisabled(value) {
		this.setState({ stepDisabled: value });
	}

	newItemClick(e) {
		e.preventDefault();
		this.setState({ item: {} });
		ModalFactory.show('delegationModal');
	}

	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteDelegation'),
				description: i18n.t('WishDeleteDelegationDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, delegations: { pageNumber, }, } = this.props;
							dispatch(actions.delete({
								token,
								item: { id },
								success: () => {
									ModalFactory.hide('confirmModal');
									this.fetchPage(pageNumber);
								},
							}));
						},
						disabled: () => this.state.itemDisabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleItemChange(item) {
		const { dispatch, token, delegations: { pageNumber, } } = this.props;
		const success = () => {
			const page = item.id ? pageNumber : 1;
			ModalFactory.hide('delegationModal');
			this.setItemDisabled(false);
			this.fetchPage(page);
		};
		const failure = () => this.setItemDisabled(false);
		this.setItemDisabled(true);
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}

	canDelete() {
		const { user, } = this.props;
		return user.hasPermissions('delegations.delete');
	}

	selectItem(item) {
		// console.log('delegation clicked: ', item);
		this.setState({ item: Object.assign({}, item) });
		ModalFactory.show('delegationModal');
	}

	renderFilters() {
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="id">{i18n.t('ID')}:</label>
					<Input
						type="number" className="form-control"
						value={this.filterValue('id', '')}
						onFieldChange={(e) => this.filterChange({
							name: 'id',
							value: e.target.value,
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="user">{i18n.t('User')}:</label>
					<Input
						type="text" className="form-control"
						value={this.filterValue('user', '')}
						onFieldChange={(e) => {
							const separator = 'or';
							const criteria = 'like';
							const value = e.target.value;
							this.filterChange({
								label: 'user',
								value,
								group: [
									{ separator, name: 'id_user', criteria, value, },
									{ separator, name: 'user_name', criteria, value, },
								]
							});
						}}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="delegate">{i18n.t('Delegate')}:</label>
					<Input
						type="text" className="form-control"
						value={this.filterValue('delegate', '')}
						onFieldChange={(e) => {
							const separator = 'or';
							const criteria = 'like';
							const value = e.target.value;
							this.filterChange({
								label: 'delegate',
								value,
								group: [
									{ separator, name: 'id_delegate', criteria, value, },
									{ separator, name: 'delegate_name', criteria, value, },
								]
							});
						}}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="endDate">{i18n.t('EndDate')}:</label>
					<Input
						type="date" className="form-control"
						value={this.filterValue('end_date', '')}
						onFieldChange={(e) => this.filterChange({
							type: 'date',
							name: 'end_date',
							value: e.target.value,
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			delegations: { list, pageNumber, pageSize, totalRows, },
			user,
		} = this.props;
		const { item, itemDisabled, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('delegations.add');
		return (
			<Page>
				<Factory
					modalref="delegationModal" title={i18n.t('DelegationInfo', item)}
					factory={DelegationModal}
					item={item} disabled={itemDisabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={{
								fields: {
									end_date: {
										type: 'Date',
										label: i18n.t('EndDate'),
									},
									delegate_name: {
										label: i18n.t('Delegate'),
										format: (row) => `${row.delegate_name || ''} (${row.id_delegate})`,
									},
									user_name: {
										label: i18n.t('User'),
										format: (row) => `${row.user_name || ''} (${row.id_user})`,
									},
									id: i18n.t('ID'),
								}
							}}
							addTooltip={i18n.t('TooltipTableAdd', {
								screen: i18n.t('Delegations')
							})}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={(row) => this.canDelete(row)}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Delegations);
