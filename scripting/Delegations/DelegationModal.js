import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	Input, FormGroup, Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import { NameIDText } from '../../Utils';
import RemoteAutocomplete from '../RemoteAutocomplete';

/**
 * Componente que controla el formulario de los detalles de una delegación.
 *
 * @class DelegationModal
 * @extends {Component}
 */
class DelegationModal extends Component {

	state = {};

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('delegations.add') || user.hasPermissions('delegations.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.id_user && item.id_delegate && item.end_date
			&& item.id_user !== item.id_delegate;
	}


	render() {
		const { id,
			id_user: user, user_name: userName,
			id_delegate: delegate, delegate_name: delegateName,
			end_date: endDate,
		} = this.getItem();

		return (
			<form>
				{id && <FormGroup>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input className="form-control" value={id || ''} disabled />
				</FormGroup>}

				<FormGroup isValid={!!user && user !== delegate}>
					<label className="control-label" htmlFor="user">{i18n.t('User')}:</label>
					{user ? <span> {user}</span> : null}
					<Tooltip className="balloon" tag="TooltipDelegationUserField">
						<RemoteAutocomplete
							wrapperStyle={{ display: 'block' }}
							value={userName || ''}
							url={`${window.config.backend}/employees/employee`}
							getItemValue={(item) => NameIDText(item)}
							onFieldChange={(e, text) => {
								this.handleChange('id_user', 0);
								this.handleChange('user_name', text);
							}}
							onSelect={(text, item) => {
								// console.log('autocomplete selected: ', text, item);
								this.handleChange('id_user', item.id);
								this.handleChange('user_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_user) {
									this.handleChange('user_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>

				<FormGroup isValid={!!delegate && user !== delegate}>
					<label className="control-label" htmlFor="delegate">{i18n.t('Delegate')}:</label>
					{delegate ? <span> {delegate}</span> : null}
					<Tooltip className="balloon" tag="TooltipDelegationDelegateField">
						<RemoteAutocomplete
							wrapperStyle={{ display: 'block' }}
							value={delegateName || ''}
							url={`${window.config.backend}/employees/employee`}
							getItemValue={(item) => NameIDText(item)}
							onFieldChange={(e, text) => {
								this.handleChange('id_delegate', 0);
								this.handleChange('delegate_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_delegate', item.id);
								this.handleChange('delegate_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_delegate) {
									this.handleChange('delegate_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>

				<FormGroup isValid={!!endDate}>
					<label className="control-label" htmlFor="endDate">{i18n.t('EndDate')}:</label>
					<Tooltip className="balloon" tag="TooltipDelegationEndField">
						<Input
							type="date" className="form-control"
							value={/* dateWithoutTime */(endDate) || ''}
							onFieldChange={(e) => {
								// this.handleChange('end_date', dateWithTimeZone(e.target.value));
								this.handleChange('end_date', e.target.value);
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() && <button
						type="submit" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(DelegationModal);
