import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	DataTable,
	Row, Col, Page,
	Panel, Button,
	ModalFactory, Factory,
	Input, TagInput, AnimatedSpinner,
} from '../../reactadmin/components/';

import {
	DependentWorkflowsModal,
	WorkflowVersionsModal,
	WorkflowPublishResultModal,
	WorkflowPublishModal,
} from '../Workflows/';
import i18n from '../../i18n';
import { containsIgnoringCaseAndAccents } from '../../reactadmin/Utils';
import { WorkflowStepTypes, WorkflowStepTypeNames, DateFormats } from '../../store/enums';
import { DialogModal } from '../DialogModal';

const stepsSchema = {
	fields: {
		order_index: i18n.t('Order'),
		// initial_step: {
		// 	type: 'Boolean',
		// 	label: i18n.t('InitialStep'),
		// },
		id_type: {
			label: i18n.t('Type'),
			format: (row) => WorkflowStepTypeNames()[row.id_type],
		},
		name: i18n.t('Name'),
		id: i18n.t('ID'),
	}
};

const actions = ReduxOutlet('workflows', 'workflow').actions;
const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;
const workflowVersionActions = ReduxOutlet('workflows', 'workflowVersion').actions;

/**
 * Componente que muestra la configuración de un flujo para la versión seleccionada.
 *
 * @class PreviousWorkflow
 * @extends {Component}
 */
class PreviousWorkflow extends Component {
	state = {
		textFilter: '',
		loadingPrevious: false,
		publish: {},
		publishDisabled: false,
	}
	componentWillMount() {
		this.init(this.props);
		// window.Scripting_PreviousWorkflow = this;
	}

	componentWillReceiveProps(props) {
		if (props.params.id_workflow !== this.props.params.id_workflow ||
			props.params.id_version !== this.props.params.id_version) {
			this.init(props);
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getBreadcrumbItems() {
		const { workflows: { item: workflow }, params, } = this.props;
		const workflowID = params.id_workflow;
		const versionID = params.id_version;
		const workflowName = (workflow.name || `#${workflowID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Workflows'), to: '/data/workflows' },
			{ label: `${i18n.t('Workflow')} ${workflowName}`, to: `/data/workflows/${workflowID}`, },
			{ label: `${i18n.t('Version')} ${versionID}`, },
		];
	}
	getItem() {
		const {
			workflows,
			params: { id_workflow: workflowID, id_version: versionID, },
		} = this.props;
		const path = `previousWorkflow_${workflowID}_${versionID}`;
		const data = workflows[path];
		if (data && data.items) {
			return data.items[0];
		}
		return {};
	}
	getCurrentVersion() {
		const { workflows, params } = this.props;
		const current = workflows[`workflow_${params.id_workflow}_active`];
		if (current && current.items) {
			return current.items[0];
		}
		return {};
	}
	setPublishDisabled(value) {
		this.setState({ publishDisabled: value });
	}
	setWorkflowDisabled(value) {
		this.setState({ workflowDisabled: value });
	}

	init(props) {
		this.fetchPreviousWorkflow(props);
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
	}

	fetchWorkflowDependentSteps(pageNumber) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		const where = [
			{ name: 'id_type', value: WorkflowStepTypes.WORKFLOW },
			{ name: 'id_workflow_external', value: item.id },
		];
		const orderBy = 'id_workflow, id_workflow_version, initial_step, order_index';
		const success = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const failure = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const newState = {
			modalTitle: i18n.t('DependentWorkflowsFromWorkflow', item),
			fetchingDependents: true,
			temp: [],
		};
		if (pageNumber === 1) {
			newState.tempNumber = pageNumber;
			newState.tempRows = 0;
			newState.fetchDependentSteps = (page) => this.fetchWorkflowDependentSteps(page);
		}
		dispatch(workflowStepActions.updateStateObj(newState));
		dispatch(workflowStepActions.fetchPage({
			token,
			temp: true,
			where,
			orderBy,
			pageNumber,
			success,
			failure,
		}));
	}
	fetchVersions() {
		const { dispatch, token, params } = this.props;
		const where = [
			{ name: 'id_workflow', value: params.id_workflow },
		];
		dispatch(workflowVersionActions.fetch(token, where, '', true));
	}
	fetchPreviousWorkflow(props) {
		const { params: { id_workflow: workflowID, id_version: versionID, }, } = props || this.props;
		const path = `previousWorkflow_${workflowID}_${versionID}`;
		const {
			dispatch, token,
			workflows: { [path]: previousWorkflowData, },
		} = this.props;

		const success = (/* data */) => {
			this.setState({ loadingPrevious: false });
		};
		const failure = (error) => {
			console.error('fetchPreviousWorkflow', error);
			this.setState({ loadingPrevious: false });
		};
		if (this.isSuccessPostResponse(previousWorkflowData)) {
			success(previousWorkflowData);
		} else {
			this.setState({ loadingPrevious: true });
			dispatch(actions.setProp(path, {}));
			dispatch(actions.postToURL({
				token,
				route: `workflow/${workflowID}/version/${versionID}`,
				path,
				success: (data) => success(data.data),
				failure,
			}));
		}
	}
	fetchCurrentVersion() {
		const { token, dispatch, params } = this.props;
		dispatch(actions.postToURL({ token, route: `workflow/${params.id_workflow}/active` }));
	}

	handleReduxChange(prop, value) {
		const { dispatch, } = this.props;
		dispatch(actions.setProp(prop, value));
	}
	handleStepClick(step) {
		const workflowID = step.id_workflow;
		const versionID = step.version_id_version;
		const stepID = step.id;
		this.props.router.push(
			`/data/workflows/${workflowID}/version/${versionID}/step/${stepID}`
		);
	}

	isSuccessPostResponse(data) {
		return (data && data.code === 200);
	}

	openPublishModal({ e, item }) {
		this.fetchCurrentVersion();
		const isItemWorkflow = !item.workflow_name;
		this.setState({
			publish: {
				id_workflow: isItemWorkflow ? item.id : item.id_workflow,
				id_workflow_version: isItemWorkflow ? item.version_id : item.id,
				version: item.active === 0 ? '' : item.id_version,
				notes: item.active === 0 ? '' : item.notes,
				isItemWorkflow: isItemWorkflow || item.active === 0,
			}
		});
		ModalFactory.show('publishModal', e);
	}
	publishWorkflow(config) {
		const { dispatch, token } = this.props;
		// TODO: Retornar el flujo con la nueva versión para actualizar esta pantalla
		//		 evitando llamar fetchCurrentVersion.
		if (!config.version) {
			ModalFactory.show('publishErrorModal');
		} else {
			const item = Object.assign({}, config, { isItemWorkflow: undefined, });
			this.setPublishDisabled(true);
			dispatch(actions.postToURL({
				token,
				route: 'workflow/publish',
				item,
				success: (resp) => {
					this.setPublishDisabled(false);
					if (resp.data.items && resp.data.items.result < 3) {
						ModalFactory.hide('publishModal');
					}
					ModalFactory.show('publishResultModal');
					this.fetchCurrentVersion();
				},
				failure: () => this.setPublishDisabled(false),
			}));
		}
	}

	renderPublishErrorModal() {
		const modalref = 'publishErrorModal';
		const title = i18n.t('IncompleteInfo');
		const description = i18n.t('PublishMissingVersion');
		const buttons = [{
			key: 'ok',
			label: i18n.t('Ok'),
			onClick: () => ModalFactory.hide('publishErrorModal'),
			className: 'btn-info',
		}];
		return (
			<DialogModal key="3" {...{ modalref, title, description, buttons }} />
		);
	}
	renderDependentsModal() {
		const {
			steps: {
				temp: dependentSteps,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
				fetchingDependents: loading,
				modalTitle = i18n.t('DependentWorkflows'),
				fetchDependentSteps,
			},
			router,
		} = this.props;
		return (
			<Factory
				modalref="dependentWorkflowsModal" factory={DependentWorkflowsModal} large
				title={modalTitle} canBeClose backdrop="true" keyboard="true"
				{...{ dependentSteps, pageNumber, pageSize, totalRows, loading, router, }}
				fetchDependentSteps={(newPage) => fetchDependentSteps(newPage)}
			/>
		);
	}
	renderStepsColumn() {
		const item = this.getItem();
		const { textFilter, loadingPrevious, } = this.state;
		const onFilterChange = (filter = '') => {
			this.setState({ textFilter: filter });
		};

		let steps = [];
		if (item && item.workflow_step) {
			steps = item.workflow_step;
		}
		const totalRows = steps.length;
		const rows = steps.filter(i => containsIgnoringCaseAndAccents(i.name, textFilter));
		const emptyRow = ({ columnNo }) => (
			<tr>
				<td colSpan={columnNo}>
					{loadingPrevious ? i18n.t('Loading') : i18n.t('NoResults')}.
				</td>
			</tr>
		);
		return (
			<Col size="7" className="col-md-pull-5">
				<Panel title={i18n.t('Steps')} style={{ backgroundColor: 'grey' }}>
					<header className="panel-heading">
						<div className="form-group col-sm-4 pull-right">
							<label htmlFor="filter">{i18n.t('Filter')}:</label>
							<Input
								clearable
								placeholder={i18n.t('Name')}
								onFieldChange={(e) => onFilterChange(e.target.value)}
								value={textFilter || ''}
							/>
						</div>
					</header>
					<DataTable
						{...{ schema: stepsSchema, emptyRow, }}
						{...{ rows, totalRows, }}
						link={(row) => this.handleStepClick(row)}
					/>
				</Panel>
			</Col>
		);
	}
	render() {
		const { publish, publishDisabled, loadingPrevious, } = this.state;
		const { router, user, } = this.props;
		const item = this.getItem();

		const current = this.getCurrentVersion();
		const canPublish = user.hasPermissions('workflows.publish');
		return (
			<Page>
				{canPublish && [
					<Factory
						key="1" modalref="publishModal" title={i18n.t('PublishDescription', item)}
						item={publish} factory={WorkflowPublishModal}
						onCreate={(config) => this.publishWorkflow(config)}
						workflow={item} current={current}
						disabled={publishDisabled}
					/>,
					<Factory
						key="2" modalref="publishResultModal" title={i18n.t('WorkflowPublishResult', item)}
						factory={WorkflowPublishResultModal}
					/>,
					this.renderPublishErrorModal()
				]}
				{this.renderDependentsModal()}
				<Factory
					modalref="workflowVersionsModal" large
					title={i18n.t('WorkflowVersions', { workflowName: item.name })}
					factory={WorkflowVersionsModal}
					openPublishModal={(...args) => this.openPublishModal(...args)}
					router={router}
					publish={publish}
					disabled={publishDisabled}
				/>
				<Col size="5" className="col-md-push-7">
					<Panel 
						title={loadingPrevious
							? <AnimatedSpinner show /> 
							: `${i18n.t('Workflow')} ${item.name || ''}`}
					>
						<Row style={{ textAlign: 'center' }}>
							<Button
								label={i18n.t('DependentWorkflows')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={() => {
									this.fetchWorkflowDependentSteps(1);
									ModalFactory.show('dependentWorkflowsModal');
								}}
								disabled={!item || !item.id}
							/>
							<Button
								label={i18n.t('OtherWorkflowVersions')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={() => {
									this.fetchVersions();
									ModalFactory.show('workflowVersionsModal');
								}}
								disabled={!item || !item.id}
							/>
						</Row>
						<div>
							<h4>
								<AnimatedSpinner show={loadingPrevious} />
								{i18n.t('Data')}:
							</h4>
							<strong>{i18n.t('ID')}: </strong> {item.id}<br />
							<strong>{i18n.t('Name')}: </strong> {item.name}<br />
							<strong>{i18n.t('Tags')}: </strong> <TagInput inline value={item.tags} /><br />
							<strong>{i18n.t('Main')}: </strong>
							{item.id && i18n.t((!!item.main).toString())}<br />
							<strong>{i18n.t('Disabled')}: </strong> {
								item.id && i18n.t((!!item.deleted).toString())
							}<br />
							<strong>{i18n.t('Version')}: </strong> {item.version_id_version || 'N/A'}<br />
							<strong>{i18n.t('PublishedDate')}: </strong> {item.version_docdate
								? moment(item.version_docdate).format(DateFormats.DATE_TIME_Z)
								: i18n.t('N_A')}<br />
							<strong>{i18n.t('Notes')}: </strong> {item.version_notes}<br />
						</div>
					</Panel>
				</Col>
				{this.renderStepsColumn()}
			</Page>
		);
	}
}

export default connect(mapStateToProps)(PreviousWorkflow);
