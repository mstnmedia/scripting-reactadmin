import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Page,
	Panel, Col,
	ModalFactory, Factory,
	AnimatedSpinner, Button,
	DataTable,
} from '../../reactadmin/components/';

import { WorkflowStepTypes, ContentTypes, WorkflowStepTypeNames, } from '../../store/enums';
import {
	WorkflowStepContentItem,
	WorkflowStepOptionItem,
	WorkflowStepOptionModal,
	WorkflowStepContentModal,
	WorkflowStepModal,
} from '../Workflows/';
import i18n from '../../i18n';
import { ContentModal } from '../Contents';
import { ContentDetailModal } from '../Transactions';

const { UNDEFINED, REGULAR, WORKFLOW, EXTERNAL } = WorkflowStepTypes;

const interfaceActions = ReduxOutlet('interfaces', 'interfaceBase').actions;
const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;

/**
 * Componente que muestra la configuración de un paso del flujo actual para la versión seleccionada.
 *
 * @class PreviousWorkflowStep
 * @extends {Component}
 */
class PreviousWorkflowStep extends Component {
	state = {
		option: {},
		stepContent: {},
		content: {},
	}
	componentWillMount() {
		this.init(this.props);
		// window.Scripting_PreviousWorkflowStep = this;
	}
	componentWillReceiveProps(props) {
		if (props.params.id_step !== this.props.params.id_step) {
			this.init(props);
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getBreadcrumbItems() {
		const {
			id_workflow: workflowID,
			id_version: versionID,
			id_step: stepID,
		} = this.props.params;
		const step = this.getItem();
		const workflow = this.getWorkflow();
		const workflowName = (workflow.name || `#${workflowID}`);
		const stepName = (step.name || `#${stepID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Workflows'), to: '/data/workflows' },
			{ label: `${i18n.t('Workflow')} ${workflowName}`, to: `/data/workflows/${workflowID}`, },
			{
				label: `${i18n.t('Version')} ${versionID}`,
				to: `/data/workflows/${workflowID}/version/${versionID}`
			},
			{ label: `${i18n.t('Step')} ${stepName}`, },
		];
	}
	getWorkflow() {
		const {
			workflows,
			params: { id_workflow: workflowID, id_version: versionID, },
		} = this.props;
		const path = `previousWorkflow_${workflowID}_${versionID}`;
		// console.log('getItem path: ', path);
		const data = workflows[path];
		if (data && data.items) {
			return data.items[0];
		}
		return {};
	}
	getItem() {
		const {
			steps,
			params: {
				id_workflow: workflowID,
				id_version: versionID,
				id_step: stepID,
			},
		} = this.props;
		const path = `previousWorkflowStep_${workflowID}_${versionID}_${stepID}`;
		const data = steps[path];
		return data || {};
	}
	getSystem(id) {
		const { interfaceBases: { list }, } = this.props;
		const inter = (list || []).find(i => i.id === id);
		return inter || {};
	}
	getSystemName(item) {
		if (item.id_type === WorkflowStepTypes.EXTERNAL) {
			const inter = this.getSystem(item.id_workflow_external);
			return inter.label;
		}
	}
	getDependentSteps() {
		const { steps: { dependentSteps, }, } = this.props;
		return dependentSteps || [];
	}

	goToPreviousWorkflowStep({ value: stepID }) {
		const {
			id_workflow: workflowID,
			id_version: versionID,
		} = this.props.params;
		this.props.router.push(
			`/data/workflows/${workflowID}/version/${versionID}/step/${stepID}`
		);
	}

	init(props) {
		this.fetchPreviousWorkflow(props);
		this.fetchPreviousWorkflowStep(props);
		this.fetchDependentSteps(props);
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
	}

	fetchPreviousWorkflow(props) {
		const {
			id_workflow: workflowID,
			id_version: versionID,
		} = props.params;
		const path = `previousWorkflow_${workflowID}_${versionID}`;
		// console.log('getItem path: ', path);
		const {
			dispatch, token,
			workflows: { [path]: previousWorkflowData, },
		} = props;

		const success = (/* data */) => {
			// console.log('fetchPreviousWorkflow', data);
		};
		const failure = (error) => {
			console.error('fetchPreviousWorkflow', error);
		};
		if (this.isSuccessPostResponse(previousWorkflowData)) {
			success(previousWorkflowData);
		} else {
			dispatch(workflowActions.postToURL({
				token,
				route: `workflow/${workflowID}/version/${versionID}`,
				path,
				success: (data) => success(data.data),
				failure,
			}));
		}
	}
	fetchPreviousWorkflowStep(props) {
		const {
			id_workflow: workflowID,
			id_version: versionID,
			id_step: stepID,
		} = props.params;
		const path = `previousWorkflowStep_${workflowID}_${versionID}_${stepID}`;
		const {
			dispatch, token,
			interfaceBases: { list, },
			steps: { [path]: previousWorkflowData, },
		} = props;

		dispatch(workflowStepActions.setProp('item', {}));
		const success = (resp) => {
			// console.log('fetchPreviousWorkflow', data);
			dispatch(workflowStepActions.setProp('isFetching', false));
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const step = (data.items || [])[0];
				if (step && step.id_type === EXTERNAL && !list.length) {
					this.fetchInterfaces();
				}
			}
		};
		const failure = (error) => {
			console.error('fetchPreviousWorkflow', error);
			dispatch(workflowStepActions.setProp('isFetching', false));
		};
		if (this.isSuccessPostResponse(previousWorkflowData)) {
			success(previousWorkflowData);
		} else {
			dispatch(workflowStepActions.setProp('isFetching', true));
			dispatch(workflowStepActions.postToURL({
				token,
				route: 'workflowstep/getStep',
				path,
				item: { id: stepID, value: 'true', },
				success,
				failure,
			}));
		}
	}
	fetchDependentSteps(props) {
		const { token, dispatch, params } = (props || this.props);
		dispatch(workflowStepActions.setProp('fetchingDependentSteps', true));
		dispatch(workflowStepActions.setProp('dependentSteps', []));
		dispatch(workflowStepActions.postToURL({
			token,
			route: `workflowstep/${params.id_step}/getDependentSteps`,
			success: (resp) => {
				dispatch(workflowStepActions.setProp('fetchingDependentSteps', false));
				dispatch(workflowStepActions.setProp('dependentSteps', resp.data));
			},
			failure: (error) => {
				dispatch(workflowStepActions.setProp('fetchingDependentSteps', false));
				window.message.error(i18n.t('ErrorFetchingDependentSteps'));
				console.error(error);
			},
		}));
	}
	fetchInterfaces() {
		const { dispatch, token, } = this.props;
		dispatch(interfaceActions.fetch(token));
	}

	isSuccessPostResponse(data) {
		return (data && data.code === 200);
	}

	openOptionDetail(option, e) {
		e.preventDefault();

		this.setState({ option: Object.assign({}, option) });
		ModalFactory.show('wsOptionModal');
	}
	openStepContentDetail(stepContent, e) {
		e.preventDefault();
		this.setState({ stepContent: Object.assign({}, stepContent) });
		ModalFactory.show('wsStepContentModal');
	}
	openContentModal(content) {
		// console.log('content clicked: ', content);
		const newItem = JSON.parse(JSON.stringify(content));
		newItem.oldType = newItem.id_type;
		newItem.oldValue = newItem.value;
		this.setState({ item: newItem });
		ModalFactory.show('addContentModal');
		this.setState({ content: newItem }, () => ModalFactory.show('wsContentModal'));
	}
	openContentDetail() {
		ModalFactory.show('wsContentDetailModal');
	}

	renderSpinner() {
		const { steps: { isFetching }, } = this.props;
		return (
			<AnimatedSpinner key="spinner" show={isFetching} />
		);
	}
	renderContentList() {
		const item = this.getItem();
		if (!item) return null;

		let stepContents = item.workflow_step_content || [];
		if (stepContents && stepContents.length) {
			stepContents = stepContents.map(sC => (
				<WorkflowStepContentItem
					key={sC.id} item={sC} step={item}
					onClick={(e) => this.openStepContentDetail(sC, e)}
				/>
			));
		} else if (this.props.steps.isFetching) {
			stepContents = i18n.t('Loading');
		} else if (item.id_type === REGULAR || item.id_type === EXTERNAL) {
			stepContents = `${i18n.t('NoContents')}.`;
		} else {
			stepContents = `${i18n.t('CantAddContentsToStep')}.`;
		}

		return (
			<Panel title={[this.renderSpinner(), i18n.t('Contents')]}>
				{stepContents}
			</Panel>
		);
	}
	renderOptionList() {
		const item = this.getItem();
		if (!item) return null;

		let options = item.workflow_step_option || [];
		if (options && options.length) {
			options = options.map(option => (
				<WorkflowStepOptionItem
					key={option.id} item={option} step={item}
					onClick={(e) => this.openOptionDetail(option, e)}
					onStepClick={(o, e) => this.goToPreviousWorkflowStep(o, e)}
				/>
			));
		} else if (this.props.steps.isFetching) {
			options = i18n.t('Loading');
		} else if (item.id_type === REGULAR) {
			options = `${i18n.t('NoOptions')}.`;
		} else {
			options = `${i18n.t('CantAddOptionsToStep')}.`;
		}
		return options;
	}
	renderDependentSteps() {
		const { steps: { fetchingDependentSteps: isFetching, }, } = this.props;
		const rows = this.getDependentSteps();
		return (
			<Panel
				title={[
					<AnimatedSpinner key="spin" show={isFetching} />,
					i18n.t('DependentWorkflowSteps')
				]}
			>
				<DataTable
					emptyRow loading={isFetching}
					{...{ rows, totalRows: rows.length, }}
					schema={{
						fields: {
							order_index: i18n.t('Order'),
							id_type: {
								label: i18n.t('Type'),
								format: (row) => WorkflowStepTypeNames()[row.id_type],
							},
							name: i18n.t('Name'),
							id: i18n.t('ID'),
						}
					}}
					// link={(row) => {
					// 	this.props.router.push(`/data/workflows/${row.id_workflow}/step/${row.id}`);
					// }}
					link={({ id: value }) => this.goToPreviousWorkflowStep({ value })}
				/>
			</Panel>
		);
	}
	render() {
		const {
			workflows: { item: workflow },
			routeParams: { id_step: stepID, },
		} = this.props;
		const { option, stepContent, content, } = this.state;
		const item = this.getItem();
		return (
			<Page>
				<Factory
					modalref="wsStepModal" title={i18n.t('StepInfo', { ...item, workflow, })}
					factory={WorkflowStepModal} large
					item={item} workflow={workflow}
					disabled
				/>
				<Factory
					modalref="wsOptionModal" title={i18n.t('OptionInfo',
						{ ...option, step: item, workflow, })}
					factory={WorkflowStepOptionModal}
					item={option} step={item}
					disabled
				/>
				<Factory
					modalref="wsStepContentModal" title={i18n.t('ContentInfo',
						{ ...stepContent, step: item, workflow, })}
					factory={WorkflowStepContentModal} large
					item={stepContent} step={item}
					openContentModal={(newContent) => this.openContentModal(newContent)}
					disabled
				/>
				<Factory
					modalref="wsContentModal" title={i18n.t('ContentInfo',
						{ ...content, step: item, workflow, })}
					factory={ContentModal} large hideDependents
					item={content} disabled
					openContentDetail={() => this.openContentDetail()}
				/>
				<Factory
					modalref="wsContentDetailModal" large
					factory={ContentDetailModal}
					title={(content.id_type !== ContentTypes.HTML && content.filename) || content.name}
					content={content}
					results={[]}
				/>
				<Col size={7}>
					{this.renderContentList()}
					{this.renderDependentSteps()}
				</Col>
				<Col size={5}>
					<Panel>
						<div style={{ textAlign: 'center' }}>
							<Button
								label={i18n.t('Info')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={(e) => {
									e.preventDefault();
									ModalFactory.show('wsStepModal');
								}}
								disabled={item.id !== (stepID * 1)}
							/>
						</div>
						<div>
							<h4>{this.renderSpinner()}{i18n.t('Data')}:</h4>
							<div><strong>{i18n.t('ID')}: </strong>{item.id}</div>
							<div><strong>{i18n.t('Name')}: </strong>{item.name}</div>
							<div><strong>{i18n.t('OrderNo')}: </strong>{item.order_index}</div>
							<div>
								<strong>{i18n.t('Type')}: </strong>{(() => {
									if (item.id_type === REGULAR) {
										return i18n.t('Regular');
									} else if (item.id_type === WORKFLOW) {
										return i18n.t('Workflow');
									} else if (item.id_type === EXTERNAL) {
										return i18n.t('ExternalSystem');
									} else if (item.id_type === UNDEFINED) {
										return i18n.t('Undefined');
									}
								})()}
							</div>

							{(item.id_type === WORKFLOW) &&
								<div>
									<strong>{i18n.t('Workflow')}: </strong>
									<Link to={item.workflow && `/data/workflows/${item.id_workflow_external}`} >
										{item.workflow && item.workflow.name} ({item.id_workflow_external})
									</Link>
								</div>
							}
							{(item.id_type === EXTERNAL) &&
								<div>
									<strong>{i18n.t('SystemName')}: </strong>
									<Link to={`/data/interfaces/${item.id_workflow_external}`} >
										{this.getSystemName(item)} ({item.id_workflow_external})
									</Link>
								</div>
							}
						</div>
						<hr />
						<h4>{this.renderSpinner()}{i18n.t('Options')}:</h4>
						<div>{this.renderOptionList()}</div>
					</Panel>
				</Col>
			</Page >
		);
	}
}

export default connect(mapStateToProps)(PreviousWorkflowStep);
