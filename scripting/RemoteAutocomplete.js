import React, { Component } from 'react';
import ReactAutocomplete from 'react-autocomplete';
import { connect } from 'react-redux';
import request from 'axios';

import { mapStateToProps } from '../outlets/ReduxOutlet';
import { isString, isFunction } from '../reactadmin/Utils';

/**
 * Componente que permite al usuario filtrar elementos desde una consulta al servidor y seleccionar 
 * uno de los que retorna el servidor.
 * 
 *
 * @class RemoteAutocomplete
 * @extends {Component}
 * @augments {Component<ReactAutocomplete.Props, ReactAutocomplete.State>}
 */
class RemoteAutocomplete extends Component {
    constructor() {
        super();
        this.getItemText = this.getItemText.bind(this);
        this.getItemValue = this.getItemValue.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }
    state = {
        textField: '',
        loading: 0,
        items: [],
        cleanner: false,
        updaterProp: new Date(),
    };
    componentWillMount() {
        const { mountValue } = this.props;
        if (mountValue) {
            this.fetchRemoteData(mountValue, (items) => {
                this.onSelect(mountValue, items[0]);
            });
        }
        // window.Scripting_Autocomplete = this;
    }
    componentWillReceiveProps(props) {
        // const textField = props.value || this.state.value;
        // this.changeState({ textField });
        const oldMValue = `${this.props.mountValue || ''}`;
        const newMValue = `${props.mountValue || ''}`;
        if (oldMValue !== newMValue) {
            this.fetchRemoteData(newMValue, (items) => {
                this.onSelect(newMValue, items[0]);
            });
        }
    }
    componentWillUnmount() {
        this.unmounted = true;
    }

    onClear(e) {
        this.onChange(e, '');
        this.onSelect('', {});
    }
    onSelect(value, item) {
        // set the menu to only the selected item
        const textField = value ? this.getItemText(item) : value;
        this.changeState({ items: [item] });
        // or you could reset it to a default list again
        // this.changeState({ unitedStates: getStates() })

        this.onChange(null, textField, true);

        const { onSelect } = this.props;
        if (isFunction(onSelect)) {
            onSelect(textField, item);
        }
    }
    onChange(event, textField, notFetch) {
        // console.log(textField);
        this.changeState({ textField });
        if (isFunction(this.props.onFieldChange)) {
            const result = this.props.onFieldChange(event, textField);
            if (result === false) return;
        }
        if (notFetch) return;
        clearTimeout(this.requestTimer);
        this.requestTimer = this.fetchRemoteData(
            textField,
            (items) => this.changeState({ items: items || [] })
        );
    }

    getInputText() {
        if (!!this.props.value || this.props.value === '') {
            return this.props.value;
        }
        return this.state.textField || '';
    }
    getRemoteURL() {
        return this.props.url || '';
    }
    getFetchProps() {
        const { fetchProps } = this.props;
        let props = ['name', 'id'];
        if (isFunction(fetchProps)) {
            props = fetchProps();
        } else if (isString(fetchProps)) {
            props = fetchProps.split(',');
        }
        return props.filter(i => !!i);
    }
    getWhere(value) {
        const preWhere = Array.isArray(this.props.preWhere) ? this.props.preWhere : [];
        const searchValue = `${value || ''}`.replace(/\s/g, '%');
        const group = this.getFetchProps()
            .map(i => ({
                separator: i.separator || 'or',
                name: i.name || i,
                value: searchValue,
                criteria: i.criteria || 'like',
                type: i.type,
            }));
        return [
            ...preWhere,
            { group },
        ];
    }
    getOrderBy() {
        return this.props.orderBy || (
            this.props.fetchProps
                ? this.getFetchProps().map(i => i.name || i).join()
                : 'name,id'
        );
    }
    getPageSize() {
        return this.props.pageSize;
    }
    getItems() {
        return this.props.items || this.state.items;
    }
    getItemValue() {
        const { getItemValue } = this.props;
        let func = (item) => item.label || item.name;
        if (isFunction(getItemValue)) {
            func = getItemValue;
        } else if (isString(getItemValue)) {
            func = (item) => {
                const parts = (getItemValue || '').split('|');
                const results = [];
                for (let i = 0; i < parts.length; i++) {
                    const part = parts[i].trim();
                    results.push(item[part]);
                }
                return results.join(' | ');
            };
        }
        return func;
    }
    getItemKey(item) {
        const { getItemKey } = this.props;
        if (isFunction(getItemKey)) {
            return getItemKey(item);
        } else if (isString(getItemKey)) {
            return item[getItemKey];
        }
        return item.key || item.id || item.label || item.value || item;
    }
    getItemText(item) {
        const func = this.getItemValue();
        return func(item);
    }

    getCleanner() {
        if (this.getInputText() && (this.props.cleanner || this.state.cleanner)) {
            return (
                <span
                    key="cleanner"
                    className="text-muted"
                    onClick={(e) => this.onClear(e)}
                    style={{
                        position: 'absolute',
                        float: 'right',
                        top: '15%',
                        right: 5,
                        fontSize: '1.5em',
                        cursor: 'pointer',
                        ...this.props.cleannerStyle,
                    }}
                >×</span>
            );
        }
        return null;
    }
    getLoading() {
        return this.props.loading || this.state.loading;
    }
    getIcon() {
        const loading = this.getLoading();
        return (
            <i
                key="icon"
                className={`fa ${loading ? 'fa-spinner fa-spin' : 'fa-search'} text-muted`}
                style={{
                    margin: 5,
                    position: 'absolute',
                    top: '15%',
                    alignSelf: 'center',
                }}
            />
        );
    }
    setCleanner(cleanner) {
        this.changeState({ cleanner });
    }

    unmounted = false;
    changeState(state, callback) {
        if (!this.unmounted) {
            this.setState(state, callback);
        }
    }

    fetchRemoteData(value, callback) {
        // const { token, } = this.props;
        const remoteURL = this.getRemoteURL();
        const where = JSON.stringify(this.getWhere(value));
        const orderBy = this.getOrderBy();
        const pageSize = this.getPageSize();
        return setTimeout(() => {
            const newLoading = this.getLoading() + 1;
            this.changeState({ loading: newLoading, });
            // console.log(value, newLoading, this.state);
            request.get(remoteURL, {
                headers: {
                    // Authorization: token, 
                },
                withCredentials: true,
                params: { where, orderBy, pageSize },
            })
                .then((resp) => {
                    const loading = this.getLoading() - 1;
                    this.changeState({ loading, });
                    // console.log(value, loading, this.state);
                    const { items } = resp.data;
                    if (isFunction(callback)) callback(items);
                }).catch(error => {
                    const loading = this.getLoading() - 1;
                    this.changeState({ loading, });
                    // console.log(value, loading, this.state);
                    console.error('Error fetching autocomplete data: ', error);
                });
        }, 500);
    }

    render() {
        const {
            onFieldChange, onSelect, orderBy, getItemValue, value: v, cleannerStyle, //eslint-disable-line no-unused-vars, max-len
            disabled, required, readOnly,
            wrapperStyle,
            onKeyDown, onBlur,
            inputProps,
            inputStyle,
            ...rest,
        } = this.props;
        const value = this.getInputText();
        const items = this.getItems();
        const icon = this.getIcon();
        const cleanner = this.getCleanner();
        const localInputStyle = {
            padding: '6px 20px',
            ...inputStyle,
        };
        return (
            <ReactAutocomplete
                wrapperStyle={{ position: 'relative', ...wrapperStyle, }}
                inputProps={{
                    className: 'form-control',
                    onFocus: () => this.setCleanner(true),
                    onBlur: (e) => {
                        setTimeout(() => this.setCleanner(false), 500);
                        if (isFunction(onBlur)) onBlur(e);
                    },
                    onClick: () => {
                        if (!value || !items.length) {
                            this.fetchRemoteData(
                                value,
                                (newItems) => this.changeState({ items: newItems || [] })
                            );
                        }
                    },
                    style: localInputStyle,
                    ...{ disabled, required, readOnly, onKeyDown, },
                    ...inputProps,
                }}
                value={value}
                getItemValue={this.getItemValue()}//{(item) => item.label || item.name}
                items={items}
                onSelect={(...args) => this.onSelect(...args)}
                onChange={(...args) => this.onChange(...args)}
                renderMenu={(children) => (<div className="autocomplete-menu">{children}</div>)}
                renderItem={(item, isHighlighted) => {
                    const itemKey = this.getItemKey(item);
                    const itemText = this.getItemText(item);
                    let className = 'autocomplete-item';
                    if (isHighlighted) {
                        className += ' autocomplete-item-highlighted';
                    }
                    return (
                        <div key={itemKey} className={className}>{itemText}</div>
                    );
                }}
                renderInput={(props) => ([
                    <input
                        key="input"
                        ref={(ref) => (this.input = ref)}
                        {...props}
                    />,
                    icon,
                    cleanner,
                ])}
                {...rest}
            />
        );
    }
}

export default connect(mapStateToProps)(RemoteAutocomplete);
