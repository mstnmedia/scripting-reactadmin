import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	Input,
	TagInput,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import ReactSummernoteInput from '../../reactadmin/components/ui/ReactSummernoteInput';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de una documentación.
 *
 * @class DocumentationModal
 * @extends {Component}
 */
class DocumentationModal extends Component {

	state = {};

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		if (item) {
			item[prop] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				// item.id_center = item.id_center || this.props.user.center.id;
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('docs.add') || user.hasPermissions('docs.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name && item.value;
	}

	render() {
		// window.Scripting_ContentModal = this;
		const { id,
			id_center: centerID, center_name: centerName,
			id_type: permission, name, tags, value,
		} = this.getItem();
		const isMaster = this.props.user.isMaster();
		return (
			<div>
				{id && <FormGroup isValid={!!id}>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input className="form-control" value={id || ''} disabled />
				</FormGroup>}

				{isMaster && <FormGroup /* isValid={!!centerID} */>
					<label
						className="control-label inline-block"
						htmlFor="center"
					>{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							getItemValue={(item) => NameIDText(item)}
							value={centerName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>}
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={name} placeholder={i18n.t('Name')}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="tags">{i18n.t('Tags')}:</label>
					<Tooltip className="balloon" tag="TooltipTagField">
						<TagInput
							value={tags} placeholder={i18n.t('Tags')}
							onChange={({ value: newTags }) => this.handleChange('tags', newTags)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="permission">{i18n.t('Permission')}:</label>
					<Tooltip position="right" tag="TooltipDocsPermissionField">
					<Input
						type="text" className="form-control"
						value={permission} placeholder={i18n.t('Permission')}
						onFieldChange={(e) => {
							this.handleChange('id_type', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!value}>
					<Tooltip position="right" tag="TooltipDocsContentField">
						<label className="control-label block" htmlFor="content">{i18n.t('Content')}:</label>
					</Tooltip>
					<ReactSummernoteInput
						value={value}
						onChange={(html) => this.handleChange('value', html)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}
export default connect(mapStateToProps)(DocumentationModal);
