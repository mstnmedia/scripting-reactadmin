/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	Input,
	PanelCollapsible,
	ButtonGroup,
	AdvancedFilters,
	TagInput,
	Button,
	NumericInput,
	Tooltip,
} from '../../reactadmin/components/';
import {
	filterAdvActions,
} from '../../store/enums';
import { setFilterHandles, } from '../../Utils';
import i18n from '../../i18n';
import { DocumentationModal, } from './';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('docs', 'doc').actions;
/**
 * Componente que controla la pantalla del mantenimiento de documentaciones.
 *
 * @class Documentations
 * @extends {Component}
 */
class Documentations extends Component {
	state = {
		item: {},
		disabled: false,
		confirmModalProps: {},
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'docs', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Documentations'), },
		];
		// window.Scripting_Documentations = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	setItemDisabled(value) {
		this.setState({ disabled: value });
	}

	newItemClick(e) {
		e.preventDefault();
		this.setState({ item: {}, disabled: false, });
		ModalFactory.show('docModal');
	}

	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteDocs'),
				description: i18n.t('WishDeleteDocsDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, docs: { pageNumber, }, } = this.props;
							dispatch(actions.delete({
								token,
								item: { id },
								success: () => {
									ModalFactory.hide('confirmModal');
									this.fetchPage(pageNumber);
								},
							}));
						},
						disabled: () => this.state.disabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleItemChange(item) {
		const { dispatch, token, docs: { pageNumber, } } = this.props;
		const success = () => {
			const page = item.id ? pageNumber : 1;
			this.itemChanged(page);
		};
		const failure = () => this.setItemDisabled(false);
		this.setItemDisabled(true);
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}
	handleItemDuplicate(row, e) {
		e.stopPropagation();
		const { dispatch, token, } = this.props;
		const path = `duplicating_${row.id}`;
		const success = () => {
			this.setState({ [path]: false });
			this.fetchPage(1);
		};
		const failure = () => {
			this.setState({ [path]: false });
		};
		this.setState({ [path]: true });
		dispatch(actions.postToURL({
			token,
			route: 'doc/duplicate',
			path,
			item: { id: row.id },
			success,
			failure,
		}));
	}

	canDelete(row) {
		const { user, } = this.props;
		if (row.id < 0) {
			return false;
		}
		return user.hasPermissions('docs.delete');
	}
	itemChanged(page) {
		ModalFactory.hide('docModal');
		this.setItemDisabled(false);
		this.fetchPage(page);
	}

	tableSchema() {
		const canAdd = this.props.user.hasPermissions('docs.add');
		const isMaster = this.props.user.isMaster();
		return {
			fields: {
				duplicate: canAdd && {
					label: ' ',
					cellStyle: { width: 60, },
					format: (row) => (
						<Button
							className="btn-success btn-xs inline-block"
							icon="fa-copy"
							onClick={(e) => this.handleItemDuplicate(row, e)}
							disabled={this.state[`duplicating_${row.id}`]}
						/>
					),
				},
				id_type: isMaster ? i18n.t('Permission') : undefined,
				tags: {
					label: i18n.t('Tags'),
					format: (row) => (<TagInput inline value={row.tags} />)
				},
				name: i18n.t('Name'),
				center_name: isMaster ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
			}
		};
	}

	renderFilters() {
		const { where, whereObj, advancedSearch } = this.state;
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group input-group-sm col-md-3">
							<label htmlFor="id">{i18n.t('ID')}:</label>
							<NumericInput
								value={this.filterValue('id', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'id',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group input-group-sm col-md-3">
							<label htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								type="text" className="form-control"
								value={this.filterValue('name', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'name',
									value: e.target.value,
									criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{/* <div className="form-group input-group-sm col-md-3">
							<label htmlFor="type">{i18n.t('Type')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('id_type', '')}
								items={ContentTypeOptions()}
								onChange={(e) => this.filterChange({ name: 'id_type', value: e.target.value })}
							/>
						</div> */}
						<div className="form-group input-group-sm col-md-3">
							<label htmlFor="tags">{i18n.t('Tags')}:</label>
							<TagInput
								value={this.filterValue('tags', '')} placeholder={i18n.t('Tags')}
								onChange={({ tags, value }) => this.filterChange({
									label: 'tags',
									value: tags.length ? value : undefined,
									group: tags.map(i => ({ name: 'tags', criteria: 'like', value: i }))
								})}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group input-group-sm col-md-3">
							<label htmlFor="value">{i18n.t('Content')}:</label>
							<Input
								type="text" className="form-control"
								value={this.filterValue('value', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'value',
									value: e.target.value,
									criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
					</div>
					:
					<AdvancedFilters
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
							id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
							name: { type: 'text', label: i18n.t('Name'), onKeyDown: this.onKeyDownSimple, },
							tags: { type: 'tags', label: i18n.t('Tags'), onKeyDown: this.onKeyDownMixed, },
							id_type: { type: 'text', label: i18n.t('Permission'), onKeyDown: this.onKeyDownSimple, },
							value: { type: 'clob', label: i18n.t('Value'), onKeyDown: this.onKeyDownSimple, },
						}}/* eslint-enable max-len */
						{...{ where, whereObj }}
					/>
				}

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			docs: { list, pageNumber, pageSize, totalRows, },
			user, router, dispatch,
		} = this.props;
		const { item, disabled, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('docs.add');
		return (
			<Page>
				<Factory
					modalref="docModal"
					title={i18n.t('DocumentationInfo', item)}
					factory={DocumentationModal} large
					item={item} disabled={disabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={this.tableSchema()}
							addTooltip={i18n.t('TooltipTableAdd', {
								screen: i18n.t('Documentations')
							})}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={(row) => this.canDelete(row)}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => {
								dispatch(actions.setSelected(row));
								router.push(`/data/docs/${row.id}`);
							}}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Documentations);
