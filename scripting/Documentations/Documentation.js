// /**
//  * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
//  * All rights reserved.
//  *
//  * This source code is licensed under the license found in the
//  * LICENSE file in the root directory of this source tree.
//  *
//  * @providesModule Workflow
//  */

import React from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import {
	ReduxOutlet,
	mapStateToProps,
} from '../../outlets/';

import {
	Col, Page,
	Panel, Button,
	ModalFactory, Factory,
	AnimatedSpinner,
} from '../../reactadmin/components/';
import { DocumentationModal } from './';
import i18n from '../../i18n';
import TransactionContentItem from '../Transactions/TransactionContentItem';
import { ContentTypes } from '../../store/enums';

const actions = ReduxOutlet('docs', 'doc').actions;

class Documentation extends React.Component {
	state = {
		item: {},
		disabled: false,
		loading: false,
	}
	componentWillMount() {
		this.fetchItem();
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
		// window.Scripting_Documentation = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getBreadcrumbItems() {
		const { docs: { item }, params: { id: docID } } = this.props;
		const name = (item.name || `#${docID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Documentations'), to: '/data/docs' },
			{ label: `${i18n.t('Documentation')} ${name}`, },
		];
	}

	fetchItem() {
		const { dispatch, token, params: { id: docID } } = this.props;
		const callback = () => this.setState({ loading: false });
		this.setState({ loading: true });
		dispatch(actions.fetchOne(token, docID, callback, callback));
	}

	handleItemChange(item) {
		const { dispatch, token, } = this.props;
		dispatch(actions.update({
			token,
			item,
			success: (/* resp */) => {
				ModalFactory.hide('docModal');
				// const newItem = resp.data.items[0];
				// dispatch(actions.setProp('item', newItem));
			},
		}));
	}

	render() {
		const { docs: { item: doc, }, user, } = this.props;
		const { item, disabled, loading, } = this.state;
		if (loading) {
			return (
				<div
					style={{
						height: '100%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<div>
						<AnimatedSpinner show style={{ fontSize: '3em', }} />
					</div>
				</div>
			);
		}
		const canEdit = user.hasPermissions('docs.edit');
		return (
			<Page>
				<Factory
					modalref="docModal"
					title={i18n.t('DocumentationInfo', item)}
					factory={DocumentationModal} large
					item={item} disabled={disabled}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				/>
				<Col>
					<Panel
						title={[
							doc.name,
							(doc.id && canEdit && <Button
								key="2"
								className="btn-xs btn-info"
								icon="fa-pencil" rounded
								style={{ position: 'absolute', right: 20, top: 5, }}
								onClick={() => {
									this.setState({ item: Object.assign({}, doc) });
									ModalFactory.show('docModal');
								}}
								disabled={disabled}
							/>)
						]}
					>
						<div style={{ position: 'relative', }}>
							<TransactionContentItem
								key={`${doc.id}_${doc.value}`}
								content={{ id_type: ContentTypes.HTML, value: doc.value, }}
							/>
						</div>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Documentation);
