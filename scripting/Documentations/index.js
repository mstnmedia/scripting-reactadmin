import Documentations from './Documentations';
import Documentation from './Documentation';
import DocumentationModal from './DocumentationModal';

export {
    Documentations,
    Documentation,
    DocumentationModal,
};
