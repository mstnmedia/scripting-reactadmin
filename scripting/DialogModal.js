import React, { PureComponent } from 'react';
import { Factory } from '../reactadmin/components';
import { isFunction } from '../reactadmin/Utils';

/**
 * Componente que muestra al usuario un modal con título, texto y opciones personalizables.
 *
 * @export
 * @class DialogModal
 * @extends {PureComponent}
 */
export class DialogModal extends PureComponent {
	getDescription() {
		return this.props.description || '';
	}
	getButtons() {
		return this.props.buttons || [];
	}
	renderContent() {
		return (
			<div role="form">
				<div className="form-group">{this.getDescription()}</div>
				<div className="">
					<hr />
					{this.getButtons().map(({ key, label, className, disabled, ...rest }, i) => (
						<button
							key={key || i}
							type="button"
							className={`btn ${className}`}
							disabled={isFunction(disabled) ? disabled() : disabled}
							{...rest}
						>{label}</button>
					))}
				</div>
			</div>
		);
	}
	render() {
		return (
			<Factory {...this.props} factory={this.renderContent.bind(this)} />
		);
	}
}
