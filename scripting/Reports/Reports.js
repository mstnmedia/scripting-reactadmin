/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { mapStateToProps } from '../../outlets';
import i18n from '../../i18n';
import { Page, } from '../../reactadmin/components';

/** 
 * Componente que muestra al usuario los controles para consultar reportes.
 *
 * @class Reports
 * @extends {Component}
 */
class Reports extends Component {
	state = {}
	componentWillMount() {
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Reports'), },
		];
		// window.Scripting_Reports = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	render() {
		return (
			<Page>
				<div className="col-xs-12" style={{ height: '100%', }}>
					<div className="panel panel-default" style={{ height: '98%', }}>
						<div className="panel-body" style={{ height: '100%', }}>
							<iframe
								ref="iframe"
								style={{ border: 'none', width: '100%', height: '100%', }}
								src="/dist/reports/mstn-reportes.html"
							/>
						</div>
					</div>
				</div>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Reports);
