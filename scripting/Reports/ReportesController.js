// /***    ReportesController    ***/
// app.factory("ReportesFactory", function ($http, settings, Utilidades, FormularioFactory, FormMsg) {
//     this.baseURL = settings.ApiUrl + "Reportes/";
//     this.LogURL = settings.LogURL;
//     var config = {};

//     Utilidades.BasicFactory(this, config);
//     this.GetSource = function (ID) {
//         return FormularioFactory.GetSource(ID);
//     };
//     return this;
// });

// app.controller("ReportesController", ["$scope", "ReportesFactory", "Utilidades", function ($scope, Factory, Utils) {
//     Utils.BasicCrud($scope, Factory, "Reportes");
//     //$scope.reporteSelected = 'factores';
//     $scope.getParameters = function (fields) {
//         var fields2 = typeof (fields) == 'string' ? fields.split(' ') : fields;
//         var res = '';
//         if (fields.length > 0) {
//             for (field in fields2) {
//                 res += '&' + fields2[field] + '=true';
//             }
//         }
//         return res;
//     };
//     $scope.ZonasColumnas = [];
//     $scope.TipoPuntosColumnas = [];
//     $scope.ClasificacionColumnas = [];
//     $scope.Init = function () {
//         $scope.GetSource('Zona', 'Zonas', false, function (data) {
//             for (var i = 0; i < $scope.Zonas.length; i++) {
//                 $scope.ZonasColumnas.push({ Name: $scope.Zonas[i].Name, Value: $scope.Zonas[i].Value });
//             }
//         });
//         $scope.GetSource('TPP', 'TipoPuntos', false, function (data) {
//             for (var i = 0; i < $scope.TipoPuntos.length; i++) {
//                 $scope.TipoPuntosColumnas.push({ Name: $scope.TipoPuntos[i].Name, Value: $scope.TipoPuntos[i].Value });
//             }
//         });
//         $scope.GetSource('CLPU', 'Clasificaciones', false, function (data) {
//             for (var i = 0; i < $scope.Clasificaciones.length; i++) {
//                 $scope.ClasificacionColumnas.push({ Name: $scope.Clasificaciones[i].Name, Value: $scope.Clasificaciones[i].Value });
//             }
//         });
//         //$scope.GetSource('TPP', 'TipoPuntos');
//         $scope.GetSource('TVisitas', 'TipoVisitas');
//         //$scope.GetSource('CLPU', 'Clasificaciones');
//         $scope.GetSource('CLPU', 'Preguntas');
//         $scope._Init = true;
//     };

//     $scope.TipoGestor = [
//         { Name: "Interno", Value: 0 },
//         { Name: "Externo", Value: 1 }
//     ];

//     $scope.convertToArray = function (fields) {
//         var fields2 = typeof (fields) == 'string' ? fields.split(' ') : fields;
//         //fields2 = fields2.sort();
//         var array = [];
//         for (var i = 0; i < fields2.length; i++) {
//             var obj = fields2[i];
//             array.push({ Name: obj, Value: obj });
//         }
//         return array;
//     };

//     $scope.arrayToString = function (s) {
//         var cadena = "";
//         for (var i = 0; i < s.length; i++) {
//             if (s[i].Resultado !== true) {
//                 cadena += s[i].Value + " ";
//             }
//         }
//         return cadena.trim();
//     }
//     $scope.GetURL = function (report) {
//         return "ReportViewer.aspx?%2fGestionAsociados%2f" + report + "&rc:Parameters=false&rs:Command=Render"
//     };
//     var d = new Date(), y = d.getFullYear(), m = d.getMonth();
//     $scope.firstDay = new Date(y, m, 1);
//     $scope.lastDay = new Date(y, m + 1, 0);
//     $scope.reportes = {
//         'factores': {
//             parameters: "",
//             url: 'FactoresExito',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Fecha_Creacion AS DATE)', 'Fin': 'CAST(Fecha_Creacion AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Nombre de Zona", Value: "Zona" },
//                 { Name: "Acciones Comerciales", Value: "Acciones_Comerciales" },
//                 { Name: "Calidad Average", Value: "Calidad_Average" },
//                 { Name: "Capacitaci\xf3n", Value: "Capacitacion" },
//                 { Name: "Cumplimiento Acuerdos", Value: "Cumplimiento_Acuerdos" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Mantiene", Value: "Expansion" },
//                 { Name: "Fecha de Creaci\xf3n", Value: "Fecha_Creacion" },
//                 { Name: "Inventario", Value: "Inventario" },
//                 { Name: "L\xcdmite de Creditos", Value: "Limite_Credito" },
//                 { Name: "Mezcla %", Value: "Mezcla" },
//                 { Name: "Portabilidad", Value: "Portabilidad" },
//                 { Name: "Promotores", Value: "Promotores" },
//                 { Name: "Recarga F\xe1cil", Value: "Recarga_Facil" },
//                 { Name: "Salario Fijo y Variable", Value: "SalarioFijoVariable" },
//                 { Name: "Socio", Value: "Socio" },
//                 { Name: "Volumen FDTV", Value: "Volumen_FDTV" },
//                 { Name: "Volumen M\xf3viles", Value: "Volumen_Moviles" },
//                 { Name: "Imagen", Value: "Imagen" },
//                 { Name: "Resultado", Value: "Resultado" }
//             ],
//             selection: []
//         },
//         'visitainspeccion': {
//             parameters: "",
//             url: 'VisitaInspeccion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Aire Acondicionado", Value: "Aire_Acondicionado" },
//                 { Name: "Ayuda Ventas", Value: "AyudaVentas" },
//                 { Name: "BAC Inventario Claro", Value: "BAC_Inventario_Claro" },
//                 { Name: "BAC Inventario Propio", Value: "BAC_Inventario_Propio" },
//                 { Name: "Canales HD", Value: "Canales_HD" },
//                 { Name: "Cantidad Empleados", Value: "Cantidad_Empleados" },
//                 { Name: "Claro TV", Value: "Claro_TV" },
//                 { Name: "Claro Video", Value: "Claro_Video" },
//                 { Name: "Cobro Facturas", Value: "Cobro_Facturas" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "C\xf3digo Socio", Value: "Codigo_Socio" },
//                 { Name: "Comentario", Value: "Comentario" },
//                 { Name: "Copiadora", Value: "Copiadora" },
//                 { Name: "Direccion", Value: "Direccion" },
//                 { Name: "Equipos Prepago Claro", Value: "Equipos_Prepago_Claro" },
//                 { Name: "Equipos Prepago Propio", Value: "Equipos_Prepago_Propio" },
//                 { Name: "Equipos Tarifarios Claro", Value: "Equipos_Tarifarios_Claro" },
//                 { Name: "Equipos Tarifarios Propio", Value: "Equipos_Tarifarios_Propio" },
//                 { Name: "Equipos Zona Experiencia", Value: "Equipos_Zona_Experiencia" },
//                 { Name: "Higiene Organizacion PDV", Value: "Higiene_Organizacion_PDV" },
//                 { Name: "Clasificaci\xf3n Punto", Value: "ID_Clasificacion" },
//                 { Name: "Ejecutivo", Value: "ID_Ejecutivo" },
//                 { Name: "Tipo Visita", Value: "ID_Tipo" },
//                 { Name: "Tipo Punto", Value: "ID_Tipo_Punto" },
//                 { Name: "Auditor", Value: "ID_Usuario" },
//                 { Name: "Zona", Value: "ID_Zona" },
//                 { Name: "Fecha Visita", Value: "Inicio" },
//                 { Name: "Inventario", Value: "Inventario" },
//                 { Name: "Inversor", Value: "Inversor" },
//                 { Name: "Letrer\xcda", Value: "Letreria" },
//                 { Name: "Luces Techo", Value: "Luces_Techo" },
//                 { Name: "Luces Vitrina", Value: "Luces_Vitrina" },
//                 { Name: "Mobiliario", Value: "Mobiliario" },
//                 { Name: "Modem ADSL Inventario Claro", Value: "Modem_ADSL_Inventario_Claro" },
//                 { Name: "Modem ADSL Inventario Propio", Value: "Modem_ADSL_Inventario_Propio" },
//                 { Name: "Nombre Tienda", Value: "NombreTienda" },
//                 { Name: "Nombre Socio", Value: "Nombre_Socio" },
//                 { Name: "Objetivo Asignado", Value: "Objetivo_Asignado" },
//                 { Name: "Papel Cobro Factura", Value: "Papel_Cobro_Facturas" },
//                 { Name: "Pintura Exterior", Value: "Pintura_Exterior" },
//                 { Name: "Pintura Interior", Value: "Pintura_Interior" },
//                 { Name: "POP", Value: "POP" },
//                 { Name: "POP Pared", Value: "POP_Pared" },
//                 { Name: "Porta Celulares", Value: "Porta_Celulares" },
//                 { Name: "Porta Precios", Value: "Porta_Precios" },
//                 { Name: "Punto Abierto", Value: "Punto_Abierto" },
//                 { Name: "Recomendado Para", Value: "Recomendado_Para" },
//                 { Name: "SIF Inventario Claro", Value: "SIF_Inventario_Claro" },
//                 { Name: "SIF Inventario Propio", Value: "SIF_Inventario_Propio" },
//                 { Name: "SIM Card Inventario Claro", Value: "SIM_Card_Inventario_Claro" },
//                 { Name: "SIM Card Inventario Propio", Value: "SIM_Card_Inventario_Propio" },
//                 { Name: "Sistemas", Value: "Sistemas" },
//                 { Name: "Tablet Inventario Claro", Value: "Tablet_Inventario_Claro" },
//                 { Name: "Tablet Inventario Propio", Value: "Tablet_Inventario_Propio" },
//                 { Name: "Tel\xe9fono Punto", Value: "Telefono_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Tipo_Local" },
//                 { Name: "TV", Value: "TV" },
//                 { Name: "UPS", Value: "UPS" },
//                 { Name: "Verifone", Value: "Verifone" },
//                 { Name: "Volantes  y/o Brochures", Value: "Volantes_Brochures" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Duraci\xf3n", Value: "Duracion" },
//                 { Name: "Fecha Planificaci\xf3n", Value: "start" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             selection: []
//         },
//         'empleadosinspeccion': {
//             parameters: "",
//             url: 'Empleados_Inspeccion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Fin': 'CAST(Inicio AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xe9dula", Value: "Cedula" },
//                 { Name: "Clave Propia", Value: "Clave_Propia" },
//                 { Name: "Comentario de Clave Propia", Value: "Clave_Propia_Comentario" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "Conocimiento Ofertas", Value: "Conocimiento_Ofertas" },
//                 { Name: "Comentario de Conocimiento Ofertas", Value: "Conocimiento_Ofertas_Comentario" },
//                 { Name: "Creado Portal", Value: "Creado_Portal" },
//                 { Name: "Comentario de Creado Portal", Value: "Creado_Portal_Comentario" },
//                 { Name: "Despedida", Value: "Despedida" },
//                 { Name: "Comentario de Despedida", Value: "Despedida_Comentario" },
//                 { Name: "Email", Value: "Email" },
//                 { Name: "Fecha de Entrada al DAC", Value: "Fecha_Entrada" },
//                 { Name: "Tipo de Punto", Value: "ID_Tipo_Punto" },
//                 { Name: "Comentario de Imagen General", Value: "Imagen_General_Comentario" },
//                 { Name: "Imagen General Correcta", Value: "Imagen_General_Correcta" },
//                 { Name: "Nombre de la Tienda", Value: "NombreTienda" },
//                 { Name: "Nombre Empleado", Value: "Nombre_Empleado" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "No. Empleado", Value: "No_Empleado" },
//                 { Name: "Posici\xf3n Representante", Value: "Posicion_Representante" },
//                 { Name: "Salario Fijo y Variable", Value: "SalarioFijoVariable" },
//                 { Name: "Saludo Bienvenida", Value: "Saludo_Bienvenida" },
//                 { Name: "Comentario de Saludo Bienvenida", Value: "Saludo_Bienvenida_Comentario" },
//                 { Name: "Tarjeta", Value: "Tarjeta" },
//                 { Name: "Tel\xe9fono", Value: "Telefono" },
//                 { Name: "Uniforme", Value: "Uniforme" },
//                 { Name: "No. Empleado", Value: "No_Empleado" },
//                 { Name: "Fecha", Value: "Fecha" },
//                 { Name: "Nombre Ejecutivo", Value: "ID_Ejecutivo" },
//                 { Name: "Nombre Zona", Value: "ID_Zona" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Tipo de Visita", Value: "Nombre_Tipo" },
//                 { Name: "Alquilado por Claro", Value: "Tipo_Local" },
//                 { Name: "Nombre de Clasificacion", Value: "ID_Clasificacion" }
//             ],
//             selection: []
//         },
//         'visitasauditorias': {
//             parameters: "",
//             url: 'VisitasAuditoria',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Nombre de Tienda", Value: "NombreTienda" },
//                 { Name: "Tipo de Punto", Value: "Tipo_Punto" },
//                 { Name: "Clasificaci\xf3n de Punto", Value: "Clasificacion_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Alquilado_Claro" },
//                 { Name: "Tipo de Visita", Value: "Tipo_Visita" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Zona", Value: "Zona" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Auditor", Value: "Auditor" },
//                 { Name: "Fecha Inicio", Value: "Fecha_Visita" },
//                 { Name: "C\xf3digo Compensaci\xf3n", Value: "CodigoCompensacion" },
//                 { Name: "Estatus del Punto", Value: "Estatus_Punto" },
//                 { Name: "Posee Letrero Exterior", Value: "Letrero_Exterior" },
//                 { Name: "Posee Accesos y o Sistemas", Value: "Accesos_Sistemas" },
//                 { Name: "Pintura del Local en Buen Estado", Value: "Pintura_Local" },
//                 { Name: "Mobiliario Cumple Est\xe1ndar de Marca", Value: "MobiliarioEstandarizado" },
//                 { Name: "Posee Letrero de Desbloqueo", Value: "Letrero_Desbloqueo" },
//                 { Name: "Nombre  del Representante", Value: "Nombre_Representante" },
//                 { Name: "C\xe9dula  del Representante", Value: "Cedula_Representante" },
//                 { Name: "Tarjeta del Representante", Value: "Tarjeta_Representante" },
//                 { Name: "Condici\xf3n Representante", Value: "Condicion_Representante" },
//                 { Name: "Recibe Comisi\xf3n por Ventas al Lograr Objetivo", Value: "Comision" },
//                 { Name: "Representante Uniformado", Value: "Uniforme" },
//                 { Name: "Tiempo Laborando", Value: "TiempoLaborando" },
//                 { Name: "Posee TV para Claro TV", Value: "ClaroTV" },
//                 { Name: "Tipo de Usuario", Value: "Tipo_Usuario" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Duraci\xf3n", Value: "Duracion" },
//                 { Name: "Fecha Planificaci\xf3n", Value: "start" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             selection: []
//         },
//         'visitasplanificacion': {
//             parameters: "",
//             url: 'VisitasPlanificacion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Tipo de Socio", Value: "Tipo_Socio" },
//                 { Name: "Hora", Value: "Hora" },
//                 { Name: "Tipo de Visita", Value: "Tipo_Visita" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Fecha Inicio", Value: "Fecha_Visita" },
//                 // { Name: "Postpago", Value: "PostPago" },
//                 { Name: "Plan de Acci\xf3n Postpago", Value: "Plan_PostPago" },
//                 { Name: "Control", Value: "Control" },
//                 { Name: "Plan de Acci\xf3n Control", Value: "Plan_Control" },
//                 { Name: "Prepago", Value: "Prepago" },
//                 { Name: "Plan de Acci\xf3n Prepago", Value: "Plan_Prepago" },
//                 { Name: "Banda Ancha Postpago", Value: "BandaAnchaPostpago" },
//                 { Name: "Plan de Acci\xf3n Banda Ancha Postpago", Value: "Plan_BandaAnchaPostpago" },
//                 { Name: "Banda Ancha Prepago", Value: "BandaAnchaPrepago" },
//                 { Name: "Plan de Acci\xf3n Banda Ancha Prepago", Value: "Plan_BandaAnchaPrepago" },
//                 { Name: "Migraci\xf3n Prepago-Control", Value: "MigracionPrepagoControl" },
//                 { Name: "Plan de Acci\xf3n Migraci\xf3n Prepago-Control", Value: "Plan_MigracionPrepagoControl" },
//                 { Name: "Migraci\xf3n Prepago-Postpago", Value: "MigracionPrepagoPostpago" },
//                 { Name: "Plan de Acci\xf3n Migraci\xf3n Prepago-Postpago", Value: "Plan_MigracionPrepagoPostpago" },
//                 { Name: "Migraci\xf3n Control-Postpago", Value: "MigracionControlPostpago" },
//                 { Name: "Plan de Acci\xf3n Migraci\xf3n Control-Postpago", Value: "Plan_MigracionControlPostpago" },
//                 { Name: "Cambiazo Postpago", Value: "CambiazoPostpago" },
//                 { Name: "Plan de Acci\xf3n Cambiazo Postpago", Value: "Plan_CambiazoPostpago" },
//                 { Name: "Cambiazo Prepago", Value: "CambiazoPrepago" },
//                 { Name: "Plan de Acci\xf3n Cambiazo Prepago", Value: "Plan_CambiazoPrepago" },
//                 { Name: "Portabilidad Postpago", Value: "PortabilidadPostpago" },
//                 { Name: "Plan de Acci\xf3n Portabilidad Postpago", Value: "Plan_PortabilidadPostpago" },
//                 { Name: "Portabilidad Control", Value: "PortabilidadControl" },
//                 { Name: "Plan de Acci\xf3n Portabilidad Control", Value: "Plan_PortabilidadControl" },
//                 { Name: "Portabilidad Prepago", Value: "PortabilidadPrepago" },
//                 { Name: "Plan de Acci\xf3n Portabilidad Prepago", Value: "Plan_PortabilidadPrepago" },
//                 { Name: "Portabilidad FDTV", Value: "PortabilidadFDTV" },
//                 { Name: "Plan de Acci\xf3n Portabilidad FDTV", Value: "Plan_PortabilidadFDTV" },
//                 { Name: "Al\xe1mbrico", Value: "Alambrico" },
//                 { Name: "Plan de Acci\xf3n Al\xe1mbrico", Value: "Plan_Alambrico" },
//                 { Name: "SIF", Value: "SIF" },
//                 { Name: "Plan de Acci\xf3n SIF", Value: "Plan_SIF" },
//                 { Name: "Datos", Value: "Datos" },
//                 { Name: "Plan de Acci\xf3n Datos", Value: "Plan_Datos" },
//                 { Name: "IPTV", Value: "IPTV" },
//                 { Name: "Plan de Acci\xf3n IPTV", Value: "Plan_IPTV" },
//                 { Name: "DTH", Value: "DTH" },
//                 { Name: "Plan de Acci\xf3n DTH", Value: "Plan_DTH" },
//                 { Name: "Carga Facil", Value: "CargaFacil" },
//                 { Name: "Plan de Acci\xf3n Carga Facil", Value: "Plan_CargaFacil" },
//                 { Name: "Flota", Value: "Flota" },
//                 { Name: "Plan de Acci\xf3n Flota", Value: "Plan_Flota" },
//                 { Name: "Claro Video", Value: "ClaroVideo" },
//                 { Name: "Plan de Acci\xf3n Claro Video", Value: "Plan_ClaroVideo" },
//                 { Name: "Tablets", Value: "Tablets" },
//                 { Name: "Plan de Acci\xf3n Tablets", Value: "Plan_Tablets" },
//                 { Name: "Equipos Tarifarios", Value: "Equipos_Tarifarios" },
//                 { Name: "Debe adquirir Equipos Tarifarios", Value: "Adquirir_Equipos_Tarifarios" },
//                 { Name: "Equipos Prepago", Value: "Equipos_Prepago" },
//                 { Name: "Debe adquirir Equipos Prepago", Value: "Adquirir_Equipos_Prepago" },
//                 { Name: "Banda Ancha", Value: "Banda_Ancha" },
//                 { Name: "Debe adquirir Banda Ancha", Value: "Adquirir_Banda_Ancha" },
//                 { Name: "Inventario SIF", Value: "InventarioSIF" },
//                 { Name: "Debe adquirir Inventario SIF", Value: "Adquirir_InventarioSIF" },
//                 { Name: "Modems ADSL", Value: "ModemsADSL" },
//                 { Name: "Debe adquirir Modems ADSL", Value: "Adquirir_ModemsADSL" },
//                 { Name: "MultiPlan de Acci\xf3n SIF", Value: "MultiplanSIF" },
//                 { Name: "Debe adquirir Multiplan de Acci\xf3n SIF", Value: "Adquirir_MultiplanSIF" },
//                 { Name: "Recarga", Value: "Recarga" },
//                 { Name: "Debe adquirir Recarga", Value: "Adquirir_Recarga" },
//                 { Name: "Inventario Tablets", Value: "InventarioTablets" },
//                 { Name: "Debe adquirir Tablets", Value: "Adquirir_Tablets" },
//                 { Name: "Cantidad de Tiendas Formales", Value: "CantidadTiendasFormales" },
//                 { Name: "Compromiso de Contrataci\xf3n Formales", Value: "CantidadMinimaFormales" },
//                 { Name: "Plan de Acci\xf3n Formales", Value: "Plan_Formales" },
//                 { Name: "Cantidad de Tiendas Terceros", Value: "CantidadTiendasTerceros" },
//                 { Name: "Cantidad M\xcdnima 5 Terceros", Value: "CantidadMinimaTerceros" },
//                 { Name: "Plan de Acci\xf3n Terceros", Value: "Plan_Terceros" },
//                 { Name: "Cantidad de Tiendas Buena Imagen Tiendas", Value: "CantidadTiendasBuenaImagenTiendas" },
//                 { Name: "Cantidad M\xcdnima 5 Buena Imagen Tiendas", Value: "CantidadMinimaBuenaImagenTiendas" },
//                 { Name: "Plan de Acci\xf3n Buena Imagen Tiendas", Value: "Plan_BuenaImagenTiendas" },
//                 { Name: "Cantidad de Tiendas Uniforme", Value: "CantidadTiendasUniforme" },
//                 { Name: "Cantidad M\xcdnima 5 Uniforme", Value: "CantidadMinimaUniforme" },
//                 { Name: "Plan de Acci\xf3n Uniforme", Value: "Plan_Uniforme" },
//                 { Name: "Cantidad de Tiendas Letreros", Value: "CantidadTiendasLetreros" },
//                 { Name: "Cantidad M\xcdnima 5 Letreros", Value: "CantidadMinimaLetreros" },
//                 { Name: "Plan de Acci\xf3n Letreros", Value: "Plan_Letreros" },
//                 { Name: "Taller de Servicios", Value: "TallerServicios" },
//                 { Name: "Programaci\xf3n Taller de Servicios", Value: "Programacion_TallerServicios" },
//                 { Name: "Taller de M\xf3viles", Value: "TallerMoviles" },
//                 { Name: "Programaci\xf3n Taller de M\xf3viles", Value: "Programacion_TallerMoviles" },
//                 { Name: "Taller de Multiplan", Value: "TallerMultiplan" },
//                 { Name: "Programaci\xf3n Taller de Multiplan", Value: "Programacion_TallerMultiplan" },
//                 { Name: "Taller de Portabilidad", Value: "TallerPortabilidad" },
//                 { Name: "Programaci\xf3n Taller de Portabilidad", Value: "Programacion_TallerPortabilidad" },
//                 { Name: "PPG y SIC", Value: "PPGSIC" },
//                 { Name: "Programaci\xf3n PPG y SIC", Value: "Programacion_PPGSIC" },
//                 { Name: "CRM", Value: "CRM" },
//                 { Name: "Programaci\xf3n CRM", Value: "Programacion_CRM" },
//                 { Name: "Horizonte", Value: "Horizonte" },
//                 { Name: "Programaci\xf3n Horizonte", Value: "Programacion_Horizonte" },
//                 { Name: "Nombre OMS", Value: "OMS" },
//                 { Name: "Programaci\xf3n OMS", Value: "Programacion_OMS" },
//                 { Name: "Portabilidad", Value: "Portabilidad" },
//                 { Name: "Programaci\xf3n Portabilidad", Value: "Programacion_Portabilidad" },
//                 { Name: "SVDG", Value: "SVDG" },
//                 { Name: "Programaci\xf3n SVDG", Value: "Programacion_SVDG" },
//                 { Name: "Bonos Representantes", Value: "BonosRepresentantes" },
//                 { Name: "Comentario Bonos Representantes", Value: "Comentario_BonosRepresentantes" },
//                 { Name: "Art\xcdculos Promocionales", Value: "ArticulosPromocionales" },
//                 { Name: "Comentario Articulos Promocionales", Value: "Comentario_ArticulosPromocionales" },
//                 { Name: "Perifoneo Radio Volantes", Value: "PerifoneoRadioVolantes" },
//                 { Name: "Comentario Perifoneo Radio Volantes", Value: "Comentario_PerifoneoRadioVolantes" },
//                 { Name: "Ferias Colocacion Stands", Value: "FeriasColocacionStands" },
//                 { Name: "Comentario Ferias Colocacion Stands", Value: "Comentario_FeriasColocacionStands" },
//                 { Name: "Promotores FDTV", Value: "PromotoresFDTV" },
//                 { Name: "Compromiso de Contrataci\xf3n Promotores FDTV", Value: "Compromiso_PromotoresFDTV" },
//                 { Name: "Promotores M\xf3viles", Value: "PromotoresMoviles" },
//                 { Name: "Compromiso de Contrataci\xf3n Promotores M\xf3viles", Value: "Compromiso_PromotoresMoviles" },
//                 { Name: "Promotores Valor", Value: "PromotoresValor" },
//                 { Name: "Compromiso de Contrataci\xf3n Promotores Valor", Value: "Compromiso_PromotoresValor" },
//                 { Name: "Promotores Portabilidad", Value: "PromotoresPortabilidad" },
//                 { Name: "Compromiso de Contrataci\xf3n Promotores Portabilidad", Value: "Compromiso_PromotoresPortabilidad" },
//                 { Name: "Promotores SIF", Value: "PromotoresSIF" },
//                 { Name: "Compromiso de Contrataci\xf3n Promotores SIF", Value: "Compromiso_PromotoresSIF" },
//                 { Name: "Comisiones", Value: "Comisiones" },
//                 { Name: "Penalidades", Value: "Penalidades" },
//                 // { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 // { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 // { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 // { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 // { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Duracion", Value: "Duracion" },
//                 { Name: "Fecha Planificaci\xf3n", Value: "start" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             selection: []
//         },
//         'visitasterceros': {
//             parameters: "",
//             url: 'VisitasTerceros',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Nombre de Tienda", Value: "NombreTienda" },
//                 { Name: "Tipo de Punto", Value: "Tipo_Punto" },
//                 { Name: "Clasificaci\xf3n de Punto", Value: "Clasificacion_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Alquilado_Claro" },
//                 { Name: "Tipo de Visita", Value: "Tipo_Visita" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Zona", Value: "Zona" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Auditor", Value: "Auditor" },
//                 { Name: "Fecha Inicio", Value: "Fecha_Visita" },
//                 //{ Name: "", Value: "CodigoCompensacion" },
//                 { Name: "Punto Abierto", Value: "Punto_Abierto" },
//                 { Name: "Pintura Exterior", Value: "Pintura_Exterior" },
//                 { Name: "Pintura Interior", Value: "Pintura_Interior" },
//                 { Name: "Material POP", Value: "Material_POP" },
//                 { Name: "POP", Value: "POP" },
//                 { Name: "Porta Precios", Value: "Porta_Precios" },
//                 { Name: "Porta Celulares", Value: "Porta_Celulares" },
//                 { Name: "POP Pared", Value: "POP_Pared" },
//                 { Name: "Volantes Brochures", Value: "Volantes_Brochures" },
//                 { Name: "Uniforme", Value: "Uniforme" },
//                 { Name: "Inventario", Value: "Inventario" },
//                 { Name: "Equipos Prepago Claro", Value: "Equipos_Prepago_Claro" },
//                 { Name: "Equipos Prepago Propio", Value: "Equipos_Prepago_Propio" },
//                 { Name: "SIF Inventario Claro", Value: "SIF_Inventario_Claro" },
//                 { Name: "SIF Inventario Propio", Value: "SIF_Inventario_Propio" },
//                 { Name: "BAC Inventario Claro", Value: "BAC_Inventario_Claro" },
//                 { Name: "BAC Inventario Propio", Value: "BAC_Inventario_Propio" },
//                 { Name: "SIM Card Inventario Claro", Value: "SIM_Card_Inventario_Claro" },
//                 { Name: "SIM Card Inventario Propio", Value: "SIM_Card_Inventario_Propio" },
//                 { Name: "Luces de Vitrina", Value: "Luces_Vitrina" },
//                 { Name: "Luces de Techo", Value: "Luces_Techo" },
//                 { Name: "Imagen General Correcta", Value: "Imagen_General_Correcta" },
//                 { Name: "Comentario de la Imagen General", Value: "Imagen_General_Comentario" },
//                 { Name: "Mobiliario", Value: "Mobiliario" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Duracion", Value: "Duracion" },
//                 { Name: "Fecha Planificaci\xf3n", Value: "start" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             selection: []
//         },
//         'visitasvendedorxdia': {
//             parameters: "",
//             url: 'VisitasVendedorXDia',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Nombre de Tienda", Value: "NombreTienda" },
//                 { Name: "Tipo de Punto", Value: "Tipo_Punto" },
//                 { Name: "Clasificaci\xf3n de Punto", Value: "Clasificacion_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Alquilado_Claro" },
//                 { Name: "Tipo de Visita", Value: "Tipo_Visita" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Zona", Value: "Zona" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Auditor", Value: "Auditor" },
//                 { Name: "Fecha Inicio", Value: "Fecha_Visita" },
//                 { Name: "Mes", Value: "Mes" },
//                 { Name: "Comentario", Value: "Comentario" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Duracion", Value: "Duracion" },
//                 { Name: "Fecha Planificaci\xf3n", Value: "start" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             selection: []
//         },
//         'resumenvisitas': {
//             parameters: "",
//             url: 'ResumenVisitas',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(start AS DATE)', 'Fin': 'CAST(start AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Formal", Value: "Formal" },
//                 { Name: "Sucursal", Value: "Sucursal" },
//                 { Name: "Tercero", Value: "Tercero" },
//                 { Name: "Cadenas", Value: "Cadenas" },
//                 { Name: "AAAPlus", Value: "AAAPlus" },
//                 { Name: "AAA", Value: "AAA" },
//                 { Name: "DVD", Value: "DVD" },
//                 { Name: "BBB", Value: "BBB" },
//                 { Name: "Total", Value: "Total" }
//             ],
//             Rango: "",
//             selection: []
//         },
//         'visitas': {
//             parameters: "",
//             url: 'Visitas',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Start AS DATE)', 'Fin': 'CAST(Start AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo del Socio", Value: "Codigo_Socio" },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Nombre de Tienda", Value: "NombreTienda" },
//                 { Name: "Tipo de Punto", Value: "Tipo_Punto" },
//                 { Name: "Clasificaci\xf3n de Punto", Value: "Clasificacion_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Alquilado_Claro" },
//                 { Name: "Tipo de Visita", Value: "Tipo_Visita" },
//                 { Name: "Fecha de Planificaci\xf3n", Value: "Start" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Zona", Value: "Zona" },
//                 { Name: "Tarjeta de Ejecutivo", Value: "ID_Ejecutivo" },
//                 { Name: "Ejecutivo", Value: "Ejecutivo" },
//                 { Name: "Tarjeta de Auditor", Value: "ID_Usuario" },
//                 { Name: "Auditor", Value: "Auditor" },
//                 { Name: "Tipo Auditor", Value: "Tipo_Usuario" },
//                 { Name: "Duraci\xf3n", Value: "Duracion" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Fecha Fin", Value: "Fin" },
//                 { Name: "Aprobado", Value: "Aprobado" },
//             ],
//             Rango: "",
//             selection: []
//         },
//         'inventario': {
//             parameters: "",
//             url: 'inventario',
//             Rango: "",
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "GAMA", Value: "GAMA" },
//                 { Name: "Propio", Value: "Propio" },
//                 { Name: "No Aplica Propio", Value: "No_AplicaPropio" },
//                 { Name: "Claro", Value: "Claro" },
//                 { Name: "No Aplica Claro", Value: "No_AplicaClaro" }
//             ],
//             selection: []
//         },
//         'cumplimientoplanificacion': {
//             parameters: "",
//             url: 'CumplimientoPlanificacion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Start AS DATE)', 'Fin': 'CAST(Start AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [],
//             Rango: "",
//             validateDate: true,
//             selection: []
//         },
//         'cumplimientoauditorias': {
//             parameters: "",
//             url: 'CumplimientoAuditorias',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Start AS DATE)', 'Fin': 'CAST(Start AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//             ],
//             Rango: "",
//             validateDate: true,
//             selection: []
//         },
//         'countbyresponseinspeccion': {
//             parameters: "",
//             url: 'Countbyresponseinspeccion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_Tipo_Punto: 'p.ID_Tipo', Tipo_Visita: 'e.ID_Tipo', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "¿Punto abierto?", Value: "Punto_Abierto" },
//                 { Name: "Aire Acondicionado", Value: "Aire_Acondicionado" },
//                 { Name: "Ayuda Ventas", Value: "AyudaVentas" },
//                 { Name: "Canales HD", Value: "Canales_HD" },
//                 { Name: "Cantidad de Empleados en el PDV", Value: "Cantidad_Empleados" },
//                 { Name: "Claro TV instalado", Value: "Claro_TV" },
//                 { Name: "Cobro de Facturas", Value: "Cobro_Facturas" },
//                 { Name: "Copiadora", Value: "Copiadora" },
//                 { Name: "Equipos zona experiencia", Value: "Equipos_Zona_Experiencia" },
//                 //{ Name: "Especifique el m\xe9todo usado", Value: "Otro_Copiadora" },
//                 { Name: "Higiene Organizaci\xf3n PDV", Value: "Higiene_Organizacion_PDV" },
//                 { Name: "Inventario", Value: "Inventario" },
//                 { Name: "Inventario equipos", Value: "Inventario" },
//                 { Name: "Inversor", Value: "Inversor" },
//                 { Name: "Letrer\xcda", Value: "Letreria" },
//                 //{ Name: "Luces funcionando de vitrinas", Value: "Luces_Vitrina" },
//                 //{ Name: "Luces funcionando del PDV", Value: "Luces_Techo" },
//                 { Name: "Luces techo", Value: "Luces_Techo" },
//                 { Name: "Luces Vitrina", Value: "Luces_Vitrina" },
//                 { Name: "Mobiliario", Value: "Mobiliario" },
//                 { Name: "Objetivo Asignado", Value: "Objetivo_Asignado" },
//                 { Name: "Papel de cobro de factura", Value: "Papel_Cobro_Facturas" },
//                 { Name: "Pintura Exterior", Value: "Pintura_Exterior" },
//                 { Name: "Pintura Interior", Value: "Pintura_Interior" },
//                 { Name: "POP", Value: "POP" },
//                 { Name: "POP de Pared", Value: "POP_Pared" },
//                 { Name: "Porta Celulares", Value: "Porta_Celulares" },
//                 { Name: "Porta Precios", Value: "Porta_Precios" },
//                 { Name: "Recomendado para", Value: "Recomendado_Para" },
//                 { Name: "Sistemas", Value: "Sistemas" },
//                 { Name: "Tiene Claro Video?", Value: "Claro_Video" },
//                 { Name: "TV", Value: "TV" },
//                 { Name: "UPS", Value: "UPS" },
//                 { Name: "Verifone", Value: "Verifone" },
//                 { Name: "Volantes y/o Brochures de diferentes productos.", Value: "Volantes_Brochures" }
//             ],
//             selection: [],
//             Zonas: [],
//             TipoPunto: [],
//             Clasificacion: [],
//             ArchivosReporte: [
//                 { id: 0, Name: "General", Value: "Countbyresponseinspeccion" },
//                 { id: 1, Name: "Zona", Value: "CountByResponseInspeccion1Agrupacion", Agrupacion: "Zona" },
//                 { id: 2, Name: "Socio", Value: "CountByResponseInspeccion1Agrupacion", Agrupacion: "Socios" },
//                 { id: 3, Name: "Ejecutivo", Value: "CountByResponseInspeccion1Agrupacion", Agrupacion: "Ejecutivo" },
//                 { id: 4, Name: "Tipo de Punto", Value: "CountByResponseInspeccion1Agrupacion", Agrupacion: "TipoPunto" },
//                 { id: 5, Name: "Clasificaci\xf3n de Punto", Value: "CountByResponseInspeccion1Agrupacion", Agrupacion: "Clasificacion" },

//                 { id: 6, Name: "Zona-Socio", Value: "CountByResponseInspeccion2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Socios" },
//                 { id: 7, Name: "Zona-Ejecutivo", Value: "CountByResponseInspeccion2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Ejecutivo" },
//                 { id: 8, Name: "Zona-Tipo de Punto", Value: "CountByResponseInspeccion2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "TipoPunto" },
//                 { id: 9, Name: "Zona-Clasificaci\xf3n de Punto", Value: "CountByResponseInspeccion2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Clasificacion" },

//                 { id: 10, Name: "Socios-Zona", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Zona" },
//                 { id: 11, Name: "Socios-Ejecutivo", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Ejecutivo" },
//                 { id: 12, Name: "Socios-Tipo de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Socios", Agrupacion2: "TipoPunto" },
//                 { id: 13, Name: "Socios-Clasificaci\xf3n de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Clasificacion" },

//                 { id: 14, Name: "Ejecutivo-Zona", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Zona" },
//                 { id: 15, Name: "Ejecutivo-Socio", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Socios" },
//                 { id: 16, Name: "Ejecutivo-Tipo de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "TipoPunto" },
//                 { id: 17, Name: "Ejecutivo-Clasificaci\xf3n de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Clasificacion" },

//                 { id: 18, Name: "Tipo de Punto-Zona", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Zona" },
//                 { id: 19, Name: "Tipo de Punto-Socio", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Socios" },
//                 { id: 20, Name: "Tipo de Punto-Ejecutivo", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Ejecutivo" },
//                 { id: 21, Name: "Tipo de Punto-Clasificaci\xf3n de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Clasificacion" },

//                 { id: 22, Name: "Clasificaci\xf3n de Punto-Zona", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Zona" },
//                 { id: 23, Name: "Clasificaci\xf3n de Punto-Socio", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Socios" },
//                 { id: 24, Name: "Clasificaci\xf3n de Punto-Ejecutivo", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Ejecutivo" },
//                 { id: 25, Name: "Clasificaci\xf3n de Punto-Tipo de Punto", Value: "CountByResponseInspeccion2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "TipoPunto" },
//             ],
//             Agrupacion: 0,
//             Rango: "",
//             FiltrosPreguntas: []
//         },
//         'countbyresponseauditorias': {
//             parameters: "",
//             url: 'CountbyresponseAuditorias',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_Tipo_Punto: 'p.ID_Tipo', Tipo_Visita: 'e.ID_Tipo', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Estatus del Punto de Venta", Value: "Estatus_Punto" },
//                 { Name: "Posee Letrero Exterior", Value: "Letrero_Exterior" },
//                 //{ Name: "Fotografia Exterior de la Tienda", Value: "Foto_Exterior" },
//                 { Name: "Posee Accesos y o Sistemas el PDV", Value: "Accesos_Sistemas" },
//                 { Name: "Pintura del Local en Buen Estado?", Value: "Pintura_Local" },
//                 //{ Name: "Foto Pintura en Mal Estado", Value: "Foto_Pintura_Local" },
//                 { Name: "Mobiliario Cumple con Estandar de Marca:", Value: "MobiliarioEstandarizado" },
//                 //{ Name: "Fotografia del Letrero con Anuncio de Desbloqueo", Value: "Foto_Letrero_Desbloqueo" },
//                 { Name: "Nombre del Representante", Value: "Nombre_Representante" },
//                 { Name: "C\xe9dula del Representante", Value: "Cedula_Representante" },
//                 { Name: "Tarjeta del Representante", Value: "Tarjeta_Representante" },
//                 { Name: "Condici\xf3n del Representante", Value: "Condicion_Representante" },
//                 //{ Name: "Especifique condici\xf3n del representante", Value: "Otra_Condicion_Representante" },
//                 { Name: "Recibe Comisi\xf3n por Ventas de cada Producto cuando logra Objetivo?", Value: "Comision" },
//                 //{ Name: "Especifique otra comisi\xf3n", Value: "Otra_Comision" },
//                 { Name: "¿Representante uniformado?", Value: "Uniforme" },
//                 //{ Name: "Fotograf\xcda del Representante sin uniforme", Value: "Foto_Uniforme" },
//                 { Name: "Tiempo Laborando en el PDV", Value: "TiempoLaborando" },
//                 //{ Name: "Fotograf\xcda del Interior del PDV", Value: "Foto_Interior" },
//                 { Name: "Posee Televisor para Claro TV?", Value: "ClaroTV" },
//                 //{ Name: "Especifique el tiempo que ha laborado", Value: "Otro_TiempoLaborando" },
//                 //{ Name: "Espeficique condici\xf3n del servicio Claro TV", Value: "Otro_ClaroTV" }
//             ],
//             selection: [],
//             Zonas: [],
//             TipoPunto: [],
//             Clasificacion: [],
//             ArchivosReporte: [

//                 { id: 0, Name: "General", Value: "CountbyresponseAuditorias" },
//                 { id: 1, Name: "Zona", Value: "CountByResponseAuditorias1Agrupacion", Agrupacion: "Zona" },
//                 { id: 2, Name: "Socio", Value: "CountByResponseAuditorias1Agrupacion", Agrupacion: "Socios" },
//                 { id: 3, Name: "Ejecutivo", Value: "CountByResponseAuditorias1Agrupacion", Agrupacion: "Ejecutivo" },
//                 { id: 4, Name: "Tipo de Punto", Value: "CountByResponseAuditorias1Agrupacion", Agrupacion: "TipoPunto" },
//                 { id: 5, Name: "Clasificaci\xf3n de Punto", Value: "CountByResponseAuditorias1Agrupacion", Agrupacion: "Clasificacion" },

//                 { id: 6, Name: "Zona-Socio", Value: "CountByResponseAuditorias2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Socios" },
//                 { id: 7, Name: "Zona-Ejecutivo", Value: "CountByResponseAuditorias2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Ejecutivo" },
//                 { id: 8, Name: "Zona-Tipo de Punto", Value: "CountByResponseAuditorias2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "TipoPunto" },
//                 { id: 9, Name: "Zona-Clasificaci\xf3n de Punto", Value: "CountByResponseAuditorias2AgrupacionStack", Agrupacion: "Zona", Agrupacion2: "Clasificacion" },

//                 { id: 10, Name: "Socios-Zona", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Zona" },
//                 { id: 11, Name: "Socios-Ejecutivo", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Ejecutivo" },
//                 { id: 12, Name: "Socios-Tipo de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Socios", Agrupacion2: "TipoPunto" },
//                 { id: 13, Name: "Socios-Clasificaci\xf3n de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Socios", Agrupacion2: "Clasificacion" },

//                 { id: 14, Name: "Ejecutivo-Zona", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Zona" },
//                 { id: 15, Name: "Ejecutivo-Socio", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Socios" },
//                 { id: 16, Name: "Ejecutivo-Tipo de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "TipoPunto" },
//                 { id: 17, Name: "Ejecutivo-Clasificaci\xf3n de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Ejecutivo", Agrupacion2: "Clasificacion" },

//                 { id: 18, Name: "Tipo de Punto-Zona", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Zona" },
//                 { id: 19, Name: "Tipo de Punto-Socio", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Socios" },
//                 { id: 20, Name: "Tipo de Punto-Ejecutivo", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Ejecutivo" },
//                 { id: 21, Name: "Tipo de Punto-Clasificaci\xf3n de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "TipoPunto", Agrupacion2: "Clasificacion" },

//                 { id: 22, Name: "Clasificaci\xf3n de Punto-Zona", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Zona" },
//                 { id: 23, Name: "Clasificaci\xf3n de Punto-Socio", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Socios" },
//                 { id: 24, Name: "Clasificaci\xf3n de Punto-Ejecutivo", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "Ejecutivo" },
//                 { id: 25, Name: "Clasificaci\xf3n de Punto-Tipo de Punto", Value: "CountByResponseAuditorias2Agrupacion", Agrupacion: "Clasificacion", Agrupacion2: "TipoPunto" },
//             ],
//             Agrupacion: 0,
//             Rango: "",
//             FiltrosPreguntas: []
//         },
//         'visitainspeccionsemaforo': {
//             parameters: "",
//             url: 'VisitaInspeccionSemaforo',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Aire Acondicionado", Value: "Aire_Acondicionado", Resultado: true },
//                 { Name: "Ayuda Ventas", Value: "AyudaVentas", Resultado: true },
//                 { Name: "BAC Inventario Claro", Value: "BAC_Inventario_Claro", Resultado: true },
//                 { Name: "BAC Inventario Propio", Value: "BAC_Inventario_Propio", Resultado: true },
//                 { Name: "Canales HD", Value: "Canales_HD", Resultado: true },
//                 { Name: "Cantidad Empleados", Value: "Cantidad_Empleados", Resultado: true },
//                 { Name: "Claro TV", Value: "Claro_TV", Resultado: true },
//                 { Name: "Claro Video", Value: "Claro_Video", Resultado: true },
//                 { Name: "Cobro Facturas", Value: "Cobro_Facturas", Resultado: true },
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "C\xf3digo Socio", Value: "Codigo_Socio" },
//                 { Name: "Comentario", Value: "Comentario", Resultado: true },
//                 { Name: "Copiadora", Value: "Copiadora", Resultado: true },
//                 { Name: "Direccion", Value: "Direccion" },
//                 { Name: "Equipos Prepago Claro", Value: "Equipos_Prepago_Claro", Resultado: true },
//                 { Name: "Equipos Prepago Propio", Value: "Equipos_Prepago_Propio", Resultado: true },
//                 { Name: "Equipos Tarifarios Claro", Value: "Equipos_Tarifarios_Claro", Resultado: true },
//                 { Name: "Equipos Tarifarios Propio", Value: "Equipos_Tarifarios_Propio", Resultado: true },
//                 { Name: "Equipos Zona Experiencia", Value: "Equipos_Zona_Experiencia", Resultado: true },
//                 { Name: "Higiene Organizacion PDV", Value: "Higiene_Organizacion_PDV", Resultado: true },
//                 { Name: "Clasificaci\xf3n Punto", Value: "ID_Clasificacion" },
//                 { Name: "Ejecutivo", Value: "ID_Ejecutivo" },
//                 { Name: "Tipo Visita", Value: "ID_Tipo" },
//                 { Name: "Tipo Punto", Value: "ID_Tipo_Punto" },
//                 { Name: "Auditor", Value: "ID_Usuario" },
//                 { Name: "Zona", Value: "ID_Zona" },
//                 { Name: "Puntuaci\xf3n", Value: "Puntuacion" },
//                 { Name: "Fecha Visita", Value: "Inicio" },
//                 { Name: "Inventario", Value: "Inventario", Resultado: true },
//                 { Name: "Inversor", Value: "Inversor", Resultado: true },
//                 { Name: "Letrer\xcda", Value: "Letreria", Resultado: true },
//                 { Name: "Luces Techo", Value: "Luces_Techo", Resultado: true },
//                 { Name: "Luces Vitrina", Value: "Luces_Vitrina", Resultado: true },
//                 { Name: "Mobiliario", Value: "Mobiliario", Resultado: true },
//                 { Name: "Modem ADSL Inventario Claro", Value: "Modem_ADSL_Inventario_Claro", Resultado: true },
//                 { Name: "Modem ADSL Inventario Propio", Value: "Modem_ADSL_Inventario_Propio", Resultado: true },
//                 { Name: "Nombre Tienda", Value: "NombreTienda" },
//                 { Name: "Nombre Socio", Value: "Nombre_Socio" },
//                 { Name: "Objetivo Asignado", Value: "Objetivo_Asignado", Resultado: true },
//                 { Name: "Papel Cobro Factura", Value: "Papel_Cobro_Facturas", Resultado: true },
//                 { Name: "Pintura Exterior", Value: "Pintura_Exterior", Resultado: true },
//                 { Name: "Pintura Interior", Value: "Pintura_Interior", Resultado: true },
//                 { Name: "POP", Value: "POP", Resultado: true },
//                 { Name: "POP Pared", Value: "POP_Pared", Resultado: true },
//                 { Name: "Porta Celulares", Value: "Porta_Celulares", Resultado: true },
//                 { Name: "Porta Precios", Value: "Porta_Precios", Resultado: true },
//                 { Name: "Punto Abierto", Value: "Punto_Abierto", Resultado: true },
//                 { Name: "Recomendado Para", Value: "Recomendado_Para", Resultado: true },
//                 { Name: "SIF Inventario Claro", Value: "SIF_Inventario_Claro", Resultado: true },
//                 { Name: "SIF Inventario Propio", Value: "SIF_Inventario_Propio", Resultado: true },
//                 { Name: "SIM Card Inventario Claro", Value: "SIM_Card_Inventario_Claro", Resultado: true },
//                 { Name: "SIM Card Inventario Propio", Value: "SIM_Card_Inventario_Propio", Resultado: true },
//                 { Name: "Sistemas", Value: "Sistemas", Resultado: true },
//                 { Name: "Tablet Inventario Claro", Value: "Tablet_Inventario_Claro", Resultado: true },
//                 { Name: "Tablet Inventario Propio", Value: "Tablet_Inventario_Propio", Resultado: true },
//                 { Name: "Tel\xe9fono Punto", Value: "Telefono_Punto" },
//                 { Name: "Alquilado por Claro", Value: "Tipo_Local" },
//                 { Name: "TV", Value: "TV", Resultado: true },
//                 { Name: "UPS", Value: "UPS", Resultado: true },
//                 { Name: "Verifone", Value: "Verifone", Resultado: true },
//                 { Name: "Volantes  y/o Brochures", Value: "Volantes_Brochures", Resultado: true },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" }
//             ],
//             Zonas: [],
//             TipoPunto: [],
//             Clasificacion: [],
//             ArchivosReporte: [
//                 { Name: "General", Value: "VisitaInspeccionSemaforo" },
//                 { Name: "Agrupacion por Zona", Value: "VisitaInspeccionSemaforozona" },
//                 { Name: "Agrupacion por Socio", Value: "VisitaInspeccionSemaforoSocio" },
//                 { Name: "Agrupacion por Ejecutivo", Value: "VisitaInspeccionSemaforoEjecutivo" },
//             ],
//             permitirElegir: true,
//             selection: []
//         },
//         'visitaauditoriassemaforo': {
//             parameters: "",
//             url: 'VisitaAuditoriasSemaforo',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_UsuarioGestion: "in" },
//             nombres: { 'Inicio': 'CAST(Inicio AS DATE)', 'Fin': 'CAST(Fin AS DATE)', ID_EjecutivoGestion: "ID_Ejecutivo", ID_UsuarioGestion: "ID_Usuario" },
//             //nombres: { 'Inicio': 'Fecha_Creacion', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "C\xf3digo Localidad", Value: "CodigoLocalidad" },
//                 { Name: "Nombre de Tienda", Value: "NombreTienda" },
//                 { Name: "Codigo del Socio", Value: "Codigo_Socio" },
//                 { Name: "Nombre del Socio", Value: "Nombre_Socio" },
//                 { Name: "Ejecutivo", Value: "ID_Ejecutivo" },
//                 { Name: "Usuario", Value: "ID_Usuario" },
//                 { Name: "Realizada", Value: "Realizada" },
//                 { Name: "Zona", Value: "ID_Zona" },
//                 { Name: "Direcci\xf3n", Value: "Direccion" },
//                 { Name: "Tipo de Punto", Value: "ID_Tipo_Punto" },
//                 { Name: "Clasificacion del Punto", Value: "ID_Clasificacion" },
//                 { Name: "Alquilado Claro", Value: "Tipo_Local" },
//                 { Name: "Tel\xe9fono Punto", Value: "Telefono_Punto" },
//                 { Name: "Puntuaci\xf3n", Value: "Puntuacion" },
//                 { Name: "Tipo Visita", Value: "ID_Tipo" },
//                 { Name: "Estado del Punto", Value: "ID_Estado_Punto" },
//                 { Name: "Rango Error al Finalizar", Value: "RangoErrorFin" },
//                 { Name: "Distancia del Punto al Finalizar", Value: "DistanciaFin" },
//                 { Name: "Rango Error al Iniciar", Value: "RangoErrorInicio" },
//                 { Name: "Distancia del Punto al Iniciar", Value: "DistanciaInicio" },
//                 { Name: "Estatus del Punto de Venta", Value: "Estatus_Punto", Resultado: true },
//                 { Name: "Posee Letrero Exterior", Value: "Letrero_Exterior", Resultado: true },
//                 //{ Name: "Fotografia Exterior de la Tienda", Value: "Foto_Exterior" , Resultado: true },
//                 { Name: "Posee Accesos y o Sistemas el PDV", Value: "Accesos_Sistemas", Resultado: true },
//                 { Name: "Pintura del Local en Buen Estado?", Value: "Pintura_Local", Resultado: true },
//                 //{ Name: "Foto Pintura en Mal Estado", Value: "Foto_Pintura_Local" , Resultado: true },
//                 { Name: "Mobiliario Cumple con Estandar de Marca:", Value: "MobiliarioEstandarizado", Resultado: true },
//                 //{ Name: "Fotografia del Letrero con Anuncio de Desbloqueo", Value: "Foto_Letrero_Desbloqueo" , Resultado: true },
//                 { Name: "Nombre del Representante", Value: "Nombre_Representante", Resultado: true },
//                 { Name: "C\xe9dula del Representante", Value: "Cedula_Representante", Resultado: true },
//                 { Name: "Tarjeta del Representante", Value: "Tarjeta_Representante", Resultado: true },
//                 { Name: "Condici\xf3n del Representante", Value: "Condicion_Representante", Resultado: true },
//                 //{ Name: "Especifique condici\xf3n del representante", Value: "Otra_Condicion_Representante" , Resultado: true },
//                 { Name: "Recibe Comisi\xf3n por Ventas de cada Producto cuando logra Objetivo?", Value: "Comision", Resultado: true },
//                 //{ Name: "Especifique otra comisi\xf3n", Value: "Otra_Comision" , Resultado: true },
//                 { Name: "¿Representante uniformado?", Value: "Uniforme", Resultado: true },
//                 //{ Name: "Fotograf\xcda del Representante sin uniforme", Value: "Foto_Uniforme" , Resultado: true },
//                 { Name: "Tiempo Laborando en el PDV", Value: "TiempoLaborando", Resultado: true },
//                 //{ Name: "Fotograf\xcda del Interior del PDV", Value: "Foto_Interior" , Resultado: true },
//                 { Name: "Posee Televisor para Claro TV?", Value: "ClaroTV", Resultado: true },
//             ],
//             Zonas: [],
//             TipoPunto: [],
//             Clasificacion: [],
//             ArchivosReporte: [
//                 { Name: "General", Value: "VisitaAuditoriasSemaforo" },
//                 { Name: "Agrupacion por Zona", Value: "VisitaAuditoriasSemaforozona" },
//                 { Name: "Agrupacion por Socio", Value: "VisitaAuditoriasSemaforoSocio" },
//                 { Name: "Agrupacion por Ejecutivo", Value: "VisitaAuditoriasSemaforoEjecutivo" },
//             ],
//             permitirElegir: true,
//             selection: []
//         },
//         'empleadoubicacion': {
//             parameters: "",
//             url: 'EmpleadosUbicacion',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", HoraInicio: ">=", HoraFin: "<=", ID_Zona: "in" },
//             nombres: { 'Inicio': 'CAST(Fecha AS DATE)', 'Fin': 'CAST(Fecha AS DATE)', ID_EjecutivoGestion: "e.ID", ID_Ejecutivo: "e.ID", ID_Zona: "e.ID", HoraInicio: "CAST(Fecha AS TIME)", HoraFin: "CAST(Fecha AS TIME)" },
//             //nombres: { 'Hora': 'CONVERT(Fecha )', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Tarjeta de Ejecutivo/ Promotor", Value: "Tarjeta_Ejecutivo" },
//                 { Name: "Nombre de Ejecutivo/ Promotor", Value: "Nombre_Ejecutivo" },
//                 //{ Name: "Estado", Value: "Status" },
//                 { Name: "Telefono", Value: "Telefono" },
//                 { Name: "Email", Value: "email" },
//                 { Name: "Tarjeta Superior", Value: "Tarjeta_Superior" },
//                 { Name: "Superior", Value: "Nombre_Superior" },
//                 { Name: "IMEI", Value: "IMEI" },
//                 { Name: "Ubicaci\xf3n (Direcci\xf3n)", Value: "Direccion" },
//                 { Name: "Fuente de Energ\xeda", Value: "PowerSource" },
//                 { Name: "Latitud", Value: "Latitud" },
//                 { Name: "Longitud", Value: "Longitud" },
//                 { Name: "Fecha", Value: "Fecha" },
//                 { Name: "Hora", Value: "Hora" },
//                 { Name: "Conectado", Value: "Online" },
//                 { Name: "WI FI", Value: "Wifi" },
//                 { Name: "Exactitud", Value: "Accuracy" },
//                 { Name: "Porcentaje de Bater\xcda", Value: "ChangePercent" },
//                 { Name: "Velocidad", Value: "Speed" },
//                 { Name: "Altura", Value: "Altitude" },
//                 { Name: "Angulo respecto al Norte", Value: "Bearing" },
//             ],
//             //permitirElegir: true,
//             currentZona: "",
//             selection: []
//         },
//         'resumengps': {
//             parameters: "",
//             url: 'ResumenGPS',
//             operadores: { 'Inicio': '>=', 'Fin': '<=', ID_EjecutivoGestion: "in", ID_Zona: "in" },
//             nombres: { 'Inicio': 'CAST(Fecha AS DATE)', 'Fin': 'CAST(Fecha AS DATE)', ID_EjecutivoGestion: "Tarjeta", ID_Zona: "Tarjeta", HoraInicio: "CAST(Fecha AS TIME)", ID_Ejecutivo: 'Tarjeta' },
//             //nombres: { 'Hora': 'CONVERT(Fecha )', 'Fin': 'Fecha_Creacion' },
//             filters: {
//                 Inicio: $scope.SQLDate($scope.firstDay),
//                 Fin: $scope.SQLDate($scope.lastDay)
//             },
//             columnas: [
//                 { Name: "Tarjeta de Ejecutivo/ Promotor", Value: "Tarjeta_Ejecutivo" },
//                 { Name: "Nombre de Ejecutivo/ Promotor", Value: "Nombre_Ejecutivo" },
//                 { Name: "Telefono", Value: "Telefono" },
//                 { Name: "IMEI", Value: "IMEI" },
//                 { Name: "Fecha", Value: "Fecha" },
//                 { Name: "Tiempo Recorrido", Value: "Tiempo_Recorrido" },
//                 { Name: "Kilometraje Recorrido", Value: "Kilometraje_Recorrido" },
//                 { Name: "Ubicaci\xf3n Inicial", Value: "Ubicacion_Inicial" },
//                 { Name: "Latitud Inicial", Value: "Latitud_Inicial" },
//                 { Name: "Longitud Inicial", Value: "Longitud_Inicial" },
//                 { Name: "Hora Inicial", Value: "Hora_Inicial" },
//                 { Name: "Ubicaci\xf3n Final", Value: "Ubicacion_Final" },
//                 { Name: "Latitud Final", Value: "Latitud_Final" },
//                 { Name: "Longitud Final", Value: "Longitud_Final" },
//                 { Name: "Hora Final", Value: "Hora_Final" },
//                 { Name: "Cantidad de veces que quedo sin conexi\xf3n", Value: "Cantidad_No_Conexion" },
//                 { Name: "Hora Ultima Conexion", Value: "Hora_Ultima_Conexion" },
//                 { Name: "Tiempo No Conexi\xf3n", Value: "Tiempo_No_Conexion" },
//                 { Name: "Ultima Ubicacion antes de no conexi\xf3n", Value: "Ultima_Ubicacion" },
//             ],
//             //permitirElegir: true,
//             currentZona: "",
//             selection: []
//         },
//     }

//     $scope.filters = {};

//     $scope.GetFiltros = function (report) {
//         //$scope.cleanfilters();
//         $("#frameReport").attr('src', "");
//         $scope.reporteSelected = report;
//     };


//     $scope.Generar = function () {
//         var reporte = $scope.reportes[$scope.reporteSelected];
//         var tempInicio = reporte.filters.Inicio;
//         var tempFin = reporte.filters.Fin;
//         var fechaReporte = "";
//         if (reporte.validateDate === true) {
//             if (reporte.filters.Inicio === undefined || reporte.filters.Fin === undefined || reporte.filters.Inicio === "" || reporte.filters.Fin === "") {
//                 alert('Debe seleccionar un Rango de fecha.');
//                 return;
//             }
//             var fechaInicio = $scope.StringToDate(reporte.filters.Inicio);;
//             var fechaFin = $scope.StringToDate(reporte.filters.Fin);;
//             if (fechaInicio.getMonth() != fechaFin.getMonth() || fechaInicio.getYear() != fechaFin.getYear()) {
//                 alert('El rango de fecha debe estar incluido en el mismo mes.');
//                 return;
//             }

//             fechaReporte = "&FechaInicio=" + encodeURIComponent("'" + reporte.filters.Inicio + "'") + "&FechaFin=" + encodeURIComponent("'" + reporte.filters.Fin + "'");
//             reporte.filters.Inicio = undefined;
//             reporte.filters.Fin = undefined;
//         }


//         reporte.parameters = $scope.getParameters($scope.arrayToString(reporte.selection));

//         //reporte.url = $scope.GetURL(reporte.url);
//         var zonas = "";
//         var TipoPunto = "";
//         var Clasificacion = "";
//         var Agrupacion = "";
//         var inicio = "";
//         var fin = "";
//         console.log(reporte);

//         if (reporte.filters.Inicio != undefined && reporte.filters.Inicio != "") {
//             inicio = " desde " + $scope.ShortDate(reporte.filters.Inicio);
//         }
//         if (reporte.filters.Fin != undefined && reporte.filters.Fin != "") {
//             fin = " hasta " + $scope.ShortDate(reporte.filters.Fin);
//         }
//         // console.log(inicio);
//         // console.log(fin);

//         if (reporte.Agrupacion !== undefined) {
//             for (var i = 0; i < reporte.ArchivosReporte.length; i++) {
//                 if (reporte.ArchivosReporte[i].id == reporte.Agrupacion) {
//                     reporte.url = reporte.ArchivosReporte[i].Value;
//                     if (reporte.ArchivosReporte[i].Agrupacion !== undefined) {
//                         Agrupacion += "&Agrupacion=" + reporte.ArchivosReporte[i].Agrupacion;
//                     }
//                     if (reporte.ArchivosReporte[i].Agrupacion2 !== undefined) {
//                         Agrupacion += "&Agrupacion2=" + reporte.ArchivosReporte[i].Agrupacion2;
//                     }
//                     break;
//                 }
//             }
//         }
//         var props = [
//             { Name: "Zonas", Value: "ID_Zona" },
//             { Name: "TipoPunto", Value: "p.ID_Tipo" },
//             { Name: "Clasificacion", Value: "ID_Clasificacion" }];
//         var ins = "";
//         for (var i = 0; i < props.length; i++) {
//             if (typeof reporte[props[i].Name] !== "undefined") {
//                 var temp = "";
//                 for (var j = 0; j < reporte[props[i].Name].length; j++) {
//                     var item = reporte[props[i].Name][j];
//                     temp += "'" + item.Value + "',";
//                 }
//                 if (temp.length > 0) {
//                     ins += (ins.length > 0 ? " AND " : "") + props[i].Value + " in (" + temp.substring(0, temp.length - 1) + " ) ";
//                 }
//             }
//         }
//         var filter = Utils.GetAdvancedFilter("", reporte.filters, reporte.operadores, reporte.nombres);
//         if (ins.length > 0) {
//             if (filter == "") {
//                 filter = "where " + ins
//             }
//             else {
//                 filter += "and " + ins
//             }
//         }
//         var stringurl = reportServer + $scope.GetURL(reporte.url) + "&FILTER= " + encodeURIComponent(filter);
//         if (typeof reporte.FiltrosPreguntas !== "undefined") {
//             var filtrosPreguntas = "";
//             for (var i = 0; i < reporte.FiltrosPreguntas.length; i++) {
//                 var item = reporte.FiltrosPreguntas[i];
//                 filtrosPreguntas += "'" + item.Value + "',";
//             }
//             filtrosPreguntas = filtrosPreguntas.substring(0, filtrosPreguntas.length - 1);
//             stringurl += "&FiltroPreguntas= " + encodeURIComponent(filtrosPreguntas);
//         }

//         if (reporte.permitirElegir === true) {
//             var filtrosPreguntas = "";
//             for (var i = 0; i < reporte.selection.length; i++) {
//                 var item = reporte.selection[i];
//                 if (item.Resultado === true) {
//                     filtrosPreguntas += "'" + item.Value + "',";
//                 }
//             }
//             filtrosPreguntas = filtrosPreguntas.substring(0, filtrosPreguntas.length - 1);
//             stringurl += "&FiltroPreguntas= " + encodeURIComponent(filtrosPreguntas);
//         }
//         reporte.filters.Inicio = tempInicio;
//         reporte.filters.Fin = tempFin;
//         stringurl += reporte.parameters + Agrupacion + fechaReporte + (reporte.Rango !== undefined ? "&Rango=" + encodeURIComponent(inicio + fin) : "");

//         console.log(stringurl);
//         $("#frameReport").attr('src', stringurl);
//     };
//     $scope.Selected = function (report) {
//         return $scope.reporteSelected == report;
//     };
//     $scope.cleanfilters = function () {
//         var reporte = $scope.reportes[$scope.reporteSelected];
//         reporte.filters = {};
//         //reporte.selection = [];
//         // for(var i = 0; i < reporte.columnas.length; i++)
//         // {
//         //     var item = reporte.columnas[i];
//         //     reporte.selection.push(item);
//         // }
//         //console.log(reporte);
//         $('#' + $scope.reporteSelected + ' .angucomplete-holder input').val('');
//     };
//     $scope.SocioFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Socio = result.originalObject.Value;
//         }
//     };
//     $scope.EjecutivoFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Ejecutivo = result.originalObject.Value;
//         }
//     };

//     $scope.GestionEjecutivoFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_EjecutivoGestion = "select ID from DACS.fn_empleados_tree_down(" + result.originalObject.Value + ")";
//         }
//     };
//     $scope.GestionAuditorFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_UsuarioGestion = "select ID from DACS.fn_empleados_tree_down(" + result.originalObject.Value + ")";
//         }
//     };
//     $scope.AuditorFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Usuario = result.originalObject.Value;
//         }
//     };
//     $scope.PuntoFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Punto = result.originalObject.Value;
//         }
//     };
//     $scope.ZonaFilterSelected = function (result) {
//         if (result) {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Zona = "SELECT ID_Ejecutivo FROM [DACS].[Puntos] WHERE ID_Zona = " + result;
//         }
//         else {
//             $scope.reportes[$scope.reporteSelected].filters.ID_Zona = undefined;
//         }
//     };

//     if (location.hash.startsWith("#/GDE/Reportes")) {
//         $scope.Init()
//     }
// }]);
