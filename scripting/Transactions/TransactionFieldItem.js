import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Button } from '../../reactadmin/components/';

/**
 * Componente en desuso.
 *
 * @export
 * @class TransactionFieldItem
 * @extends {Component}
 * @deprecated
 */
export default class TransactionFieldItem extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	handleClick(e) {
		e.preventDefault();
		if (this.props.onClick) {
			this.props.onClick(this.props.option, e);
		}
	}
	render() {
		const option = this.props.option || {};
		return (
			<Button
				style={{
					margin: 5,
					backgroundColor: option.color,
				}}
				onClick={(e) => this.handleClick(e)}
				label={option.name}
				disabled={this.props.disabled}
			/>
		);
	}
}
