import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

import { PanelCollapsible } from '../../reactadmin/components/';
import { ResultCRM } from '../Interfaces/ResultCRM';
import { ResultSACS } from '../Interfaces/ResultSACS';

/**
 * Componente en desuso.
 *
 * @export
 * @class TransactionResultItem
 * @extends {Component}
 * @deprecated
 */
export default class TransactionResultItem extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	render() {
		const { result } = this.props;
		if (!result) return null;

		const {
			Interface,
		} = result;

		return (
			<PanelCollapsible title={Interface.label}>
				{Interface.name === 'crm' ?
					<ResultCRM {...{ result }} />
					: null
				}
				{Interface.name === 'sacs' ?
					<ResultSACS {...{ result }} />
					: null
				}
				{Interface.name === 'dth' ?
					<div>dth</div>
					: null
				}
				{Interface.name === 'exchange' ?
					<div>exchange</div>
					: null
				}
			</PanelCollapsible>
		);
	}
}
