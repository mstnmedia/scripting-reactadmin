/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Workflow
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { mapStateToProps, ReduxOutlet } from '../../outlets/ReduxOutlet';
import { Page, ModalFactory, Factory, BigAnimatedSpinner, } from '../../reactadmin/components/';
import { TransactionStates, Interfaces } from '../../store/enums';

import { RunningTransaction, ResumedTransaction, } from './';
import { ConfirmModal } from '../ConfirmModal';
import i18n from '../../i18n';
import { respErrorDetails, isFunction } from '../../reactadmin/Utils';

const { RUNNING, COMPLETED, CANCELED, } = TransactionStates;

const actions = ReduxOutlet('transactions', 'transaction').actions;
const interfacesActions = ReduxOutlet('interfaces', 'interfaceBase').actions;

/**
 * Componente que maneja la ruta del detalle de una transacción para determinar si muestra el
 * resumen de la información, si la transacción se ha finalizado o cancelado, o si muestra los
 * componentes de una transacción en curso.
 *
 * @class Transaction
 * @extends {Component}
 */
class Transaction extends Component {
	state = {
		fetchingTransaction: false,
		fetchingRunning: false,
		fetchingCases: false,
		fetchingNotes: false,
		fetchingInteractions: false,
		fetchingOrders: false,
		fetchUsedVersion: false,
		backStep: null,
	}
	componentWillMount() {
		this.handleReduxChangeObj({
			goBackStep: this.goBackStep.bind(this),
			sendToInterface: this.sendToInterface.bind(this),
			sendValue: this.sendValue.bind(this),
			endCall: this.endCall.bind(this),
			fetchTransaction: this.fetchTransaction.bind(this),
		});

		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Transactions'), to: '/data/transactions' },
			{ label: `${i18n.t('Transaction')} #${this.props.params.id}`, },
		];
		this.fetchTransaction(true);

		window.Scripting_Transaction = this;
	}
	componentDidMount() {
		this.props.router.setRouteLeaveHook(this.props.route, this.routerWillLeave.bind(this));
	}
	componentWillReceiveProps(props) {
		const { transactions } = this.props;
		const { id } = props.params;
		const { item } = transactions;
		const trans = this.getTransaction(props);

		if ((`${item.id}` !== id) || (trans && item.state !== trans.state)) {
			if (!this.state.fetchingTransaction) {
				this.fetchTransaction();
			}
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getCustomerCases() {
		const { transactions: { customerCases }, } = this.props;
		return customerCases || [];
	}
	getCustomerNotes() {
		const { transactions: { customerNotes }, } = this.props;
		return customerNotes || [];
	}
	getCustomerInteractions() {
		const { transactions: { customerInteractions }, } = this.props;
		return customerInteractions || [];
	}
	getCustomerOrders() {
		const { transactions: { customerOrders }, } = this.props;
		return customerOrders || [];
	}
	getTransaction(props) {
		const { transactions, } = props || this.props;
		const data = transactions.runningTransaction;
		if (data && data.items) {
			return data.items[0];
		}
		return undefined;
	}
	getTransactionResults() {
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}
	getObjResultFromData(data) {
		const inter = data.interface || {};
		const prefix = `${inter.name}_`;
		const variables = inter.interface_results || [];
		return this.getObjResultFromVariables({ prefix, variables, });
	}
	getObjResultFromVariables({ prefix, variables, }) {
		const result = {};
		for (let i = 0; i < variables.length; i++) {
			const variable = variables[i];
			const name = variable.name.replace(prefix, '');
			let value = null;
			try {
				value = JSON.parse(variable.value);
			} catch (error) {
				value = variable.value;
			}
			result[name] = value;
		}
		return result;
	}

	/*setBreadcrumbItems(steps) {
		const { transactions: { item }, user, } = this.props;
		if (steps && item && user.id === item.id_employee && item.state === TransactionStates.RUNNING) {
			global.getBreadcrumbItems = () => {
				const isDisabled = this.isDisabled();
				return steps.map((step, i) => {
					const isLast = (i + 1) === steps.length;
					const isClickleable = !isLast && !isDisabled;
					return {
						label: step.workflow_step_name,
						to: isClickleable ? `/${step.id}` : '',
						onClick: (e) => {
							e.preventDefault();
							if (isClickleable) this.goBackStep(step);
						},
					};
				});
			};
			return;
		}

		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Transactions'), to: '/data/transactions' },
			{ label: `${i18n.t('Transaction')} #${this.props.params.id}`, },
		];
	}*/

	goBackStep(backStep) {
		this.setState({ backStep });
		ModalFactory.show('confirmModal');
	}
	handleConfirmOk() {
		const { dispatch, token, params: { id }, } = this.props;
		const { backStep } = this.state;
		if (this.state.fetchingRunning) return;
		this.setState({ fetchingRunning: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/goBackStep`,
			item: { id, value: backStep.id },
			path: 'goBackStepData',
			success: (resp) => {
				this.successRunning(resp);
				ModalFactory.hide('confirmModal');
			},
			failure: (resp) => this.failureRunning(resp, 'ErrorTransactionGoingToBackStep'),
		}));
	}
	handleConfirmCancel() {
		ModalFactory.hide('confirmModal');
	}
	sendToInterface(form) {
		const { dispatch, token, params: { id }, } = this.props;
		if (this.state.fetchingRunning) return;
		this.setState({ fetchingRunning: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/sendToInterface`,
			path: 'sendToInterfaceData',
			item: { id, value: JSON.stringify(form), },
			success: (resp) => this.successRunning(resp),
			failure: (resp) => this.failureRunning(resp, 'ErrorTransactionSendingToInterface'),
		}));
	}
	sendValue(value) {
		const { dispatch, token, params: { id }, } = this.props;
		if (this.state.fetchingRunning) return;
		this.setState({ fetchingRunning: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/sendValue`,
			path: 'sendValueData',
			item: { id, value, },
			success: (resp) => this.successRunning(resp),
			failure: (resp) => this.failureRunning(resp, 'ErrorTransactionSelectingOption'),
		}));
	}
	endCall({ cause, note }) {
		const { dispatch, token, params: { id }, } = this.props;
		if (this.state.fetchingRunning) return;
		this.setState({ fetchingRunning: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/endCall`,
			item: { id, value: JSON.stringify({ id, cause, note }) },
			path: 'endCallData',
			success: (resp) => {
				this.preventCloseTab(false);
				this.successRunning(resp);
				ModalFactory.hide('transactionEndModal');
			},
			failure: (resp) => this.failureRunning(resp, 'ErrorCancelingRunningTransaction'),
		}));
	}

	fetchTransaction(clean = false) {
		const { dispatch, token, params: { id }, user, } = this.props;
		this.setState({ fetchingTransaction: true });
		if (clean) {
			dispatch(actions.setProp('item', {}));
			dispatch(actions.setProp('runningTransaction', null));
		}
		dispatch(actions.fetchOne(token, id, (res) => {
			this.setState({ fetchingTransaction: false });
			const { code, } = res.data;
			if (code === 200) {
				const item = res.data.items[0];
				this.fetchRunningTransaction();
				if (item.id_employee === user.id && item.state === TransactionStates.RUNNING) {
					this.preventCloseTab(true);
					this.fetchUsedVersion(item.id_workflow_version);
					const customerID = item.id_customer;
					if (!this.getCustomerInteractions().length && !this.state.fetchingInteractions) {
						this.fetchCustomerInteractions({ customerID });
					}
					if (!this.getCustomerOrders().length && !this.state.fetchingOrders) {
						this.fetchCustomerOrders({ customerID });
					}
					if (!this.getCustomerCases().length && !this.state.fetchingCases) {
						this.fetchCustomerCases({ customerID });
					}
					if (!this.getCustomerNotes().length && !this.state.fetchingNotes) {
						this.fetchCustomerNotes({ customerID });
					}
				}
			}
		}));
	}
	successRunning(resp, saveState = true) {
		this.setState({ fetchingRunning: false });
		const item = resp.data.items[0];
		if (item.state !== TransactionStates.RUNNING) {
			this.preventCloseTab(false);
		}
		const results = item.transaction_results || [];

		// const transactionResults = this.getTransactionResults();
		// const transactionResultsObj = this.getTransactionResultsObj();
		const transactionResults = [];
		const transactionResultsObj = {};
		for (let i = 0; i < results.length; i++) {
			const result = results[i];
			// TODO: Evaluar si esta condición es innecesaria. 
			//		Aquí no llegan variables que no se pueden guardar.
			if (this.canSaveResult(result)) { 
				if (transactionResultsObj[result.name]) {
					const index = transactionResults.indexOf(transactionResultsObj[result.name]);
					transactionResults[index] = result;
				} else {
					transactionResults.push(result);
				}
				transactionResultsObj[result.name] = result;
			}
		}

		const stateUpdate = {
			transactionResults,
			transactionResultsObj,
		};
		if (saveState) {
			stateUpdate.runningTransaction = resp.data;
		}
		this.handleReduxChangeObj(stateUpdate);

		// const steps = item.transaction_steps || [];
		// this.setBreadcrumbItems(steps);
		$('.step-contents-wrapper').animate({ scrollTop: 0 }, 300);
	}
	failureRunning(resp, tag) {
		this.setState({ fetchingRunning: false });
		if (resp.status === 406) {
			window.message.error(i18n.t('TransactionNotModifiable'), 0,
				respErrorDetails(resp)
			);
			this.fetchTransaction(true);
		} else {
			window.message.error(i18n.t(tag || 'ErrorFetchingCurrentTransactionData'), 0,
				respErrorDetails(resp));
		}
	}
	fetchRunningTransaction() {
		const { dispatch, token, params: { id }, } = this.props;
		if (!this.state.fetchingRunning) {
			this.setState({ fetchingRunning: true });
			dispatch(actions.postToURL({
				token,
				route: `transaction/${id}/running`,
				path: 'runningTransaction',
				success: (resp) => this.successRunning(resp, false),
				failure: (resp) => this.failureRunning(resp),
			}));
		}
	}
	fetchUsedVersion(versionID) {
		const { dispatch, token, } = this.props;
		const { fetchUsedVersion } = this.state;
		if (!fetchUsedVersion) {
			this.setState({ fetchUsedVersion: true });
			dispatch(actions.postToURL({
				token,
				route: 'transaction/usedVersion',
				item: { id: versionID },
				path: 'usedVersion',
				success: (resp) => {
					if (resp.data < 2) {
						window.message.info(i18n.t('FirstUseNotification'), 5000);
					}
				},
				failure: () => this.setState({ fetchUsedVersion: false }),
			}));
		}
	}

	fetchCustomerCases({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerCases_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerCasesData, },
		} = this.props;

		const mapFunction = (data) => {
			this.setState({ fetchingCases: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerCases) {
				const customerCases = (baseList || []).slice();
				fetchResults.customerCases.forEach(i => {
					customerCases.push(i);
					if (i.notes) {
						i.notes.sort((a, b) => {
							if (a.creationDate > b.creationDate) return -1;
							else if (a.creationDate < b.creationDate) return 1;
							return 0;
						});
					}
				});
				customerCases.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerCases,
					casesLastPageLoaded: fetchResults.pageCases,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerCasesData)) {
			mapFunction(customerCasesData);
		} else {
			this.setState({ fetchingCases: true });
			const failure = (resp) => {
				this.setState({ fetchingCases: false });
				window.message.error(i18n.t('ErrorFetchingCustomerCases'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Cases}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerNotes({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerNotes_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerNotesData, },
		} = this.props;

		const mapFunction = (data) => {
			this.setState({ fetchingNotes: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerNotes) {
				const customerNotes = (baseList || []).slice();
				fetchResults.customerNotes.forEach(i => customerNotes.push(i));
				customerNotes.sort((a, b) => {
					if (a.id > b.id) return -1;
					else if (a.id < b.id) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerNotes,
					notesLastPageLoaded: fetchResults.pageNotes,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerNotesData)) {
			mapFunction(customerNotesData);
		} else {
			this.setState({ fetchingNotes: true });
			const failure = (resp) => {
				this.setState({ fetchingNotes: false });
				window.message.error(i18n.t('ErrorFetchingCustomerNotes'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Notes}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerInteractions({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerInteractions_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerInteractionsData, },
		} = this.props;

		const mapFunction = (data) => {
			this.setState({ fetchingInteractions: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerInteractions) {
				const customerInteractions = (baseList || []).slice();
				fetchResults.customerInteractions.forEach(i => customerInteractions.push(i));
				customerInteractions.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerInteractions,
					interactionsLastPageLoaded: fetchResults.pageInteractions,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerInteractionsData)) {
			mapFunction(customerInteractionsData);
		} else {
			this.setState({ fetchingInteractions: true });
			const failure = (resp) => {
				this.setState({ fetchingInteractions: false });
				window.message.error(i18n.t('ErrorFetchingCustomerInteractions'), 0,
					respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Interactions}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerOrders({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerOrders_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerOrdersData, },
		} = this.props;

		const mapFunction = (data) => {
			this.setState({ fetchingOrders: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerOrders) {
				const customerOrders = (baseList || []).slice();
				fetchResults.customerOrders.forEach(i => customerOrders.push(i));
				customerOrders.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerOrders,
					ordersLastPageLoaded: fetchResults.pageOrders,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerOrdersData)) {
			mapFunction(customerOrdersData);
		} else {
			this.setState({ fetchingOrders: true });
			const failure = (resp) => {
				this.setState({ fetchingOrders: false });
				window.message.error(i18n.t('ErrorFetchingCustomerOrders'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.OMS.Orders}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}

	clearReduxResults() {
		this.handleReduxChangeObj({
			transactionResults: [],
			transactionResultsObj: {},
		});
	}
	canSaveResult(result) {
		try {
			if (result.props) {
				const props = JSON.parse(result.props);
				return !props.notSave;
			}
		} catch (error) {
			// console.log('error parsing result.props of ', result, error);
		}
		return true;
	}
	// handleReduxChange(prop, value) {
	// 	const { dispatch, } = this.props;
	// 	dispatch(actions.setProp(prop, value));
	// }
	handleReduxChange(prop, value, callback) {
		this.props.dispatch(actions.updateState(prop, value))
			.then(() => isFunction(callback) && callback());
	}
	handleReduxChangeObj(newState, callback) {
		this.props.dispatch(actions.updateStateObj(newState))
			.then(() => isFunction(callback) && callback());
	}

	isDisabled() {
		return this.state.fetchingTransaction
			|| this.state.fetchingRunning;
	}
	isSuccessInterfaceResponse(data) {
		return (data && data.action === 'NEXT_STEP');
	}

	preventCloseTab(prevent) {
		window.onbeforeunload = prevent
			? (function (event) {
				// console.log(event);
				const e = event;
				e.returnValue = i18n.t('NotExitFromRunningTransaction');
				return e.returnValue;
			})
			: undefined;
	}
	routerWillLeave() {
		// if (window.config.ignoreActiveTransactions) return null;
		// const item = this.getTransaction();
		// if (item && item.state === TransactionStates.RUNNING) {
		// 	alert(i18n.t('NotExitFromRunningTransaction'));
		// 	return false;
		// }
		// return null;
		if (window.onbeforeunload) {
			alert(i18n.t('NotExitFromRunningTransaction'));
			return false;
		}
		return null;
	}

	renderView() {
		const { transactions: { item, }, params, router, user, } = this.props;
		if (item.state === COMPLETED || item.state === CANCELED ||
			(item.id_employee && item.id_employee !== user.id)) {
			return <ResumedTransaction {...{ item, params, router, }} />;
		} else if (item.state === RUNNING) {
			const { fetchingCases, fetchingNotes, fetchingInteractions, fetchingOrders, } = this.state;
			return (
				<RunningTransaction
					{...{ item, params, router, disabled: this.isDisabled() }}
					{...{ fetchingCases, fetchingNotes, fetchingInteractions, fetchingOrders, }}
					{...{
						isSuccessInterfaceResponse: this.isSuccessInterfaceResponse.bind(this),
						getObjResultFromData: this.getObjResultFromData.bind(this),
						getObjResultFromVariables: this.getObjResultFromVariables.bind(this),
						fetchCustomerCases: this.fetchCustomerCases.bind(this),
						fetchCustomerNotes: this.fetchCustomerNotes.bind(this),
						fetchCustomerInteractions: this.fetchCustomerInteractions.bind(this),
						fetchCustomerOrders: this.fetchCustomerOrders.bind(this),
					}}
				/>
			);
		}

		// return <div>{i18n.t('Loading')}...</div>;
		return (
			<BigAnimatedSpinner />
		);
	}
	render() {
		const { backStep } = this.state;
		const nameStep = backStep && backStep.workflow_step_name;
		const description = `${i18n.t('GoToBackStep')}${nameStep && ` "${nameStep}"`} `
			+ `${i18n.t('GoToBackStepNoDiscard')}.`
			+ `${i18n.t('GoToBackStepCanDuplicate')}.`;
		return (
			<Page>
				<Factory
					modalref="confirmModal" factory={ConfirmModal}
					title={i18n.t('SureGoToBackStep')}
					description={description}
					onClickOk={() => this.handleConfirmOk()}
					onClickCancel={() => this.handleConfirmCancel()}
					disabled={this.isDisabled()}
				/>
				{this.renderView()}
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Transaction);
