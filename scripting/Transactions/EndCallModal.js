import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Input, FormGroup, Tooltip, } from '../../reactadmin/components/';
import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';

/**
 * Componente que controla el formulario de los detalles de una razón de fin de llamada.
 *
 * @class EndCallModal
 * @extends {Component}
 */
class EndCallModal extends Component {
	state = {}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const { item, } = this.props;
		if (item) {
			item[prop] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem(e) {
		e.preventDefault();
		const { item, } = this.props;
		if (item) {
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('endcall.add') || user.hasPermissions('endcall.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name; // && (!this.props.user.master || item.id_center);
	}

	render() {
		const { item: { id, name, deleted, }, } = this.props;
		return (
			<div>
				{id &&
					<FormGroup>
						<label className="block" htmlFor="name">{i18n.t('ID')}</label>
						<Input type="number" className="form-control" value={id || ''} disabled />
					</FormGroup>
				}
				<FormGroup isValid={!!name}>
					<label className="block" htmlFor="name">{i18n.t('Name')}</label>
					<Input
						type="text" className="form-control"
						placeholder={i18n.t('Name')} value={name || ''}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isDisabled()} required
					/>
				</FormGroup>

				<FormGroup>
					<label htmlFor="deleted">{i18n.t('Disabled')}</label>
					<Tooltip position="right" tag="TooltipEndCallDisabled">
						<Input
							type="checkbox"
							className="inline" checked={deleted || false}
							containerStyle={{ margin: 5, }}
							onFieldChange={(e) => this.handleChange('deleted', e.target.checked)}
							disabled={this.isDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				<div className="">
					<hr />
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(EndCallModal);
