import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import shallowCompare from 'react-addons-shallow-compare';

import /* ReduxOutlet, */ { mapStateToProps } from '../../outlets/ReduxOutlet';

import { Input } from '../../reactadmin/components/ui/';

// const transactionNoteActions = ReduxOutlet('transactions', 'transactionNote').actions;

/**
 * Componente que muestra el detalle de una nota de un caso de CRM.
 *
 * @class NoteDetailModal
 * @extends {Component}
 */
class NoteDetailModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentWillMount() {
		const { transactionNotes } = this.props;
		const { item: note } = transactionNotes;
		this.setState({
			...note,
			createdDate: moment(note.docdate).format('DD-MM-YYYY hh:mm A')
		});
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	handleChange(property, value) {
		this.setState({ [property]: value });
	}
	render() {
		// const note = this.props.note || {};
		const { transactionNotes } = this.props;
		const { item: note } = transactionNotes;
		return (
			<div className="container-fluid">
				<form role="form">
					<div className="form-group">
						<label htmlFor="docdate">Fecha de creación: </label>
						<span>{this.state.createdDate}</span>
					</div>
					<div className="form-group">
						<label htmlFor="docdate">Paso: </label>
						<span>{note.workflow_step_name}</span>
					</div>
					<div className="form-group">
						<label htmlFor="note">Nota</label>
						<Input
							icon="fa fa-rocket"
							className="form-control has-error" 
							type="text" 
							value={this.state.value || note.value || ''}
							placeholder={'Escribe aquí el contenido'}
							onFieldChange={(e) => {
								this.handleChange('value', e.target.value);
							}}
							errorMessage="Please enter a valid URL"
						/>
						<span className="text-muted help-block m-b-none">{this.state.errormessage}</span>
					</div>
				</form>
				<div className="modal-footer">
					<button
						type="button" onClick={(e) => this.handleCreate(e)}
						className="btn btn-info btn-block w-pad"
					>Agregar</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(NoteDetailModal);
