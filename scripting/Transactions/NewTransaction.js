import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

// import { EditorState, } from 'draft-js';
import { createEditorState, } from 'medium-draft';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	// Page,
	// Row, Col,
	Panel,
	Input,
	Button,
	Editor, /* TextArea, */
	ButtonGroup,
	// DataTable,
} from '../../reactadmin/components/';
import { TransactionStates, TransactionStateNames } from '../../store/enums';

const dataSchema = {
	name: 'transactions',
	description: 'A simple messaging schema',
	fields: {
		end_date: {
			type: 'DateTime',
			label: 'Fecha de Fin'
		},
		start_date: {
			type: 'DateTime',
			label: 'Fecha de Inicio'
		},
		state: {
			type: 'String',
			label: 'Estado',
			format: (row) => TransactionStateNames()[row.state],
		},
		employee: {
			type: 'String',
			label: 'Representante',
			format: (row) => row.employee.name,
		},
		workflow_step: {
			type: 'String',
			label: 'Paso',
			format: (row) => row.workflow_step.name,
		},
		workflow: {
			type: 'String',
			label: 'Flujo',
			format: (row) => row.workflow.name,
		},
		category: {
			type: 'String',
			label: 'Categoría',
			format: (row) => row.category.name,
		},
		id: {
			type: 'Number',
			label: 'ID'
		},
	}
};

const transactionActions = ReduxOutlet('transactions', 'transaction').actions;
const interfacesActions = ReduxOutlet('interfaces', 'interface').actions;

/** 
 * Componente que se usaba para iniciar nuevas transacciones.
 *
 * @class NewTransaction
 * @extends {Component}
 * @deprecated
 */
class NewTransaction extends Component {
	state = {
		editorState: createEditorState(),
		note: '',
		searchType: 'phone',
		searchValue: '',
		selectedClient: null,
		selectedService: null,
		// service: {
		// 	id: 1,
		// 	phone: '',
		// 	id_customer: 1,
		// },
		// client: {
		// 	id: 1,
		// 	name: 'Abel Matos',
		// 	id_subscription: 1,
		// },
		error: { phone: '' },
	}

	componentWillMount() {
		// window.Scripting_NewTransaction = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	createTransaction() {
		const { dispatch, token, item } = this.props;
		const { selectedClient, selectedService, workflow, note, } = this.state;
		const newTransaction = {
			id: 0,
			id_workflow: workflow.id,
			id_workflow_version: workflow.version_id,
			start_date: item.start_date,
			state: TransactionStates.RUNNING,
			note,
			// caller_phone: '(829) 496-1819',
			// caller_alt_phone: '(809) 681-7536',
			// id_current_step: 1 || workflow.workflow_steps[0].id,
			id_customer: selectedClient.contratoid,
			id_subscription: selectedService.id,
			id_employee: 1,
		};

		this.setState({ disabled: true });
		dispatch(transactionActions.create(token, newTransaction, (data) => {
			this.setState({ disabled: false });
			const { code, items } = data.data;
			if (code === 200 && items && items[0] && items[0].id) {
				// console.log('change url');

				this.props.router.push(`/data/${dataSchema.name}/${items[0].id}`);
			}
		}, () => this.setState({ disabled: false })));
	}

	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}

	handleNoteChange(newState) {
		const note = this.editor.exporter(newState.getCurrentContent());
		this.setState({
			note,
			editorState: newState,
		});
	}

	handleWorkflowClick(e, workflow) {
		e.preventDefault();

		this.setState({ workflow }, () => this.createTransaction());
	}

	handleChange(prop, value) {
		this.setState({ [prop]: value });
	}
	fetchCustomerInfo() {
		const { dispatch, token, } = this.props;
		const { searchType, searchValue } = this.state;

		dispatch(interfacesActions.postToURL({
			token,
			route: `interface/${1001}/getForm`,
			item: { form: JSON.stringify({ [searchType]: searchValue }) },
			path: 'clientInfo',
			success: (data) => {
				const selectedClient = {};
				const variables = data.data.interface ? data.data.interface.interface_results : [];
				for (let i = 0; i < variables.length; i++) {
					const variable = variables[i];
					const name = variable.name.replace('oms_client_services_', '');
					let value = null;
					try {
						value = JSON.parse(variable.value);
					} catch (error) {
						value = variable.value;
					}
					selectedClient[name] = value;
				}
				this.setState({ selectedClient, selectedService: null });
			}
		}));
	}

	render() {
		const { workflows } = this.props;
		const { searchType, searchValue, selectedClient, selectedService } = this.state;
		return (
			<div>
				<Panel title="Búsqueda de cliente">
					<div className="form-group">
						<label htmlFor="clientPhone">Buscar por:&nbsp;</label>
						<ButtonGroup
							items={{ active: searchTypesName[searchType], values: searchTypes, }}
							onChange={(selected) => {
								this.handleChange('searchType', selected);
								this.handleChange('searchValue', '');
							}}
						/>
					</div>

					<div className="form-group">
						<label htmlFor="clientPhone">{searchTypesName[searchType]}</label>
						<Input
							classes="no-inline"
							icon="fa fa-2x fa-phone"
							name="clientPhone"
							placeholder="Búsqueda"
							onFieldChange={(e) => this.handleChange('searchValue', e.target.value)}
							onKeyPress={(e) => e.key === 'Enter' && this.fetchCustomerInfo()}
							value={searchValue}
						/>
					</div>
				</Panel>
				{selectedClient &&
					<Panel title={`Servicios de ${selectedClient.clientname}`}>
						<div className="table-responsive">
							<table className="table">
								<thead>
									<tr>
										<th>Tipo de servicio</th>
										<th>Características</th>
									</tr>
								</thead>
								{selectedClient.servicesList && selectedClient.servicesList.map((service) => (
									<tbody
										key={service.id}
										onClick={() => this.setState({ selectedService: service })}
									>
										<tr>
											<td colSpan="2">
												{service.product_name} {service.phone} (Activo)
										</td>
										</tr>
										<tr>
											<td />
											<td>Velocidad: {service.velocity}</td>
										</tr>
									</tbody>
								))}
							</table>
						</div>
					</Panel>
				}
				{selectedClient && selectedService &&
					<Panel title="¿Qué nos reporta el cliente?">
						<div className="form-group">
							<label htmlFor="symptom">Síntoma: </label>
							<Editor
								ref={ref => { this.editor = ref; }}
								className="form-control"
								onFieldChange={(body) => {
									this.handleNoteChange(body);
								}}
								placeholder={'Agrega contenido...'}
								editorState={this.state.editorState}
							/>
							{/* <TextArea
							classes="no-inline"
							icon="fa fa-2x fa-edit"
							name="symptom"
							placeholder="Descripción dada por el cliente"
							onFieldChange={(e) => this.handleChange('symptom', e.target.value)}
							value={this.state.phone}
						/> */}
						</div>

						<div className="m-b-sm">
							{workflows.temp.map((workflow, i) => (
								<div key={i} className="col-sm-3">
									<Button
										className="btn-danger m-b-sm"
										onClick={(e) => this.handleWorkflowClick(e, workflow)}
										label={workflow.name}
									/>
								</div>
							))}
						</div>
					</Panel>
				}
			</div>
		);
	}
}
const searchTypes = {
	Teléfono: 'phone',
	Cédula: 'client_document',
	RNC: 'rnc',
	Pasaporte: 'passport',
};
const searchTypesName = {
	phone: 'Teléfono',
	client_document: 'Cédula',
	rnc: 'RNC',
	passport: 'Pasaporte',
};
export default connect(mapStateToProps)(NewTransaction);
