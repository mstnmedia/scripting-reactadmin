import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import /* ReduxOutlet,  */ { mapStateToProps } from '../../outlets/ReduxOutlet';

/**
 * Componente en desuso.
 *
 * @class TransactionDetailModal
 * @extends {Component}
 * @deprecated
 */
class TransactionDetailModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	render() {
		const result = this.props.result || {};
		return (
			<section
				style={{
					marginBottom: 0
				}}
			>
				<div className="panel-body">
					{result.name}
					{result.result}
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(TransactionDetailModal);
