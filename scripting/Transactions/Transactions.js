import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col, Page,
	DataTable,
	PanelCollapsible,
	Input,
	ButtonGroup,
	AdvancedFilters,
	Button,
	Factory,
	ModalFactory,
	Tooltip
} from '../../reactadmin/components/';
import { TransactionStateNames, filterAdvActions, TransactionStates, } from '../../store/enums';
import i18n from '../../i18n';
import { setFilterHandles, NameIDText } from '../../Utils';
import RemoteAutocomplete from '../RemoteAutocomplete';
import TransactionEndModal from './TransactionEndModal';
import { DialogModal } from '../DialogModal';
import { respErrorDetails } from '../../reactadmin/Utils';

const actions = ReduxOutlet('transactions', 'transaction').actions;
/**
 * Componente que controla la pantalla de consulta de transacciones.
 *
 * @class Transactions
 * @extends {Component}
 */
class Transactions extends Component {
	state = {
		endModalProps: {
			note: '',
			disabled: false,
			onCreate: (data) => this.endCall(data)
		},
		confirmModalProps: {},
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'transactions', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Transactions'), },
		];

		// window.Scripting_Transactions = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	handleItemCancel(row, e) {
		e.stopPropagation();
		const id = row.id;
		const okClick = () => {
			this.setState({
				endModalProps: {
					note: row.note,
					disabled: () => this.state[`canceling_${id}`],
					onCreate: (data) => {
						ModalFactory.hide('confirmModal');
						this.endCall({ ...data, id, });
					},
				},
			});
			ModalFactory.show('transactionEndModal');
		};
		const closeClick = () => ModalFactory.hide('confirmModal');
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishEndCall'),
				description: i18n.t('WishEndCallDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: okClick,
						className: 'btn-danger',
						disabled: () => this.state[`canceling_${id}`],
					},
					{ label: i18n.t('Close'), onClick: closeClick, className: 'btn-default', },
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	endCall({ id, cause, note }) {
		const { dispatch, token, } = this.props;
		const path = `canceling_${id}`;
		this.setState({ [path]: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/endCall`,
			item: { id, value: JSON.stringify({ id, cause, note }) },
			path: 'runningTransaction',
			success: () => {
				this.setState({ [path]: false });
				window.message.success(i18n.t('RunningTransactionCancelingSuccess', { id }));
				ModalFactory.hide('transactionEndModal');
				this.fetchData(this.props.transactions.pageNumber);
			},
			failure: (resp) => {
				this.setState({ [path]: false });
				window.message.error(i18n.t('ErrorCancelingRunningTransaction', { id }), 0,
					respErrorDetails(resp));
			},
		}));
	}

	tableSchema() {
		return {
			fields: {
				cancel: {
					label: ' ',
					format: (row) => (row.state === TransactionStates.RUNNING &&
						<Tooltip system position="left" tag="TooltipCancelTransaction">
							<Button
								className="btn-danger btn-xs inline-block"
								icon="fa-close"
								onClick={(e) => this.handleItemCancel(row, e)}
								disabled={this.state[`canceling_${row.id}`]}
							/>
						</Tooltip>
					),
				},
				end_date: {
					type: 'DateTimeZ',
					label: i18n.t('EndDate')
				},
				state: {
					label: i18n.t('State'),
					format: (row) => TransactionStateNames()[row.state],
				},
				start_date: {
					type: 'DateTimeZ',
					label: i18n.t('StartDate'),
				},
				subscriber_no: i18n.t('SubscriberNo'),
				employee_name: this.props.user.master
					|| (this.props.user.role || '').toLowerCase() !== 'representante'
					? ({
						label: i18n.t('Salesman'),
						format: (row) => `${row.employee_name || ''} (${row.id_employee})`,
					})
					: undefined,
				category_name: i18n.t('Category'),
				workflow_step_name: i18n.t('Step'),
				workflow_name: i18n.t('Workflow'),
				id: i18n.t('ID'),
			}
		};
	}

	renderFilters() {
		const { user: { master, role, }, } = this.props;
		const { where, whereObj, advancedSearch } = this.state;
		const employeeID = this.filterValue('id_employee');
		const centerID = this.filterValue('id_center');
		const workflowID = this.filterValue('id_workflow');
		const stepID = this.filterValue('id_current_step');
		const categoryID = this.filterValue('id_category');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group col-md-3">
							<label htmlFor="from">{i18n.t('FromDate')}:</label>
							<Input
								type="date"
								value={this.filterValue('from', '')}
								onFieldChange={(e) => this.filterChange({
									type: 'date',
									label: 'from',
									name: 'start_date',
									criteria: '>=',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="to">{i18n.t('ToDate')}:</label>
							<Input
								type="date"
								value={this.filterValue('to', '')}
								onFieldChange={(e) => this.filterChange({
									type: 'date',
									label: 'to',
									name: 'start_date',
									value: e.target.value,
									criteria: '<=',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{(master || (role || '').toLowerCase() !== 'representante') &&
							<div className="form-group col-md-3">
								<label htmlFor="name">{i18n.t('Salesman')}:</label>
								{employeeID ? <span> {employeeID}</span> : null}
								<RemoteAutocomplete
									url={`${window.config.backend}/employees/employee`}
									getItemValue={(item) => NameIDText(item)}
									value={this.filterText('employee_name') || ''}
									onFieldChange={(e, text) => {
										this.filterTextChange('employee_name', text);
										this.filterChange({ name: 'id_employee', value: undefined });
									}}
									onSelect={(text, item) => {
										this.filterTextChange('employee_name', item.name);
										this.filterChange({ name: 'id_employee', value: item.id });
									}}
									onBlur={() => {
										if (!this.filterValue('id_employee')) {
											this.filterTextChange('employee_name', '');
										}
									}}
									onKeyDown={this.onKeyDownMixed}
								/>
							</div>
						}
						<div className="form-group col-md-3">
							<label htmlFor="state">{i18n.t('State')}:</label>
							<div>
								<select
									className="form-control"
									value={this.filterValue('state', '')}
									onChange={(e) => this.filterChange({ name: 'state', value: e.target.value })}
									onKeyDown={this.onKeyDownSimple}
								>
									<option value={''}>{i18n.t('Anyone')}</option>
									{Object.keys(TransactionStateNames()).map(key => (
										<option key={key} value={key}>{TransactionStateNames()[key]}</option>
									))}
								</select>
							</div>
						</div>
						{master && <div className="form-group col-md-3">
							<label htmlFor="center">{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/centers/center`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('center_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('center_name', text);
									this.filterChange({ name: 'id_center', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('center_name', item.name);
									this.filterChange({ name: 'id_center', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_center')) {
										this.filterTextChange('center_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>}
						<div className="form-group col-md-3">
							<label htmlFor="workflow">{i18n.t('Workflow')}:</label>
							{workflowID ? <span> {workflowID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/workflows/workflow`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('workflow_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('workflow_name', text);
									this.filterChange({ name: 'id_workflow', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('workflow_name', item.name);
									this.filterChange({ name: 'id_workflow', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_workflow')) {
										this.filterTextChange('workflow_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="step">{i18n.t('Step')}:</label>
							{stepID ? <span> {stepID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/workflows/workflowstep`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('step_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('step_name', text);
									this.filterChange({ name: 'id_current_step', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('step_name', item.name);
									this.filterChange({ name: 'id_current_step', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_current_step')) {
										this.filterTextChange('step_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="category">{i18n.t('Category')}:</label>
							{categoryID ? <span> {categoryID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/categories/category`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('category_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('category_name', text);
									this.filterChange({ name: 'id_category', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('category_name', item.name);
									this.filterChange({ name: 'id_category', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_category')) {
										this.filterTextChange('category_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
					</div>
					:
					<AdvancedFilters
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
							id_workflow_version: { type: 'number', label: i18n.t('WorkflowVersionID'), onKeyDown: this.onKeyDownSimple, },
							version_id_version: { type: 'number', label: i18n.t('WorkflowVersion'), onKeyDown: this.onKeyDownSimple, },
							subscriber_no: { type: 'number', label: i18n.t('SubscriberNo'), onKeyDown: this.onKeyDownSimple, },
							id_call: { type: 'text', label: i18n.t('Call'), onKeyDown: this.onKeyDownSimple, },
							category_name: { type: 'text', label: i18n.t('CategoryName'), onKeyDown: this.onKeyDownSimple, },
							end_cause_name: { type: 'text', label: i18n.t('EndCallReasonName'), onKeyDown: this.onKeyDownSimple, },
							workflow_name: { type: 'text', label: i18n.t('WorkflowName'), onKeyDown: this.onKeyDownSimple, },
							workflow_step_name: { type: 'text', label: i18n.t('StepName'), onKeyDown: this.onKeyDownSimple, },
							note: { type: 'clob', label: i18n.t('Notes'), onKeyDown: this.onKeyDownSimple, },
							start_date: { type: 'datetime', label: i18n.t('StartDate'), onKeyDown: this.onKeyDownSimple, },
							end_date: { type: 'datetime', label: i18n.t('EndDate'), onKeyDown: this.onKeyDownSimple, },
							workflow_step_entry_date: { type: 'datetime', label: i18n.t('StepEntryDate'), onKeyDown: this.onKeyDownSimple, },
							end_cause: { type: 'autocomplete', label: i18n.t('EndCallReason'), url: `${window.config.backend}/transactions/endCall`, onKeyDown: this.onKeyDownMixed, },
							id_category: { type: 'autocomplete', label: i18n.t('Category'), url: `${window.config.backend}/transactions/category`, onKeyDown: this.onKeyDownMixed, },
							id_workflow: { type: 'autocomplete', label: i18n.t('Workflow'), url: `${window.config.backend}/workflows/workflow`, onKeyDown: this.onKeyDownMixed, },
							id_current_step: { type: 'autocomplete', label: i18n.t('Step'), url: `${window.config.backend}/workflows/workflowstep`, onKeyDown: this.onKeyDownMixed, },
							state: { type: 'dropdown', label: i18n.t('State'), items: Object.keys(TransactionStates).map(k => ({ value: TransactionStates[k], name: TransactionStateNames()[TransactionStates[k]] })), onKeyDown: this.onKeyDownSimple },
							...(master ? {
								id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
								center_name: { type: 'text', label: i18n.t('CenterName'), onKeyDown: this.onKeyDownSimple, },
							} : {}),
							...((master || (role || '').toLowerCase() !== 'representante') ? {
								id_employee: {
									type: 'autocomplete',
									label: i18n.t('Salesman'),
									url: `${window.config.backend}/employees/employee`,
									onKeyDown: this.onKeyDownMixed,
								},
								employee_name: {
									type: 'text',
									label: i18n.t('SalesmanName'),
									onKeyDown: this.onKeyDownSimple,
								},
							} : {}),
						}}/* eslint-enable max-len */
						{...{ where, whereObj }}
					/>
				}

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5, }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { transactions: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
		const { endModalProps, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('transactions.add');
		return (
			<Page>
				<Factory
					modalref="transactionEndModal"
					title={i18n.t('WhyEndCall')}
					factory={TransactionEndModal}
					{...endModalProps}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={this.tableSchema()} condensed
							renderAddButton={() => (
								<span className="text-muted m-l-sm pull-right">
									<Tooltip position="left" tag="TooltipStartTransaction">
										<a
											href="#/OpenModal" className="btn btn-success btn-xs m-t-xs m-b-xs"
											onClick={canAdd && ((e) => {
												e.preventDefault();
												this.props.router.push('/data/transactions/create');
											})}
										>
											<i className="fa fa-plus" /> {i18n.t('Add')}
										</a>
									</Tooltip>
								</span>
							)}
							link={(row) => this.props.router.push(`/data/transactions/${row.id}`)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Transactions);
