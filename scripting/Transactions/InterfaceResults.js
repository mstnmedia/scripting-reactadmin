import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets';
import { DataTable, Input, DropDown, Pager } from '../../reactadmin/components';
import { PageNumbers, TransactionResultsPageSize, } from '../../store/enums';
import { containsIgnoringCaseAndAccents } from '../../reactadmin/Utils';
import i18n from '../../i18n';

const actions = ReduxOutlet('interfaces', 'interfaceBase').actions;
/**
 * Componente que muestra al usuario en una pestaña la lista de variables de la transacción en 
 * curso.
 *
 * @class InterfaceResults
 * @extends {Component}
 */
class InterfaceResults extends Component {
	state = {
		fetchingInterfaces: false,
		interfaceFilter: 0,
		textFilter: '',
		pageSize: TransactionResultsPageSize,
		pageNumber: 1,
	}
	componentWillMount() {
		this.fetchInterfaces();
		// window.Scripting_InterfaceResults = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getInterfaces() {
		const { interfaceBases: { list: interfaces, }, } = this.props;
		return interfaces || [];
	}
	getResults() {
		const {
			transactionResults,
			interfaceBases: { listObj, },
		} = this.props;

		return (transactionResults || []).map(i => {
			const inter = i;
			inter.Interface = listObj && listObj[i.id_interface];
			return inter;
		}).sort((a, b) => {
			if (a.Interface && b.Interface && a.Interface.label !== b.Interface.label) {
				return a.Interface.label > b.Interface.label ? 1 : -1;
			}
			return a.label > b.label ? 1 : -1;
		});
	}

	fetchInterfaces() {
		const { dispatch, token, } = this.props;
		const interfaces = this.getInterfaces();

		if (!interfaces.length && !this.state.fetchingInterfaces) {
			dispatch(actions.fetch(token, undefined, undefined, false, (response) => {
				this.setState({ fetchingInterfaces: false });
				const listObj = {};
				for (let i = 0; i < response.data.items.length; i++) {
					const inter = response.data.items[i];
					listObj[inter.id] = inter;
				}
				this.handleReduxChange('listObj', listObj);
			}, () => this.setState({ fetchingInterfaces: false })));
		}
	}

	handleReduxChange(prop, value) {
		const { dispatch, } = this.props;
		dispatch(actions.setProp(prop, value));
	}
	handleChange(prop, value) {
		this.setState({ [prop]: value });
	}

	render() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleChange('pageNumber', maxPage || 1);
			}
		};
		const {
			interfaceFilter, textFilter,
			pageSize, pageNumber,
		} = this.state;
		const { clearable } = this.props;

		const transactionResults = this.getResults();
		const interfaces = this.getInterfaces().filter(inter =>
			inter.id === interfaceFilter || 
			transactionResults.find(i => i.id_interface === inter.id)
		);
		const getItems = (_interfaceFilter, _textFilter) => {
			const items = transactionResults
				.filter(i =>
					(!_interfaceFilter || i.id_interface === _interfaceFilter) &&
					(!_textFilter || (
						containsIgnoringCaseAndAccents(i.label, _textFilter) ||
						containsIgnoringCaseAndAccents(i.value, _textFilter) ||
						containsIgnoringCaseAndAccents(i.Interface && i.Interface.label, _textFilter)
					))
				);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(interfaceFilter, textFilter);
		const onPageChange = ({ newPage }) => {
			this.handleChange('pageNumber', newPage);
		};
		const onPageSizeChange = (_pageSize) => {
			this.handleChange('pageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onTextFilterChange = (_textFilter) => {
			this.handleChange('textFilter', _textFilter);
			const { totalRows: _totalRows, } = getItems(interfaceFilter, _textFilter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};
		const onInterfaceFilterChange = (_interfaceFilter) => {
			this.handleChange('interfaceFilter', _interfaceFilter);
			const { totalRows: _totalRows, } = getItems(_interfaceFilter, textFilter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};
		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });

		const renderHeader = () => renderListHeader({
			interfaces,
			clearable,
			textFilter,
			onTextFilterChange,
			interfaceFilter,
			onInterfaceFilterChange,
			pageSize,
			onPageSizeChange,
		});
		return (
			<div id="results" className="tab-pane fade">
				<DataTable
					emptyRow
					schema={{
						fields: {
							value: i18n.t('Value'),
							label: {
								label: i18n.t('Name'),
								format: (row) => (row.Interface
									? row.label.replace(`${row.Interface.name}_`, '')
									: row.label
								),
							},
							Interface: (interfaceFilter ? undefined : {
								label: i18n.t('Interface'),
								format: (row) => (row.Interface ? row.Interface.label : ''),
							}),
						}
					}}
					cellStyle={{ wordBreak: 'break-word', }}
					{...{ rowKey: (row) => row.name, rows, totalRows, }}
					{...{ pageSize, pageNumber, onPageChange, renderHeader, }}
				/>
			</div>
		);
	}
}
const renderListHeader = function ({
	interfaces, clearable,
	textFilter, interfaceFilter,
	onTextFilterChange, onInterfaceFilterChange,
	pageSize, onPageSizeChange
}) {
	const pageNumbers = PageNumbers;
	return (
		<header className="panel-heading" style={{ marginTop: 10, }}>
			<div className="col-sm-3 pull-right p-l-none">
				<DropDown
					items={pageNumbers}
					value={pageSize}
					onChange={(e) => onPageSizeChange(e.target.value * 1)}
				/>
				{i18n.t('recordsByPage')}
			</div>
			<div className="col-sm-4 pull-right">
				<Input
					clearable={clearable}
					placeholder={i18n.t('Search')}
					onFieldChange={(e) => onTextFilterChange(e.target.value)}
					value={textFilter || ''}
				/>
			</div>
			<div className="col-sm-5 pull-right p-r-none p-l-none">
				<DropDown
					items={interfaces}
					value={interfaceFilter}
					onChange={(e) => onInterfaceFilterChange(e.target.value * 1)}
					emptyOption={<option value={0}>{i18n.t('AllInterfaces')}</option>}
				/>
			</div>

		</header>
	);
};

export default connect(mapStateToProps)(InterfaceResults);
