import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets';
import { DataTable, Pager, Button } from '../../reactadmin/components';
import { CasesPageSize, } from '../../store/enums';
import { containsIgnoringCaseAndAccents, sortFunction } from '../../reactadmin/Utils';
import i18n from '../../i18n';
import { renderListHeader, isFunction } from '../../Utils';

const actions = ReduxOutlet('transactions', 'transaction').actions;
/**
 * Componente que muestra al usuario en una pestaña la lista de casos de CRM cargados en Redux.
 *
 * @class CasesTab
 * @extends {Component}
 */
class CasesTab extends Component {
	state = {}
	componentWillMount() {
		window.Scripting_CasesTab = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getCustomerCases() {
		const { transactions: { customerCases }, } = this.props;
		return customerCases || [];
	}
	getReduxProp(prop, def) {
		const { transactions, } = this.props;
		return transactions[prop] || def;
	}
	getTransaction(props) {
		const { transactions, } = props || this.props;
		const data = transactions.runningTransaction;
		if (data && data.items) {
			return data.items[0];
		}
		return undefined;
	}

	handleReduxChange(prop, value, callback) {
		this.props.dispatch(
			actions.updateState(prop, value))
			.then(() => isFunction(callback) && callback());
	}
	handleChange(prop, value) {
		this.setState({ [prop]: value });
	}

	render() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('casesPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('casesFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerCases().filter(i => !_textFilter ||
				containsInsensitive(i.id, _textFilter) ||
				containsInsensitive(i.subscriberNo, _textFilter) ||
				containsInsensitive(i.topicClassification, _textFilter) ||
				containsInsensitive(i.topicSubject, _textFilter) ||
				containsInsensitive(i.topicOutcome, _textFilter) ||
				containsInsensitive(i.status, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('casesPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('casesPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('casesPageSize', CasesPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('casesPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('casesFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = casesOrderByList();
		const orderBy = this.getReduxProp('casesOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('casesOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerCases();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChange('casesOrderBy', _orderBy);
			this.handleReduxChange('caseOrderDesc', _orderDesc);
			this.handleReduxChange('customerCases', list);
		};

		const { fetchingCases, fetchCustomerCases, customerID } = this.props;
		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const onLoadMoreClick = () => {
			fetchCustomerCases({
				customerID,
				pageNumber: this.getReduxProp('casesLastPageLoaded', 0) + 1,
				baseList: this.getCustomerCases(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChange('customerCases', []);
			this.handleReduxChange('casesFilter', '');
			this.handleReduxChange('casesPageNumber', 1);
			this.handleReduxChange('casesLastPageLoaded', 0);
			this.handleReduxChange('casesOrderDesc', false);
			this.handleReduxChange('casesOrderBy', { id: 0 }, () => {
				let cleanPage = 1;
				let path = `customerCases_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerCases_${customerID}_${cleanPage}`;
				}
				Object.keys(this.props.transactions).forEach(key => {
					if (String(key).startsWith('caseHistories_')) {
						this.handleReduxChange(key, undefined);
					}
				});
				fetchCustomerCases({ customerID, pageNumber: 1, });
			});
		};
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = fetchingCases ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = fetchingCases ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="cases" className="tab-pane fade">
				<DataTable
					emptyRow loading={fetchingCases}
					schema={{
						fields: {
							closeDate: {
								type: 'DateTimeZ',
								label: i18n.t('CloseDate'),
							},
							dueDate: {
								type: 'DateTimeZ',
								label: i18n.t('DueDate'),
							},
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
							status: i18n.t('Status'),
							topicOutcome: i18n.t('TopicOutcome'),
							topicSubject: i18n.t('TopicSubject'),
							topicClassification: i18n.t('TopicClassification'),
							subscriberNo: i18n.t('SubscriberNo'),
							id: i18n.t('ID'),
							modificationDate: {
								type: 'DateTimeZ',
								label: i18n.t('ModificationDate'),
								format: (row) => {
									const note = row.notes && row.notes.length ? row.notes[0] : {};
									return note.creationDate;
								}
							},
						}
					}}
					{...{ rows, totalRows, pageSize, pageNumber, onPageChange, renderHeader, }}
					link={(row) => this.props.handleCaseClick(row)}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={fetchingCases}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={fetchingCases}
				/>
			</div>
		);
	}
}
const containsInsensitive = function (string, subString) {
	return containsIgnoringCaseAndAccents(string, subString);
};
const casesOrderByList = () => [
	{ id: 0, label: i18n.t('ModificationDate'), desc: false, sort: sortFunction('modificationDate') },
	{ id: 1, label: i18n.t('ID'), desc: false, sort: sortFunction('id') },
	{ id: 2, label: i18n.t('SubscriberNo'), desc: false, sort: sortFunction('subscriberNo') },
	{ id: 3, label: i18n.t('TopicClassification'), desc: false, sort: sortFunction('topicClassification') }, // eslint-disable-line max-len
	{ id: 4, label: i18n.t('TopicSubject'), desc: false, sort: sortFunction('topicSubject') },
	{ id: 5, label: i18n.t('TopicOutcome'), desc: false, sort: sortFunction('topicOutcome') },
	{ id: 6, label: i18n.t('Status'), desc: false, sort: sortFunction('status') },
	{ id: 7, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
	{ id: 8, label: i18n.t('Status'), desc: false, sort: sortFunction('status') },
	{ id: 9, label: i18n.t('DueDate'), desc: false, sort: sortFunction('dueDate') },
	{ id: 10, label: i18n.t('CloseDate'), desc: false, sort: sortFunction('closeDate') },
].sort(sortFunction('label'));

export default connect(mapStateToProps)(CasesTab);
