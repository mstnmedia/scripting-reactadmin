import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

import { Fa } from '../../reactadmin/components/';
import ContentModal from '../Contents/ContentModal';
import i18n from '../../i18n';
import {
	faIconFromFileExtension,
	replaceAll,
} from '../../reactadmin/Utils';
import {
	ContentTypes,
	TableTemplateColumnsSplitter,
	TableTemplateStart,
	TableTemplateEnd,
	TransactionResultListTemplate,
	TableTemplateColumnNameSplitter,
} from '../../store/enums';

/**
 * Componente que muestra al usuario un contenido: un texto si es tipo HTML, una imagen 
 * tipo IMAGEN o un enlace de descarga si es un contenido tipo ARCHIVO.
 *
 * @export
 * @class TransactionContentItem
 * @extends {Component}
 */
export default class TransactionContentItem extends Component {
	state = {
		html: '',
	}
	componentWillMount() {
		this.setStateHTML(this.props);
	}
	componentWillReceiveProps(props) {
		if (props.results !== this.props.results
			|| props.results.length !== this.props.results.length
			|| Object.keys(props.resultsObj || {})
				.length !== Object.keys(this.props.resultsObj || {}).length) {
			this.setStateHTML(props);
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getContent(props) {
		return props.content || {};
	}
	getResults(props) {
		return (props.results || []).sort((a, b) => -1 * (a.name || '').localeCompare(b.name || ''));
	}
	getResultsObj(props) {
		return props.resultsObj || {};
	}
	getContentHTML(props) {
		if (this.html && !this.props.notCached) {
			return this.html;
		}
		const html = this.getHTML(props);
		// if (!html || !this.getResults(props).length) {
		// 	console.log(html, this.getResults(props), this.getContent());
		// }

		return html;
	}
	getHTML(props) {
		const content = this.getContent(props);
		const results = this.getResults(props);
		let html = (content.value || '').trim();
		if (results.length) {
			try {
				let startIndex = html.indexOf(TableTemplateStart, 0);
				while (startIndex > -1 && startIndex < html.length) {
					const tempEndIndex = html.indexOf(TableTemplateEnd, startIndex);
					const endIndex = tempEndIndex === -1
						? html.length
						: (tempEndIndex + TableTemplateEnd.length);
					const tableTemplate = html.substring(startIndex, endIndex);
					const tableResult = this.getTableHTML(props, tableTemplate);
					html = replaceAll(html, tableTemplate, tableResult);
					startIndex = html.indexOf(TableTemplateStart, startIndex + tableResult.length);
				}
			} catch (error) {
				console.error('Error creando tabla: ', error);
			}

			results.forEach(result => {
				let resultProps;
				try {
					resultProps = JSON.parse(result.props);
				} catch (error) {
					resultProps = {};
				}
				const cssStyles = (resultProps && resultProps.cssStyles) || '';
				const spanHTML = `<span${cssStyles ? ` style="${cssStyles}"` : ''}>${result.value}</span>`;
				html = replaceAll(html, result.name, spanHTML);
			});
		}
		return html;
	}
	getTableHTML(props, tableTemplate) {
		const results = this.getResults(props);
		const resultsObj = this.getResultsObj(props);
		const columns = tableTemplate
			.replace(TableTemplateStart, '')
			.replace(TableTemplateEnd, '')
			.split(TableTemplateColumnsSplitter);
		let container = '<div class="table-responsive">\nTABLE\n</div>';
		let table = '<table class="table table-hover table-condensed table-striped">';
		table += '\nTHEAD';
		table += '\nTBODY\n';
		table += '</table>';

		let header = '\t<thead>\n';
		header += '\t\t<tr>\n';
		for (let c = 0; c < columns.length; c++) {
			const column = columns[c];
			let columnLabel = column.split(TableTemplateColumnNameSplitter)[0];
			if (!columnLabel || columnLabel.indexOf(TransactionResultListTemplate) > -1) {
				columnLabel = column.split(`${TransactionResultListTemplate}_`)[1];
			}
			columnLabel = columnLabel.trim();
			header += `\t\t\t<th>${columnLabel}</th>\n`;
		}
		header += '\t\t</tr>\n';
		header += '\t</thead>\n';

		let body = '\t<tbody>\n';
		for (let i = 0; i < results.length; i++) {
			let notFound = 0;
			let row = '\t\t<tr>\n';
			for (let c = 0; c < columns.length; c++) {
				const column = columns[c];
				let resultSearch = column.split(TableTemplateColumnNameSplitter)[1];
				if (!resultSearch) {
					resultSearch = column;
				}
				if (resultSearch.indexOf(TransactionResultListTemplate) === -1) {
					notFound++;
				}
				resultSearch = resultSearch.replace(TransactionResultListTemplate, i);
				resultSearch = resultSearch.replace(' ', '').replace('&nbsp;', '');
				const result = resultsObj[resultSearch];
				row += '\t\t\t<td>';
				if (result) {
					row += result.value || '';
				} else {
					notFound++;
				}
				row += '</td>\n';
			}
			row += '\t\t</tr>\n';
			if (notFound < columns.length) {
				body += row;
			} else {
				break;
			}
		}
		body += '\t</tbody>\n';

		table = table.replace('THEAD', header);
		table = table.replace('TBODY', body);
		container = container.replace('TABLE', table);
		return container;
	}
	setStateHTML(props) {
		if (this.getContent(props).id_type === 1) {
			this.html = this.getHTML(props);
		}
	}

	renderActions() {
		const { openModal } = this.props;
		const { value, filename, id_type: type, } = this.getContent(this.props);
		return (
			<div title={filename} className="text-right" style={{ height: 0 }}>
				{type && type !== ContentTypes.HTML &&
					<Fa
						title={i18n.t('DownloadFilename', { filename })}
						icon={'download'}
						onClick={() => ContentModal.downloadFile(value)}
					/>
				}
				{openModal && type !== ContentTypes.FILE && type !== ContentTypes.HTML &&
					<Fa
						className="m-l-sm"
						title={i18n.t('Open')} icon={'folder-open-o'}
						onClick={() => openModal()}
					/>
				}
			</div>
		);
	}
	renderHTMLContent() {
		const html = this.getContentHTML(this.props);
		return (
			<div>
				{this.renderActions()}
				<div dangerouslySetInnerHTML={{ __html: html }} />
			</div>
		);
	}
	renderImageContent() {
		const content = this.getContent(this.props);
		const { name, value, filename, } = content;
		const src = ContentModal.downloadURL(value);
		return (
			<div className="text-center">
				{this.renderActions()}
				<h4 style={{ paddingRight: 20, paddingLeft: 20 }}>{name}</h4>
				<img
					className="img"
					style={{ maxWidth: '100%' }}
					alt={filename}
					src={src}
				/>
			</div>
		);
	}
	renderFileContent() {
		const content = this.getContent(this.props);
		const { name, value, filename, } = content;
		const extension = value.substring(value.lastIndexOf('.') + 1);
		const icon = faIconFromFileExtension(extension);
		const contentIcon = <Fa title={filename} icon={icon} size={50} />;
		return (
			<div className="text-center">
				{this.renderActions()}
				<h4 style={{ paddingRight: 20, paddingLeft: 20 }}>{name}</h4>
				{contentIcon}
			</div>
		);
	}
	render() {
		const content = this.getContent(this.props);
		let view = null;
		switch (content.id_type) {
			case ContentTypes.HTML:
				view = this.renderHTMLContent(content);
				break;
			case ContentTypes.IMAGE:
				view = this.renderImageContent(content);
				break;
			case ContentTypes.FILE:
				view = this.renderFileContent(content);
				break;
			default:
				view = null;
		}
		const { className, containerStyle, separatorStyle, } = this.props;

		return (
			<div
				id={content.id}
				className={className}
				style={{ wordBreak: 'break-word', overflow: 'overlay', ...containerStyle }}
			>
				{view}
				<hr style={separatorStyle} />
			</div>
		);
	}
}
