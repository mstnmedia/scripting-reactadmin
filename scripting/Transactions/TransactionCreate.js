import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import i18n from '../../i18n';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	ID_GREETING_CONTENT,
	TransactionStates,
	CustomerSearchTypes, CustomerSearchTypeNames,
	Interfaces,
	PageNumbers,
	CustomersPageSize,
	SubcriptionsPageSize,
	CasesPageSize,
	NotesPageSize,
	InteractionsPageSize,
	OrdersPageSize,
} from '../../store/enums';
import {
	Page,
	Panel,
	Button,
	ButtonGroup,
	Input,
	DataTable,
	Row, Col,
	DateDiff,
	TextArea,
	ModalFactory,
	Factory,
	AnimatedSpinner,
	Pager,
	DropDown,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';
import {
	InterfaceResults,
	TransactionContentItem, TransactionEndModal,
	CaseModal, InteractionModal, OrderModal,
	CasesTab,
} from './';
import {
	containsIgnoringCaseAndAccents,
	isValidPhone, isValidIdentityCard, isValidPassport, isValidTaxID, isFunction, respErrorDetails,
} from '../../reactadmin/Utils';

const customersSchema = {
	fields: {
		contactGender: i18n.t('Gender'),
		segment: i18n.t('Segment'),
		contactName: i18n.t('ContactName'),
		document: i18n.t('Document'),
		taxID: i18n.t('TaxID'),
		name: i18n.t('Name'),
		type: i18n.t('CustomerType'),
	}
};
const subscriptionsSchema = {
	fields: {
		billingAccountNo: i18n.t('BillingAccount'),
		status: {
			label: i18n.t('Status'),
			format: (row) => (
				<b
					className={`badge bg-${row.status === 'ACTIVE' ? 'success' : 'danger'}`}
					style={{ minWidth: '50%' }}
					children={i18n.t(row.status)}
				/>
			),
		},
		subscriptionStartDate: {
			type: 'Date',
			label: i18n.t('SubscriptionDate'),
		},
		subscriberNo: i18n.t('Telephone'),
		offerName: i18n.t('OfferName'),
		productTypeDescription: i18n.t('Plan'),
		productTypeName: i18n.t('ProductType'),
	}
};

const actions = ReduxOutlet('transactions', 'transaction').actions;
const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
const contentActions = ReduxOutlet('contents', 'content').actions;
const interfacesActions = ReduxOutlet('interfaces', 'interfaceBase').actions;

const {
	ADVANCED,
	TAX_ID,
	BAN_ACCOUNT_NO,
	CLARO_VIDEO_NO, CONTACT_NAME,
	CUSTOMER_ID, CUSTOMER_NAME,
	IDENTITY_CARD,
	PASSPORT, PHONE,
	SMART_CARD, //STB_NO,
	GUID_PARCIAL,
	// IMEI, ORDER_NO, SIM_CARD,
} = CustomerSearchTypes;
/**
 * Componente que controla la pantalla que recolecta los datos iniciales para crear una nueva 
 * transacción.
 *
 * @class TransactionCreate
 * @extends {Component}
 */
class TransactionCreate extends Component {
	state = {
		callerName: '',
		advSearch: {},
		searchValidation: {},
		selectedCase: {},
		selectedInteraction: {},
		selectedOrder: {},
		note: '',
		error: { phone: '' },
		disabled: false,
		fetchingCustomer: false,
		fetchingMoreSubscriptions: false,
		fetchingSubscription: false,
		fetchingNotes: false,
		fetchingCases: false,
		// fetchingCaseHistories: false,
		fetchingInteractions: false,
		fetchingOrders: false,
		fetchingContent: false,
		fetchingEmployee: false,
		fetchingServerDateTime: false,
		fetchingWorkflows: false,
	}

	componentWillMount() {
		if (!this.props.user.hasPermissions('transactions.add')) {
			// Se necesita tener disponible el id del usuario logueado antes de continuar esta función.
			this.props.router.push('/data/transactions');
			return;
		}
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Transactions'), to: '/data/transactions' },
			{ label: i18n.t('NewTransaction'), },
		];
		this.handleChange('note', '');
		const reduxState = this.props.transactions;
		Object.keys(reduxState).forEach(prop => {
			if (prop.startsWith('customerOrders_') || prop.startsWith('customerCases_')
				|| prop.startsWith('customerNotes_') || prop.startsWith('customerInteractions_')
			) {
				delete reduxState[prop];
			}
		});
		this.handleReduxChangeObj({
			item: {},
			runningTransaction: {},
			transactionResults: [],
			transactionResultsObj: {},

			initialServerDateTime: {},

			usedVersion: 0,
			searchType: '',
			searchValue: '',
			selectedCustomer: null,
			customerList: null,
			customersFilter: '',
			customersPageSize: CustomersPageSize,
			customersPageNumber: 1,
			customersLastPageLoaded: 0,

			selectedSubscription: null,
			subscriptionList: null,
			subscriptionsFilter: '',
			subscriptionsPageSize: SubcriptionsPageSize,
			subscriptionsPageNumber: 1,
			subscriptionsLastPageLoaded: 0,

			casesFilter: '',
			casesPageSize: CasesPageSize,
			casesPageNumber: 1,
			casesLastPageLoaded: 0,
			casesOrderDesc: false,
			casesOrderBy: { id: 0 },
			customerCases: [],

			notesFilter: '',
			notesPageSize: NotesPageSize,
			notesPageNumber: 1,
			notesLastPageLoaded: 0,
			notesOrderDesc: false,
			notesOrderBy: { id: 0 },
			customerNotes: [],

			interactionsFilter: '',
			interactionsPageSize: InteractionsPageSize,
			interactionsPageNumber: 1,
			interactionsLastPageLoaded: 0,
			interactionsOrderDesc: false,
			interactionsOrderBy: { id: 0 },
			customerInteractions: [],

			ordersFilter: '',
			ordersPageSize: OrdersPageSize,
			ordersPageNumber: 1,
			ordersLastPageLoaded: 0,
			ordersOrderDesc: false,
			ordersOrderBy: { id: 0 },
			customerOrders: [],
		}, () => {
			this.fetchEmployeeInfo();
			this.fetchGreetingContent();
			const locationState = this.getLocationState();
			if (locationState.id_customer) {
				this.fetchServerDateTime();
				this.handleChange('callerName', locationState.caller_name || '');
				this.fetchCustomerSearch({
					searchType: 'customer_id',
					searchValue: locationState.id_customer,
					updateList: true,
				});
			}
			this.fetchWorkflows();
		});
		// window.Scripting_TransactionCreate = this;
	}
	componentWillReceiveProps(props) {
		if (props.transactions.runningTransaction) return;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	componentWillUnmount() {
		this.unmounting = true;
	}

	getWorkflowMeetCriteriaCondition(workflow, resultObj, condition) {
		const { operator, value } = condition;
		const resultValue = JSON.stringify(resultObj.value);
		const expression = `${resultValue} ${operator} ${JSON.stringify(value)}`;
		try {
			// console.info('criteria expression: ', expression, workflow);
			const expressionValue = window.eval(expression);
			// console.info('expression value: ', expressionValue, workflow);
			return expressionValue;
		} catch (error) {
			console.error(false, error, workflow);
			return false;
		}
	}
	getWorkflowMeetCriteria(workflow, wc) {
		const resultsObj = this.getTransactionResultsObj();
		const conditions = (wc.criteria && wc.criteria.conditions) || [];
		if (conditions.length === 0) {
			// console.info(true, 'criteria not has conditions: ', wc, workflow);
		}
		for (let i = 0; i < conditions.length; i++) {
			const condition = conditions[i];
			const resultObj = resultsObj[condition.name];
			if (!resultObj) {
				// console.log(false, 'results not have key for condition: ', condition, workflow);
				return false;
			}
			const conditionResult = this.getWorkflowMeetCriteriaCondition(workflow, resultObj, condition);
			if (!conditionResult) {
				// console.log(false, 'workflow not meets condition: ', condition, workflow);
				return false;
			}
		}
		return true;
	}
	getWorkflows() {
		const { workflows: { workflowsWithCriterias }, } = this.props;
		const selectedSubscription = this.getSelectedSubscription();
		if (!selectedSubscription) return [];
		if (!workflowsWithCriterias || workflowsWithCriterias.code || workflowsWithCriterias.message) {
			return [];
		}
		const applyableWorkflows = (workflowsWithCriterias || [])
			.filter(item => {
				if (item.deleted) {
					// console.log(false, 'deleted workflow: ', item);
					return false;
				}
				if (item.main !== 1 || item.version_active !== 1) {
					// console.log(false, 'not main or active workflow: ', item);
					return false;
				}
				if (item.workflow_criteria && item.workflow_criteria.length) {
					let apply = false;
					for (let i = 0; i < item.workflow_criteria.length; i++) {
						apply = this.getWorkflowMeetCriteria(item, item.workflow_criteria[i]);
						if (apply) {
							break;
						}
					}
					return apply;
				}
				// console.log(true, 'workflow doesn\'t have criterias: ', item);
				return true;
			})
			.sort(sortFunction('name')(false));
		return applyableWorkflows;
	}
	getLocalStartDate() {
		const initialServerDateTime = this.getServerDateTime('initial');
		// if (initialServerDateTime.datetime) {
		// 	// return `${initialServerDateTime.datetime}+0000`;
		// 	return `${initialServerDateTime.datetime}`;
		// }
		// return undefined;
		return initialServerDateTime.fulldate;
	}
	getServerDateTime() {
		return this.props.transactions.initialServerDateTime || {};
	}
	getCustomerList() {
		const { transactions: { customerList }, } = this.props;
		return customerList || [];
	}
	getSubscriptionList() {
		const { transactions: { subscriptionList }, } = this.props;
		return subscriptionList || [];
	}
	getSelectedCustomer() {
		const { transactions: { selectedCustomer }, } = this.props;
		return selectedCustomer || {};
	}
	getSelectedSubscription() {
		const { transactions: { selectedSubscription }, } = this.props;
		return selectedSubscription;
	}
	getCustomerCases() {
		const { transactions: { customerCases }, } = this.props;
		return customerCases || [];
	}
	getCustomerNotes() {
		const { transactions: { customerNotes }, } = this.props;
		return customerNotes || [];
	}
	getCustomerInteractions() {
		const { transactions: { customerInteractions }, } = this.props;
		return customerInteractions || [];
	}
	getCustomerOrders() {
		const { transactions: { customerOrders }, } = this.props;
		return customerOrders || [];
	}
	getTransactionResults() {
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}
	getLocationState() {
		const { location: { state } } = this.props;
		return state || {};
	}
	getObjResultFromData(data) {
		const inter = data.interface || {};
		const prefix = `${inter.name}_`;
		const variables = inter.interface_results || [];
		return this.getObjResultFromVariables({ prefix, variables, });
	}

	getObjResultFromVariables({ prefix, variables, }) {
		const result = {};
		for (let i = 0; i < variables.length; i++) {
			const variable = variables[i];
			const name = variable.name.replace(prefix, '');
			let value = null;
			try {
				value = JSON.parse(variable.value);
			} catch (error) {
				value = variable.value;
			}
			result[name] = value;
		}
		return result;
	}
	getReduxProp(prop, def) {
		return this.props.transactions[prop] || def;
	}
	getSearchTypeValue() {
		const searchType = this.getReduxProp('searchType', PHONE);
		const searchValue = this.getReduxProp('searchValue', '');
		return { searchType, searchValue, };
	}

	unmounting = false;
	changeState(state, callback) {
		if (!this.unmounting) {
			this.setState(state, callback);
		}
	}

	endCallClick(e) {
		e.preventDefault();
		ModalFactory.show('transactionEndModal');
	}
	endCall({ cause, note }) {
		this.changeState({ disabled: true });
		this.createTransaction({
			end_cause: cause,
			note,
			state: TransactionStates.CANCELED,
		});
	}
	createTransaction(customProps) {
		const {
			dispatch, token,
		} = this.props;
		const selectedCustomer = this.getSelectedCustomer();
		const selectedSubscription = this.getSelectedSubscription();
		const { workflow, note, callerName, } = this.state;
		const results = this.getTransactionResults().map((i) => {
			const result = i;
			result.Interface = result.interface = result.transaction = result.workflow_step = undefined;
			return i;
		});
		const variableName = `${Interfaces.SCRIPTING.TransactionInfoName}_caller_name`;
		this.handleVariableChange(results, variableName, callerName, i18n.t('CallerName'), {
			id_interface: Interfaces.SCRIPTING.TransactionInfoBase,
		});
		const newTransaction = {
			id: 0,
			id_center: 0,
			id_call: this.getLocationState().id_call || '',
			id_workflow: (workflow && workflow.id) || 0,
			id_workflow_version: (workflow && workflow.version_id) || 0,
			id_customer: selectedCustomer.customerID,
			id_subscription: selectedSubscription.id,
			subscriber_no: selectedSubscription.subscriberNo,
			id_employee: 1,
			state: TransactionStates.RUNNING,
			start_date: this.getLocalStartDate(),
			note,
			transaction_results: results,
			...customProps,
		};

		this.changeState({ disabled: true });
		const success = (resp) => {
			this.changeState({ disabled: false });
			const { code, items } = resp.data;
			if (code === 200 && items && items[0] && items[0].id) {
				this.props.router.push(`/data/transactions/${items[0].id}`);
			}
		};
		const failure = (resp) => {
			this.changeState({ disabled: false });
			window.message.error(i18n.t('ErrorCreatingTransaction'), 0, respErrorDetails(resp));
		};
		dispatch(actions.create(token, newTransaction, success, failure, false));
	}

	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}
	isSearchDisabled() {
		return this.isDisabled() || this.state.fetchingCustomer;
	}
	isSubscriptionDisabled() {
		return this.isSearchDisabled()
			|| this.state.fetchingSubscription;
	}
	isFetchingCustomer() {
		return this.state.fetchingCustomer;
	}
	isFetchingSubscription() {
		return this.state.fetchingSubscription;
	}
	isFetchingMoreSubscriptions() {
		return this.state.fetchingMoreSubscriptions;
	}
	isFetchingCases() {
		return this.isFetchingCustomer()
			|| this.state.fetchingCases;
	}
	isFetchingNotes() {
		return this.isFetchingCustomer()
			|| this.state.fetchingNotes;
	}
	isFetchingInteractions() {
		return this.isFetchingCustomer()
			|| this.state.fetchingInteractions;
	}
	isFetchingOrders() {
		return this.isFetchingCustomer()
			|| this.state.fetchingOrders;
	}
	isFetchingResults() {
		return this.isFetchingCustomer()
			|| this.isFetchingSubscription()
			|| this.state.fetchingEmployee
			|| this.state.fetchingServerDateTime
			;
	}
	isFetchingContent() {
		return this.state.fetchingContent;
	}
	isFetchingServerDateTime() {
		return this.state.fetchingServerDateTime;
	}
	isFetchingWorkflows() {
		return this.isFetchingSubscription()
			|| this.state.fetchingWorkflows;
	}
	isSuccessInterfaceResponse(data) {
		return (data && data.action === 'NEXT_STEP');
	}

	handleChange(prop, value) {
		this.changeState({ [prop]: value });
	}
	handleReduxChange(prop, value, callback) {
		this.props.dispatch(actions.updateState(prop, value))
			.then(() => isFunction(callback) && callback());
	}
	handleReduxChangeObj(newState, callback) {
		this.props.dispatch(actions.updateStateObj(newState))
			.then(() => isFunction(callback) && callback());
	}
	handleCustomerChange(selectedCustomer) {
		this.handleReduxChangeObj({
			selectedCustomer,
			selectedSubscription: null,
			subscriptionList: null,
			casesPageNumber: 1,
			notesPageNumber: 1,
			interactionsPageNumber: 1,
			ordersPageNumber: 1,
		});
		const { customerID } = selectedCustomer;
		this.fetchCustomerCases({ customerID, pageNumber: 1 });
		this.fetchCustomerInteractions({ customerID, pageNumber: 1 });
		this.fetchCustomerOrders({ customerID, pageNumber: 1 });
		this.fetchCustomerNotes({ customerID, pageNumber: 1 });
	}
	handleSubscriptionChange(selectedSubscription) {
		this.handleReduxChange('selectedSubscription', selectedSubscription);
		if (selectedSubscription) {
			this.fetchSubscriptionInfo(selectedSubscription);
		}
	}
	handleCustomerClick(clickedCustomer) {
		if (clickedCustomer.customerID) {
			this.handleReduxChangeObj({
				selectedCustomer: null,
				selectedSubscription: null,
				subscriptionList: null,
				subscriptionsLastPageLoaded: 0,
				subscriptionsPageNumber: 1,
			});
			this.fetchCustomerSearch({
				searchType: CUSTOMER_ID,
				searchValue: clickedCustomer.customerID,
				baseList: this.getCustomerList(),
			});
		}
	}
	handleWorkflowClick(e, workflow) {
		e.preventDefault();
		this.changeState({ workflow }, () => this.createTransaction());
	}
	handleCustomerSearchChange(searchButton) {
		let searchType = '';
		switch (searchButton) {
			case PHONE:
				searchType = PHONE;
				break;
			case IDENTITY_CARD:
				searchType = IDENTITY_CARD;
				break;
			case PASSPORT:
				searchType = PASSPORT;
				break;
			case ADVANCED:
				searchType = TAX_ID;
				break;
			default:
				searchType = TAX_ID;
				break;
		}
		this.changeState({ searchValidation: {} });
		this.handleReduxChangeObj({
			searchType,
			searchValue: '',
		});
	}
	handleCustomerSearch() {
		const { searchType, searchValue, } = this.getSearchTypeValue();
		if (searchValue) {
			this.fetchServerDateTime();
			this.handleReduxChangeObj({
				selectedCustomer: null,
				customerList: null,
				customersPageNumber: 1,
				customersLastPageLoaded: 0,

				selectedSubscription: null,
				subscriptionList: null,
				subscriptionsPageNumber: 1,
				subscriptionsLastPageLoaded: 0,

				customerCases: null,
				customerNotes: null,
				customerInteractions: null,
				customerOrders: null,
			});
			const searchValidation = this.validateCustomerSearch(searchType, searchValue);
			this.changeState({ searchValidation });
			if (searchValidation.searchValid) {
				this.fetchCustomerSearch({ searchType, searchValue, pageNumber: 1, });
			}
		}
	}
	handleLoadMoreCustomers() {
		const { searchType, searchValue, } = this.getSearchTypeValue();
		this.fetchCustomerSearch({
			searchType,
			searchValue,
			pageNumber: this.getReduxProp('customersLastPageLoaded', 0) + 1,
			notDisable: true,
			baseList: this.getCustomerList(),
		});
	}
	handleLoadMoreSubscriptions() {
		const { customerID, document, } = this.getSelectedCustomer();
		this.fetchCustomerSubscriptions({
			customerID,
			document,
			pageNumber: this.getReduxProp('subscriptionsLastPageLoaded', 0) + 1,
			baseList: this.getSubscriptionList(),
		});
	}
	handleInteractionClick(selectedInteraction) {
		this.changeState({ selectedInteraction });
		ModalFactory.show('interactionModal');
	}
	handleOrderClick(selectedOrder) {
		this.changeState({ selectedOrder });
		ModalFactory.show('orderModal', {});
	}
	handleVariableChange(list, name, value, label, props) {
		let result = list.find(i => i.name === name);
		if (!result) {
			result = {
				id: 0,
				id_center: 0,
				id_transaction: 0,
				id_workflow_step: 0,
				id_interface: 0,
				name,
				label,
				value,
				props: '',
				...props,
			};
			list.push(result);
		}
		result.value = value;
	}

	validateCustomerSearch(searchType, searchValue) {
		let searchValid = true;
		let searchClass = 'success';
		let searchMessage = i18n.t('Loading');

		const regex = /^(?!\s*$).+/;
		if (searchType === CustomerSearchTypes.PHONE) {
			searchValid = isValidPhone(searchValue);
		} else if (searchType === CustomerSearchTypes.IDENTITY_CARD) {
			searchValid = isValidIdentityCard(searchValue);
		} else if (searchType === CustomerSearchTypes.PASSPORT) {
			searchValid = isValidPassport(searchValue);
		} else if (searchType === CustomerSearchTypes.TAX_ID) {
			searchValid = isValidTaxID(searchValue);
		} else {
			searchValid = regex.test(searchValue);
		}
		if (!searchValid) {
			searchClass = 'danger';
			if (searchType === CustomerSearchTypes.PHONE) {
				searchMessage = i18n.t('CustomerSearchInvalidPhone');
			} else if (searchType === CustomerSearchTypes.IDENTITY_CARD) {
				searchMessage = i18n.t('CustomerSearchInvalidIdentityCard');
			} else if (searchType === CustomerSearchTypes.PASSPORT) {
				searchMessage = i18n.t('CustomerSearchInvalidPassport');
			} else if (searchType === CustomerSearchTypes.TAX_ID) {
				searchMessage = i18n.t('CustomerSearchInvalidTaxID');
			} else {
				searchMessage = i18n.t('CustomerSearchInvalidValue');
			}
		}
		return { searchValid, searchClass, searchMessage, };
	}

	canSaveResult(result) {
		try {
			if (result.props) {
				const props = JSON.parse(result.props);
				return !props.notSave;
			}
		} catch (error) {
			// console.log('error parsing result.props of ', result, error);
		}
		return true;
	}
	handleResultsChange(results = [], interfaceID) {
		const transactionResults = this.getTransactionResults();
		const transactionResultsObj = this.getTransactionResultsObj();
		for (let i = 0; i < (results || []).length; i++) {
			const result = results[i];
			if (interfaceID) result.id_interface = interfaceID;
			if (this.canSaveResult(result)) {
				if (transactionResultsObj[result.name]) {
					const index = transactionResults.indexOf(transactionResultsObj[result.name]);
					transactionResults[index] = result;
				} else {
					transactionResults.push(result);
				}
				transactionResultsObj[result.name] = result;
			}
		}
		this.handleReduxChangeObj({
			transactionResults,
			transactionResultsObj,
		});
	}

	fetchGreetingContent() {
		const { dispatch, token, } = this.props;
		const success = () => this.changeState({ fetchingContent: false });
		const failure = () => {
			window.message.error(i18n.t('ErrorFetchingGreetingContent'));
			this.changeState({ fetchingContent: false });
		};
		this.changeState({ fetchingContent: true });
		dispatch(contentActions.setProp('item', {}));
		dispatch(contentActions.fetchOne(token, ID_GREETING_CONTENT, success, failure));
	}
	fetchWorkflows() {
		const { dispatch, token, } = this.props;
		this.changeState({ fetchingWorkflows: true });
		dispatch(workflowActions.postToURL({
			token,
			route: 'workflow/workflowsWithCriterias',
			path: 'workflowsWithCriterias',
			success: () => this.changeState({ fetchingWorkflows: false }),
			failure: () => {
				window.message.error(i18n.t('ErrorFetchingTransactionsWorkflows'));
				this.changeState({ fetchingWorkflows: false });
			},
		}));
	}
	fetchServerDateTime() {
		const { dispatch, token, } = this.props;
		this.changeState({ fetchingServerDateTime: true });
		dispatch(interfacesActions.postToURL({
			token,
			route: `interface/${Interfaces.SCRIPTING.CurrentTime}/getForm`,
			item: {},
			path: 'initialServerDateTimeResponse',
			success: (data) => {
				this.changeState({ fetchingServerDateTime: false });
				const serverDateTime = {};
				// console.log('serverDateTime');
				const variables = data.data.interface ? data.data.interface.interface_results : [];
				const vPrefix = `${data.data.interface.name}_`;
				for (let i = 0; i < variables.length; i++) {
					const variable = variables[i];
					variable.id_interface = Interfaces.SCRIPTING.CurrentTimeBase;
					const name = variable.name.replace(vPrefix, '');
					let value = null;
					try {
						value = JSON.parse(variable.value);
					} catch (error) {
						value = variable.value;
					}
					serverDateTime[name] = value;
				}
				this.handleReduxChange('initialServerDateTime', serverDateTime);
			},
			failure: () => {
				window.message.error(i18n.t('ErrorFetchingServerTimeInfo'));
				this.changeState({ fetchingServerDateTime: false });
			},
		}));
	}
	fetchEmployeeInfo() {
		const { dispatch, token, user, } = this.props;
		this.changeState({ fetchingEmployee: true });
		dispatch(interfacesActions.postToURL({
			token,
			route: `interface/${Interfaces.SCRIPTING.EmployeeInfo}/getForm`,
			item: { form: JSON.stringify({ id_employee: user.id, }) },
			path: 'employeeInfoResponse',
			success: (data) => {
				this.changeState({ fetchingEmployee: false });
				// console.log('employeeInfo');
				const employeeVariables = data.data.interface ? data.data.results : [];
				this.handleResultsChange(employeeVariables, Interfaces.SCRIPTING.EmployeeInfoBase);
			},
			failure: () => {
				window.message.error(i18n.t('ErrorFetchingEmployeeInfo'));
				this.changeState({ fetchingEmployee: false });
			},
		}));
	}
	fetchCustomerSearch({ searchType, searchValue, pageNumber, notDisable, baseList }) {
		const pageNo = pageNumber || 1;
		const path = `customerSearch_${searchType}_${searchValue}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerSearchData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({
				fetchingCustomer: false,
				searchValidation: {},
			});

			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerResults) {
				const prefix = `${Interfaces.CRM.CustomerInfo.Name}_`;
				const variables = fetchResults.customerResults;
				const selectedCustomer = this.getObjResultFromVariables({ prefix, variables, });

				this.handleResultsChange(variables, Interfaces.SCRIPTING.CustomerInfoBase);
				this.handleCustomerChange(selectedCustomer);
			}
			const newState = {};
			if (fetchResults.customerList) {
				const objCustomers = {};
				const customerList = (baseList || []).slice().map(i => {
					objCustomers[i.customerID] = i;
					return i;
				});
				fetchResults.customerList.forEach(i => {
					if (!objCustomers[i.customerID]) {
						objCustomers[i.customerID] = i;
						customerList.push(i);
					}
				});
				newState.customerList = customerList;
				newState.customersLastPageLoaded = fetchResults.pageCustomers;
			}
			if (fetchResults.subscriptionList) {
				newState.subscriptionList = fetchResults.subscriptionList;
				newState.subscriptionsLastPageLoaded = fetchResults.pageSubscriptions;
				this.handleSubscriptionChange(fetchResults.subscriptionList[0]);
			}
			this.handleReduxChangeObj(newState);
		};
		if (this.isSuccessInterfaceResponse(customerSearchData)) {
			mapFunction(customerSearchData);
		} else {
			if (!notDisable) {
				this.changeState({ fetchingCustomer: true });
			}
			const failure = () => {
				window.message.error(i18n.t('ErrorFetchingCustomerInfo'));
				this.changeState({ fetchingCustomer: false });
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.CustomerSearch}/getForm`,
				item: {
					form: JSON.stringify({
						search_type: searchType,
						search_value: searchValue,
						page_number: pageNo,
					}),
				},
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerSubscriptions({ customerID, document, pageNumber, baseList, }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerSubscriptions_${customerID}_${document}_${pageNumber}`;
		const {
			dispatch, token,
			transactions: { [path]: customerSubscriptionsData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({ fetchingMoreSubscriptions: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.subscriptionList) {
				const objSubscriptions = {};
				const subscriptionList = (baseList || []).slice().map(i => {
					objSubscriptions[i.id] = i;
					return i;
				});
				fetchResults.subscriptionList.forEach(i => {
					if (!objSubscriptions[i.id]) {
						objSubscriptions[i.id] = i;
						subscriptionList.push(i);
					}
				});
				// TODO: sort subscriptionList by active first before updating state
				this.handleReduxChangeObj({
					subscriptionList,
					subscriptionsLastPageLoaded: fetchResults.pageSubscriptions,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerSubscriptionsData)) {
			mapFunction(customerSubscriptionsData);
		} else {
			this.changeState({ fetchingMoreSubscriptions: true });
			const failure = () => {
				window.message.error(i18n.t('ErrorFetchingCustomerSusbcriptions'));
				this.changeState({ fetchingMoreSubscriptions: false });
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.CustomerSubscriptions}/getForm`,
				item: {
					form: JSON.stringify({
						customer_id: customerID,
						document,
						page_number: pageNo,
					})
				},
				path,
				success,
				failure,
			}));
		}
	}
	fetchSubscriptionInfo(subscription) {
		const { subscriberNo, productTypeCode } = subscription;
		const path = `subscriptionInfo_${subscriberNo}_${productTypeCode}`;
		const {
			dispatch, token,
			transactions: { [path]: subscriptionInfoData, },
		} = this.props;
		const mapFunction = (data) => {
			// console.log('subscriptionInfo');
			this.changeState({ fetchingSubscription: false }, () => {
				const subscriptionVariables = (data.interface && data.interface.interface_results) || [];
				const subscriptionList = this.getSubscriptionList();
				const productList = subscriptionList
					.filter(i => i.billingAccountNo === subscription.billingAccountNo
						&& ['ACTIVE', 'SUSPENDED'].indexOf(i.status) >= 0
					)
					.sort(sortFunction('subscriberNo')(false))
					.map(i => i.productTypeCode)
					.join();
				const variableName = `${Interfaces.SCRIPTING.SubscriptionInfoName}_products`;
				this.handleVariableChange(subscriptionVariables, variableName, productList, 'Productos');
				this.handleResultsChange(subscriptionVariables, Interfaces.SCRIPTING.SubscriptionInfoBase);
			});
		};
		if (this.isSuccessInterfaceResponse(subscriptionInfoData)) {
			mapFunction(subscriptionInfoData);
		} else {
			this.changeState({ fetchingSubscription: true });
			const failure = () => {
				window.message.error(i18n.t('ErrorFetchingSubscriptionInfo'));
				this.changeState({ fetchingSubscription: false });
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.ENSAMBLE.SubscriptionByPhone}/getForm`,
				item: {
					form: JSON.stringify({
						phone_number: subscriberNo,
						product_code: productTypeCode,
					})
				},
				path,
				success,
				failure,
			}));
		}
	}
	fetchCaseHistories({ id, histories, }) {
		if (!id || (histories && histories.length)) return;
		const path = `caseHistories_${id}`;
		const fetchingCaseHistories = `fetchingCaseHistories_${id}`;
		const {
			dispatch, token,
			transactions: { [path]: caseHistoriesData, },
		} = this.props;

		const mapFunction = (data) => {
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			const caseHistories = [];
			let caseDetails = {};
			if (fetchResults.caseHistories) {
				fetchResults.caseHistories.forEach(i => {
					caseHistories.push(i);
				});
			}
			if (fetchResults.caseDetails) {
				caseDetails = fetchResults.caseDetails;
			}
			this.changeState({
				[fetchingCaseHistories]: false,
				selectedCase: Object.assign({},
					this.state.selectedCase,
					{ histories: caseHistories, caseDetails, },
				),
			});
		};
		if (this.isSuccessInterfaceResponse(caseHistoriesData)) {
			mapFunction(caseHistoriesData);
		} else {
			this.changeState({ [fetchingCaseHistories]: true });
			const failure = (resp) => {
				this.changeState({ [fetchingCaseHistories]: false, });
				window.message.error(i18n.t('ErrorFetchingCaseHistory'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.CaseHistories}/getForm`,
				item: { form: JSON.stringify({ case_id: id, }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerCases({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerCases_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerCasesData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({ fetchingCases: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerCases) {
				const customerCases = (baseList || []).slice();
				fetchResults.customerCases.forEach(i => {
					customerCases.push(i);
					if (i.notes) {
						i.notes.sort((a, b) => {
							if (a.creationDate > b.creationDate) return -1;
							else if (a.creationDate < b.creationDate) return 1;
							return 0;
						});
					}
				});
				customerCases.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerCases,
					casesLastPageLoaded: fetchResults.pageCases,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerCasesData)) {
			mapFunction(customerCasesData);
		} else {
			this.changeState({ fetchingCases: true });
			const failure = (resp) => {
				this.changeState({ fetchingCases: false });
				window.message.error(i18n.t('ErrorFetchingCustomerCases'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Cases}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerNotes({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerNotes_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerNotesData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({ fetchingNotes: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerNotes) {
				const customerNotes = (baseList || []).slice();
				fetchResults.customerNotes.forEach(i => customerNotes.push(i));
				customerNotes.sort((a, b) => {
					if (a.id > b.id) return -1;
					else if (a.id < b.id) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerNotes,
					notesLastPageLoaded: fetchResults.pageNotes,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerNotesData)) {
			mapFunction(customerNotesData);
		} else {
			this.changeState({ fetchingNotes: true });
			const failure = (resp) => {
				this.changeState({ fetchingNotes: false });
				window.message.error(i18n.t('ErrorFetchingCustomerNotes'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Notes}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerInteractions({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerInteractions_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerInteractionsData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({ fetchingInteractions: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerInteractions) {
				const customerInteractions = (baseList || []).slice();
				fetchResults.customerInteractions.forEach(i => customerInteractions.push(i));
				customerInteractions.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerInteractions,
					interactionsLastPageLoaded: fetchResults.pageInteractions,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerInteractionsData)) {
			mapFunction(customerInteractionsData);
		} else {
			this.changeState({ fetchingInteractions: true });
			const failure = (resp) => {
				this.changeState({ fetchingInteractions: false });
				window.message.error(i18n.t('ErrorFetchingCustomerInteractions'), 0,
					respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.Interactions}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerOrders({ customerID, pageNumber, baseList }) {
		if (!customerID) return;

		const pageNo = pageNumber || 1;
		const path = `customerOrders_${customerID}_${pageNo}`;
		const {
			dispatch, token,
			transactions: { [path]: customerOrdersData, },
		} = this.props;

		const mapFunction = (data) => {
			this.changeState({ fetchingOrders: false });
			// console.log(path);
			const fetchResults = this.getObjResultFromData(data);
			if (fetchResults.customerOrders) {
				const customerOrders = (baseList || []).slice();
				fetchResults.customerOrders.forEach(i => customerOrders.push(i));
				customerOrders.sort((a, b) => {
					if (a.creationDate > b.creationDate) return -1;
					else if (a.creationDate < b.creationDate) return 1;
					return 0;
				});
				this.handleReduxChangeObj({
					customerOrders,
					ordersLastPageLoaded: fetchResults.pageOrders,
				});
			}
		};
		if (this.isSuccessInterfaceResponse(customerOrdersData)) {
			mapFunction(customerOrdersData);
		} else {
			this.changeState({ fetchingOrders: true });
			const failure = (resp) => {
				this.changeState({ fetchingOrders: false });
				window.message.error(i18n.t('ErrorFetchingCustomerOrders'), 0, respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.OMS.Orders}/getForm`,
				item: { form: JSON.stringify({ customer_id: customerID, page_number: pageNo }) },
				path,
				success,
				failure,
			}));
		}
	}

	renderSearchType() {
		const { searchType, } = this.getSearchTypeValue();
		let searchButton;
		const commonSearch = [PHONE, IDENTITY_CARD, PASSPORT];
		const advancedSearch = [
			TAX_ID, BAN_ACCOUNT_NO,
			CONTACT_NAME, CUSTOMER_ID,
			CUSTOMER_NAME,
			SMART_CARD, //STB_NO,
			GUID_PARCIAL,
			// CLARO_VIDEO_NO,
			// IMEI, SIM_CARD, ORDER_NO,
		];
		if (commonSearch.indexOf(searchType) !== -1) {
			searchButton = searchType;
		} else if (advancedSearch.indexOf(searchType) !== -1) {
			searchButton = ADVANCED;
		} else {
			searchButton = PHONE;
		}

		const commonItems = {
			[CustomerSearchTypeNames()[PHONE]]: PHONE,
			[CustomerSearchTypeNames()[IDENTITY_CARD]]: IDENTITY_CARD,
			[CustomerSearchTypeNames()[PASSPORT]]: PASSPORT,
			[CustomerSearchTypeNames()[ADVANCED]]: ADVANCED,
		};
		const advancedItems = advancedSearch
			.map(searchItem => ({
				value: searchItem, label: [CustomerSearchTypeNames()[searchItem]],
			}))
			.sort(sortFunction('label')(false));

		return (
			<div>
				<div className="form-group">
					<label htmlFor="searchBy">{i18n.t('SearchBy')}:&nbsp;</label>
					<ButtonGroup
						items={{ active: CustomerSearchTypeNames()[searchButton], values: commonItems, }}
						onChange={(button) => this.handleCustomerSearchChange(button)}
						disabled={this.isSearchDisabled()}
					/>
				</div>
				<div className="form-group" hidden={(searchButton !== ADVANCED)}>
					<label htmlFor="searchBySelect">{i18n.t('SearchBy')}:&nbsp;</label>
					<DropDown
						items={advancedItems}
						value={searchType}
						onChange={(e) => {
							this.handleReduxChangeObj({
								searchType: e.target.value,
								searchValue: '',
							});
						}}
					/>
				</div>
			</div>
		);
	}
	renderSearchValue() {
		const { searchType, searchValue, } = this.getSearchTypeValue();
		const { advSearch, } = this.state;
		const searchIcons = {
			[PHONE]: 'fa-phone',
			[CLARO_VIDEO_NO]: 'fa-phone',
			[IDENTITY_CARD]: 'fa-user',
			[PASSPORT]: 'fa-pied-piper',
			[TAX_ID]: 'fa-building',
		};
		const onEnterPress = (e) => {
			if (e.key === 'Enter') this.btnSearch.click();
		};
		let onSearchValueChange = (e) => this.handleReduxChange('searchValue', e.target.value);
		const onNumericValueChange = (e) => {
			const value = e.target.value.replace(/\D/g, '');
			this.handleReduxChange('searchValue', value);
		};

		const searchIcon = searchIcons[searchType] || 'fa-search';
		if (searchType === CUSTOMER_ID) {
			const onCIDChange = (group, entity) => {
				this.handleChange('advSearch', { group, entity });
				this.handleReduxChange('searchValue', `${group}_${entity}`);
			};
			return [
				<div key="1" className="form-group">
					<label className="block" htmlFor="group">{i18n.t('CustomerIDGroup')}:</label>
					<Tooltip className="balloon" tag="TooltipCustomerIDGroupSearch">
						<Input
							type="text" name="group"
							className="form-control" classes="no-inline"
							containerStyle={{ flex: 'auto' }}
							placeholder="00"
							value={advSearch.group || ''}
							onFieldChange={(e) => onCIDChange(e.target.value, advSearch.entity)}
							onKeyPress={onEnterPress}
							disabled={this.isSearchDisabled()}
						/>
					</Tooltip>
				</div>,
				<div key="2">
					<label className="block" htmlFor="entity">{i18n.t('CustomerIDEntity')}:</label>
					<Input
						type="text" name="entity"
						className="form-control" classes="no-inline"
						value={advSearch.entity || ''}
						onFieldChange={(e) => onCIDChange(advSearch.group, e.target.value)}
						onKeyPress={onEnterPress}
						disabled={this.isSearchDisabled()}
					/>
				</div>
			];
		} else if (searchType === CONTACT_NAME) {
			const onContactNameChange = (name, lastName) => {
				this.handleChange('advSearch', { name, lastName });
				this.handleReduxChange('searchValue', `${name}_${lastName}`);
			};
			return [
				<div key="1" className="form-group">
					<label className="block" htmlFor="name">{i18n.t('ContactName')}:</label>
					<Input
						type="text" name="group"
						className="form-control" classes="no-inline"
						containerStyle={{ flex: 'auto' }}
						value={advSearch.name || ''}
						onFieldChange={(e) => onContactNameChange(e.target.value, advSearch.lastName)}
						onKeyPress={onEnterPress}
						disabled={this.isSearchDisabled()}
					/>
				</div>,
				<div key="2">
					<label className="block" htmlFor="lastName">{i18n.t('ContactLastName')}:</label>
					<Input
						type="text" name="entity"
						className="form-control" classes="no-inline"
						value={advSearch.lastName || ''}
						onFieldChange={(e) => onContactNameChange(advSearch.name, e.target.value)}
						onKeyPress={onEnterPress}
						disabled={this.isSearchDisabled()}
					/>
				</div>
			];
		} else if ([BAN_ACCOUNT_NO, PHONE, TAX_ID, SMART_CARD].indexOf(searchType) > -1) {
			onSearchValueChange = onNumericValueChange;
		}
		return (
			<Input
				classes="no-inline"
				containerStyle={{ flex: 'auto' }}
				style={{ height: 36 }}
				icon={`fa fa-2x ${searchIcon}`}
				name="customerSearch"
				placeholder={i18n.t('Search')}
				onFieldChange={onSearchValueChange}
				onKeyPress={onEnterPress}
				value={searchValue}
				disabled={this.isSearchDisabled()}
			/>
		);
	}
	renderCustomerSearch() {
		const {
			searchValidation: { searchValid, searchClass, searchMessage },
		} = this.state;

		const btnIcon = this.isFetchingCustomer() ? 'fa-spinner fa-spin' : 'fa-search';
		return (
			<Panel title={i18n.t('CustomerSearch')} >
				{this.renderSearchType()}
				<div className="form-group">
					<div style={{ display: 'flex' }}>
						<div className="full">
							{this.renderSearchValue()}
						</div>
						<div style={{ display: 'flex', flexDirection: 'column-reverse' }}>
							<Button
								funcRef={(ref) => (this.btnSearch = ref)}
								className="btn-danger"
								icon={btnIcon}
								label={i18n.t('ToSearch')}
								onClick={() => this.handleCustomerSearch()}
								disabled={this.isFetchingCustomer()}
							/>
						</div>
					</div>
					{!searchValid &&
						<label htmlFor="search" className={`label label-${searchClass}`}>
							{searchMessage}
						</label>
					}
				</div>
			</Panel>
		);
	}
	renderCustomerList() {
		const loading = this.isFetchingCustomer();
		const { customerID } = this.getSelectedCustomer();
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('customersPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('customersFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerList().filter(i => !_textFilter ||
				containsInsensitive(i.name, _textFilter) ||
				containsInsensitive(i.taxID, _textFilter) ||
				containsInsensitive(i.document, _textFilter) ||
				containsInsensitive(i.segment, _textFilter) ||
				containsInsensitive(i.contactGender, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('customersPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('customersPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('customersPageSize', CustomersPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('customersPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('customersFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = customersOrderByList();
		const orderBy = this.getReduxProp('customersOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('customersOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerList();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChangeObj({
				customersOrderBy: _orderBy,
				customerOrderDesc: _orderDesc,
				customerList: list,
			});
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const selected = rows.find(i => i.customerID === customerID);

		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});

		return (
			<Panel
				title={[
					<AnimatedSpinner key="1" show={loading} />,
					i18n.t('TrasactionCustomerList'),
				]}
			>
				<DataTable
					key="1" emptyRow loading={loading}
					schema={customersSchema}
					rowKey={(row) => row.customerID}
					rowDisabled={(row) => row === selected}
					link={(row) => !this.isSearchDisabled() && this.handleCustomerClick(row)}
					{...{ rows, totalRows, pageSize, pageNumber, onPageChange, }}
					{...{ selected, renderHeader, }}
				/>
				<Button
					key="2"
					className='btn-danger'
					icon={loading ? 'fa-spinner fa-spin' : 'fa-plus-square'}
					label={i18n.t('LoadMore')}
					onClick={() => this.handleLoadMoreCustomers()}
					disabled={loading}
				/>
			</Panel>
		);
	}
	renderSubscriptionList() {
		const { customerID, name } = this.getSelectedCustomer();
		if (!customerID) return null;
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('subscriptionsPageNumber', maxPage || 1);
			}
		};

		const textFilter = this.getReduxProp('subscriptionsFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getSubscriptionList().filter(i => !_textFilter ||
				containsInsensitive(i.productTypeName, _textFilter) ||
				containsInsensitive(i.productTypeDescription, _textFilter) ||
				containsInsensitive(i.offerName, _textFilter) ||
				containsInsensitive(i.subscriberNo, _textFilter) ||
				// containsInsensitive(i.subscriptionStartDate, _textFilter) ||
				containsInsensitive(i.status, _textFilter) ||
				containsInsensitive(i.billingAccountNo, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('subscriptionsPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('subscriptionsPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('subscriptionsPageSize', SubcriptionsPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('subscriptionsPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('subscriptionsFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = subscriptionsOrderByList();
		const orderBy = this.getReduxProp('subscriptionsOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('subscriptionsOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getSubscriptionList();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChangeObj({
				subscriptionsOrderBy: _orderBy,
				subscriptionOrderDesc: _orderDesc,
				subscriptionList: list,
			});
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const selectedSubscription = this.getSelectedSubscription();
		const selected = rows.find(
			i => selectedSubscription && i.id === selectedSubscription.id
		);
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = this.isFetchingMoreSubscriptions() ? 'fa-spinner fa-spin' : 'fa-plus-square';
		return (
			<Panel
				title={[
					i18n.t('TrasactionCustomerSubscriptionList'),
					<b key="2" style={{ color: '#777', margin: '0 5px' }}>{name && `"${name.trim()}"`}</b>
				]}
			>
				<DataTable
					key='1' emptyRow
					schema={subscriptionsSchema}
					link={(row) => this.handleSubscriptionChange(row)}
					rowDisabled={() => this.isSubscriptionDisabled()}
					{...{ rows, totalRows, pageSize, pageNumber, onPageChange, }}
					{...{ selected, renderHeader, }}
				/>
				<Button
					key='2'
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={() => this.handleLoadMoreSubscriptions()}
					disabled={this.isFetchingMoreSubscriptions()}
				/>
			</Panel>
		);
	}
	renderInteractionRow({ key, row: interaction, cells, }) {
		const notes = (interaction.notes || '').trim();
		return [
			<tr key={`${key}_1`} onClick={(e) => this.handleInteractionClick(interaction, e)}>
				{cells}
				<td />
			</tr>,
			<tr key={`${key}_2`}>
				{notes &&
					<td colSpan={cells.length + 1}>
						<div style={{ whiteSpace: 'pre-wrap' }} >{notes}</div>
					</td>
				}
			</tr>
		];
	}
	renderNoteRow({ key, row: note, cells, }) {
		const notes = (note.description || '').trim();
		return [
			<tr key={`${key}_1`}>
				{cells}
				<td />
			</tr>,
			<tr key={`${key}_2`}>
				{notes &&
					<td colSpan={cells.length + 1}>
						<div style={{ whiteSpace: 'pre-wrap' }}>{notes}</div>
					</td>
				}
			</tr>
		];
	}
	renderNotesTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('notesPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('notesFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerNotes().filter(i => !_textFilter ||
				containsInsensitive(i.id, _textFilter) ||
				// containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.status, _textFilter) ||
				containsInsensitive(i.description, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('notesPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('notesPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('notesPageSize', NotesPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('notesPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('notesFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = notesOrderByList();
		const orderBy = this.getReduxProp('notesOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('notesOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerNotes();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChangeObj({
				notesOrderBy: _orderBy,
				noteOrderDesc: _orderDesc,
				customerNotes: list,
			});
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { customerID } = this.getSelectedCustomer();
		const onLoadMoreClick = () => {
			this.fetchCustomerNotes({
				customerID,
				pageNumber: this.getReduxProp('notesLastPageLoaded', 0) + 1,
				baseList: this.getCustomerNotes(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChangeObj({
				customerNotes: [],
				notesFilter: '',
				notesPageNumber: 1,
				notesLastPageLoaded: 0,
				notesOrderDesc: false,
				notesOrderBy: { id: 0 },
			}, () => {
				let cleanPage = 1;
				let path = `customerNotes_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerNotes_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerNotes({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.isFetchingNotes();
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="notes" className="tab-pane fade">
				<DataTable
					emptyRow notStriped tableClassName="table-striped-each-2"
					loading={loadMoreDisabled}
					renderRow={(props) => this.renderNoteRow(props)}
					{...{ renderHeader, rows, totalRows, pageSize, pageNumber, onPageChange, }}
					schema={{
						fields: {
							// status: i18n.t('Status'),
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
							id: i18n.t('ID'),
						}
					}}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderInteractionsTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('interactionsPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('interactionsFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerInteractions().filter(i => !_textFilter ||
				containsInsensitive(i.id, _textFilter) ||
				containsInsensitive(i.title, _textFilter) ||
				containsInsensitive((i.source || '').replace('AMDOCS_', ''), _textFilter) ||
				// containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.channelName, _textFilter) ||
				containsInsensitive(i.creatorName, _textFilter) ||
				containsInsensitive(i.notes, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('interactionsPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('interactionsPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('interactionsPageSize', InteractionsPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('interactionsPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('interactionsFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = interactionsOrderByList();
		const orderBy = this.getReduxProp('interactionsOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('interactionsOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerInteractions();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChangeObj({
				interactionsOrderBy: _orderBy,
				interactionOrderDesc: _orderDesc,
				customerInteractions: list,
			});
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { customerID } = this.getSelectedCustomer();
		const onLoadMoreClick = () => {
			this.fetchCustomerInteractions({
				customerID,
				pageNumber: this.getReduxProp('interactionsLastPageLoaded', 0) + 1,
				baseList: this.getCustomerInteractions(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChangeObj({
				customerInteractions: [],
				interactionsFilter: '',
				interactionsPageNumber: 1,
				interactionsLastPageLoaded: 0,
				interactionsOrderDesc: false,
				interactionsOrderBy: { id: 0 },
			}, () => {
				let cleanPage = 1;
				let path = `customerInteractions_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerInteractions_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerInteractions({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.isFetchingInteractions();
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="interactions" className="tab-pane fade in active" >
				<DataTable
					emptyRow loading={loadMoreDisabled}
					link={(row, e) => this.handleInteractionClick(row, e)}
					{...{ renderHeader, rows, totalRows, pageSize, pageNumber, onPageChange, }}
					schema={{
						name: 'results',
						description: '',
						fields: {
							creatorName: {
								label: i18n.t('Salesman'),
								format: (row) => `${row.creatorName || ''}${row.creatorID ? ` (${row.creatorID})` : ''}`, // eslint-disable-line max-len
							},
							channelName: i18n.t('Channel'),
							source: {
								label: i18n.t('Source'),
								format: (row) => (row.source || '').replace('AMDOCS_', ''),
							},
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
							title: i18n.t('Title'),
							id: i18n.t('ID'),
						}
					}}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderOrdersTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('ordersPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('ordersFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerOrders().filter(i => !_textFilter ||
				containsInsensitive(i.orderUid, _textFilter) ||
				// containsInsensitive(i.parentUID, _textFilter) ||
				// containsInsensitive(i.actionUid, _textFilter) ||
				containsInsensitive(i.action, _textFilter) ||
				containsInsensitive(i.creatorID, _textFilter) ||
				containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.dueDate, _textFilter) ||
				containsInsensitive(i.effectiveDate, _textFilter) ||
				containsInsensitive(i.serviceRequiredDate, _textFilter) ||
				// containsInsensitive(i.channelCode, _textFilter) ||
				// containsInsensitive(i.channelName, _textFilter) ||
				// containsInsensitive(i.reasonCode, _textFilter) ||
				containsInsensitive(i.reasonDescription, _textFilter) ||
				false
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('ordersPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('ordersPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('ordersPageSize', OrdersPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('ordersPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('ordersFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = ordersOrderByList();
		const orderBy = this.getReduxProp('ordersOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('ordersOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerOrders();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChangeObj({
				ordersOrderBy: _orderBy,
				orderOrderDesc: _orderDesc,
				customerOrders: list,
			});
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { customerID } = this.getSelectedCustomer();
		const onLoadMoreClick = () => {
			this.fetchCustomerOrders({
				customerID,
				pageNumber: this.getReduxProp('ordersLastPageLoaded', 0) + 1,
				baseList: this.getCustomerOrders(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChangeObj({
				customerOrders: [],
				ordersFilter: '',
				ordersPageNumber: 1,
				ordersLastPageLoaded: 0,
				ordersOrderDesc: false,
				ordersOrderBy: { id: 0 },
			}, () => {
				let cleanPage = 1;
				let path = `customerOrders_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerOrders_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerOrders({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.isFetchingOrders();
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="orders" className="tab-pane">
				<DataTable
					emptyRow loading={loadMoreDisabled}
					{...{ renderHeader, rows, totalRows, pageSize, pageNumber, onPageChange, }}
					link={(selectedOrder) => this.handleOrderClick(selectedOrder)}
					schema={{
						fields: {
							reasonDescription: i18n.t('ReasonDescription'),
							// reasonCode: i18n.t('ReasonCode'),
							channelName: i18n.t('ChannelName'),
							// channelCode: i18n.t('ChannelCode'),
							// serviceRequiredDate: {
							// 	type: 'DateTimeZ',
							// 	label: i18n.t('ServiceRequiredDate'),
							// },
							// effectiveDate: {
							// 	type: 'DateTimeZ',
							// 	label: i18n.t('EffectiveDate'),
							// },
							dueDate: {
								type: 'DateTimeZ',
								label: i18n.t('DueDate'),
							},
							statusLabel: i18n.t('Status'),
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
							creatorID: i18n.t('CreatorID'),
							action: i18n.t('Action'),
							actionUid: i18n.t('ActionUid'),
							// orderUid: i18n.t('OrderUid'),
							parentUID: i18n.t('ParentUid'),
						}
					}}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderTabs() {
		const { customerID } = this.getSelectedCustomer();
		if (!customerID) return null;

		const transactionResults = this.getTransactionResults();
		const loadingResults = this.isFetchingResults();
		return (
			<Panel>
				<ul className="nav nav-tabs not-nav-justified">
					<li>
						<a data-toggle="tab" href="#results">
							<AnimatedSpinner show={loadingResults} />
							{i18n.t('TransactionResults')}
						</a>
					</li>
					<li className="active">
						<a data-toggle="tab" href="#interactions">
							<AnimatedSpinner show={this.isFetchingInteractions()} />
							{i18n.t('Interactions')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#orders">
							<AnimatedSpinner show={this.isFetchingOrders()} />
							{i18n.t('Orders')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#cases">
							<AnimatedSpinner show={this.isFetchingCases()} />
							{i18n.t('Cases')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#notes">
							<AnimatedSpinner show={this.isFetchingNotes()} />
							{i18n.t('Notes')}
						</a>
					</li>
				</ul>
				<div className="tab-content">
					<InterfaceResults
						clearable
						transactionResults={transactionResults}
					/>
					{this.renderInteractionsTabContent()}
					{this.renderOrdersTabContent()}
					<CasesTab
						fetchingCases={this.isFetchingCases()}
						fetchCustomerCases={(...args) => this.fetchCustomerCases(...args)}
						customerID={customerID}
						handleCaseClick={(selectedCase) => {
							this.changeState({ selectedCase }, () => {
								this.fetchCaseHistories(selectedCase);
								ModalFactory.show('caseModal');
							});
						}}
					/>
					{this.renderNotesTabContent()}
				</div>
			</Panel>
		);
	}
	renderWorkflows() {
		const { callerName, } = this.state;
		const { customerID } = this.getSelectedCustomer();
		const selectedSubscription = this.getSelectedSubscription();
		if (!customerID || !selectedSubscription) return null;

		const applyableWorkflows = this.getWorkflows();
		const loading = this.isFetchingWorkflows();
		return (
			<Panel
				title={[
					<AnimatedSpinner key="1" show={loading} />,
					i18n.t('Workflows'),
				]}
			>
				{!callerName &&
					<label
						className="label label-danger cursor-pointer"
						style={{ fontSize: '100%' }}
						htmlFor="invalidCallerName"
						onClick={() => {
							this.callerName.refs.editor.focus();
						}}
					>
						{i18n.t('YouMustWriteCallerName')}
					</label>
				}
				<div className="m-t-sm m-b-sm">
					{applyableWorkflows.map((workflow, i) => (
						<div key={i} className="inline">
							<Button
								className="m-b-sm btn-danger"
								onClick={(e) => this.handleWorkflowClick(e, workflow)}
								label={workflow.name}
								disabled={!callerName || loading || this.isDisabled()}
							/>
						</div>
					))}
				</div>
			</Panel>
		);
	}
	render() {
		const {
			contents: { item: greetingContent },
			user,
		} = this.props;
		const { callerName, note,
			selectedCase,
			[`fetchingCaseHistories_${selectedCase && selectedCase.id}`]: fetchingCaseHistories,
			selectedInteraction, selectedOrder,
		} = this.state;
		const startDate = this.getLocalStartDate();
		const results = this.getTransactionResults();
		const marginRight = 100;
		return (
			<Page>
				<Factory
					modalref="transactionEndModal"
					title={i18n.t('WhyEndCall')} factory={TransactionEndModal}
					note={note}
					disabled={this.isDisabled()}
					onCreate={(data) => this.endCall(data)}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="caseModal" title={i18n.t('CaseInfo', selectedCase)}
					factory={CaseModal} item={selectedCase} large
					fetchingCaseHistories={fetchingCaseHistories}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="interactionModal" title={i18n.t('InteractionInfo', selectedInteraction)}
					factory={InteractionModal} item={selectedInteraction}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="orderModal" title={i18n.t('OrderInfo', selectedOrder)}
					factory={OrderModal} item={selectedOrder} large
				/>
				<Col>
					<div className="inline" style={stylesDateDiff}>
						{this.isFetchingServerDateTime() ?
							<AnimatedSpinner show /> :
							<DateDiff realTime from={startDate} />
						}
					</div>
					<Panel
						title={!user.centerId
							? (<label
								className="label label-danger"
								style={{ fontSize: '100%' }}
								htmlFor="user_without_center"
							>
								{i18n.t('CantCreateTransactionWithoutCenter')}
							</label>)
							: null
						}
					>
						<AnimatedSpinner show={this.isFetchingContent()} />
						<TransactionContentItem
							notCached
							content={greetingContent}
							results={results}
							containerStyle={{ marginRight }}
							separatorStyle={{ display: 'none' }}
						/>
					</Panel>

					<Row>
						<Col size="6">
							{this.renderCustomerSearch()}
						</Col>
						<Col size="6">
							<Panel title={i18n.t('WhatCustomerIsReporting')}>
								<FormGroup isValid={!!callerName}>
									<label className="control-label" htmlFor="callerName">
										{i18n.t('CallerName')}:
									</label>
									<Input
										ref={(ref) => (this.callerName = ref)}
										classes="no-inline"
										icon="fa fa-2x fa-user"
										name="callerName"
										placeholder={i18n.t('Name')}
										onFieldChange={(e) => this.handleChange('callerName', e.target.value)}
										value={callerName}
										disabled={this.isDisabled()} required
									/>
								</FormGroup>
								<div className="form-group">
									<label htmlFor="note">{i18n.t('Notes')}: </label>
									<TextArea
										className="form-control no-inline"
										style={{ resize: 'none' }}
										name="note"
										placeholder={i18n.t('TransactionNotePlaceholder')}
										value={this.state.note}
										onFieldChange={(e) => this.handleChange('note', e.target.value)}
										rows="3"
										disabled={this.isDisabled()}
									/>
								</div>
							</Panel>
						</Col>
					</Row>

					{this.renderCustomerList()}
					{this.renderSubscriptionList()}
				</Col>

				<Col>{this.renderTabs()}</Col>

				<Col>{this.renderWorkflows()}</Col>
			</Page >
		);
	}
}

const stylesDateDiff = {
	fontSize: 25,
	position: 'fixed',
	right: 20,
	marginTop: 10,
	zIndex: 10000,
	background: 'white',
	padding: 5,
	border: '1px solid #eee',
};
const containsInsensitive = function (string, subString) {
	return containsIgnoringCaseAndAccents(string, subString);
};
const renderListHeader = function ({
	textFilter, onFilterChange,
	pageSize, onPageSizeChange,
	orderBy, orderDesc, orderByList, onOrderChange,
}) {
	const pageNumbers = PageNumbers;
	return (
		<header className="panel-heading" style={{ marginTop: 10, }}>
			<div className="col-sm-3 pull-right p-l-none">
				<DropDown
					items={pageNumbers}
					value={pageSize}
					onChange={(e) => onPageSizeChange(e.target.value * 1)}
				/>
				{i18n.t('recordsByPage')}
			</div>
			<div className="col-sm-4 pull-right">
				<Input
					clearable
					placeholder={i18n.t('Search')}
					onFieldChange={(e) => onFilterChange(e.target.value)}
					value={textFilter || ''}
				/>
			</div>
			{onOrderChange &&
				<div className="col-sm-3 pull-right p-l-none">
					<DropDown
						items={orderByList}
						value={orderBy.id}
						emptyOption={<option disabled>{i18n.t('OrderBy')}</option>}
						onChange={(e) => {
							const newOrder = orderByList.find(i => `${i.id}` === e.target.value);
							onOrderChange(newOrder);
						}}
					/>
					{/* {i18n.t('OrderBy')}: */}
					<span className="m-l-sm m-r-sm">{i18n.t('DescendentOrder')}: </span>
					<input
						type="checkbox"
						value={orderDesc || false}
						onChange={(e) => onOrderChange(orderBy, e.target.checked)}
					/>
				</div>
			}
		</header>
	);
};

const sortFunction = (prop) => (desc) => (a, b) => {
	let result;
	if (a[prop] > b[prop]) result = 1;
	else if (a[prop] < b[prop]) result = -1;
	else result = 0;
	return result * (desc ? -1 : 1);
};
const customersOrderByList = () => [
	{ id: 1, label: i18n.t('Name'), desc: false, sort: sortFunction('name') },
	{ id: 2, label: i18n.t('TaxID'), desc: false, sort: sortFunction('taxID') },
	{ id: 3, label: i18n.t('CustomerType'), desc: false, sort: sortFunction('type') },
	{ id: 4, label: i18n.t('Document'), desc: false, sort: sortFunction('document') },
	{ id: 5, label: i18n.t('Segment'), desc: false, sort: sortFunction('segment') },
	{ id: 6, label: i18n.t('Gender'), desc: false, sort: sortFunction('contactGender') },
].sort(sortFunction('label')());
const subscriptionsOrderByList = () => [
	{ id: 1, label: i18n.t('ProductType'), desc: false, sort: sortFunction('productTypeName') },
	{ id: 2, label: i18n.t('Plan'), desc: false, sort: sortFunction('productTypeDescription') },
	{ id: 3, label: i18n.t('OfferName'), desc: false, sort: sortFunction('offerName') },
	{ id: 4, label: i18n.t('Telephone'), desc: false, sort: sortFunction('subscriberNo') },
	{ id: 5, label: i18n.t('SubscriptionDate'), desc: false, sort: sortFunction('subscriptionStartDate') }, //eslint-disable-line max-len
	{ id: 6, label: i18n.t('Status'), desc: false, sort: sortFunction('status') },
	{ id: 7, label: i18n.t('BillingAccount'), desc: false, sort: sortFunction('billingAccountNo') },
].sort(sortFunction('label')());
const interactionsOrderByList = () => [
	{ id: 1, label: i18n.t('ID'), desc: false, sort: sortFunction('id') },
	{ id: 2, label: i18n.t('Title'), desc: false, sort: sortFunction('title') },
	{ id: 3, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
	{ id: 4, label: i18n.t('Source'), desc: false, sort: sortFunction('source') },
	{ id: 5, label: i18n.t('Channel'), desc: false, sort: sortFunction('channelName') },
	{ id: 6, label: i18n.t('Salesman'), desc: false, sort: sortFunction('creatorName') },
].sort(sortFunction('label')());
const notesOrderByList = () => [
	{ id: 1, label: i18n.t('ID'), desc: false, sort: sortFunction('id') },
	{ id: 2, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
].sort(sortFunction('label')());
const ordersOrderByList = () => [
	{ id: 1, label: i18n.t('OrderUid'), desc: false, sort: sortFunction('orderUid') },
	{ id: 2, label: i18n.t('Action'), desc: false, sort: sortFunction('action') },
	{ id: 3, label: i18n.t('CreatorID'), desc: false, sort: sortFunction('creatorID') },
	{ id: 4, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
	{ id: 5, label: i18n.t('DueDate'), desc: false, sort: sortFunction('dueDate') },
	{ id: 6, label: i18n.t('EffectiveDate'), desc: false, sort: sortFunction('effectiveDate') },
	{ id: 7, label: i18n.t('ServiceRequiredDate'), desc: false, sort: sortFunction('serviceRequiredDate') }, // eslint-disable-line max-len
	{ id: 8, label: i18n.t('ReasonDescription'), desc: false, sort: sortFunction('reasonDescription') }, // eslint-disable-line max-len
].sort(sortFunction('label')());
export default connect(mapStateToProps)(TransactionCreate);
