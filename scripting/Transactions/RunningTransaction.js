import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Col, Panel,
	DataTable,
	Pager, Fa,
	Input, TextArea,
	DropDown, Button,
	DateDiff, H4,
	Factory, ModalFactory,
	AnimatedSpinner, BigAnimatedSpinner, Tooltip,
} from '../../reactadmin/components/';

import {
	TransactionEndModal,
	TransactionStepRegular,
	TransactionStepInterface,
	CaseModal, InteractionModal, OrderModal,
	InterfaceResults,
	CasesTab,
} from './';
import {
	WorkflowStepTypes,
	PageNumbers,
	NotesPageSize,
	InteractionsPageSize,
	Interfaces,
	OrdersPageSize,
	DateFormats,
} from '../../store/enums';
import {
	containsIgnoringCaseAndAccents,
	isFunction, numberFormat, respErrorDetails,
} from '../../reactadmin/Utils';
import i18n from '../../i18n';

const transactionActions = ReduxOutlet('transactions', 'transaction').actions;
const interfacesActions = ReduxOutlet('interfaces', 'interfaceBase').actions;

const interactionsSchema = {
	name: 'results',
	description: '',
	fields: {
		creatorName: i18n.t('Salesman'),
		channelName: i18n.t('Channel'),
		source: {
			label: i18n.t('Source'),
			format: (row) => (row.source || '').replace('AMDOCS_', ''),
		},
		creationDate: {
			type: 'DateTimeZ',
			label: i18n.t('CreationDate'),
		},
		title: i18n.t('Title'),
		id: i18n.t('ID'),
	}
};
const ordersSchema = {
	fields: {
		reasonDescription: i18n.t('ReasonDescription'),
		// reasonCode: i18n.t('ReasonCode'),
		channelName: i18n.t('ChannelName'),
		// channelCode: i18n.t('ChannelCode'),
		// serviceRequiredDate: {
		// 	type: 'DateTimeZ',
		// 	label: i18n.t('ServiceRequiredDate'),
		// },
		// effectiveDate: {
		// 	type: 'DateTimeZ',
		// 	label: i18n.t('EffectiveDate'),
		// },
		dueDate: {
			type: 'DateTimeZ',
			label: i18n.t('DueDate'),
		},
		statusLabel: i18n.t('Status'),
		creationDate: {
			type: 'DateTimeZ',
			label: i18n.t('CreationDate'),
		},
		creatorID: i18n.t('CreatorID'),
		action: i18n.t('Action'),
		actionUid: i18n.t('ActionUid'),
		// orderUid: i18n.t('OrderUid'),
		parentUID: i18n.t('ParentUid'),
	}
};

/**
 * Componente que muestra al usuario los componentes necesarios para manejar una transacción en 
 * curso.
 *
 * @class RunningTransaction
 * @extends {Component}
 */
class RunningTransaction extends Component {
	state = {
		note: '',
		disabled: false,
		selectedCase: {},
		// fetchingCaseHistories: false,
		selectedInteraction: {},
		selectedOrder: {},
		spacing: 0,
	}
	//TODO: Cambiar disabled cuando haga espera por el servidor.
	//	Pasar a los hijos para que también esperen.
	//	Hacerla númerico para hacer varias peticiones juntas y disminuir según son respondidas.
	componentWillMount() {
		// window.Scripting_RunningTransaction = this;
	}
	componentWillReceiveProps(props) {
		let note = this.state.note;
		// TODO: Está mostrando la nota de la transaccion abierta anteriormente
		//			porque se queda en memoria.
		if (!note) {
			const trans = this.getItem(props);
			note = trans && trans.note;
			if (!note) {
				note = props.transactions.item.note || '';
			}
		}
		this.setState({
			note,
		});
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	componentWillUnmount() {
		this.unmounting = true;
	}

	getCustomerNotes() {
		const { transactions: { customerNotes }, } = this.props;
		return customerNotes || [];
	}
	getCustomerInteractions() {
		const { transactions: { customerInteractions }, } = this.props;
		return customerInteractions || [];
	}
	getCustomerOrders() {
		const { transactions: { customerOrders }, } = this.props;
		return customerOrders || [];
	}
	getItem(props) {
		const { transactions, } = (props || this.props);
		return transactions.item;
	}
	getTransaction(props) {
		const { transactions, } = props || this.props;
		const data = transactions.runningTransaction;
		if (data && data.items) {
			return data.items[0];
		}
		return undefined;
	}
	getTransactionResults() {
		// const item = this.getTransaction();
		// return (item && item.transaction_results) || [];
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}
	getTransactionResultsObjValue(prop) {
		const result = this.getTransactionResultsObj()[prop];
		return (result && result.value) || null;
	}
	getTransactionSteps() {
		const item = this.getTransaction();
		return (item && item.transaction_steps) || [];
	}
	getReduxProp(prop, def) {
		const { transactions, } = this.props;
		return transactions[prop] || def;
	}
	getPreviousStep() {
		const steps = this.getTransactionSteps();
		return steps[steps.length - 2];
	}
	goToPreviousStep() {
		const { transactions: { goBackStep, } } = this.props;
		const step = this.getPreviousStep();
		goBackStep(step);
	}

	unmounting = false;
	changeState(state, callback) {
		if (!this.unmounting) {
			this.setState(state, callback);
		}
	}

	fetchCaseHistories({ id, histories, }) {
		if (!id || (histories && histories.length)) return;
		const path = `caseHistories_${id}`;
		const fetchingCaseHistories = `fetchingCaseHistories_${id}`;
		const {
			dispatch, token,
			transactions: { [path]: caseHistoriesData, },
		} = this.props;

		const mapFunction = (data) => {
			// console.log(path);
			const fetchResults = this.props.getObjResultFromData(data);
			const caseHistories = [];
			let caseDetails = {};
			if (fetchResults.caseHistories) {
				fetchResults.caseHistories.forEach(i => {
					caseHistories.push(i);
				});
			}
			if (fetchResults.caseDetails) {
				caseDetails = fetchResults.caseDetails;
			}
			this.changeState({
				[fetchingCaseHistories]: false,
				selectedCase: Object.assign({},
					this.state.selectedCase,
					{ histories: caseHistories, caseDetails, },
				),
			});
		};
		if (this.props.isSuccessInterfaceResponse(caseHistoriesData)) {
			mapFunction(caseHistoriesData);
		} else {
			this.changeState({ [fetchingCaseHistories]: true });
			const failure = (resp) => {
				this.changeState({ [fetchingCaseHistories]: false, });
				window.message.error(i18n.t('ErrorFetchingCaseHistory'), 0,
					respErrorDetails(resp));
			};
			const success = (resp) => {
				if (this.props.isSuccessInterfaceResponse(resp.data)) {
					mapFunction(resp.data);
				} else {
					failure(resp);
				}
			};
			dispatch(interfacesActions.postToURL({
				token,
				route: `interface/${Interfaces.CRM.CaseHistories}/getForm`,
				item: { form: JSON.stringify({ case_id: id, }) },
				path,
				success,
				failure,
			}));
		}
	}
	fetchCustomerNotes({ customerID, pageNumber, baseList }) {
		const { fetchCustomerNotes } = this.props;
		fetchCustomerNotes({ customerID, pageNumber, baseList });
	}
	fetchCustomerInteractions({ customerID, pageNumber, baseList }) {
		const { fetchCustomerInteractions } = this.props;
		fetchCustomerInteractions({ customerID, pageNumber, baseList });
	}
	fetchCustomerOrders({ customerID, pageNumber, baseList }) {
		const { fetchCustomerOrders } = this.props;
		fetchCustomerOrders({ customerID, pageNumber, baseList });
	}

	endCallClick(e) {
		e.preventDefault();
		ModalFactory.show('transactionEndModal');
	}
	endCall(data) {
		this.props.transactions.endCall(data);
	}

	handleNoteChange(note) {
		const { dispatch, token, params: { id }, } = this.props;
		this.changeState({ note, });
		dispatch(transactionActions.postToURL({
			token,
			route: `transaction/${id}/updateNote`,
			item: { id, value: note },
			path: 'updateNote',
			failure: (resp) => {
				if (resp.status === 406) {
					this.changeState({ disabled: true });
					window.message.error(i18n.t('TransactionNotModifiable'), 0,
						respErrorDetails(resp));
					this.props.transactions.fetchTransaction(true);
				}
			}
		}));
	}
	handleReduxChange(prop, value, callback) {
		this.props.dispatch(
			transactionActions.updateState(prop, value))
			.then(() => isFunction(callback) && callback());
	}
	handleInteractionClick(selectedInteraction) {
		this.changeState({ selectedInteraction });
		ModalFactory.show('interactionModal');
	}
	handleOrderClick(selectedOrder) {
		this.changeState({ selectedOrder });
		ModalFactory.show('orderModal');
	}

	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}

	renderNoteEditor() {
		return (
			<div
				style={{
					flex: 'none',
					display: 'flex',
					flexDirection: 'row',
					alignItems: 'stretch',
					height: '25%',
					minHeight: 100,
					backgroundColor: '#FAFAFA',
					padding: 10,
					boxShadow: '0px -10px 15px #cecece',
					zIndex: 10,
				}}
			>
				<div style={{ padding: 0 }}>
					<label htmlFor="notes">{i18n.t('Notes')}:</label>
				</div>
				<div style={{ padding: '0px 5px', flex: 'auto' }}>
					<TextArea
						className="form-control" name="note"
						style={{ height: '100%', resize: 'none' }}
						placeholder={i18n.t('TransactionNotePlaceholder')}
						value={this.state.note}
						onFieldChange={(e) => this.handleNoteChange(e.target.value)}
						rows="3"
					/>
				</div>
			</div>
		);
	}
	renderInfoTabContent() {
		const getValue = (prop) => this.getTransactionResultsObjValue(prop);

		const onLine = getValue(`${Interfaces.SCRIPTING.TransactionInfoName}_caller_name`);
		const titular = getValue('customer_info_name');
		const phoneNumber = getValue(`${Interfaces.SCRIPTING.SubscriptionInfoName}_subscriberNo`);
		const statusName = getValue(`${Interfaces.SCRIPTING.SubscriptionInfoName}_status`);
		const document = getValue('customer_info_document');
		const subscriptionStartDate = getValue(
			`${Interfaces.SCRIPTING.SubscriptionInfoName}_subscriptionStartDate`
		);

		const email = getValue('customer_info_contactEmail')
			|| getValue('subscription_info_billEmail')
			|| getValue('customer_info_email');
		const miClaro = getValue('customer_info_hasMiClaro') === 'true';
		const hasMiClaro = this.isDisabled() && !miClaro ? '' :
			i18n.t(`${miClaro}`);
		const profile = getValue('customer_info_type');
		const segment = getValue('customer_info_segment');

		const aprovDate = getValue(`${Interfaces.SCRIPTING.SubscriptionInfoName}_instalationDate`);
		const productName = getValue(`${Interfaces.SCRIPTING.SubscriptionInfoName}_productTypeName`);
		const planName = getValue(
			`${Interfaces.SCRIPTING.SubscriptionInfoName}_productTypeDescription`
		);
		const lastOrderDate = getValue('customer_info_lastOrderDate');
		const lastOrderReason = getValue('customer_info_lastOrderReason');
		const billPreviousBalanceAmount = getValue('subscription_info_billPreviousBalanceAmount');
		const billCycleCode = getValue('subscription_info_billCycleCode');
		const billPaymentsReceivedAmount = getValue('subscription_info_billPaymentsReceivedAmount');
		const billCloseDay = getValue('subscription_info_billCloseDay');
		const billPastDueAmount = getValue('subscription_info_billPastDueAmount');
		const billDueDate = getValue('subscription_info_billDueDate');
		const billCurrentChargesOrCreditsAmount =
			getValue('subscription_info_billCurrentChargesOrCreditsAmount');
		const billCreditsOrAjustAppliedAmount =
			getValue('subscription_info_billCreditsOrAjustAppliedAmount');
		const billTotalDueAmount = getValue('subscription_info_billTotalDueAmount');
		const billBalanceAmount = getValue('subscription_info_billBalanceAmount');
		return (
			<div id="info" className="tab-pane fade in active">
				<table className="table table-responsive">
					<tbody>
						<tr>
							<td><strong>{i18n.t('CallerName')}: </strong><br />{onLine}</td>
							<td><strong>{i18n.t('Owner')}: </strong><br />{titular}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('Number')}: </strong>{phoneNumber}</td>
							<td><strong>{i18n.t('Document')}: </strong>{document}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('Status')}: </strong>{i18n.t(statusName)}</td>
							<td><strong>{i18n.t('Profile')}: </strong>{i18n.t(profile)}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('Segment')}: </strong>{segment}</td>
							<td><strong>{i18n.t('Plan')}: </strong>{planName}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('Product')}: </strong>{productName}</td>
							<td><strong>{i18n.t('HasMiClaro')}: </strong>{hasMiClaro}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('Email')}: </strong><br />{email}</td>
							<td><strong>{i18n.t('FirstInstallationDate')}: </strong><br />
								{(subscriptionStartDate && moment(subscriptionStartDate).format(DateFormats.DATE))}
							</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('LastOrderDate')}: </strong><br />
								{(lastOrderDate && moment(lastOrderDate).format(DateFormats.DATE_TIME_Z))}
							</td>
							<td><strong>{i18n.t('ProvisioningDate')}: </strong><br />
								{(aprovDate && moment(aprovDate).format(DateFormats.DATE_TIME_Z))}
							</td>
						</tr>
						<tr>
							<td colSpan="2"><strong>{i18n.t('LastOrderReason')}: </strong>{lastOrderReason}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('BillPreviousBalanceAmount')}: </strong>{
								billPreviousBalanceAmount ? numberFormat(billPreviousBalanceAmount) : null}</td>
							<td><strong>{i18n.t('BillCloseDay')}: </strong>{billCloseDay}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('BillPaymentsReceivedAmount')}: </strong>{
								billPaymentsReceivedAmount ? numberFormat(billPaymentsReceivedAmount) : null}</td>
							<td><strong>{i18n.t('BillCycleCode')}: </strong>{billCycleCode}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('BillPastDueAmount')}: </strong>{
								billPastDueAmount ? numberFormat(billPastDueAmount) : null}</td>
							<td><strong>{i18n.t('BillDueDate')}: </strong><br />
								{(billDueDate && moment(billDueDate).format(DateFormats.DATE_TIME_Z))}
							</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('BillCurrentChargesOrCreditsAmount')}: </strong>{
								billCurrentChargesOrCreditsAmount
									? numberFormat(billCurrentChargesOrCreditsAmount) : null}</td>
							<td><strong>{i18n.t('BillCreditsOrAjustAppliedAmount')}: </strong>{
								billCreditsOrAjustAppliedAmount
									? numberFormat(billCreditsOrAjustAppliedAmount) : null}</td>
						</tr>
						<tr>
							<td><strong>{i18n.t('BillTotalDueAmount')}: </strong>{
								billTotalDueAmount ? numberFormat(billTotalDueAmount) : null}</td>
							<td><strong>{i18n.t('BillBalanceAmount')}: </strong>{
								billBalanceAmount ? numberFormat(billBalanceAmount) : null}</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
	renderInteractionRow({ key, row: interaction, cells, }) {
		const notes = (interaction.notes || '').trim();
		return [
			<tr key={`${key}_1`} onClick={(e) => this.handleInteractionClick(interaction, e)}>
				{cells}
				<td />
			</tr>,
			<tr key={`${key}_2`} onClick={(e) => this.handleInteractionClick(interaction, e)}>
				{notes &&
					<td colSpan={cells.length + 1}>
						<div style={{ whiteSpace: 'pre-wrap' }} >{notes}</div>
					</td>
				}
			</tr>
		];
	}
	renderNoteRow({ key, row: note, cells, }) {
		const notes = (note.description || '').trim();
		return [
			<tr key={`${key}_1`}>
				{cells}
				<td />
			</tr>,
			<tr key={`${key}_2`}>
				{notes &&
					<td colSpan={cells.length + 1}>
						<div style={{ whiteSpace: 'pre-wrap' }}>{notes}</div>
					</td>
				}
			</tr>
		];
	}
	renderNotesTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('notesPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('notesFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerNotes().filter(i => !_textFilter ||
				containsInsensitive(i.id, _textFilter) ||
				// containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.status, _textFilter) ||
				containsInsensitive(i.description, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('notesPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('notesPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('notesPageSize', NotesPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('notesPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('notesFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = notesOrderByList();
		const orderBy = this.getReduxProp('notesOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('notesOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerNotes();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChange('notesOrderBy', _orderBy);
			this.handleReduxChange('noteOrderDesc', _orderDesc);
			this.handleReduxChange('customerNotes', list);
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { id_customer: customerID } = this.getTransaction();
		const onLoadMoreClick = () => {
			this.fetchCustomerNotes({
				customerID,
				pageNumber: this.getReduxProp('notesLastPageLoaded', 0) + 1,
				baseList: this.getCustomerNotes(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChange('customerNotes', []);
			this.handleReduxChange('notesFilter', '');
			this.handleReduxChange('notesPageNumber', 1);
			this.handleReduxChange('notesLastPageLoaded', 0);
			this.handleReduxChange('notesOrderDesc', false);
			this.handleReduxChange('notesOrderBy', { id: 0 }, () => {
				let cleanPage = 1;
				let path = `customerNotes_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerNotes_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerNotes({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.props.fetchingNotes;
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="notes" className="tab-pane fade">
				<DataTable
					emptyRow loading={loadMoreDisabled}
					notStriped tableClassName="table-striped-each-2"
					renderRow={(props) => this.renderNoteRow(props)}
					{...{ renderHeader, rows, totalRows, pageSize, pageNumber, onPageChange, }}
					schema={{
						name: 'transactions',
						description: '',
						fields: {
							status: i18n.t('Status'),
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
							id: i18n.t('ID'),
						}
					}}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderInteractionsTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('interactionsPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('interactionsFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerInteractions().filter(i => !_textFilter ||
				containsInsensitive(i.id, _textFilter) ||
				containsInsensitive(i.title, _textFilter) ||
				// containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.channelName, _textFilter) ||
				containsInsensitive(i.creatorName, _textFilter) ||
				containsInsensitive(i.notes, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('interactionsPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('interactionsPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('interactionsPageSize', InteractionsPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('interactionsPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('interactionsFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = interactionsOrderByList();
		const orderBy = this.getReduxProp('interactionsOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('interactionsOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerInteractions();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChange('interactionsOrderBy', _orderBy);
			this.handleReduxChange('interactionOrderDesc', _orderDesc);
			this.handleReduxChange('customerInteractions', list);
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { id_customer: customerID } = this.getTransaction();
		const onLoadMoreClick = () => {
			this.fetchCustomerInteractions({
				customerID,
				pageNumber: this.getReduxProp('interactionsLastPageLoaded', 0) + 1,
				baseList: this.getCustomerInteractions(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChange('customerInteractions', []);
			this.handleReduxChange('interactionsFilter', '');
			this.handleReduxChange('interactionsPageNumber', 1);
			this.handleReduxChange('interactionsLastPageLoaded', 0);
			this.handleReduxChange('interactionsOrderDesc', false);
			this.handleReduxChange('interactionsOrderBy', { id: 0 }, () => {
				let cleanPage = 1;
				let path = `customerInteractions_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerInteractions_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerInteractions({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.props.fetchingInteractions;
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="interactions" className="tab-pane fade">
				<DataTable
					emptyRow loading={loadMoreDisabled}
					// notStriped tableClassName="table-striped-each-2"
					// renderRow={(props) => this.renderInteractionRow(props)}
					link={(row, e) => this.handleInteractionClick(row, e)}
					{...{ renderHeader, schema: interactionsSchema, }}
					{...{ rows, totalRows, pageSize, pageNumber, onPageChange, }}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderOrdersTabContent() {
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('ordersPageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('ordersFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getCustomerOrders().filter(i => !_textFilter ||
				containsInsensitive(i.orderUid, _textFilter) ||
				// containsInsensitive(i.parentUID, _textFilter) ||
				// containsInsensitive(i.actionUid, _textFilter) ||
				containsInsensitive(i.action, _textFilter) ||
				containsInsensitive(i.creatorID, _textFilter) ||
				containsInsensitive(i.creationDate, _textFilter) ||
				containsInsensitive(i.dueDate, _textFilter) ||
				containsInsensitive(i.effectiveDate, _textFilter) ||
				containsInsensitive(i.serviceRequiredDate, _textFilter) ||
				// containsInsensitive(i.channelCode, _textFilter) ||
				// containsInsensitive(i.channelName, _textFilter) ||
				// containsInsensitive(i.reasonCode, _textFilter) ||
				containsInsensitive(i.reasonDescription, _textFilter) ||
				false
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('ordersPageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('ordersPageNumber', newPage);
		};
		const pageSize = this.getReduxProp('ordersPageSize', OrdersPageSize);
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('ordersPageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('ordersFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};

		const orderByList = ordersOrderByList();
		const orderBy = this.getReduxProp('ordersOrderBy', { id: 0 });
		const orderDesc = this.getReduxProp('ordersOrderDesc', false);
		const onOrderChange = (_orderBy, _orderDesc) => {
			const list = this.getCustomerOrders();
			if (_orderBy && _orderBy.sort) {
				list.sort(_orderBy.sort(_orderDesc));
			} else {
				list.reverse();
			}
			this.handleReduxChange('ordersOrderBy', _orderBy);
			this.handleReduxChange('orderOrderDesc', _orderDesc);
			this.handleReduxChange('customerOrders', list);
		};

		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const { id_customer: customerID } = this.getTransaction();
		const onLoadMoreClick = () => {
			this.fetchCustomerOrders({
				customerID,
				pageNumber: this.getReduxProp('ordersLastPageLoaded', 0) + 1,
				baseList: this.getCustomerOrders(),
			});
		};
		const onResetRows = () => {
			this.handleReduxChange('customerOrders', []);
			this.handleReduxChange('ordersFilter', '');
			this.handleReduxChange('ordersPageNumber', 1);
			this.handleReduxChange('ordersLastPageLoaded', 0);
			this.handleReduxChange('ordersOrderDesc', false);
			this.handleReduxChange('ordersOrderBy', { id: 0 }, () => {
				let cleanPage = 1;
				let path = `customerOrders_${customerID}_${cleanPage}`;
				while (this.getReduxProp(path, undefined)) {
					this.handleReduxChange(path, undefined);
					cleanPage++;
					path = `customerOrders_${customerID}_${cleanPage}`;
				}
				this.fetchCustomerOrders({ customerID, pageNumber: 1, });
			});
		};
		const loadMoreDisabled = this.props.fetchingOrders;
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
			orderBy,
			orderDesc,
			orderByList,
			onOrderChange,
		});
		const btnIcon = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square';
		const btnReset = loadMoreDisabled ? 'fa-spinner fa-spin' : 'fa-repeat';
		return (
			<div id="orders" className="tab-pane">
				<DataTable
					emptyRow loading={loadMoreDisabled}
					{...{ renderHeader, schema: ordersSchema, }}
					{...{ rows, totalRows, pageSize, pageNumber, onPageChange, }}
					link={(selectedOrder) => this.handleOrderClick(selectedOrder)}
				/>
				<Button
					className='btn-danger'
					icon={btnIcon}
					label={i18n.t('LoadMore')}
					onClick={onLoadMoreClick}
					disabled={loadMoreDisabled}
				/>
				<Button
					className='btn-danger'
					icon={btnReset}
					label={i18n.t('ToUpdate')}
					onClick={onResetRows}
					disabled={loadMoreDisabled}
				/>
			</div>
		);
	}
	renderStepsTabContent() {
		const stepName = (row) => `${row.id_workflow_step} | ${row.workflow_step_name}`;
		const steps = this.getTransactionSteps();
		return (
			<div id="steps" className="tab-pane fade">
				<DataTable
					tableClassName="table-condensed"
					rows={steps}
					schema={{
						fields: {
							// active: {
							// 	type: 'Boolean',
							// 	label: i18n.t('ActiveTransactionStep'),
							// },
							// id_parent: {
							// 	label: i18n.t('ParentTransactionStep'),
							// 	format: (row, rows) => {
							// 		for (let i = 0; i < rows.length; i++) {
							// 			const iRow = rows[i];
							// 			if (iRow.id === row.id_parent) {
							// 				return formatStepName(iRow);
							// 			}
							// 		}
							// 	},
							// },
							// value: i18n.t('Value'),
							workflow_step_name: {
								label: i18n.t('Step'),
								format: (row) => stepName(row),
							},
							workflow_name: {
								label: i18n.t('Workflow'),
								format: (row) => `${row.id_workflow} | ${row.workflow_name}`,
							},
							// docdate: {
							// 	type: 'DateTimeZ',
							// 	label: i18n.t('Date'),
							// },
							id: i18n.t('ID'),
						}
					}}
					// link={(row, e) => {
					// 	this.props.router.push(`/data/workflows/${row.id_workflow}/step/${row.id}`);
					// 	const isLast = (i + 1) === steps.length;
					// 	const isClickleable = !isLast && !isDisabled;
					// 	return {
					// 		label: row.workflow_step_name,
					// 		to: isClickleable ? `/${row.id}` : '',
					// 		onClick: (e) => {
					// 			e.preventDefault();
					// 			if (isClickleable) this.goBackStep(step);
					// 		},
					// 	};
					// }}
					link={(step) => this.props.transactions.goBackStep(step)}
					rowDisabled={(row, rows) => row.id === rows[rows.length - 1].id}
				/>
			</div>
		);
	}
	renderTabs() {
		const {
			fetchCustomerCases,
			fetchingCases, fetchingNotes,
			fetchingOrders, fetchingInteractions,
		} = this.props;
		const item = this.getTransaction();
		const startDate = item.start_date;
		const transactionResults = this.getTransactionResults();

		const loadingInfo = this.isDisabled();
		const loadingResults = this.isDisabled();
		return (
			<Panel
				containerStyle={{
					display: 'flex',
					flexFlow: 'column',
					maxHeight: '98%',
					marginBottom: 0,
				}}
				contentStyle={{ flex: '1 1 auto', overflow: 'auto', maxHeight: '100%', }}
				title={
					<DateDiff
						className="inline"
						style={{ margin: 0 }}
						realTime
						from={startDate}
						container={H4}
					/>}
			>
				<div style={{ top: 8, right: 50, position: 'absolute' }}>
					{this.getPreviousStep() &&
						<Button
							label={i18n.t('GoToBackStep')} size="btn-sm"
							color="btn-danger" rounded
							onClick={() => this.goToPreviousStep()}
							disabled={this.isDisabled()}
						/>
					}
					<Button
						label={i18n.t('EndCall')} size="btn-sm"
						color="btn-danger" rounded
						onClick={(e) => this.endCallClick(e)}
						disabled={this.isDisabled()}
					/>
				</div>
				<ul className="nav nav-tabs not-nav-justified">
					<li className="active">
						<a data-toggle="tab" href="#info">
							<AnimatedSpinner show={loadingInfo} />
							{i18n.t('Customer')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#results">
							<AnimatedSpinner show={loadingResults} />
							{i18n.t('TransactionResults')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#interactions">
							<AnimatedSpinner show={fetchingInteractions} />
							{i18n.t('Interactions')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#orders">
							<AnimatedSpinner show={fetchingOrders} />
							{i18n.t('Orders')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#cases">
							<AnimatedSpinner show={fetchingCases} />
							{i18n.t('Cases')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#notes">
							<AnimatedSpinner show={fetchingNotes} />
							{i18n.t('Notes')}
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#steps">
							<AnimatedSpinner show={loadingInfo} />
							{i18n.t('TransactionSteps')}
						</a>
					</li>
				</ul>
				<div className="tab-content">
					<InterfaceResults clearable transactionResults={transactionResults} />
					{this.renderInfoTabContent()}
					{this.renderInteractionsTabContent()}
					{this.renderOrdersTabContent()}
					<CasesTab
						{...{ fetchingCases, fetchCustomerCases, }}
						customerID={item.id_customer}
						handleCaseClick={(selectedCase) => {
							this.changeState({ selectedCase }, () => {
								this.fetchCaseHistories(selectedCase);
								ModalFactory.show('caseModal');
							});
						}}
					/>
					{this.renderNotesTabContent()}
					{this.renderStepsTabContent()}
				</div>
			</Panel>
		);
	}
	renderView() {
		const { router, } = this.props;
		const item = this.getTransaction();
		const disabled = this.isDisabled();
		if (!item.workflow_step) {
			return (
				<Panel>
					{i18n.t('AnErrorHasOccurredInTransaction')}
				</Panel>
			);
		}
		if (item.id === 0 || item.workflow_step.id_type === WorkflowStepTypes.REGULAR) {
			return (
				<TransactionStepRegular {...{ item, router, disabled, }} />
			);
		}
		if (item.workflow_step.id_type === WorkflowStepTypes.EXTERNAL) {
			return (
				<TransactionStepInterface {...{ item, router, disabled, }} />
			);
		}
		return null;
	}
	render() {
		const { note,
			selectedCase,
			[`fetchingCaseHistories_${selectedCase && selectedCase.id}`]: fetchingCaseHistories,
			selectedInteraction,
			selectedOrder,
			spacing,
		} = this.state;
		const item = this.getTransaction();
		if (!item) {
			// return <div>{i18n.t('LoadingYet')}...</div>;
			return (
				<BigAnimatedSpinner />
			);
		}

		let tabsClass = '';
		let contentsClass = '';
		if (spacing === 0) {
			tabsClass += 'col-sm-push-6 col-sm-6';
			contentsClass += 'col-sm-pull-6 col-sm-6';
		} else if (spacing === 1) {
			tabsClass += 'col-sm-12 p-l-md';
			contentsClass += 'hidden';
		} else {
			tabsClass += 'hidden';
			contentsClass += 'col-sm-12 p-r-lg';
		}
		let right = '';
		if (spacing === 0) {
			right = 'calc(50% - 10px)';
		} else if (spacing === 1) {
			right = 'calc(100% - 20px)';
		} else {
			right = 10;
		}
		return (
			<div className="runningT" style={{ height: '100%', position: 'relative', }}>
				<Factory
					modalref="transactionEndModal"
					title={i18n.t('WhyEndCall')} factory={TransactionEndModal}
					note={note}
					disabled={this.isDisabled()}
					onCreate={(data) => this.endCall(data)}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="caseModal" title={i18n.t('CaseInfo', selectedCase)}
					factory={CaseModal} item={selectedCase} large
					fetchingCaseHistories={fetchingCaseHistories}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="interactionModal" title={i18n.t('InteractionInfo', selectedInteraction)}
					factory={InteractionModal} item={selectedInteraction}
				/>
				<Factory
					canBeClose backdrop="true" keyboard="true"
					modalref="orderModal" title={i18n.t('OrderInfo', selectedOrder)}
					factory={OrderModal} item={selectedOrder} large
				/>
				<div
					className="hidden-xs"
					style={{
						position: 'absolute',
						right,
						width: 15,
						fontSize: 16,
						color: '#777',
						zIndex: 1000,
					}}
				>
					<Tooltip position="left" tag="TooltipRunningTransactionShowTabs">
						<Fa
							icon="step-backward"
							onClick={() => this.changeState({ spacing: 1 })}
							style={{ visibility: spacing === 1 ? 'hidden' : 'unset', }}
						/>
					</Tooltip>
					<Tooltip
						position={spacing === 1 ? 'right' : `${spacing === -1 ? 'left' : 'down'}`}
						tag="TooltipRunningTransactionShowBoth"
						style={{ visibility: spacing === 0 ? 'hidden' : 'unset', }}
					>
						<Fa
							icon="pause"
							onClick={() => this.changeState({ spacing: 0 })}
							style={{ marginLeft: -2, visibility: spacing === 0 ? 'hidden' : 'unset', }}
						/>
					</Tooltip>
					<Tooltip position="right" tag="TooltipRunningTransactionShowContents">
						<Fa
							icon="step-forward"
							onClick={() => this.changeState({ spacing: -1 })}
							style={{ visibility: spacing === -1 ? 'hidden' : 'unset', }}
						/>
					</Tooltip>
				</div>
				<Col
					size={{}} className={`tabs-column p-r-sm ${tabsClass}`}
					style={{ height: '100%', }}
				>
					<div style={{ flex: '1 1 auto', overflow: 'auto', height: '100%', }}>
						{this.renderTabs()}
					</div>
				</Col>
				<Col
					size={{}} className={`contents-column -p-l-sm ${contentsClass}`}
					style={{ height: '100%', display: 'flex', flexDirection: 'column', }}
				>
					<div
						className="step-contents-wrapper"
						style={{ flex: '1 1 auto', overflow: 'auto', height: '100%', }}
					>
						{this.renderView()}
					</div>
					{this.renderNoteEditor()}
				</Col>
			</div >
		);
	}
}
const containsInsensitive = function (string, subString) {
	return containsIgnoringCaseAndAccents(string, subString);
};
const renderListHeader = function ({
	textFilter, onFilterChange,
	pageSize, onPageSizeChange,
	orderBy, orderDesc, orderByList, onOrderChange,
}) {
	const pageNumbers = PageNumbers;
	return (
		<header className="panel-heading" style={{ marginTop: 10, }}>
			<div className="col-sm-3 pull-right p-l-none">
				<DropDown
					items={pageNumbers}
					value={pageSize}
					onChange={(e) => onPageSizeChange(e.target.value * 1)}
				/>
				{i18n.t('recordsByPage')}
			</div>
			<div className="col-sm-4 pull-right">
				<Input
					clearable
					placeholder={i18n.t('Search')}
					onFieldChange={(e) => onFilterChange(e.target.value)}
					value={textFilter || ''}
				/>
			</div>
			{onOrderChange &&
				<div className="col-sm-5 pull-right p-l-none">
					<DropDown
						items={orderByList}
						value={orderBy.id}
						emptyOption={<option disabled>{i18n.t('OrderBy')}</option>}
						onChange={(e) => {
							const newOrder = orderByList.find(i => `${i.id}` === e.target.value);
							onOrderChange(newOrder);
						}}
					/>
					{/* {i18n.t('OrderBy')}: */}
					<span className="m-l-sm m-r-sm">{i18n.t('DescendentOrder')}: </span>
					<input
						type="checkbox"
						value={orderDesc || false}
						onChange={(e) => onOrderChange(orderBy, e.target.checked)}
					/>
				</div>
			}
		</header>
	);
};
const sortFunction = (prop) => (desc) => (a, b) => {
	let result;
	if (a[prop] > b[prop]) result = 1;
	else if (a[prop] < b[prop]) result = -1;
	else result = 0;
	return result * (desc ? -1 : 1);
};
const interactionsOrderByList = () => [
	{ id: 1, label: i18n.t('ID'), desc: false, sort: sortFunction('id') },
	{ id: 2, label: i18n.t('Title'), desc: false, sort: sortFunction('title') },
	{ id: 3, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
	{ id: 4, label: i18n.t('Source'), desc: false, sort: sortFunction('source') },
	{ id: 5, label: i18n.t('Channel'), desc: false, sort: sortFunction('channelName') },
	{ id: 6, label: i18n.t('Salesman'), desc: false, sort: sortFunction('creatorName') },
].sort(sortFunction('label')());
const notesOrderByList = () => [
	{ id: 1, label: i18n.t('ID'), desc: false, sort: sortFunction('id') },
	{ id: 2, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
].sort(sortFunction('label')());
const ordersOrderByList = () => [
	{ id: 1, label: i18n.t('OrderUid'), desc: false, sort: sortFunction('orderUid') },
	{ id: 2, label: i18n.t('Action'), desc: false, sort: sortFunction('action') },
	{ id: 3, label: i18n.t('CreatorID'), desc: false, sort: sortFunction('creatorID') },
	{ id: 4, label: i18n.t('CreationDate'), desc: false, sort: sortFunction('creationDate') },
	{ id: 5, label: i18n.t('DueDate'), desc: false, sort: sortFunction('dueDate') },
	{ id: 6, label: i18n.t('EffectiveDate'), desc: false, sort: sortFunction('effectiveDate') },
	{ id: 7, label: i18n.t('ServiceRequiredDate'), desc: false, sort: sortFunction('serviceRequiredDate') }, // eslint-disable-line max-len
	{ id: 8, label: i18n.t('ReasonDescription'), desc: false, sort: sortFunction('reasonDescription') }, // eslint-disable-line max-len
].sort(sortFunction('label')());

export default connect(mapStateToProps)(RunningTransaction);
