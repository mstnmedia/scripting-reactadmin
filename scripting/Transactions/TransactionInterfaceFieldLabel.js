import React, { Component } from 'react';
import {
	InterfaceFieldTypes,
} from '../../store/enums';

/**
 * Componente en desuso
 *
 * @export
 * @class TransactionInterfaceFieldLabel
 * @extends {Component}
 * @deprecated
 */
export default class TransactionInterfaceFieldLabel extends Component {

	render() {
		const { name, label, id_type: type, } = this.props.field;
		if ([InterfaceFieldTypes.HIDDEN, InterfaceFieldTypes.LINK].indexOf(type) > -1) {
			return null;
		}
		return <label className="control-label" htmlFor={name}>{label}:</label>;
	}
}
