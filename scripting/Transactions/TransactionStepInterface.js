import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { mapStateToProps } from '../../outlets/';
import {
	Panel,
	Button,
	ModalFactory, Factory,
} from '../../reactadmin/components/';
import {
	ContentDetailModal,
	TransactionContentItem,
	TransactionInterfaceFieldItem,
} from './';
import i18n from '../../i18n';
import {
	ContentTypes,
	InterfaceFieldTypes,
	TransactionResultListTemplate,
} from '../../store/enums';
import * as Utils from '../../reactadmin/Utils';

/**
 * Componente que controla los formularios en los pasos tipos Sistemas externos de las 
 * transacciones en curso. Muestra los contenidos agregados al paso y los campos de la 
 * interfaz que debe llenar el usuario.
 *
 * @class TransactionStepInterface
 * @extends {Component}
 */
class TransactionStepInterface extends Component {
	state = {
		form: {},
		selectedContent: {},
	};
	componentWillMount() {
		this.updateForm(this.props);
		// window.Scripting_TransactionStepInterface = this;
	}
	componentWillReceiveProps(props) {
		this.updateForm(props);
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	onKeyDown(e, field) {
		if (e.key === 'Enter') {
			if ((!e.altKey) && (field.id_type === InterfaceFieldTypes.AUTOCOMPLETE
				|| field.id_type === InterfaceFieldTypes.HTML
				|| field.id_type === InterfaceFieldTypes.TAGS
				|| field.id_type === InterfaceFieldTypes.TEXTAREA
			)) {
				// Estos controles ya manejan el evento keydown para la tecla 'Enter'.
				// Esta condición permite enviar el formulario desde estos controles con Ctrl o Alt
			} else {
				this.sendForm();
			}
		}
	}

	getTransaction(props) {
		const base = props || this.props;
		return base.item;
	}
	getStep(props) {
		const item = this.getTransaction(props);
		return item.workflow_step;
	}
	getTransactionResults() {
		// const item = this.getTransaction();
		// return (item && item.transaction_results) || [];
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}
	getInterface(props) {
		const step = this.getStep(props);
		return step.interface;
	}
	getFormStr(props) {
		const inter = this.getInterface(props);
		return inter && inter.form;
	}
	getFields(props) {
		const inter = this.getInterface(props);
		return (inter && inter.interface_field) || [];
	}

	handleOpenModal(selectedContent) {
		this.setState({ selectedContent });
		ModalFactory.show('contentDetailModal');
	}

	updateForm(props) {
		const { form: oldForm } = this.state;
		const strForm = this.getFormStr(props);
		if (strForm) {
			const newForm = JSON.parse(strForm);
			const comparativeProp = '__generationDate';
			if (oldForm[comparativeProp] !== newForm[comparativeProp]) {
				const form = newForm;
				this.setState({ form });
			}
		}
	}

	handleChange(prop, value) {
		const form = Object.assign({}, this.state.form, { [prop]: value });
		this.setState({ form });
	}

	sendForm() {
		const fields = this.getFields();
		if (this.isValidForm(fields)) {
			const { transactions: { sendToInterface, } } = this.props;
			const { form } = this.state;
			sendToInterface(form);
		}
	}

	isDisabled() {
		return this.props.disabled
			|| this.state.disabled
			|| (this.state.form && this.state.form.__disabled) // eslint-disable-line no-underscore-dangle
			;
	}
	isValidField(pField, form, fields) {
		const field = pField;
		field.isValid = [true];
		if (field.id_type === InterfaceFieldTypes.HIDDEN) {
			return field;
		}
		let value = field.name === TransactionResultListTemplate ? form : form[field.name];
		if (field.id_type === InterfaceFieldTypes.TABLE) {
			if (field.required && (!Array.isArray(value) || !value.length)) {
				field.isValid[0] = false;
			} else {
				value = value || [];
				const children = (field.children || []);
				for (let iR = 0; iR < value.length; iR++) {
					const row = value[iR];
					field.isValid[iR + 1] = [];
					for (let iC = 0; iC < children.length; iC++) {
						const child = children[iC];
						this.isValidField(child, row, fields);
						field.isValid[iR + 1][iC] = child.isValid;
					}
				}
			}
			return field;
		}
		if (!field.required && (!value)) {
			return field;
		}
		if (!field.Validate) {
			if (typeof field.onvalidate === 'string' && !!field.onvalidate.trim()) {
				field.Validate = new Function('value', 'form', 'field', 'fields', 'InterfaceFieldTypes', 'Utils', 'screen', field.onvalidate); //eslint-disable-line max-len, no-new-func
			} else {
				field.Validate = () => {
					if (field.required) {
						return (!!value || value === 0 || value === false);
					}
					return true;
				};
			}
		}
		field.isValid[0] = field.Validate(value, form, field, fields, InterfaceFieldTypes, Utils, 'form'); //eslint-disable-line max-len
		return field;
	}
	isValidForm(fields) {
		const isInvalid = (results) => {
			const filtered = results
				.filter(value => {
					if (Array.isArray(value)) {
						return isInvalid(value);
					}
					return !value;
				});
			return filtered.length > 0;
		};
		const form = this.state.form;
		const result = fields
			.map(field => this.isValidField(field, form, fields))
			.filter(field => isInvalid(field.isValid));
		return result.length === 0;
	}

	render() {
		const fields = this.getFields();
		if (!fields.length) {
			return null;
		}
		const { form } = this.state;
		const step = this.getStep();
		const contents = (step.workflow_step_content || [])
			.map((i) => i.content);
		const results = this.getTransactionResults();
		const resultsObj = this.getTransactionResultsObj();
		const inter = this.getInterface();

		const disabled = this.isDisabled();
		const invalidForm = !this.isValidForm(fields);

		const { selectedContent, } = this.state;
		const modalTitle = selectedContent.id_type !== ContentTypes.HTML
			? selectedContent.filename || selectedContent.name || ''
			: '';
		return (
			<div>
				<Factory
					modalref="contentDetailModal"
					factory={ContentDetailModal} large
					title={modalTitle}
					content={selectedContent}
					results={results}
				/>
				<Panel>
					<h4 className="text-center">{step.name}</h4>
					{contents.map((content, i) => (
						<TransactionContentItem
							key={`${content.id}_${i}`} content={content}
							results={results}
							resultsObj={resultsObj}
							openModal={() => this.handleOpenModal(content)}
						/>
					))}
					{fields.map(field =>
						<TransactionInterfaceFieldItem
							{...{ key: field.name, field, form, step, interface: inter, disabled }}
							onChange={(prop, value) => this.handleChange(prop, value)}
							onKeyDown={(e, iField) => this.onKeyDown(e, iField)}
						/>
					)}
					<div className="text-center">
						<Button
							className="btn-danger"
							onClick={(e) => {
								e.preventDefault();
								this.sendForm();
							}}
							label={i18n.t('SendToInterface')}
							disabled={disabled || invalidForm}
						/>
					</div>
				</Panel>
			</div>
		);
	}
}

export default connect(mapStateToProps)(TransactionStepInterface);
