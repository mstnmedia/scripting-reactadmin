import React, { PureComponent } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

import { Fa } from '../../reactadmin/components/';
import ContentModal from '../Contents/ContentModal';
import i18n from '../../i18n';
import { faIconFromFileExtension } from '../../reactadmin/Utils';

/**
 * Componente que permite hacer zoom a una imagen de un contenido mostrado en una transacción en 
 * curso.
 *
 * @export
 * @class ContentDetailModal
 * @extends {PureComponent}
 */
export default class ContentDetailModal extends PureComponent {

	state = {
		html: '',
		scale: '50',
	}
	componentWillMount() {
		if (this.getContent().id_type === 1) {
			this.setStateHTML();
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getContent() {
		return this.props.content || {};
	}
	getResults() {
		return this.props.results || [];
	}
	getHTMLContent() {
		if (this.html && !this.props.notCached) {
			return this.html;
		}
		const html = this.getHTML();
		// if (!html || !this.getResults().length) {
		// 	console.log(html, this.getResults(), this.getContent());
		// }

		return html;
	}
	getHTML() {
		const content = this.getContent();
		let html = (content.value || '').trim();
		const results = this.getResults();
		if (html && results.length) {
			results.forEach(result => {
				let props;
				try {
					props = JSON.parse(result.props);
				} catch (error) {
					props = {};
				}
				const cssStyles = (props && props.cssStyles) || '';
				const spanHTML = `<span style="${cssStyles}">${result.value}</span>`;
				html = html.replace(result.name, spanHTML);
			});
			return html;
		}
		return '';
	}
	setStateHTML() {
		this.html = this.getHTML();
	}

	renderHTMLContent() {
		const html = this.getHTMLContent();
		return (
			<div dangerouslySetInnerHTML={{ __html: html }} />
		);
	}
	renderImageContent() {
		const content = this.getContent();
		const { scale } = this.state;
		const { name, oldValue: value, filename, } = content;
		const src = ContentModal.downloadURL(value);
		return (
			<div className="text-center">
				<h4 style={{ paddingRight: 20, paddingLeft: 20 }}>{name}</h4>
				<div>
					<input
						type="range"
						list="scalemarks"
						min="25" max="100"
						value={scale}
						onChange={(e) => this.setState({ scale: e.target.value })}
					/>
					<datalist id="scalemarks">
						<option value="25" label="25%" />
						<option value="50" label="50%" />
						<option value="75" label="75%" />
						<option value="100" label="100%" />
					</datalist>
				</div>
				<img
					className="img"
					style={{ maxWidth: '100%', width: `${scale}%` }}
					alt={filename}
					src={src}
				/>
			</div>
		);
	}
	renderFileContent() {
		const content = this.getContent();
		const { name, oldValue: value, filename, } = content;
		const extension = value.substring(value.lastIndexOf('.') + 1);
		const icon = faIconFromFileExtension(extension);
		const contentIcon = <Fa title={filename} icon={icon} size={50} />;
		return (
			<div className="text-center">
				{this.renderActions()}
				<h4 style={{ paddingRight: 20, paddingLeft: 20 }}>{name}</h4>
				{contentIcon}
			</div>
		);
	}
	renderActions() {
		const { oldValue: value, filename, } = this.getContent();
		return (
			<div title={filename} className="text-right" style={{ height: 0 }}>
				<Fa title={filename} icon={'download'} onClick={() => ContentModal.downloadFile(value)} />
			</div>
		);
	}
	render() {
		const content = this.getContent();
		let view = null;
		switch (content.id_type) {
			case 1:
				view = this.renderHTMLContent(content);
				break;
			case 2:
				view = this.renderImageContent(content);
				break;
			case 3:
				view = this.renderFileContent(content);
				break;
			default:
				view = null;
		}
		const { className, containerStyle, } = this.props;
		return (
			<div id={content.id} className={className} style={containerStyle}>

				{view}

				<div className="">
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}
