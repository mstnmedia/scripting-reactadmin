/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	Button,
	Fa,
	ButtonGroup,
	PanelCollapsible,
	Input,
	DropDown,
	AdvancedFilters,
	Tooltip,
} from '../../reactadmin/components/';
import { CategoryModal } from './';
import i18n from '../../i18n';
import {
	CategoryTypeNames,
	CategoryTypeOptions,
	filterAdvActions,
	CategoryTypes,
} from '../../store/enums';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { setFilterHandles, NameIDText } from '../../Utils';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('transactions', 'category').actions;

/**
 * Componente que controla la pantalla del mantenimiento de categorías.
 *
 * @class Categories
 * @extends {Component}
 */
class Categories extends Component {
	state = {
		category: {},
		disabled: false,
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'categories', true)
			.initSamePage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Categories'), },
		];
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	newItemClick(e, parent = {}) {
		e.preventDefault();
		e.stopPropagation();
		let newType;
		switch (parent.id_type) {
			case CategoryTypes.TYPE1:
				newType = CategoryTypes.TYPE2;
				break;
			case CategoryTypes.TYPE2:
				newType = CategoryTypes.TYPE3;
				break;
			case CategoryTypes.TYPE3:
				newType = CategoryTypes.ACTION;
				break;
			default:
				newType = '';
				break;
		}
		const category = {
			id_center: parent ? parent.id_center : undefined,
			center_name: parent ? parent.center_name : undefined,
			id_parent: parent ? parent.id : undefined,
			parent_name: parent ? parent.name : undefined,
			id_type: newType,
		};
		this.setState({ category });
		ModalFactory.show('categoryModal');
	}

	handleItemDelete(id) {
		const okClick = () => {
			const { dispatch, token, categories: { pageNumber, }, } = this.props;
			this.setState({ disabled: true });
			const success = () => {
				ModalFactory.hide('confirmModal');
				this.setState({ disabled: false });
				this.fetchPage(pageNumber);
			};
			const failure = () => this.setState({ disabled: false });
			dispatch(actions.delete({ token, item: { id }, success, failure, }));
		};
		const closeClick = () => {
			ModalFactory.hide('confirmModal');
		};

		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteCategory'),
				description: i18n.t('WishDeleteCategoryDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: okClick,
						className: 'btn-danger',
						disabled: () => this.state.disabled,
					},
					{
						label: i18n.t('Close'),
						onClick: closeClick,
						className: 'btn-default',
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	selectItem(item) {
		// console.log('category clicked: ', item);
		this.setState({ category: Object.assign({}, item) });
		ModalFactory.show('categoryModal');
	}

	handleItemChange(item) {
		const { dispatch, token, categories: { pageNumber } } = this.props;
		const success = () => {
			ModalFactory.hide('categoryModal');
			this.setState({ disabled: false });
			const page = item.id ? pageNumber : 1;
			this.fetchPage(page);
		};
		const failure = () => this.setState({ disabled: false });
		this.setState({ disabled: true });
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}

	tableSchema() {
		const isMaster = this.props.user.isMaster();
		const dataSchema = {
			fields: {
				addChildren: {
					label: ' ',
					format: (row) => (
						<Tooltip position="left" tag="TooltipCategoriesCreateChild">
							<Button
								type="button"
								className="btn-success btn-xs inline-block"
								style={{ marginLeft: 5, }}
								label={<Fa icon="plus" />}
								onClick={(e) => this.newItemClick(e, row)}
							/>
						</Tooltip>
					),
				},
				// deleted: {
				// 	type: 'Boolean',
				// 	label: i18n.t('Disabled'),
				// },
				name: i18n.t('Name'),
				id_type: {
					label: i18n.t('Type'),
					format: (row) => CategoryTypeNames()[row.id_type],
				},
				parent_name: i18n.t('ParentCategory'),
				center_name: isMaster ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
			}
		};
		return dataSchema;
	}

	renderFilters() {
		const { where, whereObj, advancedSearch } = this.state;
		const isMaster = this.props.user.isMaster();
		const centerID = this.filterValue('id_center');
		const parentID = this.filterValue('id_parent');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group col-md-3">
							<label htmlFor="id">{i18n.t('ID')}:</label>
							<Input
								type="number" className="form-control"
								value={this.filterValue('id', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'id',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{isMaster && <div className="form-group col-md-3">
							<label htmlFor="center">{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/centers/center`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('center_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('center_name', text);
									this.filterChange({ name: 'id_center', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('center_name', item.name);
									this.filterChange({ name: 'id_center', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_center')) {
										this.filterTextChange('center_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>}
						<div className="form-group col-md-3">
							<label htmlFor="parent">{i18n.t('ParentCategory')}:</label>
							{parentID ? <span> {parentID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/transactions/category`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('parent_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('parent_name', text);
									this.filterChange({ name: 'id_parent', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('parent_name', item.name);
									this.filterChange({ name: 'id_parent', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_parent')) {
										this.filterTextChange('parent_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="type">{i18n.t('Type')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('id_type', '')}
								items={CategoryTypeOptions()}
								onChange={(e) => this.filterChange({
									name: 'id_type',
									value: e.target.value
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								type="text" className="form-control"
								value={this.filterValue('name', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'name',
									value: e.target.value,
									criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{/* <div className="form-group col-md-3">
							<label htmlFor="deleted">{i18n.t('Disabled')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('deleted', '')}
								items={NumericBooleanOptions()}
								onChange={(e) => this.filterChange({ name: 'deleted', value: e.target.value })}
							/>
						</div> */}
					</div>
					:
					<AdvancedFilters
						{...{ where, whereObj }}
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
							center_name: { type: 'text', label: i18n.t('CenterName'), onKeyDown: this.onKeyDownSimple, },
							parent_name: { type: 'text', label: i18n.t('ParentName'), onKeyDown: this.onKeyDownSimple, },
							name: { type: 'text', label: i18n.t('Name'), onKeyDown: this.onKeyDownSimple, },
							id_type: { type: 'dropdown', label: i18n.t('ActiveAlert'), items: CategoryTypeOptions(), onKeyDown: this.onKeyDownSimple },
							id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
							id_parent: { type: 'autocomplete', label: i18n.t('ParentCategory'), url: `${window.config.backend}/categories/category`, onKeyDown: this.onKeyDownMixed, },
						}}/* eslint-enable max-len */
					/>
				}
				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { categories: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
		const { category, disabled, confirmModalProps, } = this.state;
		const rows = list || [];

		const canAdd = user.hasPermissions('categories.add');
		const canDelete = user.hasPermissions('categories.delete');
		return (
			<Page>
				<Factory
					modalref="categoryModal" title={i18n.t('CategoryDetails', category)}
					factory={CategoryModal} item={category}
					disabled={disabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={this.tableSchema()}
                            addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Categories') })}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={canDelete}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Categories);
