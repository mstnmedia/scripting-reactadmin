/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	Page,
	Col,
	Panel, PanelCollapsible,
	ButtonGroup,
	DataTable,
	Factory,
	Input, DropDown, Tooltip,
} from '../../reactadmin/components/';
import { EndCallModal } from './';
import { DialogModal } from '../DialogModal';
import i18n from '../../i18n';
import { NumericBooleanOptions, filterActions } from '../../store/enums';
import { setFilterHandles, setItemHandles, } from '../../Utils';

const actions = ReduxOutlet('transactions', 'endCall').actions;
/**
 * Componente que controla la pantalla del mantenimiento de razones de fines de llamadas.
 *
 * @class EndCalls
 * @extends {Component}
 */
class EndCalls extends Component {
	state = {
		item: {},
		itemDisabled: false,
	}
	componentWillMount() {
		setItemHandles({ pageComponent: this, actions, itemName: 'EndCall' });
		setFilterHandles(this, actions, 'endCalls', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('EndCallReasons'), },
		];
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	renderFilters() {
		return (
			<PanelCollapsible 
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>} 
				contentStyle={{ padding: 5 }}
			>
				<div>
					<div className="form-group input-group-sm col-md-3">
						<label htmlFor="id">{i18n.t('ID')}:</label>
						<Input
							type="number" className="form-control"
							value={this.filterValue('id', '')}
							onFieldChange={(e) => this.filterChange({
								name: 'id',
								value: e.target.value,
							})}
							onKeyDown={this.onKeyDownSimple}
						/>
					</div>
					<div className="form-group input-group-sm col-md-3">
						<label htmlFor="name">{i18n.t('Name')}:</label>
						<Input
							type="text" className="form-control"
							value={this.filterValue('name', '')}
							onFieldChange={(e) => this.filterChange({
								name: 'name',
								value: e.target.value,
								criteria: 'like',
							})}
							onKeyDown={this.onKeyDownSimple}
						/>
					</div>
					<div className="form-group input-group-sm col-md-3">
						<label htmlFor="deleted">{i18n.t('Disabled')}:</label>
						<DropDown
							emptyOption
							value={this.filterValue('deleted', '')}
							items={NumericBooleanOptions()}
							onChange={(e) => this.filterChange({ name: 'deleted', value: e.target.value })}
							onKeyDown={this.onKeyDownSimple}
						/>
					</div>
				</div>
				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { endCalls: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
		const { item, itemDisabled, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('endcall.add');
		const canDelete = user.hasPermissions('endcall.delete');
		return (
			<Page>
				<Factory
					modalref={this.modalNameRef} factory={EndCallModal} 
					title={i18n.t('EndCallReasonDetails', item)} 
					item={item} parent={{}} disabled={itemDisabled}
					onCreate={(newItem) => this.saveItem(newItem)}
					onUpdate={(newItem) => this.saveItem(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							schema={{
								fields: {
									deleted: {
										type: 'Boolean',
										label: i18n.t('Disabled'),
									},
									name: i18n.t('Name'),
									id: i18n.t('ID'),
								}
							}}
							{...{ pageSize, pageNumber, totalRows, rows, canDelete, }}
							onPageChange={(data) => this.pageChange(data)}
                            addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('EndCallReasons') })}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(EndCalls);
