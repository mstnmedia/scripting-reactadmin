import React, { Component } from 'react';
import {
	FormGroup,
} from '../../reactadmin/components';
import { TransactionInterfaceFieldControl } from '.';
import { InterfaceFieldTypes } from '../../store/enums';

/**
 * Componente que según el tipo de campo argumentado muestra al usuario el control del campo en
 * el formulario y/o el título del campo, si aplica.
 *
 * @export
 * @class TransactionInterfaceFieldItem
 * @extends {Component}
 */
export default class TransactionInterfaceFieldItem extends Component {

	render() {
		const {
			form,
			field,
			// interface: inter,
			// step,
			disabled,
			onChange,
			onKeyDown,
		} = this.props;
		let controlLabel = <label className="control-label" htmlFor={field.name}>{field.label}:</label>;
		if ([InterfaceFieldTypes.HIDDEN, InterfaceFieldTypes.LINK].indexOf(field.id_type) > -1) {
			controlLabel = null;
		}
		return (
			<FormGroup isValid={field.isValid[0]}>
				{controlLabel}
				<TransactionInterfaceFieldControl
					{...{ field, form, disabled, onKeyDown, onChange, }}
				/>
			</FormGroup>
		);
	}
}
