import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { mapStateToProps, ReduxOutlet } from '../../outlets/';

import {
	Col,
	Panel,
	Button,
	AnimatedSpinner,
	DataTable,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import TransactionContentItem from './TransactionContentItem';
import {
	ID_END_TRANSACTION_CONTENT,
	Interfaces,
} from '../../store/enums';
import { InterfaceResults } from './';

const actions = ReduxOutlet('contents', 'content').actions;
/**
 * Componente que muestra la información de resumen de una transacción, las variables que generó
 * y los pasos recorridos.
 *
 * @class ResumedTransaction
 * @extends {Component}
 */
class ResumedTransaction extends Component {
	state = {
		disabled: false,
		fetchingContent: false,
	}
	componentDidMount() {
		const { dispatch, token } = this.props;
		const contentCallback = () => this.changeState({ fetchingContent: false });
		this.changeState({ fetchingContent: true });
		dispatch(actions.setProp('item', {}));
		dispatch(actions.fetchOne(
			token, ID_END_TRANSACTION_CONTENT,
			contentCallback, contentCallback
		));
		// window.Scripting_ResumedTransaction = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
    componentWillUnmount() {
        this.unmounted = true;
    }

	getTransaction() {
		const { transactions, } = this.props;
		const data = transactions.runningTransaction;
		if (data && data.items) {
			return data.items[0];
		}
		return undefined;
	}
	getTransactionResults() {
		// const item = this.getTransaction();
		// return (item && item.transaction_results) || [];
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}
	getTransactionResultsObjValue(prop) {
		const result = this.getTransactionResultsObj()[prop];
		return (result && result.value) || null;
	}
	getTransactionSteps() {
		const item = this.getTransaction();
		return (item && item.transaction_steps) || [];
	}

    unmounted = false;
    changeState(state, callback) {
        if (!this.unmounted) {
            this.setState(state, callback);
        }
    }

	newDependentTransaction() {
		const item = this.getTransaction();
		if (item) {
			this.changeState({ disabled: true });
			this.props.router.push({
				pathname: '/data/transactions/create',
				state: {
					id_customer: item.id_customer,
					id_call: item.id_call,
					caller_name: this.getTransactionResultsObjValue(
						`${Interfaces.SCRIPTING.TransactionInfoName}_caller_name`
					),
				},
				query: ' ',
			});
		}
	}
	newTransaction() {
		this.changeState({ disabled: true });
		this.props.router.push('/data/transactions/create');
	}

	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isFetchingContent() {
		return this.state.fetchingContent;
	}

	formatStepName = (row) => `${row.id_workflow_step} | ${row.workflow_step_name}`

	render() {
		const { contents: { item: endTransactionContent }, user, } = this.props;
		const item = this.getTransaction();
		const steps = this.getTransactionSteps();
		const results = this.getTransactionResults();
		const resultsObj = this.getTransactionResultsObj();
		return (
			<Col>
				<Panel title={i18n.t('TransactionDetails')} containerStyle={{ width: '100%' }}>
					<div style={{ top: 9, right: 50, position: 'absolute' }}>
						{item && item.id_employee === user.id &&
							<div>
								<Button
									label={i18n.t('StartNewTransactionSameClient')}
									color="btn-danger" size="btn-sm" rounded
									onClick={(e) => this.newDependentTransaction(e)}
									disabled={this.isDisabled()}
								/>

								<Button
									label={i18n.t('StartNewTransactionOtherClient')}
									color="btn-danger" size="btn-sm" rounded
									onClick={(e) => this.newTransaction(e)}
									disabled={this.isDisabled()}
								/>
							</div>
						}
					</div>
					<AnimatedSpinner show={this.isFetchingContent()} />
					<TransactionContentItem
						notCached
						content={endTransactionContent}
						results={results}
						resultsObj={resultsObj}
						separatorStyle={{ display: 'none' }}
					/>
				</Panel>
				{item &&
					<Panel>
						<ul className="nav nav-tabs not-nav-justified">
							<li className="active">
								<a data-toggle="tab" href="#steps">
									{i18n.t('TransactionSteps')}
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#results">
									{i18n.t('TransactionResults')}
								</a>
							</li>
						</ul>
						<div className="tab-content">
							<div id="steps" className="tab-pane fade in active">
								<DataTable
									rows={steps}
									schema={{
										fields: {
											active: {
												type: 'Boolean',
												label: i18n.t('ActiveTransactionStep'),
											},
											id_parent: {
												label: i18n.t('ParentTransactionStep'),
												format: (row, rows) => {
													for (let i = 0; i < rows.length; i++) {
														const iRow = rows[i];
														if (iRow.id === row.id_parent) {
															return this.formatStepName(iRow);
														}
													}
												},
											},
											value: i18n.t('Value'),
											workflow_step_name: {
												label: i18n.t('Step'),
												format: (row) => this.formatStepName(row),
											},
											workflow_name: {
												label: i18n.t('Workflow'),
												format: (row) => `${row.id_workflow} | ${row.workflow_name}`,
											},
											docdate: {
												type: 'DateTimeZ',
												label: i18n.t('Date'),
											},
											id: i18n.t('ID'),
										}
									}}
								/>
							</div>
							<InterfaceResults clearable transactionResults={results} />
						</div>
					</Panel>
				}
			</Col>
		);
	}
}

export default connect(mapStateToProps)(ResumedTransaction);
