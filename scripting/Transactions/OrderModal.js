import React, { Component } from 'react';
import moment from 'moment';

import {
	Col, DataTable, Row,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { DateFormats } from '../../store/enums';

/**
 * Componente que muestra el detalle de una orden de OMS.
 *
 * @export
 * @class OrderModal
 * @extends {Component}
 */
export default class OrderModal extends Component {
	state = {}

	formatMoney(value) {
		if (value) {
			return `${value.currency} $${value.units}.${value.cents}`;
		}
		return null;
	}

	renderPricingRow({ key, row: pricing, cells, }) {
		const charge = pricing.charge;
		return [
			<tr key={`${key}_1`}>
				{cells}
			</tr>,
			<tr key={`${key}_2`}>
				<td />
				<td />
				{charge &&
					<td colSpan="6">
						<DataTable
							notStriped
							schema={{
								fields: {
									overrideAmount: {
										label: i18n.t('PricingOverrideAmount'),
										format: (row) => (
											this.formatMoney(
												row.charge && row.charge.override && row.charge.override.amount
											)
										),
									},
									overrideReason: {
										label: i18n.t('PricingOverrideReason'),
										format: (row) => (
											(row.charge && row.charge.override && row.charge.override.reasonDesc)
											|| undefined
										),
									},
								}
							}}
							rows={[pricing]}
						/>
					</td>
				}
			</tr>
		];
	}
	render() {
		const item = this.props.item || {};
		const phases = item.phases || [];
		const activities = item.activities || [];
		const pricing = item.pricing || [];
		const headerStyle = {
			margin: '20px 0px -10px',
		};
		return (
			<div>
				<div>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="id">{i18n.t('ID')}:</label><br />
							<span>{item.orderUid}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="displayID">{i18n.t('DisplayID')}:</label><br />
							<span>{item.orderDisplayID}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="id">{i18n.t('ParentUid')}:</label><br />
							<span>{item.parentUID}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="action">{i18n.t('Action')}:</label><br />
							<span>{item.action}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="id">{i18n.t('ActionUid')}:</label><br />
							<span>{item.actionUid}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="displayID">{i18n.t('ActionDisplayID')}:</label><br />
							<span>{item.actionDisplayID}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="creatorID">{i18n.t('CreatorID')}:</label><br />
							<span>{item.creatorID}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="creationDate">{i18n.t('CreationDate')}:</label><br />
							<span>
								{((item.creationDate && moment(item.creationDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="dueDate">{i18n.t('DueDate')}:</label><br />
							<span>
								{((item.dueDate && moment(item.dueDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="effectiveDate">{i18n.t('EffectiveDate')}:</label><br />
							<span>
								{((item.effectiveDate &&
									moment(item.effectiveDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="serviceRequiredDate">{i18n.t('ServiceRequiredDate')}:</label><br />
							<span>
								{((item.serviceRequiredDate &&
									moment(item.serviceRequiredDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="status">{i18n.t('Status')}:</label><br />
							<span>{item.statusLabel}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="channelCode">{i18n.t('ChannelCode')}:</label><br />
							<span>{item.channelCode}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="channelName">{i18n.t('ChannelName')}:</label><br />
							<span>{item.channelName}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="reasonCode">{i18n.t('ReasonCode')}:</label><br />
							<span>{item.reasonCode}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="reasonDescription">{i18n.t('ReasonDescription')}:</label><br />
							<span>{item.reasonDescription}</span><br />
						</Col>
					</Row>
				</div>
				<h4 style={headerStyle}>{i18n.t('Phases')}:</h4>
				<DataTable
					schema={{
						fields: {
							endDate: {
								type: 'DateTimeZ',
								label: i18n.t('EndDate'),
							},
							startDate: {
								type: 'DateTimeZ',
								label: i18n.t('StartDate')
							},
							nameLabel: i18n.t('Name'),
						}
					}}
					rows={phases}
				/>
				<h4 style={headerStyle}>{i18n.t('Activities')}:</h4>
				<DataTable
					schema={{
						fields: {
							statusLabel: i18n.t('Status'),
							employeeId: i18n.t('EmployeeID'),
							nameLabel: i18n.t('Name'),
							executionDate: {
								type: 'DateTimeZ',
								label: i18n.t('ExecutionDate'),
							},
							creationDate: {
								type: 'DateTimeZ',
								label: i18n.t('CreationDate'),
							},
						}
					}}
					rows={activities}
				/>
				<h4 style={headerStyle}>{i18n.t('OrderPricing')}:</h4>
				<DataTable
					notStriped tableClassName="table-striped-each-2"
					schema={{
						fields: {
							actualAmount: {
								label: i18n.t('ActualAmount'),
								format: (row) => this.formatMoney(row.charge && row.charge.actualAmount),
							},
							originalAmount: {
								label: i18n.t('OriginalAmount'),
								format: (row) => this.formatMoney(row.charge && row.charge.originalAmount),
							},
							// overrideAmount: {
							// 	label: i18n.t('PricingOverrideReason'),
							// 	format: (row) => (
							// 		this.formatMoney(
							//			row.charge && row.charge.override && row.charge.override.amount
							//		)
							// 	),
							// },
							// overrideReason: {
							// 	label: i18n.t('PricingOverrideReason'),
							// 	format: (row) => (
							// 		(row.charge && row.charge.override && row.charge.override.reasonDesc)
							// 		|| undefined
							// 	),
							// },
							frequency: {
								label: i18n.t('Frequency'),
								format: (row) => {
									const frequency = row.charge && row.charge.frequency;
									if (frequency) {
										return `${frequency.count} ${frequency.units}`;
									}
								},
							},
							endDate: {
								type: 'DateTimeZ',
								label: i18n.t('EndDate'),
								format: (row) => (row.charge ? row.charge.endDate : undefined),
							},
							startDate: {
								type: 'DateTimeZ',
								label: i18n.t('StartDate'),
								format: (row) => (row.charge ? row.charge.startDate : undefined),
							},
							description: {
								label: i18n.t('Description'),
								format: (row) => (row.charge ? row.charge.description : undefined),
							},
							pricePlanName: i18n.t('OrderPricePlanName'),
							componentName: i18n.t('OrderComponentName'),
						}
					}}
					renderRow={(props) => this.renderPricingRow(props)}
					rows={pricing}
				/>
				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}
