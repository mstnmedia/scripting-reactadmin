import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	DropDown,
	TextArea,
	FormGroup,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

const endCallActions = ReduxOutlet('transactions', 'endCall').actions;

/**
 * Componente que controla el formulario para cerrar abruptamente o cancelar una transacción.
 *
 * @class TransactionEndModal
 * @extends {Component}
 */
class TransactionEndModal extends Component {
	state = {
		causes: [],
		cause: '',
		note: '',
		disabled: false,
	}
	componentWillMount() {
		this.resetState();
		this.loadCauses();

		// window.Scripting_TransactionEndModal = this;
	}

	getNote() {
		return this.state.note || this.props.note || '';
	}

	resetState() {
		this.setState({
			cause: '',
			note: '',
			disabled: false,
		});
	}
	loadCauses() {
		const { dispatch, token, } = this.props;
		const filters = [
			{ name: 'deleted', value: 0 },
		];
		dispatch(endCallActions.fetch(token, filters));

		// if (this.state.causes.length) return;
		// let causes = [];
		// Object.keys(TransactionEndCauseNames).forEach(key => {
		// 	causes.push({ id: key, label: TransactionEndCauseNames[key] });
		// });
		// causes = causes.sort((a, b) => (a.label > b.label ? 1 : -1));
		// this.setState({ causes });
	}


	handleChange(property, value) {
		this.setState({ [property]: value });
	}
	handleCreate() {
		const { cause } = this.state;
		const note = this.getNote();
		if (this.isValid()) {
			this.props.onCreate({ cause, note, });
		}
	}

	isDisabled() {
		const disabled = this.props.disabled || this.state.disabled;
		return isFunction(disabled) ? disabled() : disabled;
	}
	isValid() {
		const { cause } = this.state;
		const note = this.getNote();
		return cause && note;
	}

	render() {
		const { cause, } = this.state;
		const { endCalls: { list } } = this.props;
		const causes = list || [];
		const note = this.getNote();
		return (
			<div className="panel-body m-b-none">
				<div role="form">
					<FormGroup isValid={!!cause}>
						<label htmlFor="version" className="control-label">{i18n.t('Reason')}:</label>
						<DropDown
							items={causes} value={cause}
							emptyOption={<option disabled={!!cause}>{i18n.t('Select')}</option>}
							onChange={(e) => this.handleChange('cause', e.target.value * 1)}
							disabled={this.isDisabled()} required
						/>
					</FormGroup>
					<FormGroup isValid={!!note}>
						<label htmlFor="notes" className="control-label">{i18n.t('Notes')}:</label>
						<TextArea
							className="form-control"
							style={{ resize: 'vertical', minHeight: 100, }}
							value={note} rows="10"
							placeholder={i18n.t('NotesPlaceholder')}
							onFieldChange={(e) => this.handleChange('note', e.target.value)}
							disabled={this.isDisabled()} required
						/>
					</FormGroup>
				</div>

				<div className="">
					<hr />
					<button
						className="btn btn-danger" type="button"
						onClick={(e) => this.handleCreate(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>{i18n.t('End')}</button>
					<button
						className="btn -btn-danger" type="button"
						onClick={() => this.resetState()}
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(TransactionEndModal);
