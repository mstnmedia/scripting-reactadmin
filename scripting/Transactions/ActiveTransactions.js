import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col, Page,
	DataTable,
	DateDiff,
	ButtonGroup,
	Input, NumericInput,
	PanelCollapsible,
	ModalFactory, Factory,
	Button, Pager, Tooltip,
} from '../../reactadmin/components/';
import { TransactionStateNames, filterActions, TransactionStates } from '../../store/enums';
import TransactionEndModal from './TransactionEndModal';
import { DialogModal } from '../DialogModal';
import i18n from '../../i18n';
import { setFilterHandles, NameIDText } from '../../Utils';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { respErrorDetails } from '../../reactadmin/Utils';

const actions = ReduxOutlet('vTransactions', 'v_transaction').actions;
/**
 * Componente que muestra al usuario la lista de transacciones activas.
 *
 * @class ActiveTransactions
 * @extends {Component}
 */
class ActiveTransactions extends Component {

	state = {
		endModalProps: {
			note: '',
			disabled: false,
			onCreate: (data) => this.endCall(data)
		},
		confirmModalProps: {},
	}

	componentWillMount() {
		setFilterHandles(this, actions, 'vTransactions', true)
			// .initFirstPage()
			;
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Transactions'), to: '/data/transactions' },
			{ label: i18n.t('ActiveTransactions'), },
		];
		const { dispatch, } = this.props;
		dispatch(actions.setProp('list', []));
		dispatch(actions.setProp('pageNumber', 1));
		dispatch(actions.setProp('totalRows', 0));
		this.resetInterval(1);
		// window.Scripting_ActiveTransactions = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	componentWillUnmount() {
		this.autoReload = false;
	}

	handleItemCancel(row, e) {
		e.stopPropagation();
		const id = row.id;
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishEndCall'),
				description: i18n.t('WishEndCallDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: () => {
							this.setState({
								endModalProps: {
									note: row.note,
									disabled: () => this.state[`canceling_${id}`],
									onCreate: (data) => {
										ModalFactory.hide('confirmModal');
										this.endCall({ ...data, id, });
									},
								},
							});
							ModalFactory.show('transactionEndModal');
						},
						className: 'btn-danger',
						disabled: () => this.state[`canceling_${id}`],
					},
					{
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
						className: 'btn-default',
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	endCall({ id, cause, note }) {
		const { dispatch, token, } = this.props;
		const path = `canceling_${id}`;
		this.setState({ [path]: true });
		dispatch(actions.postToURL({
			token,
			route: `transaction/${id}/endCall`,
			item: { id, value: JSON.stringify({ id, cause, note }) },
			path: 'runningTransaction',
			success: () => {
				this.setState({ [path]: false });
				window.message.success(i18n.t('RunningTransactionCancelingSuccess', { id }));
				ModalFactory.hide('transactionEndModal');
				this.fetchData(this.props.vTransactions.pageNumber);
			},
			failure: (resp) => {
				this.setState({ [path]: false });
				window.message.error(i18n.t('ErrorCancelingRunningTransaction', { id }), 0,
					respErrorDetails(resp));
			},
		}));
	}

	autoReload = true;
	interval = null;

	resetInterval(pageNumber) {
		clearInterval(this.interval);
		this.fetchPage(pageNumber);
		this.interval = setTimeout(() => {
			if (this.autoReload) {
				this.resetInterval(pageNumber);
			}
		}, 5000);
	}

	searchClick() {
		const callback = () => this.resetInterval(1);
		if (this.state.advancedSearch) {
			this.saveWhereObj(callback);
		} else {
			this.saveWhere(callback);
		}
	}
	filterClear() {
		const { dispatch } = this.props;
		const where = [];
		const whereObj = {};
		const whereText = {};
		this.setState({ where, whereObj, whereText });
		dispatch(actions.setProp('where', where));
		dispatch(actions.setProp('whereObj', whereObj));
		dispatch(actions.setProp('whereText', whereText));
		dispatch(actions.setProp('totalRows', 0));
		dispatch(actions.setProp('pageNumber', 1));
		dispatch(actions.updateState('list', [])).then(() => this.resetInterval(1));
	}
	pageChange({ newPage }) {
		this.resetInterval(newPage);
	}
	fetchPage(pageNumber) {
		const { dispatch, token, vTransactions: { pageSize, }, } = this.props;
		const { where: filters, } = this.state;
		const where = (filters || []).map(i => i);
		where.push({ name: 'state', value: 1 });
		const orderBy = 'id';

		const success = (resp) => {
			// console.log(resp);
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const { items, totalRows } = data;
				if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
					const maxPage = Pager.getMaxPage(totalRows, pageSize);
					this.fetchPage(maxPage);
				}
			}
		};
		// dispatch(actions.setProp('list', []));
		// dispatch(actions.setProp('pageNumber', pageNumber));
		// dispatch(actions.setProp('totalRows', 0));
		dispatch(actions.fetchPage({ token, where, pageNumber, pageSize, success, orderBy, }));
	}

	tableSchema() {
		return {
			fields: {
				cancel: {
					label: ' ',
					format: (row) => (row.state === TransactionStates.RUNNING &&
						<Tooltip system position="left" tag="TooltipCancelTransaction">
							<Button
								className="btn-danger btn-xs inline-block"
								icon="fa-close"
								onClick={(e) => this.handleItemCancel(row, e)}
								disabled={this.state[`canceling_${row.id}`]}
							/>
						</Tooltip>
					),
				},
				state: {
					label: i18n.t('State'),
					format: (row) => TransactionStateNames()[row.state],
				},
				category: i18n.t('Category'),
				workflow_step_entry_date: {
					label: i18n.t('TimeInStep'),
					cellStyle: { textAlign: 'center' },
					format: (row) => DateDiff.calc({
						from: row.workflow_step_entry_date,
						to: new Date(),
					}),
				},
				start_date: {
					type: 'DateTimeZ',
					label: i18n.t('StartDate'),
				},
				subscriber_no: i18n.t('SubscriberNo'),
				workflow_step_name: i18n.t('Step'),
				workflow_name: i18n.t('Workflow'),
				employee_name: this.props.user.master
					|| (this.props.user.role || '').toLowerCase() !== 'representante'
					? ({
						label: i18n.t('Salesman'),
						format: (row) => `${row.employee_name || ''} (${row.id_employee})`,
					})
					: undefined,
				center_name: this.props.user.master ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
			}
		};
	}
	renderFilters() {
		const { user: { master, role, } } = this.props;
		const centerID = this.filterValue('id_center');
		const employeeID = this.filterValue('id_employee');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				<div className="form-group col-md-3">
					<label htmlFor="from">{i18n.t('FromDate')}:</label>
					<Input
						type="date" className="form-control"
						value={this.filterValue('from', '')}
						onFieldChange={(e) => this.filterChange({
							label: 'from',
							name: 'start_date',
							value: e.target.value,
							criteria: '>=',
							type: 'date',
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group col-md-3">
					<label htmlFor="to">{i18n.t('ToDate')}:</label>
					<Input
						type="date" className="form-control"
						value={this.filterValue('to', '')}
						onFieldChange={(e) => this.filterChange({
							label: 'to', name: 'start_date', value: e.target.value, criteria: '<=', type: 'date',
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				{master && <div className="form-group col-md-3">
					<label htmlFor="center">{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<RemoteAutocomplete
						url={`${window.config.backend}/centers/center`}
						getItemValue={(item) => NameIDText(item)}
						value={this.filterText('center_name') || ''}
						onFieldChange={(e, text) => {
							this.filterTextChange('center_name', text);
							this.filterChange({ name: 'id_center', value: undefined });
						}}
						onSelect={(text, item) => {
							this.filterChange({ name: 'id_center', value: item.id });
							this.filterTextChange('center_name', item.name);
						}}
						onBlur={() => {
							if (!this.filterValue('id_center')) {
								this.filterTextChange('center_name', '');
							}
						}}
						onKeyDown={this.onKeyDownMixed}
					/>
				</div>}
				{(master || (role || '').toLowerCase() !== 'representante') &&
					<div className="form-group col-md-3">
						<label htmlFor="name">{i18n.t('Salesman')}:</label>
						{employeeID ? <span> {employeeID}</span> : null}
						<RemoteAutocomplete
							url={`${window.config.backend}/employees/employee`}
							getItemValue={(item) => NameIDText(item)}
							value={this.filterText('employee_name') || ''}
							onFieldChange={(e, text) => {
								this.filterTextChange('employee_name', text);
								this.filterChange({ name: 'id_employee', value: undefined });
							}}
							onSelect={(text, item) => {
								this.filterChange({ name: 'id_employee', value: item.id });
								this.filterTextChange('employee_name', item.name);
							}}
							onBlur={() => {
								if (!this.filterValue('id_employee')) {
									this.filterTextChange('employee_name', '');
								}
							}}
							onKeyDown={this.onKeyDownMixed}
						/>
					</div>
				}
				<div className="form-group col-md-3">
					<label htmlFor="subscriberNo">{i18n.t('SubscriberNo')}:</label>
					<NumericInput
						type="number" className="form-control"
						value={this.filterValue('subscriberNo', '')}
						onFieldChange={(e) => this.filterChange({
							label: 'subscriberNo',
							name: 'subscriber_no',
							criteria: 'like',
							value: e.target.value,
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5, }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { vTransactions: { list, pageNumber, pageSize, totalRows, }, } = this.props;
		const { endModalProps, confirmModalProps, } = this.state;
		const rows = list || [];
		return (
			<Page>
				<Factory
					modalref="transactionEndModal"
					title={i18n.t('WhyEndCall')}
					factory={TransactionEndModal}
					{...endModalProps}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							schema={this.tableSchema()} condensed
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							link={(row) => this.props.router.push(`/data/transactions/${row.id}`)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(ActiveTransactions);
