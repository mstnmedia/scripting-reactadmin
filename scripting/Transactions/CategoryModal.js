import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Input, DropDown, FormGroup, Tooltip,
} from '../../reactadmin/components/';
import RemoteAutocomplete from '../RemoteAutocomplete';
import i18n from '../../i18n';
import { CategoryTypeOptions } from '../../store/enums';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de una categoría.
 *
 * @class CategoryModal
 * @extends {Component}
 */
class CategoryModal extends Component {
	state = {
		category: {},
		categoryDisabled: false,
	}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const { item, } = this.props;
		if (item) {
			item[prop] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('categories.add') || user.hasPermissions('categories.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const isMaster = this.props.user.isMaster();
		const item = this.getItem();
		return item.id_type && item.name && (!isMaster || item.id_center);
	}

	render() {
		const { id,
			id_center: centerID,
			center_name: centerName,
			id_parent: parentID,
			parent_name: parentName,
			id_type: type, name,
		} = this.getItem();
		const isMaster = this.props.user.isMaster();
		return (
			<div>
				{id && <FormGroup isValid={!!id}>
					<label className="control-label" htmlFor="name">{i18n.t('ID')}</label>
					<Input type="number" className="form-control" value={id} disabled />
				</FormGroup>}
				{isMaster && <FormGroup isValid={!!centerID}>
					<label className="control-label inline-block" htmlFor="center">{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							value={centerName || ''}
							getItemValue={(item) => NameIDText(item)}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={id || this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>}
				<FormGroup>
					<label className="control-label inline-block" htmlFor="parent">
						{i18n.t('ParentCategory')}:
					</label>
					{parentID ? <span> {parentID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCategoryParentField">
						<RemoteAutocomplete
							url={`${window.config.backend}/transactions/category`}
							value={parentName || ''}
							getItemValue={(item) => NameIDText(item)}
							onFieldChange={(e, text) => {
								this.handleChange('id_parent', 0);
								this.handleChange('parent_name', text);
							}}
							preWhere={[// TODO: Ignorar Where que no son grupos ni tienen valor
								...(id ? [{ name: 'id', criteria: '!=', value: id }] : []),
								...(centerID ? [{ name: 'id_center', value: centerID }] : []),
							]}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id_center);
								this.handleChange('center_name', item.center_name);
								this.handleChange('id_parent', item.id);
								this.handleChange('parent_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_parent) {
									this.handleChange('parent_name', '');
								}
							}}
							disabled={this.isFieldDisabled()} required
						/></Tooltip>
				</FormGroup>
				<FormGroup isValid={!!type}>
					<label className="control-label" htmlFor="type">{i18n.t('Type')}:</label>
					<Tooltip className="balloon" tag="TooltipCategoryTypeField">
						<DropDown
							emptyOption="SelectDisabled"
							items={CategoryTypeOptions()}
							value={type || ''}
							onChange={(e) => this.handleChange('id_type', e.target.value)}
							disabled={this.isFieldDisabled()}
						/></Tooltip>
				</FormGroup>
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						placeholder={i18n.t('Name')} value={name || ''}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>
				{/* <FormGroup>
					<label htmlFor="deleted">{i18n.t('Disabled')}:</label>
					<Input
						type="checkbox"
						className="inline" checked={deleted}
						containerStyle={{ margin: 5, }}
						onFieldChange={(e) => this.handleChange('deleted', e.target.checked)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup> */}

				<div className="">
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(CategoryModal);
