import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import {
	Panel, Factory, ModalFactory, Button
} from '../../reactadmin/components/';

import { mapStateToProps } from '../../outlets/';
import {
	ContentDetailModal,
	TransactionContentItem,
} from './';
import i18n from '../../i18n';
import { ContentTypes } from '../../store/enums';

class TransactionStepRegular extends Component {
	state = {
		disabled: false,
		selectedContent: {},
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getTransaction() {
		const { transactions, } = this.props;
		const data = transactions.runningTransaction;
		if (data && data.items) {
			return data.items[0];
		}
		return undefined;
	}
	getTransactionResults() {
		// const item = this.getTransaction();
		// return (item && item.transaction_results) || [];
		return this.props.transactions.transactionResults || [];
	}
	getTransactionResultsObj() {
		return this.props.transactions.transactionResultsObj || {};
	}

	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}

	handleClick(e) {
		e.preventDefault();
		if (this.props.onClick) {
			this.props.onClick(this.props.option, e);
		}
	}
	handleOpenModal(selectedContent) {
		this.setState({ selectedContent });
		ModalFactory.show('contentDetailModal');
	}
	optionClick(selectedOption) {
		const { transactions: { sendValue, } } = this.props;
		sendValue(selectedOption.value);
	}
	render() {
		const { item, } = this.props;

		const contents = item.workflow_step.workflow_step_content.map((i) => i.content);
		const options = item.workflow_step.workflow_step_option;
		const results = this.getTransactionResults();
		const resultsObj = this.getTransactionResultsObj();

		const { selectedContent, } = this.state;
		const modalTitle = selectedContent.id_type !== ContentTypes.HTML
			? selectedContent.filename || selectedContent.name || ''
			: '';
		return (
			<div>
				<Factory
					modalref="contentDetailModal" large
					factory={ContentDetailModal}
					title={modalTitle}
					content={selectedContent}
					results={results}
				/>
				<Panel>
					<h4 className="text-center">{item.workflow_step.name}</h4>
					{contents.map((content, i) => (
						<TransactionContentItem
							key={`${content.id}_${i}`} content={content}
							results={results}
							resultsObj={resultsObj}
							openModal={() => this.handleOpenModal(content)}
						/>
					))}
				</Panel>
				<Panel title={i18n.t('Options')}>
					{options.map(option => (
						// <TransactionOptionItem
						// 	key={option.id} option={option}
						// 	onClick={() => this.optionClick(option)}
						// 	disabled={this.isDisabled()}
						// />
						<Button
							key={option.id}
							label={option.name}
							style={{
								margin: 5,
								backgroundColor: option.color,
							}}
							onClick={(e) => {
								e.preventDefault();
								this.optionClick(option);
							}}
							disabled={this.isDisabled()}
						/>
					))}
				</Panel>
			</div>
		);
	}
}
export default connect(mapStateToProps)(TransactionStepRegular);
