import React, { Component } from 'react';
import {
	InterfaceFieldTypes, TransactionResultListTemplate,
} from '../../store/enums';
import { sortFunction } from '../../reactadmin/Utils';
import { TransactionInterfaceFieldControl } from '.';
import { FormGroup, Button, Fa } from '../../reactadmin/components';
import i18n from '../../i18n';

/**
 * Componente que controla la entrada del usuario para los campos tipo LISTA.
 *
 * @export
 * @class TransactionInterfaceFieldTable
 * @extends {Component}
 */
export default class TransactionInterfaceFieldTable extends Component {

	componentWillReceiveProps(props) {
		const field = props.field;
		const value = props.form[field.name] || field.default_value;
		if (props.form !== this.props.form || props.form[field.name] !== this.props.form[field.name]) {
			if (!Array.isArray(value)) {
				const rows = value ? JSON.parse(value) : [];
				props.onChange(rows);
			}
		}
	}

	onRowChange(iR, pRow, name, value) {
		const rows = this.getRows();
		if (name === TransactionResultListTemplate) {
			rows[iR] = value;
		} else {
			const row = pRow;
			row[name] = value;
		}
		this.props.onChange(rows);
	}

	getChildren() {
		return (this.props.field.children || [])
			.filter(child => child.id_type !== InterfaceFieldTypes.HIDDEN)
			.sort(sortFunction('order_index'));
	}
	getRows() {
		const { form, field } = this.props;
		const rows = form[field.name];
		if (Array.isArray(rows)) {
			return rows;
		}
		return rows ? JSON.parse(rows) : [];
	}

	render() {
		const {
			field,
			disabled,
			onChange,
			onKeyDown,
		} = this.props;

		const children = this.getChildren();
		const rows = this.getRows();
		return (
			<div className="table-responsive m-b-md">
				<table className="table table-condensed" id={field.name}>
					<thead>
						<tr>
							{children.map((child) => (
								<th key={child.name} className="text-center">
									{child.label === TransactionResultListTemplate
										? i18n.t('TableValues')
										: child.label
									}
								</th>
							))}
							<th />
						</tr>
					</thead>
					<tbody>
						{rows.map((iRow, iR) => (
							<tr key={iR}>
								{children.map((child, iC) => {
									const rowValid = (field.isValid && field.isValid[iR + 1]) || false;
									const isValid = rowValid && rowValid[iC][0];
									return (
										<td key={child.name}>
											<FormGroup className=" " isValid={isValid}>
												<TransactionInterfaceFieldControl
													{...{ field: child, form: iRow, onKeyDown, disabled, }}
													onChange={(name, value) => this.onRowChange(iR, iRow, name, value)}
												/>
											</FormGroup>
										</td>
									);
								})}
								{!disabled && <td style={{ width: 25, }}>
									<Button
										className="btn-danger btn-xs inline-block"
										// style={{ marginLeft: 5, }}
										label={<Fa icon="remove" />}
										onClick={() => {
											rows.splice(iR, 1);
											onChange(rows);
										}}
										disabled={disabled}
									/>
								</td>}
							</tr>
						))}
					</tbody>
					{!disabled && <tbody>
						<tr>
							<td className="text-center" colSpan={children.length + 1}>
								<Button
									className="btn btn-info m-t-sm"
									label={i18n.t('Add')}
									onClick={() => {
										rows.push(children.length === 1 ? '' : {});
										onChange(rows);
									}}
									disabled={disabled}
								/>
							</td>
						</tr>
					</tbody>}
				</table>
			</div>
		);
	}
}
