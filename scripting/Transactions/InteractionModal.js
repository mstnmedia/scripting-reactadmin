import React, { Component } from 'react';
import moment from 'moment';

import {
	Col, DataTable, Row,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { DateFormats } from '../../store/enums';

/**
 * Componente que muestra el detalle de una interacción de CRM.
 *
 * @export
 * @class InteractionModal
 * @extends {Component}
 */
export default class InteractionModal extends Component {
	state = {}

	render() {
		const item = this.props.item || {};
		const topics = item.topics || [];
		return (
			<div>
				<div>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="id">{i18n.t('ID')}:</label><br />
							<span>{item.id}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="title">{i18n.t('Title')}:</label><br />
							<span>{item.title}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="customerID">{i18n.t('CustomerID')}:</label><br />
							<span>{item.customerID}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="contactID">{i18n.t('ContactID')}:</label><br />
							<span>{item.contactID}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="creatorID">{i18n.t('CreatorID')}:</label><br />
							<span>{item.creatorID}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="creatorName">{i18n.t('CreatorName')}:</label><br />
							<span>{item.creatorName}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="creationDate">{i18n.t('CreationDate')}:</label><br />
							<span>
								{((item.creationDate &&
									moment(item.creationDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="medium">{i18n.t('Medium')}:</label><br />
							<span>{item.medium}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="interactionDirection">{i18n.t('InteractionDirection')}:</label><br />
							<span>{item.interactionDirection}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="interactionType">{i18n.t('InteractionType')}:</label><br />
							<span>{item.interactionType}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="status">{i18n.t('Status')}:</label><br />
							<span>{item.status}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 6 }}>
							<label htmlFor="source">{i18n.t('Source')}:</label><br />
							<span>{(item.source || '').replace('AMDOCS_', '')}</span><br />
						</Col>
						<Col size={{ sm: 6 }}>
							<label htmlFor="phone">{i18n.t('Telephone')}:</label><br />
							<span>{item.phone}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col>
							<h3>{i18n.t('Topics')}:</h3>
							<DataTable
								schema={{
									fields: {
										outcome: i18n.t('Outcome'),
										subject: i18n.t('Subject'),
										classification: i18n.t('Classification'),
									}
								}}
								rows={topics}
							/>
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col>
							<label htmlFor="note">{i18n.t('Note')}:</label><br />
							<span style={{ whiteSpace: 'pre-wrap', }}>{item.notes}</span><br />
						</Col>
					</Row>
				</div>

				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}
