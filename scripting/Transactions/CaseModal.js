import React, { Component } from 'react';
import moment from 'moment';

import {
	Col, DataTable, Row, AnimatedSpinner,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { DateFormats } from '../../store/enums';

/**
 * Componente que muestra el detalle de un caso de CRM.
 *
 * @export
 * @class CaseModal
 * @extends {Component}
 */
export default class CaseModal extends Component {
	state = {}

	renderSpinner() {
		return <AnimatedSpinner key="spinner" show={this.props.fetchingCaseHistories} />;
	}
	renderHistoryRow({ key, row, cells, }) {
		const infoAdicional = (row.infoAdicional || '').trim();
		const llamadaEntrante = (row.llamadaEntrante || '').trim();
		const notaAdministrativa = (row.notaAdministrativa || '').trim();
		const reason = (row.reason || '').trim();
		const notaCierre = (row.notaCierre || '').trim();
		return [
			<tr key={`${key}_1`}>
				{cells}
			</tr>,
			<tr key={`${key}_2`}>
				{(!!infoAdicional || !!llamadaEntrante ||
					!!notaAdministrativa || !!reason || !!notaCierre) &&
					<td colSpan={cells.length}>
						{infoAdicional && <p style={{ whiteSpace: 'pre-wrap' }} >
							<span style={{ fontWeight: 'bold', }}>{i18n.t('CaseInfoAdicional')}</span>:
							<br />
							{infoAdicional}
						</p>}
						{llamadaEntrante && <p style={{ whiteSpace: 'pre-wrap' }} >
							<span style={{ fontWeight: 'bold', }}>{i18n.t('CaseLlamadaEntrante')}</span>:
							<br />
							{llamadaEntrante}
						</p>}
						{notaAdministrativa && <p style={{ whiteSpace: 'pre-wrap' }} >
							<span style={{ fontWeight: 'bold', }}>{i18n.t('CaseNotaAdministrativa')}</span>:
							<br />
							{notaAdministrativa}
						</p>}
						{reason && <p style={{ whiteSpace: 'pre-wrap' }} >
							<span style={{ fontWeight: 'bold', }}>{i18n.t('CaseRazon')}</span>:
							<br />
							{reason}
						</p>}
						{notaCierre && <p style={{ whiteSpace: 'pre-wrap' }} >
							<span style={{ fontWeight: 'bold', }}>{i18n.t('CaseNotaCierre')}</span>:
							<br />
							{notaCierre}
						</p>}
					</td>
				}
			</tr>
		];
	}
	render() {
		const caseItem = this.props.item || {};
		const fetchingCaseHistories = !!this.props.fetchingCaseHistories;
		const histories = caseItem.histories || [];
		return (
			<div>
				<div>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="id">{i18n.t('ID')}:</label><br />
							<span>{caseItem.id}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="contactName">{i18n.t('ContactName')}:</label><br />
							<span>{caseItem.contactName}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="subscriberNo">{i18n.t('SubscriberNo')}:</label><br />
							<span>{caseItem.subscriberNo}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="customerID">{i18n.t('CustomerID')}:</label><br />
							<span>{caseItem.customerID}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="contactPhone">{i18n.t('ContactPhone')}:</label><br />
							<span>{caseItem.contactPhone}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="contactInfo">{i18n.t('ContactInfo')}:</label><br />
							<span>{caseItem.contactInfo}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="contactMode">{i18n.t('ContactMode')}:</label><br />
							<span>{caseItem.contactMode}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="channelName">{i18n.t('ChannelName')}:</label><br />
							<span>{caseItem.channelName}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ sm: 12 }}>
							<label htmlFor="creatorName">{i18n.t('Salesman')}:</label><br />
							<span>
								{caseItem.creatorName ? `${caseItem.creatorName} (${caseItem.creatorID})` : ''}
							</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="creationDate">{i18n.t('CreationDate')}:</label><br />
							<span>
								{((caseItem.creationDate &&
									moment(caseItem.creationDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="dueDate">{i18n.t('DueDate')}:</label><br />
							<span>
								{((caseItem.dueDate &&
									moment(caseItem.dueDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							{this.renderSpinner()}
							<label htmlFor="parentNo">{i18n.t('CaseParentNo')}:</label><br />
							<span>{caseItem.caseDetails
								? `${caseItem.caseDetails.parentCaseId || i18n.t('CaseNotHasParent')}`
								: ''}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							{this.renderSpinner()}
							<label htmlFor="dueDate">{i18n.t('CaseIsParent')}:</label><br />
							<span>{caseItem.caseDetails &&
								i18n.t(`${!!caseItem.caseDetails.parentChild}`)}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="includesMaintenanceInContract">
								{i18n.t('IncludesMaintenanceInContract')}:
							</label><br />
							<span>{i18n.t(`${caseItem.includesMaintenanceInContract === 'TRUE'}`)}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="source">{i18n.t('Source')}:</label><br />
							<span>{(caseItem.source || '').replace('AMDOCS_', '')}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="topicClassification">
								{i18n.t('TopicClassification')}:
							</label><br />
							<span>{caseItem.topicClassification}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="topicSubject">{i18n.t('TopicSubject')}:</label><br />
							<span>{caseItem.topicSubject}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="topicOutcome">{i18n.t('TopicOutcome')}:</label><br />
							<span>{caseItem.topicOutcome}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="status">{i18n.t('Status')}:</label><br />
							<span>{caseItem.status}</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							{this.renderSpinner()}
							<label htmlFor="status">{i18n.t('CaseCondition')}:</label><br />
							<span>{caseItem.caseDetails && caseItem.caseDetails.condition}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							{this.renderSpinner()}
							<label htmlFor="status">{i18n.t('CaseQueue')}:</label><br />
							<span>{caseItem.caseDetails && caseItem.caseDetails.queue}</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ xs: 6 }}>
							<label htmlFor="closeDate">{i18n.t('CloseDate')}:</label><br />
							<span>
								{((caseItem.closeDate &&
									moment(caseItem.closeDate).format(DateFormats.DATE_TIME_Z)) || '')}
							</span><br />
						</Col>
						<Col size={{ xs: 6 }}>
							<label htmlFor="closerName">{i18n.t('CaseCloseSalesman')}:</label><br />
							<span>
								{caseItem.closerName ? `${caseItem.closerName} (${caseItem.closerID})` : ''}
							</span><br />
						</Col>
					</Row>
					<Row classes="m-b-sm">
						<Col size={{ md: 12 }}>
							<label htmlFor="closeReason">{i18n.t('CloseReason')}:</label><br />
							<span>{caseItem.closeReason}</span><br />
						</Col>
					</Row>
				</div>

				<h3 style={{ marginBottom: -10, }}>{i18n.t('CaseHistory')}:</h3>
				<DataTable
					schema={{
						fields: {
							fecha: {
								type: 'DateTimeZ',
								label: i18n.t('CaseFecha'),
							},
							usuario: i18n.t('CaseUsuario'),
							actividad: i18n.t('CaseActividad'),
							id: i18n.t('CaseID'),
						}
					}}
					emptyRow loading={fetchingCaseHistories}
					rows={histories}
					renderRow={(props) => this.renderHistoryRow(props)}
				/>
				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}
