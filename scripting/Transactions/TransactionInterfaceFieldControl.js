import React, { Component } from 'react';
import {
	InterfaceFieldTypes,
	InterfaceFieldBooleanOptions,
	InterfaceFieldTypesHTML,
	TransactionResultListTemplate,
} from '../../store/enums';
import {
	Input,
	NumericInput,
	DateTimeInput,
	DropDown,
	TextArea,
	TagInput,
	InputRadio,
} from '../../reactadmin/components';
import i18n from '../../i18n';
import RemoteAutocomplete from '../RemoteAutocomplete';
import ReactSummernoteInput from '../../reactadmin/components/ui/ReactSummernoteInput';
import { TransactionInterfaceFieldTable } from '.';
import { isFunction } from '../../reactadmin/Utils';

/**
 * Componente que según el tipo de campo argumentado muestra al usuario el control del campo.
 *
 * @export
 * @class TransactionInterfaceFieldControl
 * @extends {Component}
 */
export default class TransactionInterfaceFieldControl extends Component {

	parseFieldProps(auxF) {
		const field = auxF;
		if (!field.json || !field.json.parsed) {
			try {
				field.json = JSON.parse(field.source);
				field.json.parsed = true;
			} catch (error) {
				// console.info('Error parsing field props: ', field, error);
				field.json = { props: {} };
			}
		}
	}

	render() {
		const {
			form,
			field,
			disabled,
			onChange,
			onKeyDown,
		} = this.props;
		const {
			// id,
			name,
			label,
			id_type: type,
			required,
			readonly: readOnly,
			default_value: defValue,
		} = field;
		this.parseFieldProps(field);
		const props = {
			className: 'form-control',
			name,
			...field.json.props, //{ props: { "style": { "color": "red", "textAlign": "center" } } }
			type: InterfaceFieldTypesHTML[type],
			value: name === TransactionResultListTemplate ? form : form[name] || defValue || '',
			onFieldChange: (e) => onChange(name, e.target.value),
			onKeyDown: (e) => isFunction(onKeyDown) && onKeyDown(e, field),
			disabled,
			required,
			readOnly,
		};
		switch (type) {
			case InterfaceFieldTypes.TEXT:
			case InterfaceFieldTypes.PASSWORD:
			case InterfaceFieldTypes.DATE:
			case InterfaceFieldTypes.TIME:
			case InterfaceFieldTypes.EMAIL:
			case InterfaceFieldTypes.HIDDEN:
				return (
					<Input {...props} />
				);
			case InterfaceFieldTypes.CHECKBOX: {
				props.onFieldChange = (e) => onChange(name, e.target.checked);
				props.checked = props.value || false;
				return (
					<Input {...props} />
				);
			}
			case InterfaceFieldTypes.DATETIME: {
				return (
					<DateTimeInput {...props} />
				);
			}
			case InterfaceFieldTypes.NUMBER: {
				return (
					<NumericInput {...props} />
				);
			}
			case InterfaceFieldTypes.BOOLEAN:
			case InterfaceFieldTypes.OPTIONS: {
				props.onChange = props.onFieldChange;
				props.emptyOption = props.emptyOption || field.json.emptyOption || 'SelectDisabled';
				props.editable = props.editable || field.json.editable;
				delete props.onFieldChange;
				if (field.id_type === InterfaceFieldTypes.BOOLEAN) {
					props.items = InterfaceFieldBooleanOptions();
				} else {
					props.items = props.items || field.json.items || [];
				}
				return (
					<DropDown {...props} />
				);
			}
			case InterfaceFieldTypes.RADIO: {
				const items = field.json.items || [];
				return (
					<InputRadio {...{ items, ...props, }} />
				);
			}
			case InterfaceFieldTypes.TEXTAREA: {
				return (
					<TextArea rows="4" {...props} />
				);
			}
			case InterfaceFieldTypes.LINK: {
				return (
					<a href={field.json.href || ''} target="_blank" without rel="noopener noreferrer">
						{field.json.label || label}
					</a>
				);
			}
			case InterfaceFieldTypes.AUTOCOMPLETE: {
				return (
					<RemoteAutocomplete
						url={field.json.url}
						getItemValue={field.json.getItemValue || 'name'}
						fetchProps={field.json.fetchProps}
						{...field.json.props}
						mountValue={props.value}
						disabled={props.disabled}
						readOnly={props.readOnly}
						required={props.required}
						onSelect={(text, item) => {
							const newValue = item[field.json.asignProp || 'id'];
							onChange(name, newValue || '');
						}}
					/>
				);
			}
			case InterfaceFieldTypes.TAGS: {
				props.onChange = ({ value }) => onChange(name, value);
				delete props.onFieldChange;
				return (
					<TagInput {...props} />
				);
			}
			case InterfaceFieldTypes.HTML: {
				props.className = '';
				props.onChange = (value) => onChange(name, value);
				delete props.onFieldChange;
				return (
					<ReactSummernoteInput {...props} />
				);
			}
			case InterfaceFieldTypes.TABLE: {
				(field.children || []).map(child => this.parseFieldProps(child));
				return (
					<TransactionInterfaceFieldTable
						{...{ field, form, onKeyDown, disabled, }}
						onChange={(value) => onChange(name, value)}
					/>
				);
			}
			default:
				return <div>{i18n.t('FieldTypeNotImplement')}</div>;
		}
	}
}
