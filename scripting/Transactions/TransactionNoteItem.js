import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import { Row } from '../../reactadmin/components/ui/Layout';

/**
 * Componente en desuso.
 *
 * @export
 * @class TransactionNoteItem
 * @extends {Component}
 * @deprecated
 */
export default class TransactionNoteItem extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	render() {
		const note = this.props.note || {};
		return (
			<Row>
				{note.value}
				Este panel debe mostrar los primeros caracteres de la nota.
				La nota debe permitir negritas, subrayado, ...
				Panel de Bootstrap que se expanda para ver más contenido de la nota
			</Row>
		);
	}
}
