// /**
//  * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
//  * All rights reserved.
//  *
//  * This source code is licensed under the license found in the
//  * LICENSE file in the root directory of this source tree.
//  *
//  * @providesModule Workflow
//  */

import React from 'react';
import { connect } from 'react-redux';
// import shallowCompare from 'react-addons-shallow-compare';

import {
    // ReduxOutlet,
    mapStateToProps,
} from '../../outlets/';

// import {
// 	Row, Col, Page,
// 	Panel, Button,
// 	ModalFactory, Factory,
// } from '../../reactadmin/components/';

// import {
// 	WorkflowModal,
// 	WorkflowStepItem,
// 	WorkflowStepModal,
// 	WorkflowPublishModal,
// } from './';

// //const noteActions = ReduxOutlet('note', '576a87c1e0b944000054d234').actions;
// const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
// const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;

/** 
 * Componente que controla la pantalla de detalle de contenidos.
 *
 * @class Content
 * @extends {React.Component}
 * @deprecated No implementado. 
 * El detalle de los contenidos se visualizan en el mismo formulario de creación.
 */
class Content extends React.Component {
    // 	state = {
    // 		workflow: {},
    // 		step: {},
    // 		workflowDisabled: false,
    // 		stepDisabled: false,
    // 	}
    // 	componentWillMount() {
    // 		this.fetchCurrentWorkflow();
    // 		this.fetchCurrentVersion();
    // 	}

    // 	shouldComponentUpdate(nextProps, nextState) {
    // 		return shallowCompare(this, nextProps, nextState);
    // 	}

    // 	getCurrentVersion() {
    // 		const { workflows, params } = this.props;
    // 		const current = workflows[`workflow_${params.id}_active`];
    // 		if (current && current.items) {
    // 			return current.items[0];
    // 		}
    // 		return {};
    // 	}

    // 	setWorkflowDisabled(value) {
    // 		this.setState({ workflowDisabled: value });
    // 	}
    // 	setStepDisabled(value) {
    // 		this.setState({ stepDisabled: value });
    // 	}

    // 	fetchCurrentWorkflow() {
    // 		const { dispatch, token, params } = this.props;
    // 		dispatch(workflowActions.fetchOne(token, params.id));
    // 	}
    // 	fetchCurrentVersion() {
    // 		const { token, dispatch, params } = this.props;
    // 		dispatch(workflowActions.postToURL({ token, route: `workflow/${params.id}/active` }));
    // 	}

    // 	openWorkflowModal(e) {
    // 		e.preventDefault();

    // 		const { workflows: { item: workflow } } = this.props;
    // 		this.setState({ workflow: Object.assign({}, workflow) });
    // 		ModalFactory.show('wfWorkflowModal', e);
    // 	}
    // 	handleWorkflowChange(workflow) {
    // 		const { dispatch, token } = this.props;
    // 		dispatch(workflowActions.update({
    // 			token,
    // 			item: workflow,
    // 			success: () => this.workflowChanged(),
    // 		}));
    // 	}
    // 	workflowChanged() {
    // 		ModalFactory.hide('wfWorkflowModal');
    // 		this.fetchCurrentWorkflow();
    // 	}

    // 	newStepClick(e) {
    // 		e.preventDefault();
    // 		this.setState({ step: {} });
    // 		ModalFactory.show('addStepModal');
    // 	}

    // 	openStepDetail(step, e) {
    // 		e.preventDefault();
    // 		this.setState({ step: Object.assign({}, step) });
    // 		ModalFactory.show('addStepModal');
    // 	}
    // 	handleStepChange(step) {
    // 		const { dispatch, token, workflows: { item } } = this.props;
    // 		const success = () => {
    // 			this.setStepDisabled(false);
    // 			ModalFactory.hide('addStepModal');
    // 		};
    // 		const failure = () => this.setStepDisabled(false);
    // 		if (!step.id) {
    // 			dispatch(workflowStepActions.createChild(
    // 				token, step, workflowActions, item.id,
    // 				success, failure
    // 			));
    // 		} else {
    // 			dispatch(workflowStepActions.updateChild(
    // 				token, step, workflowActions, item.id,
    // 				success, failure
    // 			));
    // 		}
    // 	}
    // 	handleStepDelete(step, e) {
    // 		e.preventDefault();
    // 		const { dispatch, token, workflows: { item } } = this.props;
    // 		dispatch(workflowStepActions.deleteChild(token, step, workflowActions, item.id));
    // 	}

    // 	deleteItem(id) {
    // 		const { token, dispatch } = this.props;
    // 		dispatch(workflowActions.delete(token, { _id: id }));
    // 	}

    // 	render() {
    // 		const { workflows: { list } } = this.props;
    // 		const contents = list || [];
    // 		return (
    // 			<Page>
    // 				{/* <Factory
    // 					modalref="wfWorkflowModal" title="Información del Árbol"
    // 					item={this.state.workflow} factory={WorkflowModal}
    // 					disabled={workflowDisabled}
    // 					onUpdate={(newWorkflow) => this.handleWorkflowChange(newWorkflow)}
    // 				/>
    // 				<Factory
    // 					modalref="addStepModal"
    // 					title="Agregar Paso" factory={WorkflowStepModal}
    // 					item={step} workflow={item}
    // 					disabled={stepDisabled} setDisabled={this.setStepDisabled.bind(this)}
    // 					onCreate={(newStep) => this.handleStepChange(newStep)}
    // 					onUpdate={(newStep) => this.handleStepChange(newStep)}
    // 				/>
    // 				<Factory
    // 					modalref="publishModal"
    // 					title="Publicar nueva versión del flujo" factory={WorkflowPublishModal}
    // 					onCreate={(config) => { this.publishWorkflow(config); }}
    // 					workflow={item} current={current}
    // 				/> */}
    // 				<Col size="5" className="col-md-push-7">
    // 					<Panel title={`Contenido ${item.name}`}>
    // 						<div style={{ textAlign: 'center' }}>
    // 							<Button
    // 								label="Modificar" size="btn-sm"
    // 								icon="fa-plus-square"
    // 								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
    // 								onClick={(e) => this.openWorkflowModal(e)}
    // 							/>
    // 							<Button
    // 								label="Criterio" size="btn-sm"
    // 								icon="fa-plus-square"
    // 								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
    // 							//onClick={(e) => this.newStepClick(e)}
    // 							/>
    // 							<Button
    // 								label="Publicar" size="btn-sm"
    // 								icon="fa-plus-square"
    // 								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
    // 								onClick={(e) => this.openPublishModal(e)}
    // 							/>
    // 						</div>
    // 						<div>
    // 							<h4>Datos:</h4>
    // 							<strong>Nombre: </strong> {item.name}<br />
    // 							<strong>Tags: </strong> {item.tags}<br />
    // 							<strong>Principal: </strong> {item.main ? 'Sí' : 'No'}<br />
    // 						</div>
    // 						<div>
    // 							<h4>Version:</h4>
    // 							<strong>Actual: </strong> {current.version_id_version || 'N/A'}<br />
    // 							<strong>Fecha publicada: </strong> {current.version_docdate}<br />
    // 							<strong>Notas: </strong> {current.version_notes}<br />
    // 						</div>
    // 						<div>
    // 							<h4>Criterio:</h4>
    // 							<strong>Servicio: </strong>[AVERIA_INTERNET]<br />
    // 							<strong>Zona: </strong>[METRO, METRO1]<br />
    // 							<strong>Tipo de Internet: </strong>[FIJO]<br />
    // 						</div>
    // 					</Panel>
    // 				</Col>
    // 				<Col size="7" className="col-md-pull-5">
    // 					<Panel title="Pasos" style={{ backgroundColor: 'grey' }}>
    // 						<Button
    // 							label="Paso" size="btn-sm"
    // 							icon="fa-plus-square"
    // 							color="btn-info" rounded className={'m-r-sm  m-b-sm'}
    // 							style={{ top: 9, right: 50, position: 'absolute' }}
    // 							onClick={(e) => this.newStepClick(e)}
    // 						/>

    // 						{contents.map()}
    // 						<Row />
    // 					</Panel>
    // 				</Col>
    // 			</Page>
    // 		);
    // 	}
}

export default connect(mapStateToProps)(Content);
