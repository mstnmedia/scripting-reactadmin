import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import request from 'axios';

import { EditorState, } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { createEditorState, } from 'medium-draft';
import {
	Input,
	ButtonGroup, Button,
	ModalFactory,
	TagInput,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps, ReduxOutlet } from '../../outlets/ReduxOutlet';
import { ContentTypeNames, ContentTypes, } from '../../store/enums';
import i18n from '../../i18n';
import { isFunction, /* getCookie, */ respErrorDetails } from '../../reactadmin/Utils';
import ReactSummernoteInput from '../../reactadmin/components/ui/ReactSummernoteInput';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

const wscActions = ReduxOutlet('workflows', 'workflowStepContent').actions;
/**
 * Componente que controla el formulario de los detalles de un contenido.
 *
 * @class ContentModal
 * @extends {PureComponent}
 */
class ContentModal extends PureComponent {

	static downloadURL(fileName) {
		return fileName.startsWith('data')
			? fileName
			: `${window.config.backend.replace('api', '')}download/${fileName}`;
	}
	static downloadFile(fileName) {
		window.open(this.downloadURL(fileName), '_blank');
	}

	state = {
		selectedFile: null,
		editorState: createEditorState(),
	};

	componentWillMount() {
		this.activeTypes = {
			[ContentTypes.HTML]: ContentTypeNames()[ContentTypes.HTML],
			[ContentTypes.IMAGE]: ContentTypeNames()[ContentTypes.IMAGE],
			[ContentTypes.FILE]: ContentTypeNames()[ContentTypes.FILE],
		};
		this.valueTypes = {
			[ContentTypeNames()[ContentTypes.HTML]]: ContentTypes.HTML,
			[ContentTypeNames()[ContentTypes.IMAGE]]: ContentTypes.IMAGE,
			[ContentTypeNames()[ContentTypes.FILE]]: ContentTypes.FILE,
		};
	}
	componentWillReceiveProps(props) {
		const item = this.getItem(props);
		if (item.id_type === ContentTypes.HTML) {
			const contentState = stateFromHTML(item.value);
			this.setState({ editorState: EditorState.createWithContent(contentState) });
		} else {
			this.setState({ editorState: EditorState.createEmpty() });
		}
	}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(prop, value) {
		const item = this.getItem();
		if (item) {
			if (prop === 'type' && !item.oldType) {
				item.oldType = item.id_type;
			} else if (prop === 'value' && !item.oldValue) {
				item.oldValue = item.value;
			}
			item[prop] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	handleContent(editorState) {
		const value = this.editor.exporter(editorState.getCurrentContent());
		this.handleChange('value', value);
		this.setState({ editorState });
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('contents.add') || user.hasPermissions('contents.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name && item.id_type && item.value
			&& (!this.props.user.master || item.id_center);
	}

	fetchContentDependentSteps(pageNumber) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		const where = [
			{ name: 'id_content', value: item.id },
		];
		const orderBy = 'id_workflow, id_workflow_version, workflow_step_order';
		const success = () => dispatch(wscActions.setProp('fetchingDependents', false));
		const failure = () => dispatch(wscActions.setProp('fetchingDependents', false));
		if (pageNumber === 1) {
			dispatch(wscActions.setProp(
				'fetchDependentSteps', (page) => this.fetchContentDependentSteps(page)
			));
		}
		dispatch(wscActions.setProp('fetchingDependents', true));
		dispatch(wscActions.setProp('temp', []));
		// dispatch(wscActions.setProp('tempNumber', 1));
		// dispatch(wscActions.setProp('tempRows', 0));
		dispatch(wscActions.fetchPage({
			token,
			temp: true,
			where,
			orderBy,
			pageNumber,
			success,
			failure,
		}));
	}

	fileChange(e) {
		this.setState({ selectedFile: e.target.files[0] });
	}
	uploadFile() {
		// const { token } = this.props;
		const { selectedFile } = this.state;
		const data = new FormData();
		data.append('file', selectedFile, selectedFile.name);
		request.post(`${window.config.backend}/uploadContent`, data, {
			headers: {
				// Authorization: `Bearer ${getCookie(window.config.TOKEN_COOKIE_KEY)}`, 
			},
			withCredentials: true,
			onUploadProgress: (/* progress */) => {
				// console.log(progress.loaded, ' / ', progress.total);
			}
		})
			.then((resp) => {
				// console.log('File uploaded: ', resp);
				this.handleChange('filename', resp.data.name);
				this.handleChange('value', resp.data.path);
				this.setState({ selectedFile: null, });
				window.message.success(i18n.t('UploadFileSuccess'));
			}).catch(error => {
				console.error('Error uploading file: ', error);
				window.message.error(i18n.t('UploadFileError', { fileName: selectedFile.name }), 0,
					respErrorDetails(error));
			});
	}

	draftsOnBlur() {
		this.drafs.save().then((html) => {
			this.handleChange('value', html);
		});
	}

	renderType() {
		const { selectedFile } = this.state;
		const { openContentDetail } = this.props;
		const { id, id_type: type, oldType, value, oldValue, filename, } = this.getItem();
		if (type === 1) {
			return (
				<div>
					<ReactSummernoteInput
						value={value}
						onChange={(html) => this.handleChange('value', html)}
						disabled={this.isFieldDisabled()}
					/>
				</div>
			);
		} else if (type === 2 || type === 3) {
			return [
				(!id ? null :
					<FormGroup key="current">
						<label className="control-label" htmlFor="id">{i18n.t('CurrentFileName')}:</label>
						{id && ContentTypes.IMAGE === oldType && <Button
							key="1" type="button"
							className="btn-success btn-xs inline-block"
							style={{ margin: 5, }}
							label={i18n.t('Open')}
							onClick={() => isFunction(openContentDetail) && openContentDetail()}
						/>}
						{id && <Button
							key="2" type="button"
							className="btn-success btn-xs inline-block"
							style={{ margin: 5, }}
							label={i18n.t('DownloadContent')}
							onClick={() => ContentModal.downloadFile(oldValue)}
						/>}
						<Input className="form-control" value={oldValue} disabled />
					</FormGroup>
				),
				(value === oldValue ? null :
					<FormGroup key="new" isValid={!!value}>
						<label className="control-label" htmlFor="id">{i18n.t('NewFileName')}:</label>
						<Input className="form-control" value={value} disabled />
					</FormGroup>
				),
				<FormGroup key="file" isValid={!!value} className="form-group m-t-sm">
					<label className="control-label block" htmlFor="file">{i18n.t('SelectFile')}:</label>
					<input
						type="file" accept={type === 2 ? 'image/*' : '*'}
						className="inline"
						onChange={(e) => this.fileChange(e)}
						onClick={(e) => { const ev = e; ev.target.value = null; }}
						disabled={this.isFieldDisabled()}
					/>
					<Button
						type="button"
						className="btn-success btn-xs inline-block"
						label={i18n.t('UploadContent')}
						onClick={() => this.uploadFile()}
						disabled={!selectedFile || this.isFieldDisabled()}
					/>
					{id && oldType === ContentTypes.IMAGE &&
						<img
							key={new Date()}
							className="img img-responsive"
							alt={filename}
							src={ContentModal.downloadURL(oldValue)}
							onError={(e) => {
								const target = e.target;
								// target.src = '/dist/images/image-no-available.png';
								target.alt = i18n.t('ImageNotFound');
							}}
						/>
					}
				</FormGroup>
			];
		}
		return null;
	}
	render() {
		// window.Scripting_ContentModal = this;
		const {
			user, hideDependents, hiddenClose, centerDisabled, 
		} = this.props;
		const { id,
			id_center: centerID,
			center_name: centerName,
			name, tags, deleted,
			id_type: type, //value,
		} = this.getItem();
		const isMaster = this.props.user.isMaster();
		const showDependents = id && user.hasPermissions('workflows.list') && !hideDependents;
		return (
			<div>
				{showDependents && <div className="text-right">
					<Button
						label={i18n.t('DependentWorkflows')} size="btn-sm"
						icon="fa-search"
						color="btn-info" rounded className="m-r-sm  m-b-sm"
						onClick={() => {
							this.fetchContentDependentSteps(1);
							ModalFactory.show('dependentStepsModal');
						}}
					/>
				</div>}
				{id && <FormGroup isValid={!!id}>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input className="form-control" value={id || ''} disabled />
				</FormGroup>}
				{isMaster && <FormGroup isValid={!!centerID}>
					<label className="inline-block" htmlFor="center">{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							getItemValue={(item) => NameIDText(item)}
							value={centerName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={id || this.isFieldDisabled() || centerDisabled} required
						/>
					</Tooltip>
				</FormGroup>}
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={name || ''} placeholder={i18n.t('Name')}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="tags">{i18n.t('Tags')}:</label>
					<Tooltip className="balloon" tag="TooltipTagField">
						<TagInput
							value={tags} placeholder={i18n.t('Tags')}
							onChange={({ value: newTags }) => this.handleChange('tags', newTags)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="deleted">{i18n.t('Disabled')}:</label>
					<Tooltip className="balloon" tag="TooltipContentDisabledField">
						<select
							className="form-control"
							value={!!deleted}
							onChange={(e) => this.handleChange('deleted', JSON.parse(e.target.value))}
							disabled={this.isFieldDisabled()}
						>
							<option value="true">{i18n.t('Yes')}</option>
							<option value="false">{i18n.t('No')}</option>
						</select>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!type}>
					<label className="control-label block" htmlFor="content">{i18n.t('Content')}:</label>
					<Tooltip position="top" tag="TooltipContentTypeField">
						<ButtonGroup
							className="m-b-sm"
							items={{ active: this.activeTypes[type], values: this.valueTypes, }}
							onChange={(selected) => {
								this.handleChange('value', '');
								this.handleChange('filename', '');
								this.handleChange('id_type', selected);
							}}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>
				{this.renderType()}

				<div className="">
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					{!hiddenClose && <button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>}
				</div>
			</div >
		);
	}
}
export default connect(mapStateToProps)(ContentModal);
