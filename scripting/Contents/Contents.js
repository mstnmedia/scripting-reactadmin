/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	Input,
	PanelCollapsible,
	ButtonGroup,
	DropDown,
	AdvancedFilters,
	TagInput,
	Button,
	NumericInput,
	Tooltip,
} from '../../reactadmin/components/';
import {
	ContentTypeNames,
	ID_GREETING_CONTENT,
	ID_END_TRANSACTION_CONTENT,
	ContentTypes,
	NumericBooleanOptions,
	ContentTypeOptions,
	filterAdvActions,
} from '../../store/enums';
import { setFilterHandles, NameIDText, } from '../../Utils';
import i18n from '../../i18n';
import { ContentModal, DependentStepsModal } from './';
import { ContentDetailModal } from '../Transactions';
import { DialogModal } from '../DialogModal';
import RemoteAutocomplete from '../RemoteAutocomplete';

const actions = ReduxOutlet('contents', 'content').actions;
const wscActions = ReduxOutlet('workflows', 'workflowStepContent').actions;

/**
 * Componente que controla la pantalla del mantenimiento de contenidos.
 *
 * @class Contents
 * @extends {Component}
 */
class Contents extends Component {
	state = {
		item: {},
		itemDisabled: false,
	}
	componentWillMount() {
		setFilterHandles(this, actions, 'contents', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Contents'), },
		];
		// window.Scripting_Contents = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	newItemClick(e) {
		e.preventDefault();
		this.setState({ item: {} });
		ModalFactory.show('addContentModal');
	}

	handleItemDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteContent'),
				description: i18n.t('WishDeleteContentDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, } = this.props;
							dispatch(actions.delete({
								token,
								item: { id },
								success: () => {
									ModalFactory.hide('confirmModal');
									this.fetchPage(this.props.contents.pageNumber);
								},
							}));
						},
						disabled: () => this.state.itemDisabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleItemChange(newItem) {
		const item = Object.assign({}, newItem, { oldValue: undefined, oldType: undefined, });
		const { dispatch, token, contents: { pageNumber, } } = this.props;
		const success = () => {
			const page = item.id ? pageNumber : 1;
			ModalFactory.hide('addContentModal');
			this.setState({ itemDisabled: false });
			this.fetchPage(page);
		};
		const failure = () => this.setState({ itemDisabled: false });
		this.setState({ itemDisabled: true });
		if (!item.id) {
			dispatch(actions.create(token, item, success, failure));
		} else {
			dispatch(actions.update({ token, item, success, failure }));
		}
	}
	handleItemDuplicate(row, e) {
		e.stopPropagation();
		const { dispatch, token, } = this.props;
		const path = `duplicating_${row.id}`;
		const success = () => {
			this.setState({ [path]: false });
			this.fetchPage(1);
		};
		const failure = () => {
			this.setState({ [path]: false });
		};
		this.setState({ [path]: true });
		dispatch(actions.postToURL({
			token,
			route: 'content/duplicate',
			path,
			item: { id: row.id },
			success,
			failure,
		}));
	}

	canDelete(row) {
		if ([ID_GREETING_CONTENT, ID_END_TRANSACTION_CONTENT].indexOf(row.id) >= 0) {
			return false;
		}
		return this.props.user.hasPermissions('contents.delete');
	}

	selectItem(item) {
		// console.log('content clicked: ', item);
		const newItem = JSON.parse(JSON.stringify(item));
		newItem.oldType = newItem.id_type;
		newItem.oldValue = newItem.value;
		this.setState({ item: newItem });
		ModalFactory.show('addContentModal');
	}

	canWSCDelete(row) {
		const { user, } = this.props;
		return row.version_active === 0 && user.hasPermissions('contents.delete');
	}
	handleWSCDelete(id) {
		const { dispatch, token, } = this.props;
		dispatch(wscActions.delete({ token, item: { id }, }));
	}

	tableSchema() {
		const canAdd = this.props.user.hasPermissions('contents.add');
		const isMaster = this.props.user.isMaster();
		return {
			name: 'contents',
			fields: {
				duplicate: canAdd && {
					label: ' ',
					cellStyle: { width: 60, },
					format: (row) => (
						<Tooltip tag="TooltipContentCopy">
							<Button
								className="btn-success btn-xs inline-block"
								icon="fa-copy"
								onClick={(e) => this.handleItemDuplicate(row, e)}
								disabled={this.state[`duplicating_${row.id}`]}
							/>
						</Tooltip>
					),
				},
				id_type: {
					label: i18n.t('Type'),
					format: (row) => (ContentTypeNames()[row.id_type]),
				},
				deleted: {
					type: 'Boolean',
					label: i18n.t('Disabled'),
				},
				tags: {
					label: i18n.t('Tags'),
					format: (row) => (<TagInput inline value={row.tags} />)
				},
				name: i18n.t('Name'),
				center_name: isMaster ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
			}
		};
	}

	renderFilters() {
		const { where, whereObj, advancedSearch } = this.state;
		const isMaster = this.props.user.isMaster();
		const centerID = this.filterValue('id_center');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group col-md-3">
							<label htmlFor="id">{i18n.t('ID')}:</label>
							<NumericInput
								value={this.filterValue('id', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'id',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{isMaster && <div className="form-group col-md-3">
							<label htmlFor="center">{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/centers/center`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('center_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('center_name', text);
									this.filterChange({ name: 'id_center', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('center_name', item.name);
									this.filterChange({ name: 'id_center', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_center')) {
										this.filterTextChange('center_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>}
						<div className="form-group col-md-3">
							<label htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								value={this.filterValue('name', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'name',
									value: e.target.value,
									criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="type">{i18n.t('Type')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('id_type', '')}
								items={ContentTypeOptions()}
								onChange={(e) => this.filterChange({ name: 'id_type', value: e.target.value })}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="tags">{i18n.t('Tags')}:</label>
							<TagInput
								value={this.filterValue('tags', '')} placeholder={i18n.t('Tags')}
								onChange={({ tags, value }) => this.filterChange({
									label: 'tags',
									value: tags.length ? value : undefined,
									group: tags.map(i => ({ name: 'tags', criteria: 'like', value: i }))
								})}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="deleted">{i18n.t('Disabled')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('deleted', '')}
								items={NumericBooleanOptions()}
								onChange={(e) => this.filterChange({ name: 'deleted', value: e.target.value })}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
					</div>
					:
					<AdvancedFilters
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple },
							id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
							name: { type: 'text', label: i18n.t('Name'), onKeyDown: this.onKeyDownSimple, },
							tags: { type: 'tags', label: i18n.t('Tags'), onKeyDown: this.onKeyDownMixed, },
							value: { type: 'clob', label: i18n.t('Value'), onKeyDown: this.onKeyDownSimple, },
							id_type: { type: 'dropdown', label: i18n.t('Type'), items: ContentTypeOptions(), onKeyDown: this.onKeyDownSimple },
							deleted: { type: 'dropdown', label: i18n.t('Disabled'), items: NumericBooleanOptions(), onKeyDown: this.onKeyDownSimple },
						}}/* eslint-enable max-len */
						{...{ where, whereObj }}
					/>
				}

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			contents: { list, pageNumber, pageSize, totalRows, },
			stepContent: { 
				temp: dependentSteps,
				tempNumber,
				tempSize,
				tempRows,
				fetchingDependents: loading,
				fetchDependentSteps,
			},
			user,
			router,
		} = this.props;
		const { item, itemDisabled, confirmModalProps, } = this.state;
		const contents = list || [];

		const canAdd = user.hasPermissions('contents.add');
		const showDependents = user.hasPermissions('workflows.list');
		const modalTitle = item.id_type !== ContentTypes.HTML
			? item.filename || item.name || ''
			: '';
		return (
			<Page>
				<Factory
					modalref="addContentModal"
					title={i18n.t('ContentInfo', item)}
					factory={ContentModal} large
					item={item} disabled={itemDisabled}
					onCreate={(newItem) => this.handleItemChange(newItem)}
					onUpdate={(newItem) => this.handleItemChange(newItem)}
					openContentDetail={() => ModalFactory.show('contentDetailModal')}
				/>
				<Factory
					modalref="contentDetailModal"
					factory={ContentDetailModal} large
					title={modalTitle}
					content={item} results={[]}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				{showDependents &&
					<Factory
						modalref="dependentStepsModal" factory={DependentStepsModal} large
						title={i18n.t('DependentWorkflowsFromContent', item)}
						canBeClose backdrop="true" keyboard="true"
						{...{ dependentSteps, loading, router, }}
						{...{ pageNumber: tempNumber, pageSize: tempSize, totalRows: tempRows, }}
						fetchDependentSteps={(newPage) => fetchDependentSteps(newPage)}
						canDelete={(row) => this.canWSCDelete(row)}
						onDelete={(id) => this.handleWSCDelete(id)}
						goToStep={() => ModalFactory.hide('addContentModal')}
					/>
				}
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={this.tableSchema()}
							rows={contents}
							addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Contents') })}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={(row) => this.canDelete(row)}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Contents);
