import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps, } from '../../outlets/ReduxOutlet';
import {
	DataTable,
	Col, /* PanelCollapsible, */
	ModalFactory,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

/**
 * Componente que lista todos pasos que muestran al usuario el contenido actual.
 *
 * @class DependentStepsModal
 * @extends {Component}
 */
class DependentStepsModal extends Component {
	state = {}

	onPageChange({ newPage }) {
		if (this.props.fetchDependentSteps) {
			this.props.fetchDependentSteps(newPage);
		}
	}
	goToStep(stepContent) {
		const { router, goToStep, } = this.props;
		const workflowID = stepContent.id_workflow;
		const versionID = stepContent.version_id_version;
		const stepID = stepContent.id_workflow_step;
		ModalFactory.hide(this.props.modalref);
		if (isFunction(goToStep)) {
			goToStep();
		}
		if (router) {
			router.push(`/data/workflows/${workflowID}/version/${versionID}/step/${stepID}`);
		}
	}
	render() {
		const {
			dependentSteps: list,
			pageNumber, pageSize, totalRows, 
			canDelete, onDelete, loading,
		} = this.props;
		const rows = (list || [])
			// .map(i => i)
			// .sort((a, b) => {
			// 	const x = a.id_workflow_version - b.id_workflow_version;
			// 	return x === 0 ? a.order_index - b.order_index : x;
			// })
			;
		// const groups = [];
		// const groupsObj = {};
		// for (let i = 0; i < dependentSteps.length; i++) {
		// 	const step = dependentSteps[i];
		// 	if (!groupsObj[step.id_workflow]) {
		// 		groupsObj[step.id_workflow] = {
		// 			id: step.id_workflow,
		// 			name: step.workflow_name,
		// 			steps: [],
		// 		};
		// 		groups.push(groupsObj[step.id_workflow]);
		// 	}
		// 	groupsObj[step.id_workflow].steps.push(step);
		// }

		return (
			<div>
				<div>
					{/* groups.length
						? groups.map(group => (
							<PanelCollapsible key={group.id} title={`${group.id} ${group.name}`}>
								<label htmlFor="steps">{i18n.t('DependentWorkflowSteps')}:</label>
								<DataTable
									schema={{
										fields: {
											version_id_version: i18n.t('Version'),
											workflow_step_name: i18n.t('Name'),
											id: i18n.t('ID'),
										},
									}}
									rows={group.steps}
									canDelete={canDelete}
									onDelete={onDelete}
									link={router && ((row) => this.goToStep(row))}
								/>
							</PanelCollapsible>
						))
						: <h5>{loading ? i18n.t('Loading') : i18n.t('NoDependentWorkflows')}</h5>
					 */}
					<DataTable
						schema={{
							fields: {
								workflow_step_name: {
									label: i18n.t('Step'),
									format: (row) => `${row.id_workflow_step} | ${row.workflow_step_name}`,
								},
								version_id_version: i18n.t('Version'),
								workflow_name: {
									label: i18n.t('Workflow'),
									format: (row) => `${row.id_workflow} | ${row.workflow_name}`,
								},
							},
						}}
						{...{ rows, pageNumber, pageSize, totalRows, loading, }}
						onPageChange={(data) => this.onPageChange(data)}
						canDelete={canDelete}
						onDelete={onDelete}
						link={(row) => this.goToStep(row)}
					/>
				</div>
				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}

export default connect(mapStateToProps)(DependentStepsModal);
