import React, { Component } from 'react';
import { connect } from 'react-redux';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	DataTable,
	Input, Pager, TagInput,
} from '../../reactadmin/components/';
import {
	ContentTypeNames,
} from '../../store/enums';

import ContentModal from './ContentModal';
import i18n from '../../i18n';

const actions = ReduxOutlet('contents', 'content').actions;
/**
 * Componente que permite al usuario filtrar un contenido y seleccionarlo.
 *
 * @class ContentSearch
 * @extends {Component}
 */
class ContentSearch extends Component {
	static fetchContentSearch(that, search, page) {
		const {
			dispatch, token,
			workflows: { item: workflow, },
			contents: { tempSize: pageSize, },
			user: { master, },
		} = that.props;
		const pageNumber = page || 1;

		const value = `${search}%`;
		const criteria = 'like';
		const separator = 'or';
		const where = [
			{ name: 'id_center', value: workflow.id_center, },
			{
				group: [
					{ name: 'id', value, criteria, separator, },
					...(master ? [
						{ name: 'center_name', value, criteria, separator, },
					] : []),
					{ name: 'name', value, criteria, separator, },
					{ name: 'tags', value, criteria, separator, },
				]
			},
			{ name: 'deleted', value: '0', },
		];
		const success = (resp) => {
			// console.log(resp);
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const { items, totalRows } = data;
				if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
					const maxPage = Pager.getMaxPage(totalRows, pageSize);
					ContentSearch.fetchContentSearch(that, search, maxPage);
				}
			}
		};
		dispatch(actions.fetchPage({
			token, where, pageNumber, pageSize, temp: true, success,
		}));
	}
	state = {
		item: {},
		selected: null,
		disabled: false,
		search: '',
		errormessage: '',
	}

	componentWillMount() {
		const { dispatch, } = this.props;
		dispatch(actions.setProp('temp', []));
		dispatch(actions.setProp('tempNumber', 1));
		dispatch(actions.setProp('tempRows', 0));
		this.resetItem(this.props);
		// window.Scripting_ContentSearch = this;
	}
	componentWillReceiveProps(props) {
		if (props.workflow && props.workflow !== this.props.workflow) {
			this.resetItem(props);
		}
	}

	handleChange(search, pageNumber) {
		this.setState({ search });
		ContentSearch.fetchContentSearch(this, search, pageNumber);
	}

	selectRow(selected) {
		this.setState({ selected });
		if (this.props.onSelect) {
			this.props.onSelect(selected);
		}
	}

	resetItem(props) {
		if (props.workflow) {
			this.setState({
				item: {
					id_center: props.workflow.id_center,
					center_name: props.workflow.center_name,
				},
				disabled: false,
			});
		}
	}
	handleNewContent(newItem) {
		const { dispatch, token, } = this.props;
		const success = (data) => {
			// console.log('ContentSearch > created content data: ', data);
			this.resetItem(this.props);
			if (data.data && data.data.code === 200) {
				const item = data.data.items[0];
				this.selectRow(item);
				this.handleChange(item.name);
				$('[href="#CnHome"]').click();
			}
		};
		const failure = () => this.setState({ disabled: false });

		this.setState({ disabled: true });
		const item = Object.assign({}, newItem, { oldValue: undefined, oldType: undefined, });
		dispatch(actions.create(token, item, success, failure));
	}

	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}

	render() {
		const {
			contents: {
				temp,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
			},
			user,
		} = this.props;
		const { item, search, } = this.state;
		const selected = this.props.selected || this.state.selected;
		const rows = temp || [];
		const onPageChange = ({ newPage }) => {
			this.handleChange(search, newPage);
		};
		return (
			<section style={{ marginBottom: 0 }}>
				<ul className="nav nav-tabs">
					<li className="active"><a data-toggle="tab" href="#CnHome">{i18n.t('ToSearch')}</a></li>
					<li><a data-toggle="tab" href="#CnNew">{i18n.t('ContentNewTab')}</a></li>
				</ul>

				<div className="tab-content">
					<div id="CnHome" className="tab-pane fade in active">
						<section style={{ marginBottom: 0 }} >
							<div className="panel-body m-b-none">
								<div className="form-group">
									<Input
										type="text" className="form-control"
										value={search} placeholder={i18n.t('Name')}
										onFieldChange={(e) => this.handleChange(e.target.value, pageNumber)}
									/>
									<DataTable
										hover
										schema={{
											fields: {
												id_type: {
													label: i18n.t('Type'),
													format: (row) => (ContentTypeNames()[row.id_type]),
												},
												tags: {
													label: i18n.t('Tags'),
													format: (row) => (<TagInput inline value={row.tags} />)
												},
												name: i18n.t('Name'),
												center_name: user.master ? i18n.t('Center') : undefined,
												id: i18n.t('ID'),
											}
										}}
										link={(row) => this.selectRow(row)}
										{...{ selected, rows, totalRows, pageNumber, pageSize, onPageChange, }}
									/>
								</div>
							</div>
						</section>
					</div>
					<div id="CnNew" className="tab-pane fade" style={{ paddingTop: 10 }}>
						<ContentModal
							item={item} hiddenClose centerDisabled
							onCreate={(newItem) => this.handleNewContent(newItem)}
							disabled={this.isDisabled()}
						/>
					</div>
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(ContentSearch);
