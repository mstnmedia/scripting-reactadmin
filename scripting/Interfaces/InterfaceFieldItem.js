import React, { Component } from 'react';

/**
 * Componente usado en la lista de campos de una interfaz para mostrar los detalles de cada uno.
 *
 * @export
 * @class InterfaceFieldItem
 * @extends {Component}
 */
export default class InterfaceFieldItem extends Component {
	render() {
		const { item, onClick, onDelete } = this.props;
		//{ name, label, id_type: type, readonly, default_value: defValue }
		return (
			<div
				style={{
					display: 'flex',
					cursor: 'pointer',
					marginBottom: 2,
					borderBottom: '1px solid #eee',
				}}
			>
				<div onClick={onClick} style={{ flex: 'auto', padding: 5, wordBreak: 'break-all', }}>
					{item.label}
				</div>

				{onDelete &&
					<div style={{ padding: 5, }}>
						<a href="#deleteRow" onClick={onDelete}>
							<i className="fa text-muted fa-trash-o" />
						</a>
					</div>
				}
			</div>
		);
	}
}
