import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

/**
 * Componente de la versión de prototipo que simulaba resultados de una interfaz.
 *
 * @export
 * @class ResultCRM
 * @extends {Component}
 * @deprecated Todos los resultados aparecen en la pestaña Resultados externos.
 */
export default class ResultCRM extends Component {
	state = {
		showAll: false,
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	toggleShowAll() {
		this.setState({ showAll: !this.state.showAll });
	}
	render() {
		const { result } = this.props;
		const { showAll } = this.state;
		
		return (
			<div className="table-responsive">
				<table className="table table-condensed">
					<tbody>
						<tr>
							<td><b>Miembro de equipo:</b></td>
							<td>{result.docdate.toString()}</td>
						</tr>
						<tr>
							<td><b>Contactos adicionales:</b></td>
							<td>.</td>
						</tr>
						<tr>
							<td><b>Género:</b></td>
							<td>.</td>
						</tr>
						<tr>
							<td><b>Dirección de la Cédula:</b></td>
							<td>.</td>
						</tr>
						<tr 
							className="cursor-pointer text-center btn-info" 
							style={{ cursor: 'pointer' }}
							onClick={() => this.toggleShowAll()}
						>
							<td colSpan="1000">
								{showAll ? 'Ver menos' : 'Ver más'}
							</td>
						</tr>
					</tbody>
					<tbody style={{ display: !showAll ? 'none' : null }}>
						<tr>
							<td><b>Other Prop:</b></td>
							<td>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop:</b></td>
							<td>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop:</b></td>
							<td>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop:</b></td>
							<td>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop:</b></td>
							<td>More Data</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}
