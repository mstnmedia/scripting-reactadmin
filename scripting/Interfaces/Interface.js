import React, { Component } from 'react';
import { connect } from 'react-redux';

import i18n from '../../i18n';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Col, Page,
	Panel, Button,
	ModalFactory, Factory,
	DataTable, TagInput,
	Pager,
	AnimatedSpinner,
	Tooltip,
} from '../../reactadmin/components/';

import {
	InterfaceFieldModal,
	// InterfaceOptionModal,
	InterfaceServiceModal,
	InterfaceOptionItem,
	InterfaceFieldItem,
} from './';
import {
	renderListHeader,
	containsIgnoringCaseAndAccents,
} from '../../Utils';
import { DependentWorkflowsModal } from '../Workflows';
import { WorkflowStepTypes } from '../../store/enums';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('interfaces', 'interfaceBase').actions;
const interfaceFieldActions = ReduxOutlet('interfaces', 'interfaceField').actions;
const interfaceOptionActions = ReduxOutlet('interfaces', 'interfaceOption').actions;
const interfaceServiceActions = ReduxOutlet('interfaces', 'interfaceService').actions;
const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;

/**
 *
 *
 * @class Interface
 * @extends {Component}
 */
class Interface extends Component {
	state = {
		field: {},
		option: {},
		service: {},
		serviceDisabled: false,
		editing: false,
	}
	componentWillMount() {
		this.init(this.props);
		// window.Scripting_Interface = this;
	}
	componentWillReceiveProps(props) {
		if (props.params.id !== this.props.params.id) {
			this.init(props);
		}
	}

	getItem() {
		return this.props.interfaceBases.item;
	}
	getServices() {
		const currentInterface = this.getItem();
		return (currentInterface && currentInterface.interface_services) || [];
	}
	getBreadcrumbItems() {
		const interfaceID = this.props.params.id;
		const inter = this.getItem();
		const interfaceLabel = (inter.label || `#${interfaceID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Interfaces'), to: '/data/interfaces' },
			{ label: `${i18n.t('Interface')} ${interfaceLabel}`, },
		];
	}
	getProp(prop, def) {
		return this.state[prop] || def;
	}

	init(props) {
		this.fetchCurrentInterface(props);
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
	}
	fetchCurrentInterface(props) {
		const { dispatch, token, params, } = props;
		const callback = () => dispatch(actions.setProp('isFetching', false));
		const success = (resp) => {
			callback();
			dispatch(actions.setSelected(resp.data.items[0]));
		};
		dispatch(actions.setProp('isFetching', true));
		dispatch(actions.setProp('item', {}));
		dispatch(actions.fetchOne(token, params.id, success, callback));
	}
	fetchInterfaceDependentSteps(pageNumber) {
		const currentInterface = this.getItem();
		const { dispatch, token, params } = this.props;
		const where = [
			{ name: 'id_type', value: WorkflowStepTypes.EXTERNAL },
			{ name: 'id_workflow_external', value: params.id },
		];
		const orderBy = 'id_workflow, id_workflow_version, initial_step, order_index';
		const success = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const failure = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		if (pageNumber === 1) {
			dispatch(workflowStepActions.setProp(
				'fetchDependentSteps', (page) => this.fetchInterfaceDependentSteps(page)
			));
		}
		dispatch(workflowStepActions.setProp(
			'modalTitle', i18n.t('DependentWorkflowsFromInterface', currentInterface)
		));
		dispatch(workflowStepActions.setProp('fetchingDependents', true));
		dispatch(workflowStepActions.setProp('temp', []));
		// dispatch(workflowStepActions.setProp('tempNumber', pageNumber));
		// dispatch(workflowStepActions.setProp('tempRows', 0));
		dispatch(workflowStepActions.fetchPage({
			token,
			temp: true,
			where,
			orderBy,
			pageNumber,
			success,
			failure,
		}));
	}

	/**
	 *
	 *
	 * @param {*} e
	 * @memberof Interface
	 */
	newFieldClick(e) {
		e.preventDefault();

		this.setState({ field: {} });
		ModalFactory.show('addFieldModal');
	}
	newOptionClick(e) {
		e.preventDefault();

		this.setState({ option: {} });
		ModalFactory.show('addOptionModal');
	}
	newServiceClick(e) {
		e.preventDefault();
		this.setState({ service: {} });
		ModalFactory.show('addServiceModal');
	}

	openFieldDetail(field, e) {
		e.preventDefault();

		this.setState({ field: Object.assign({}, field) });
		ModalFactory.show('addFieldModal');
	}
	openOptionDetail(option, e) {
		e.preventDefault();

		this.setState({ option: Object.assign({}, option) });
		ModalFactory.show('addOptionModal');
	}
	openServiceDetail(service) {
		this.setState({ service: Object.assign({}, service) });
		ModalFactory.show('addServiceModal');
	}

	handleChange(property, value) {
		this.setState({ [property]: value });
	}
	handleFieldChange(field) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		// const { item } = interfaces;
		if (!field.id) {
			dispatch(interfaceFieldActions.createChild(
				token, field, actions,
				item.id, () => ModalFactory.hide('addFieldModal'),
			));
		} else {
			dispatch(interfaceFieldActions.updateChild(
				token, field, actions, item.id,
				() => ModalFactory.hide('addFieldModal'),
			));
		}
	}
	handleOptionChange(option) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		// const { item } = interfaces;
		if (!option.id) {
			dispatch(interfaceOptionActions.createChild(
				token, option, actions,
				item.id, () => ModalFactory.hide('addOptionModal'),
			));
		} else {
			dispatch(interfaceOptionActions.updateChild(
				token, option, actions, item.id,
				() => ModalFactory.hide('addOptionModal'),
			));
		}
	}
	handleServiceChange(service) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		const success = () => {
			this.setState({ serviceDisabled: false });
			ModalFactory.hide('addServiceModal');
		};
		const failure = () => this.setState({ serviceDisabled: false });
		this.setState({ serviceDisabled: true });
		if (!service.id) {
			dispatch(interfaceServiceActions.createChild(
				token, service, actions,
				item.id, success, failure,
			));
		} else {
			dispatch(interfaceServiceActions.updateChild(
				token, service, actions, item.id,
				success, failure,
			));
		}
	}

	handleFieldDelete(field, e) {
		e.preventDefault();

		const { dispatch, token, } = this.props;
		const item = this.getItem();
		// const { item } = interfaces;
		dispatch(interfaceFieldActions.deleteChild(token, field, actions, item.id));
	}
	handleOptionDelete(option, e) {
		e.preventDefault();

		const { dispatch, token, } = this.props;
		const item = this.getItem();
		// const { item } = interfaces;
		dispatch(interfaceOptionActions.deleteChild(token, option, actions, item.id));
	}
	handleServiceDelete(id) {
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteInterfaceService'),
				description: i18n.t('WishDeleteInterfaceServiceDescription'),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const { dispatch, token, } = this.props;
							const item = this.getItem();
							dispatch(interfaceServiceActions.deleteChild(
								token,
								{ id },
								actions,
								item.id,
								() => ModalFactory.hide('confirmModal'),
							));
						},
						disabled: () => this.state.serviceDisabled,
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	renderSpinner() {
		const { interfaceBases: { isFetching }, } = this.props;
		return (
			<AnimatedSpinner key="spinner" show={isFetching} />
		);
	}
	renderDetails() {
		const item = this.getItem();
		return (
			<Panel>
				<div style={{ textAlign: 'center' }}>
					<Tooltip tag="TooltipInterfaceDependents">
						<Button
							label={i18n.t('DependentWorkflows')} size="btn-sm"
							icon="fa-search"
							color="btn-info" rounded className={'m-r-sm  m-b-sm'}
							onClick={() => {
								this.fetchInterfaceDependentSteps(1);
								ModalFactory.show('dependentWorkflowsModal');
							}}
						/>
					</Tooltip>
				</div>

				<div>
					<h4>{this.renderSpinner()}{i18n.t('Data')}:</h4>
					<strong>{i18n.t('ID')}: </strong>{item.id}<br />
					<strong>{i18n.t('Name')}: </strong>{item.name}<br />
					<strong>{i18n.t('Title')}: </strong>{item.label}<br />
					<strong>{i18n.t('URL')}: </strong>{item.server_url}<br />
					{/* <strong>{i18n.t('Value')}: </strong>{item.value_step}<br /> */}
				</div>

				{/* {this.renderOptions()} */}
			</Panel>
		);
	}
	renderFields() {
		const currentInterface = this.getItem();
		let fields = [];
		if (currentInterface) {
			if (currentInterface.interface_field) {
				fields = currentInterface.interface_field;
			}
		}
		return (
			<Panel
				title={[
					this.renderSpinner(),
					<Tooltip key="2" position="right" tag="TooltipInterfaceFields">
						{i18n.t('Fields')}
					</Tooltip>
				]}
			>
				{fields && fields.length ?
					fields.map(item => (
						<InterfaceFieldItem
							key={item.name} item={item} parent={currentInterface}
							onClick={(e) => this.openFieldDetail(item, e)}
						// onDelete={(e) => this.handleFieldDelete(item, e)}
						/>
					))
					: i18n.t('NoFieldRecord')
				}
			</Panel>
		);
	}
	renderOptions() {
		const currentInterface = this.getItem();
		let options = [];
		if (currentInterface && currentInterface.interface_option) {
			options = currentInterface.interface_option;
		}
		return [
			<hr />,
			<h4>{i18n.t('Options')}:</h4>,
			<div>
				{options && options.length ?
					options.map(item => (
						<InterfaceOptionItem
							key={item.id} item={item} parent={currentInterface}
							onClick={(e) => this.openOptionDetail(item, e)}
						// onDelete={(e) => this.handleOptionDelete(item, e)}
						/>
					))
					: i18n.t('NoOptions')
				}
			</div>
		];
	}
	renderResults() {
		const currentInterface = this.getItem();
		if (currentInterface && currentInterface.interface_results) {
			const results = currentInterface.interface_results;
			return (
				<Panel
					title={[
						this.renderSpinner(),
						<Tooltip key="2" position="right" tag="TooltipInterfaceResults">
							{i18n.t('Results')}
						</Tooltip>
					]}
				>
					{results && results.length ?
						results.map(item => (
							<div
								key={item.name}
								style={{
									display: 'flex',
									marginBottom: 2,
									borderBottom: '1px solid #eee',
								}}
							>
								<div style={{ flex: 'auto', padding: 5, wordBreak: 'break-all', }}>
									{item.label} {item.name !== item.label ? `(${item.name})` : ''}
								</div>
							</div>
						))
						: i18n.t('NoResultRecord')
					}
				</Panel>
			);
		}
		return null;
	}
	renderServices() {
		const pageSize = this.getProp('pageSize', 10);
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleChange('pageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getProp('textFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getServices().filter(i => !_textFilter
				|| containsIgnoringCaseAndAccents(i.id, _textFilter)
				|| containsIgnoringCaseAndAccents(i.name, _textFilter)
				|| containsIgnoringCaseAndAccents(i.returned_values, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getProp('pageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleChange('pageNumber', newPage);
		};
		const onPageSizeChange = (_pageSize) => {
			this.handleChange('pageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleChange('textFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
		});
		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });

		const canAdd = this.props.user.hasPermissions('interfaces.add');
		return (
			<Panel title={[this.renderSpinner(), i18n.t('Services')]}>
				{canAdd &&
					<Tooltip
						tag="TooltipInterfaceAddService"
						style={{ top: 9, right: 50, position: 'absolute' }}
					>
						<Button
							label={i18n.t('AddService')} size="btn-sm"
							icon="fa-plus-square"
							color="btn-info" rounded className={'m-r-sm m-b-sm'}
							onClick={(e) => this.newServiceClick(e)}
						/>
					</Tooltip>
				}
				<DataTable
					schema={{
						fields: {
							returned_values: {
								label: i18n.t('ReturnedValues'),
								cellStyle: { wordBreak: 'break-word', },
								format: (row) => (<TagInput inline value={row.returned_values} />)
							},
							name: i18n.t('Name'),
							id: i18n.t('ID'),
						}
					}}
					emptyRow={emptyRow}
					{...{ pageSize, pageNumber, onPageChange, }}
					{...{ renderHeader, rows, totalRows, }}
					link={(row) => this.openServiceDetail(row)}
					onDelete={(id) => this.handleServiceDelete(id)}
				/>
			</Panel>
		);
	}
	render() {
		const {
			steps: {
				temp: dependentSteps,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
				fetchingDependents: loading,
				modalTitle = i18n.t('DependentWorkflows'),
				fetchDependentSteps,
			},
			router,
		} = this.props;
		const {
			field,
			service, serviceDisabled,
			confirmModalProps,
		} = this.state;
		const currentInterface = this.getItem();
		return (
			<Page>
				<Factory
					modalref="addFieldModal" title={i18n.t('AddField')}
					factory={InterfaceFieldModal} large
					item={field} canBeClose backdrop keyboard
					parent={currentInterface}
					onCreate={(newField) => this.handleFieldChange(newField)}
					onUpdate={(newField) => this.handleFieldChange(newField)}
				/>
				{/* <Factory
					modalref="addOptionModal"
					title={i18n.t('AddOption')} factory={InterfaceOptionModal}
					item={option}
					parent={currentInterface}
					onCreate={(newOption) => this.handleOptionChange(newOption)}
					onUpdate={(newOption) => this.handleOptionChange(newOption)}
				/> */}
				<Factory
					modalref="addServiceModal" large
					title={i18n.t('AddService', currentInterface)} factory={InterfaceServiceModal}
					item={service} disabled={serviceDisabled}
					parent={currentInterface}
					onCreate={(newService) => this.handleServiceChange(newService)}
					onUpdate={(newService) => this.handleServiceChange(newService)}
				/>
				<Factory
					modalref="dependentWorkflowsModal" factory={DependentWorkflowsModal} large
					title={modalTitle} canBeClose backdrop="true" keyboard="true"
					{...{ dependentSteps, pageNumber, pageSize, totalRows, loading, router, }}
					fetchDependentSteps={(newPage) => fetchDependentSteps(newPage)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>

				<Col size={5} className="col-md-push-7">
					{this.renderDetails()}
					{this.renderFields()}
				</Col>

				<Col size={7} className="col-md-pull-5">
					{this.renderServices()}
					{this.renderResults()}
				</Col>
			</Page>
		);
	}
}
const emptyRow = ({ columnNo }) => (
	<tr><td colSpan={columnNo}>{i18n.t('NoServiceRecord')}</td></tr>
);

export default connect(mapStateToProps)(Interface);
