import React, { Component } from 'react';
import { connect } from 'react-redux';

import i18n from '../../i18n';
import {
	Input, TextArea,
	TagInput,
	FormGroup,
	Button,
	ModalFactory,
	Tooltip,
} from '../../reactadmin/components/';
import { WorkflowStepTypes } from '../../store/enums';
import { mapStateToProps, ReduxOutlet } from '../../outlets/ReduxOutlet';

// const Codemirror = require('react-codemirror');
// require('codemirror/mode/javascript/javascript');
// require('codemirror/theme/monokai.css');

const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;
/**
 * Componente que controla el formulario de los detalles de un servicio de una interfaz.
 *
 * @class InterfaceServiceModal
 * @extends {Component}
 */
class InterfaceServiceModal extends Component {

	state = {}

	getItem(props) {
		return (props || this.props).item || {};
	}

	fetchInterfaceServiceDependentSteps(pageNumber) {
		const { dispatch, token, } = this.props;
		const item = this.getItem();
		const where = [
			{ name: 'id_type', value: WorkflowStepTypes.EXTERNAL },
			{ name: 'id_workflow_external', value: item.id_interface },
			{ name: 'id_interface_service', value: item.id },
		];
		const orderBy = 'id_workflow, id_workflow_version, initial_step, order_index';
		const success = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const failure = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		if (pageNumber === 1) {
			dispatch(workflowStepActions.setProp(
				'fetchDependentSteps', (page) => this.fetchInterfaceServiceDependentSteps(page)
			));
		}
		dispatch(workflowStepActions.setProp(
			'modalTitle', i18n.t('DependentWorkflowsFromInterfaceService', item)
		));
		dispatch(workflowStepActions.setProp('fetchingDependents', true));
		dispatch(workflowStepActions.setProp('temp', []));
		// dispatch(workflowStepActions.setProp('tempNumber', 1));
		// dispatch(workflowStepActions.setProp('tempRows', 0));
		dispatch(workflowStepActions.fetchPage({
			token,
			temp: true,
			where,
			orderBy,
			pageNumber,
			success,
			failure,
		}));
	}

	handleChange(property, value) {
		const item = this.getItem();
		item[property] = value;
		this.setState({ updaterProp: new Date() });
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			const { parent } = this.props;
			if (!item.id) {
				item.id = 0;
				item.id_interface = parent.id;
				item.order_index = 0;
				// (parent.interface_services && parent.interface_services.length) || 0;
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('interfaces.add') || user.hasPermissions('interfaces.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name && item.code && item.returned_values;
	}

	render() {
		const { id,
			name, code,
			returned_values: returnedValues,
		} = this.getItem();

		return (
			<div role="form">
				{id && <div style={{ textAlign: 'right' }}>
					<Button
						label={i18n.t('DependentWorkflows')} size="btn-sm"
						icon="fa-search"
						color="btn-info" rounded className="m-r-sm m-b-sm"
						onClick={() => {
							this.fetchInterfaceServiceDependentSteps(1);
							ModalFactory.show('dependentWorkflowsModal');
						}}
					/>
				</div>
				}
				{id &&
					<FormGroup isValid={!!id}>
						<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
						<Input type="number" className="form-control" value={id} disabled />
					</FormGroup>
				}
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						value={name || ''} placeholder={i18n.t('Name')}
						onFieldChange={(e) => this.handleChange('name', e.target.value)}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>
				<FormGroup isValid={!!code}>
					<label className="control-label" htmlFor="code">{i18n.t('Code')}:</label>
					<Tooltip className="balloon" tag="TooltipServiceCodeField">
						<TextArea
							name="code"
							style={{ resize: 'unset', }}
							placeholder={i18n.t('ServiceCodeDescription')}
							value={code || ''}
							onFieldChange={(e) => this.handleChange('code', e.target.value)}
							rows="8"
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>
				<FormGroup isValid={!!returnedValues}>
					<label className="control-label" htmlFor="returned_values">
						{i18n.t('ReturnedValues')}:
					</label>
					<Tooltip className="balloon" tag="TooltipServiceValuesField">
						<TagInput
							value={returnedValues || ''} placeholder={i18n.t('ServiceValuesDescription')}
							onChange={({ value }) => this.handleChange('returned_values', value)}
							disabled={this.isFieldDisabled()}
						/>
					</Tooltip>
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(InterfaceServiceModal);
