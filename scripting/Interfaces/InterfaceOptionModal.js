import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SliderPicker, } from 'react-color';
import tinycolor from 'tinycolor2';

import { mapStateToProps } from '../../outlets/ReduxOutlet';

import {
	Input,
} from '../../reactadmin/components/';
import i18n from '../../i18n';

/**
 * Componente que controla el formulario de los detalles de una opción retornada por una interaz.
 *
 * @class InterfaceOptionModal
 * @extends {Component}
 * @deprecated Las opciones de las interfaces fueron reemplazadas por los valores retornados de 
 * los servicios de las interfaces.
 */
class InterfaceOptionModal extends Component {

	componentWillReceiveProps(props) {
		this.setState({ color: tinycolor(props.item && props.item.color).toHsv() });
	}

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem() {
		const { item, parent } = this.props;
		if (item) {
			if (!item.id) {
				// TODO: Validar también que el nombre no sea sólo espacios'
				item.id_interface = parent.id;
				item.id_type = 1;
				item.order_index = (parent.interface_option && parent.interface_option.length) || 0;
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	render() {
		const { item, } = this.props;
		const { id, name, color, value } = item;

		return (
			<form role="form">
				<div className="form-group">
					<label className="block" htmlFor="name">{i18n.t('Name')}</label>
					<Input
						type="text" className="form-control" placeholder={i18n.t('Name')} value={name || ''}
						onFieldChange={(e/* , valid */) => this.handleChange('name', e.target.value)}
						required
					/>
				</div>

				<div className="form-group">
					<label className="-block" htmlFor="color">{i18n.t('Color')}</label>
					<div
						style={{
							backgroundColor: color,
							display: 'inline-block',
							margin: 5,
							border: '1px solid #cccccc',
							borderColor: '#d9d9d9',
							borderRadius: 3,
							alignItems: 'center',
							justifyContent: 'center',
						}}
					>
						<input
							className="form-control" type="text"
							value={color || ''}
							onChange={(e) => this.handleChange('color', e.target.value)}
							style={{
								backgroundColor: 'transparent',
								width: 'auto',
								fontWeight: 'bold',
								border: 'none',
							}}
						/>
					</div>

					<SliderPicker
						//TODO: Probar ChromePicker para permitir mejores tonos de colores
						color={color || 'white'}
						onChange={(newColor) => {
							this.handleChange('color', newColor.hex);
						}}
					/>
				</div>
				<div className="form-group">
					<label className="block" htmlFor="value">{i18n.t('Value')}</label>
					<Input
						type="text" className="form-control" placeholder={i18n.t('Value')} value={value || ''}
						onFieldChange={(e) => { this.handleChange('value', e.target.value); }}
						required
					/>
				</div>

				<div className="">
					<hr />
					<button type="button" onClick={(e) => this.saveItem(e)} className="btn btn-info">
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"// btn-block w-pad
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(InterfaceOptionModal);
