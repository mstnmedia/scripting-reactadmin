/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import i18n from '../../i18n';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	Page, Col,
	Panel, // Input,
	ModalFactory,
	DataTable,
	Pager,
} from '../../reactadmin/components/';

// import { InterfaceModal } from './';
import {
	setFilterHandles,
	renderListHeader,
	containsIgnoringCaseAndAccents
} from '../../Utils';

// const Factory = ModalFactory.modalFromFactory;
const dataSchema = {
	name: 'interfaces',
	description: '',
	fields: {
		server_url: i18n.t('URL'),
		label: i18n.t('Title'),
		id: i18n.t('ID'),
	}
};

const actions = ReduxOutlet('interfaces', 'interfaceBase').actions;

/**
 * Componente que controla la pantalla de consulta de interfaces disponibles.
 *
 * @class Interfaces
 * @extends {Component}
 */
class Interfaces extends Component {
	state = {
		interface: {},
	}

	componentWillMount() {
		const { dispatch, token } = this.props;
		dispatch(actions.setProp('list', []));
		setFilterHandles(this, actions, 'interfaceBases')
			.initFetch(token);
		// dispatch(actions.fetch(token));
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Interfaces'), to: '/' },
		];
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getReduxProp(prop, def) {
		return this.props.interfaceBases[prop] || def;
	}
	getItems() {
		const { interfaceBases: { list }, } = this.props;
		return list || [];
	}

	handleReduxChange(prop, value) {
		const { dispatch, } = this.props;
		dispatch(actions.setProp(prop, value));
	}
	handleItemChange(item) {
		const { dispatch, token, } = this.props;
		if (!item.id) {
			dispatch(actions.create(token, item,
				() => ModalFactory.hide('addInterfaceModal')
			));
		} else {
			dispatch(actions.update({
				token, item, success: () => ModalFactory.hide('addInterfaceModal'),
			}));
		}
	}

	newItemClick(e) {
		e.preventDefault();

		this.setState({ interface: {} });
		ModalFactory.show('addInterfaceModal');
	}

	handleItemEdit(item) {
		this.setState({ interface: Object.assign({}, item) });
		ModalFactory.show('addInterfaceModal');
	}

	selectRow(row) {
		this.props.router.push(`/data/${dataSchema.name}/${row.id}`);
	}

	render() {
		const pageSize = this.getReduxProp('pageSize', 10);
		const setValidPageNumber = (_totalRows, _pageSize, _pageNumber) => {
			const maxPage = Math.ceil(_totalRows / _pageSize);
			if (maxPage < _pageNumber) {
				this.handleReduxChange('pageNumber', maxPage || 1);
			}
		};
		const textFilter = this.getReduxProp('textFilter', '');
		const getItems = (_textFilter) => {
			const items = this.getItems().filter(i => !_textFilter
				|| containsIgnoringCaseAndAccents(i.id, _textFilter)
				|| containsIgnoringCaseAndAccents(i.name, _textFilter)
				|| containsIgnoringCaseAndAccents(i.label, _textFilter)
				|| containsIgnoringCaseAndAccents(i.server_url, _textFilter)
			);
			return {
				items,
				totalRows: items.length,
			};
		};
		const { items, totalRows, } = getItems(textFilter);
		const pageNumber = this.getReduxProp('pageNumber', 1);
		const onPageChange = ({ newPage }) => {
			this.handleReduxChange('pageNumber', newPage);
		};
		const onPageSizeChange = (_pageSize) => {
			this.handleReduxChange('pageSize', _pageSize);
			setValidPageNumber(totalRows, _pageSize, pageNumber);
		};
		const onFilterChange = (filter) => {
			this.handleReduxChange('textFilter', filter);
			const { totalRows: _totalRows, } = getItems(filter);
			setValidPageNumber(_totalRows, pageSize, pageNumber);
		};
		const renderHeader = () => renderListHeader({
			textFilter,
			onFilterChange,
			pageSize,
			onPageSizeChange,
		});
		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		return (
			<Page>
				<Col>
					<Panel>
						<DataTable
							{...{ pageSize, pageNumber, onPageChange, }}
							{...{ renderHeader, schema: dataSchema, rows, totalRows, }}
							link={(row) => this.selectRow(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Interfaces);
