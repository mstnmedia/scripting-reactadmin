import React, { Component } from 'react';
import i18n from '../../i18n';

/**
 * Componente usado en la lista de opciones de una interfaz para mostrar los detalles de cada uno.
 *
 * @export
 * @class InterfaceOptionItem
 * @extends {Component}
 * @deprecated Las opciones de las interfaces fueron reemplazadas por los valores retornados de 
 * los servicios de las interfaces.
 */
export default class InterfaceOptionItem extends Component {
	render() {
		const { item, onClick, onDelete } = this.props;
		const typeText = `${i18n.t('ReturnsValue')}: ${item.value}`;

		return (
			<div
				style={{
					display: 'flex',
					cursor: 'pointer',
					marginBottom: 2,
					borderBottom: '1px solid #eee',
				}}
			>
				<div onClick={onClick} style={{ flex: 'auto', padding: 5, }}>
					{item.name} - {typeText}
				</div>

				<div style={{ padding: 5, }}>
					<a href="#deleteRow" onClick={onDelete}>
						<i className="fa text-muted fa-trash-o" />
					</a>
				</div>
			</div>
		);
	}
}
