import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

/**
 * Componente de la versión de prototipo que simulaba resultados de una interfaz.
 *
 * @export
 * @class ResultSACS
 * @extends {Component}
 * @deprecated Todos los resultados aparecen en la pestaña Resultados externos.
 */
export default class ResultSACS extends Component {
	state = {
		showAll: false,
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	toggleShowAll() {
		this.setState({ showAll: !this.state.showAll });
	}
	render() {
		const { result } = this.props;
		const { showAll } = this.state;

		return (
			<div className="table-responsive">
				<table className="table table-condensed">
					<tbody>
						<tr>
							<td><b>DSLAM: </b>{result.prop}AMELLA2</td>
							<td><b>Puerto: </b>{result.prop}1-2-6-25</td>
							<td><b>Fabricante: </b>{result.prop}ALCATEL</td>
						</tr>
						<tr>
							<td><b>Condición Adm: </b>{result.prop}UP</td>
							<td><b>Condición Operativa: </b>{result.prop}UP</td>
							<td><b>Velocidad Internet: </b>{result.prop}2MB/768Kbps</td>
						</tr>
						<tr>
							<td><b>Estatus: </b>{result.prop}OK</td>
							<td><b>Rinitid: </b>{result.prop}0</td>
							<td><b>Atenuación: </b>{result.prop}False</td>
						</tr>
						<tr>
							<td><b>Desbalance: </b>{result.prop}False</td>
							<td><b>Ruido: </b>{result.prop}False</td>
							<td><b>Profile: </b>{result.prop}DP 2mb/768kbps</td>
						</tr>
						<tr>
							<td><b>Calidad de servicio: </b>{result.prop}N/A</td>
							<td><b>Sección IP: </b>{result.prop}False</td>
							<td><b>Vlan Internet: </b>{result.prop}False</td>
						</tr>
						<tr>
							<td><b>PVC 33: </b>{result.prop}True</td>
							<td><b>PVC 35: </b>{result.prop}False</td>
							<td><b>Vlan IPTV: </b>{result.prop}False</td>
						</tr>
						<tr>
							<td><b>User Id & Customer Id: </b>{result.prop}809220111/8092201111</td>
							<td><b>Tipo de tarjeta: </b>{result.prop}ADSL</td>
							<td><b>Service Profile IPTV: </b>{result.prop}True</td>
						</tr>
						<tr 
							className="cursor-pointer text-center btn-info" 
							style={{ cursor: 'pointer' }}
							onClick={() => this.toggleShowAll()}
						>
							<td colSpan="1000">
								{showAll ? 'Ver menos' : 'Ver más'}
							</td>
						</tr>
					</tbody>
					<tbody style={{ display: !showAll ? 'none' : null }}>
						<tr>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
						</tr>
						<tr>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
							<td><b>Other Prop: </b>More Data</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}
