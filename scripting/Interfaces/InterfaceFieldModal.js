import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';

import {
	FormGroup,
	Input,
	DropDown,
	DataTable,
} from '../../reactadmin/components/';
import {
	InterfaceFieldTypes,
	InterfaceFieldTypeNames,
	InterfaceFieldBooleanOptions
} from '../../store/enums';
import i18n from '../../i18n';
import { TransactionInterfaceFieldControl } from '../Transactions';

/**
 * Componente que controla el formulario de los detalles de un campo de una interfaz.
 *
 * @class InterfaceFieldModal
 * @extends {Component}
 */
class InterfaceFieldModal extends Component {

	componentWillMount() {
		// window.Scripting_InterfaceFieldModal = this;
	}

	render() {
		const { item, } = this.props;
		const { /* id, */ name, label,
			id_type: type, required, readonly,
			source,
			// order_index: order,
			children,
		} = item;
		const fieldTypeNames = InterfaceFieldTypeNames();
		const fieldTypes = Object.keys(fieldTypeNames)
			.map(value => ({ value, label: fieldTypeNames[value] }));
		const numericBooleanOptions = InterfaceFieldBooleanOptions();
		return (
			<form role="form">
				<FormGroup>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input value={name} disabled />
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="label">{i18n.t('Title')}:</label>
					<Input value={label} disabled />
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="type">{i18n.t('Type')}:</label>
					<DropDown items={fieldTypes} value={type} disabled />
				</FormGroup>
				{type === InterfaceFieldTypes.TABLE ?
					<FormGroup>
						<label className="control-label" htmlFor="type">{i18n.t('Fields')}:</label>
						<DataTable
							schema={{
								fields: {
									readonly: {
										type: 'Boolean',
										label: i18n.t('ReadOnly'),
									},
									required: {
										type: 'Boolean',
										label: i18n.t('Required'),
									},
									type: {
										label: i18n.t('Type'),
										format: (row) => fieldTypeNames[row.id_type]
									},
									label: i18n.t('Title'),
									name: i18n.t('Name'),
								}
							}}
							emptyRow
							rows={children || []}
						/>
					</FormGroup>
					: null
				}
				<FormGroup>
					<label className="control-label" htmlFor="default">{i18n.t('InitialValue')}:</label>
					<TransactionInterfaceFieldControl
						field={Object.assign({}, item,
							{ id_type: type === InterfaceFieldTypes.HIDDEN ? InterfaceFieldTypes.TEXT : type }
						)}
						{...{ form: {}, disabled: true, onKeyDown: false, onChange: () => false, }}
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="source">{i18n.t('Data')}:</label>
					<Input value={source} disabled />
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="required">{i18n.t('Required')}:</label>
					<DropDown items={numericBooleanOptions} value={required} disabled />
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="readonly">{i18n.t('ReadOnly')}:</label>
					<DropDown items={numericBooleanOptions} value={readonly} disabled />
				</FormGroup>

				<div className="">
					<hr />
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}
export default connect(mapStateToProps)(InterfaceFieldModal);
