import React, { Component } from 'react';

import { Input, } from '../../reactadmin/components/';
import i18n from '../../i18n';

/**
 * Componente que controla el formulario de los detalles de una interfaz.
 *
 * @export
 * @class InterfaceModal
 * @extends {Component}
 * @deprecated El usuario no modifica esta tabla.
 */
export default class InterfaceModal extends Component {

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	saveItem() {
		const { item, } = this.props;
		if (item) {
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	render() {
		const { id, name, label, server_url: serverURL, value_step: valueStep, } = this.props.item;
		return (
			<div className="panel-body m-b-none">
				<form role="form">
					<div className="form-group">
						<label htmlFor="name">{i18n.t('Name')}:</label>
						<Input
							type="text" className="form-control has-error" value={name}
							placeholder={i18n.t('Name')}
							onFieldChange={(e) => this.handleChange('name', e.target.value)}
							// errorMessage="Debe especificar un nombre"
							required
						/>
					</div>
					<div className="form-group">
						<label htmlFor="label">{i18n.t('Title')}</label>
						<Input
							type="text" className="form-control has-error" value={label}
							placeholder={i18n.t('Title')}
							onFieldChange={(e) => this.handleChange('label', e.target.value)}
							// errorMessage="Debe especificar un título"
							required
						/>
					</div>
					<div className="form-group">
						<label htmlFor="server_url">{i18n.t('URL')}</label>
						<Input
							type="text" className="form-control has-error" value={serverURL}
							placeholder={i18n.t('URL')}
							onFieldChange={(e) => this.handleChange('server_url', e.target.value)}
							// errorMessage="Debe especificar una URL"
							required
						/>
					</div>
					<div className="form-group">
						<label htmlFor="value_step">{i18n.t('ReturnedValues')}</label>
						<Input
							type="text" className="form-control has-error" value={valueStep}
							placeholder={i18n.t('ReturnedValues')}
							onFieldChange={(e) => this.handleChange('value_step', e.target.value)}
							// errorMessage="Debe especificar una nombre"
							required
						/>
					</div>
				</form>
				<div className="">
					<hr />
					<button type="button" onClick={(e) => this.saveItem(e)} className="btn btn-info">
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}
