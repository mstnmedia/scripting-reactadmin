// import React from 'react';

// import {
//   convertFromRaw,
//   convertToRaw,
//   CompositeDecorator,
//   ContentState,
//   Editor,
//   EditorState,
//   RichUtils,
// } from 'draft-js';
//
// export default class LinkEditorExample extends React.Component {
//   constructor(props) {
//     super(props);
//     const decorator = new CompositeDecorator([
//       {
//         strategy: findLinkEntities,
//         component: Link,
//       },
//     ]);
//     this.state = {
//       editorState: EditorState.createEmpty(decorator),
//       showURLInput: false,
//       urlValue: '',
//     };
//     this.focus = () => this.refs.editor.focus();
//     this.onChange = (editorState) => this.setState({editorState});
//     this.logState = () => {
//       const content = this.state.editorState.getCurrentContent();
//       console.log(convertToRaw(content));
//     };
//     this.promptForLink = this._promptForLink.bind(this);
//     this.onURLChange = (e) => this.setState({urlValue: e.target.value});
//     this.confirmLink = this._confirmLink.bind(this);
//     this.onLinkInputKeyDown = this._onLinkInputKeyDown.bind(this);
//     this.removeLink = this._removeLink.bind(this);
//   }
//   _promptForLink(e) {
//     e.preventDefault();
//     const {editorState} = this.state;
//     const selection = editorState.getSelection();
//     if (!selection.isCollapsed()) {
//       const contentState = editorState.getCurrentContent();
//       const startKey = editorState.getSelection().getStartKey();
//       const startOffset = editorState.getSelection().getStartOffset();
//       const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
//       const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
//       let url = '';
//       if (linkKey) {
//         const linkInstance = contentState.getEntity(linkKey);
//         url = linkInstance.getData().url;
//       }
//       this.setState({
//         showURLInput: true,
//         urlValue: url,
//       }, () => {
//         setTimeout(() => this.refs.url.focus(), 0);
//       });
//     }
//   }
//   _confirmLink(e) {
//     e.preventDefault();
//     const {editorState, urlValue} = this.state;
//     const contentState = editorState.getCurrentContent();
//     const contentStateWithEntity = contentState.createEntity(
//       'LINK',
//       'MUTABLE',
//       {url: urlValue}
//     );
//     const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
//     const newEditorState = EditorState.set(editorState,
//            { currentContent: contentStateWithEntity });
//     this.setState({
//       editorState: RichUtils.toggleLink(
//         newEditorState,
//         newEditorState.getSelection(),
//         entityKey
//       ),
//       showURLInput: false,
//       urlValue: '',
//     }, () => {
//       setTimeout(() => this.refs.editor.focus(), 0);
//     });
//   }
//   _onLinkInputKeyDown(e) {
//     if (e.which === 13) {
//       this._confirmLink(e);
//     }
//   }
//   _removeLink(e) {
//     e.preventDefault();
//     const {editorState} = this.state;
//     const selection = editorState.getSelection();
//     if (!selection.isCollapsed()) {
//       this.setState({
//         editorState: RichUtils.toggleLink(editorState, selection, null),
//       });
//     }
//   }
//   render() {
//     let urlInput;
//     if (this.state.showURLInput) {
//       urlInput =
//         <div style={styles.urlInputContainer}>
//           <input
//             onChange={this.onURLChange}
//             ref="url"
//             style={styles.urlInput}
//             type="text"
//             value={this.state.urlValue}
//             onKeyDown={this.onLinkInputKeyDown}
//           />
//           <button onMouseDown={this.confirmLink}>
//             Confirm
//           </button>
//         </div>;
//     }
//     return (
//       <div style={styles.root}>
//         <div style={{marginBottom: 10}}>
//           Select some text, then use the buttons to add or remove links
//           on the selected text.
//         </div>
//         <div style={styles.buttons}>
//           <button
//             onMouseDown={this.promptForLink}
//             style={{marginRight: 10}}>
//             Add Link
//           </button>
//           <button onMouseDown={this.removeLink}>
//             Remove Link
//           </button>
//         </div>
//         {urlInput}
//         <div style={styles.editor} onClick={this.focus}>
//           <Editor
//             editorState={this.state.editorState}
//             onChange={this.onChange}
//             placeholder="Enter some text..."
//             ref="editor"
//           />
//         </div>
//         <input
//           onClick={this.logState}
//           style={styles.button}
//           type="button"
//           value="Log State"
//         />
//       </div>
//     );
//   }
// }
// function findLinkEntities(contentBlock, callback, contentState) {
//   contentBlock.findEntityRanges(
//     (character) => {
//       const entityKey = character.getEntity();
//       return (
//         entityKey !== null &&
//         contentState.getEntity(entityKey).getType() === 'LINK'
//       );
//     },
//     callback
//   );
// }
// const Link = (props) => {
//   const {url} = props.contentState.getEntity(props.entityKey).getData();
//   return (
//     <a href={url} style={styles.link}>
//       {props.children}
//     </a>
//   );
// };
// const styles = {
//   root: {
//     fontFamily: '\'Georgia\', serif',
//     padding: 20,
//     width: 600,
//   },
//   buttons: {
//     marginBottom: 10,
//   },
//   urlInputContainer: {
//     marginBottom: 10,
//   },
//   urlInput: {
//     fontFamily: '\'Georgia\', serif',
//     marginRight: 10,
//     padding: 3,
//   },
//   editor: {
//     border: '1px solid #ccc',
//     cursor: 'text',
//     minHeight: 80,
//     padding: 10,
//   },
//   button: {
//     marginTop: 10,
//     textAlign: 'center',
//   },
//   link: {
//     color: '#3b5998',
//     textDecoration: 'underline',
//   },
// };

// export default class RichEditorExample extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { editorState: EditorState.createEmpty() };
//     this.focus = () => this.refs.editor.focus();
//     this.onChange = (editorState) => this.setState({ editorState });
//     this.handleKeyCommand = this._handleKeyCommand.bind(this);
//     this.onTab = this._onTab.bind(this);
//     this.toggleBlockType = this._toggleBlockType.bind(this);
//     this.toggleInlineStyle = this._toggleInlineStyle.bind(this);
//   }
//   _handleKeyCommand(command, editorState) {
//     const newState = RichUtils.handleKeyCommand(editorState, command);
//     if (newState) {
//       this.onChange(newState);
//       return true;
//     }
//     return false;
//   }
//   _onTab(e) {
//     const maxDepth = 4;
//     this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
//   }
//   _toggleBlockType(blockType) {
//     this.onChange(
//       RichUtils.toggleBlockType(
//         this.state.editorState,
//         blockType
//       )
//     );
//   }
//   _toggleInlineStyle(inlineStyle) {
//     this.onChange(
//       RichUtils.toggleInlineStyle(
//         this.state.editorState,
//         inlineStyle
//       )
//     );
//   }
//   render() {
//     const { editorState } = this.state;
//     // If the user changes block type before entering any text, we can
//     // either style the placeholder or hide it. Let's just hide it now.
//     let className = 'RichEditor-editor';
//     const contentState = editorState.getCurrentContent();
//     if (!contentState.hasText()) {
//       if (contentState.getBlockMap().first().getType() !== 'unstyled') {
//         className += ' RichEditor-hidePlaceholder';
//       }
//     }
//     return (
//       <div className="RichEditor-root">
//         <BlockStyleControls
//           editorState={editorState}
//           onToggle={this.toggleBlockType}
//         />
//         <InlineStyleControls
//           editorState={editorState}
//           onToggle={this.toggleInlineStyle}
//         />
//         <div className={className} onClick={this.focus}>
//           <Editor
//             blockStyleFn={getBlockStyle}
//             customStyleMap={styleMap}
//             editorState={editorState}
//             handleKeyCommand={this.handleKeyCommand}
//             onChange={this.onChange}
//             onTab={this.onTab}
//             placeholder="Tell a story..."
//             ref="editor"
//             spellCheck
//           />
//         </div>
//       </div>
//     );
//   }
// }
// // Custom overrides for "code" style.
// const styleMap = {
//   CODE: {
//     backgroundColor: 'rgba(0, 0, 0, 0.05)',
//     fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
//     fontSize: 16,
//     padding: 2,
//   },
// };
// function getBlockStyle(block) {
//   switch (block.getType()) {
//     case 'blockquote': return 'RichEditor-blockquote';
//     default: return null;
//   }
// }
// class StyleButton extends React.Component {
//   constructor() {
//     super();
//     this.onToggle = (e) => {
//       e.preventDefault();
//       this.props.onToggle(this.props.style);
//     };
//   }
//   render() {
//     let className = 'RichEditor-styleButton';
//     if (this.props.active) {
//       className += ' RichEditor-activeButton';
//     }
//     return (
//       <span className={className} onMouseDown={this.onToggle}>
//         {this.props.label}
//       </span>
//     );
//   }
// }
// const BLOCK_TYPES = [
//   { label: 'H1', style: 'header-one' },
//   { label: 'H2', style: 'header-two' },
//   { label: 'H3', style: 'header-three' },
//   { label: 'H4', style: 'header-four' },
//   { label: 'H5', style: 'header-five' },
//   { label: 'H6', style: 'header-six' },
//   { label: 'Blockquote', style: 'blockquote' },
//   { label: 'UL', style: 'unordered-list-item' },
//   { label: 'OL', style: 'ordered-list-item' },
//   { label: 'Code Block', style: 'code-block' },
// ];
// const BlockStyleControls = (props) => {
//   const { editorState } = props;
//   const selection = editorState.getSelection();
//   const blockType = editorState
//     .getCurrentContent()
//     .getBlockForKey(selection.getStartKey())
//     .getType();
//   return (
//     <div className="RichEditor-controls">
//       {BLOCK_TYPES.map((type) =>
//         <StyleButton
//           key={type.label}
//           active={type.style === blockType}
//           label={type.label}
//           onToggle={props.onToggle}
//           style={type.style}
//         />
//       )}
//     </div>
//   );
// };
// const INLINE_STYLES = [
//   { label: 'Bold', style: 'BOLD' },
//   { label: 'Italic', style: 'ITALIC' },
//   { label: 'Underline', style: 'UNDERLINE' },
//   { label: 'Monospace', style: 'CODE' },
// ];
// const InlineStyleControls = (props) => {
//   const currentStyle = props.editorState.getCurrentInlineStyle();
//   return (
//     <div className="RichEditor-controls">
//       {INLINE_STYLES.map(type =>
//         <StyleButton
//           key={type.label}
//           active={currentStyle.has(type.style)}
//           label={type.label}
//           onToggle={props.onToggle}
//           style={type.style}
//         />
//       )}
//     </div>
//   );
// };

import React, { Component } from 'react';
import ReactDrafts from 'react-drafts';
import 'react-drafts/dist/react-drafts.css';
import {
	convertFromRaw,
	EditorState,
} from 'draft-js';

import Editor, { composeDecorators } from 'draft-js-plugins-editor';

import createImagePlugin from 'draft-js-image-plugin';
import createAlignmentPlugin from 'draft-js-alignment-plugin';
import createFocusPlugin from 'draft-js-focus-plugin';
import createResizeablePlugin from 'draft-js-resizeable-plugin';
// import createBlockDndPlugin from 'draft-js-drag-n-drop-plugin';
// import createDragNDropUploadPlugin from 'draft-js-drag-n-drop-upload-plugin';
import './Test1/editorStyles.css';
// import mockUpload from './mockUpload';

const focusPlugin = createFocusPlugin();
const resizeablePlugin = createResizeablePlugin();
// const blockDndPlugin = createBlockDndPlugin();
const alignmentPlugin = createAlignmentPlugin();
const { AlignmentTool } = alignmentPlugin;

const decorator = composeDecorators(
	resizeablePlugin.decorator,
	alignmentPlugin.decorator,
	focusPlugin.decorator,
	// blockDndPlugin.decorator,
);
const imagePlugin = createImagePlugin({ decorator });

// const dragNDropFileUploadPlugin = createDragNDropUploadPlugin({
//     handleUpload: mockUpload,
//     addImage: imagePlugin.addImage,
// });

const plugins = [
	// dragNDropFileUploadPlugin,
	// blockDndPlugin,
	focusPlugin,
	alignmentPlugin,
	resizeablePlugin,
	imagePlugin
];

/**
 * Componente de prueba de funciones.
 *
 * @class DemoEditor
 * @extends {Component}
 */
class DemoEditor extends Component {
	constructor() {
		super();

		this.state = {
			isSaving: false,
			lastSavedAt: null
		};

		this.handleLoadSavedContent = this.handleLoadSavedContent.bind(this);
		this.handleFileUpload = this.handleFileUpload.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.handleClear = this.handleClear.bind(this);
	}

	handleLoadSavedContent() {
		const saved = localStorage.getItem('myContent');
		return saved || '';
	}

	handleFileUpload(file) {
		return Promise.resolve({
			src: file.preview,
			name: file.name
		});
	}

	handleSave() {
		this.setState({
			isSaving: true
		}, () => {
			this.editor.save().then(content => {
				localStorage.setItem('myContent', content);

				const date = new Date();
				setTimeout(() => {
					this.setState({
						isSaving: false,
						lastSavedAt: date.toLocaleString()
					});
				}, 1000);
			});
		});
	}

	handleClear() {
		localStorage.removeItem('myContent');
		this.editor.clear();
	}

	render() {
		const { isSaving, lastSavedAt } = this.state;
		const storedContent = this.handleLoadSavedContent();

		return (
			<div className="col-md-6" style={{ background: 'white', position: 'relative', }}>
				<div className="demo-editor">
					<p style={{ position: 'absolute', top: '16px', right: '10%', color: '#bebebe' }}>
						{lastSavedAt && `Last saved: ${lastSavedAt || ''}`}
					</p>
					<div className="drafts-editor__external-controls">
						<button
							className="drafts-editor__external-control clear"
							onClick={this.handleClear}
						>Clear</button>
						<button
							className="drafts-editor__external-control save"
							onClick={this.handleSave}
						>
							{isSaving ? 'Saving' : 'Save'}
						</button>
					</div>
					<ReactDrafts
						ref={editor => (this.editor = editor)}
						content={storedContent}
						onFileUpload={this.handleFileUpload}
						allowPhotoLink
						allowPhotoSizeAdjust
						maxImgWidth={960}
						linkInputAcceptsFiles
						exportTo="html"
						plugins={plugins}
					/>
				</div>
			</div>
		);
	}
}

export default DemoEditor;
