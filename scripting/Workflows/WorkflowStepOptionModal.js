import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { SliderPicker, } from 'react-color';
import tinycolor from 'tinycolor2';

import { mapStateToProps } from '../../outlets/ReduxOutlet';

import { Input, ButtonGroup, FormGroup, Fa } from '../../reactadmin/components/';
import WorkflowStepSearch from './WorkflowStepSearch';
import { WorkflowStepTypes, WorkflowStepOptionTypes } from '../../store/enums';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

/**
 * Componente que controla el formulario de los detalles de una opcion de un paso.
 *
 * @class WorkflowStepOptionModal
 * @extends {PureComponent}
 */
class WorkflowStepOptionModal extends PureComponent {

	state = {
		updaterProp: null,
	}

	componentWillReceiveProps(props) {
		this.setState({ color: tinycolor(props.item && props.item.color).toHsv() });
		// window.Scripting_WorkflowStepOptionModal = this;
	}

	onWorkflowStepClick(e) {
		e.preventDefault();
		const { goToWorkflowStep } = this.props;
		if (isFunction(goToWorkflowStep)) {
			goToWorkflowStep(this.getItem());
		}
	}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(property, value) {
		const item = this.getItem();
		item[property] = value;
		this.setState({ updaterProp: new Date() });
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('worflows.add') || user.hasPermissions('workflows.edit');
	}
	isFieldDisabled(iff) {
		if (iff) return iff;
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}
	isValid() {
		const { name, id_type: type, value } = this.getItem();
		if (name && type) {
			if (type === WorkflowStepOptionTypes.STEP) {
				return !!value;
			} else if (type === WorkflowStepOptionTypes.VALUE) {
				return value && String(value).trim();
			}
		}
		return false;
	}

	saveItem() {
		if (this.isValid()) {
			const { step } = this.props;
			const item = this.getItem();
			if (!item.id) {
				item.id_center = step.id_center;
				item.id_workflow = step.id_workflow;
				item.id_workflow_version = step.id_workflow_version;
				item.id_workflow_step = step.id;
				item.id_workflow_step = step.id;
				item.order_index = item.order_index || 0;

				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	renderType() {
		const item = this.getItem();
		if (item) {
			const { id_type: type, value, referedWorkflowStep: rStep } = item;

			switch (type) {
				case WorkflowStepOptionTypes.STEP:
					return ((!rStep || !value) &&
						<WorkflowStepSearch
							onSelect={(step) => {
								this.handleChange('value', step.id);
								this.handleChange('referedWorkflowStep', step);
								this.handleChange('guid_step', step.guid);
							}}
						/>
					);
				case WorkflowStepOptionTypes.VALUE:
					return (
						<FormGroup isValid={!!value}>
							<label className="control-label" htmlFor="value">{i18n.t('Value')}:</label>
							<Input
								type="text" className="form-control"
								placeholder={i18n.t('Value')} value={value || ''}
								onFieldChange={(e) => this.handleChange('value', e.target.value)}
								disabled={this.isFieldDisabled()}
							/>
						</FormGroup>
					);
				default:
					return null;
			}
		}

		return null;
	}

	render() {
		const { step, } = this.props;
		const item = this.getItem();
		const { id, name, color, id_type: type, order_index: orderIndex } = item;
		const rStep = item.referedWorkflowStep;
		return (
			<form role="form">
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control" placeholder={i18n.t('Name')} value={name || ''}
						onFieldChange={(e) => this.handleChange('name', e.target.value)}
						disabled={this.isFieldDisabled(step.id_type !== WorkflowStepTypes.REGULAR)}
						required
					/>
				</FormGroup>

				<FormGroup>
					<label className="control-label" htmlFor="orderNo">{i18n.t('OrderNo')}:</label>
					<Input
						type="number" className="form-control"
						placeholder={i18n.t('OrderNo')} value={orderIndex || 0}
						onFieldChange={(e) => this.handleChange('order_index', e.target.value)}
						disabled={this.isDisabled()}
						required
					/>
				</FormGroup>
				<FormGroup>
					<label className="control-label" htmlFor="color">{i18n.t('Color')}:</label>
					<div
						style={{
							backgroundColor: color,
							display: 'inline-block',
							margin: 5,
							border: '1px solid #cccccc',
							borderColor: '#d9d9d9',
							borderRadius: 3,
							alignItems: 'center',
							justifyContent: 'center',
						}}
					>
						<input
							className="form-control" type="text"
							value={color || ''}
							onChange={(e) => this.handleChange('color', e.target.value)}
							style={{
								backgroundColor: 'transparent',
								width: 'auto',
								fontWeight: 'bold',
								border: 'none',
							}}
							disabled={this.isFieldDisabled()}
						/>
					</div>

					<SliderPicker
						//TODO: Probar ChromePicker para permitir mejores tonos de colores
						color={color || 'white'}
						onChange={(newColor) => {
							this.handleChange('color', newColor.hex);
						}}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>

				<FormGroup isValid={!!type}>
					<label className="control-label" htmlFor="type">{i18n.t('Type')}:</label>
					<ButtonGroup
						items={{
							active: activeTypes[type],
							values: valueTypes,
						}}
						onChange={(selected) => {
							this.handleChange('id_type', selected);
							if (selected === WorkflowStepOptionTypes.VALUE) {
								WorkflowStepSearch.fetchStepSearch(this, '');
							}
							this.handleChange('value', '');
							this.handleChange('referedWorkflowStep', {});
						}}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				{type === WorkflowStepOptionTypes.STEP &&
					<div>
						<label htmlFor="selectedStep">{i18n.t('SelectedStep')}: </label> {
							item.value && rStep && rStep.name && (
								<span>
									<Link onClick={(e) => this.onWorkflowStepClick(e)}>
										{`${rStep.name} (${item.value})`}
									</Link>
									{!this.isFieldDisabled() && (
										<Fa
											icon="remove" color="red"
											style={{ padding: 5 }}
											onClick={(e) => {
												e.stopPropagation();
												this.handleChange('value', null);
												this.handleChange('referedWorkflowStep', {});
											}}
										/>
									)}
								</span>
							)
						}
					</div>
				}

				{this.renderType()}

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button"
							className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"// btn-block w-pad
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}
const activeTypes = {
	[WorkflowStepOptionTypes.STEP]: i18n.t('Step'),
	[WorkflowStepOptionTypes.VALUE]: i18n.t('Value'),
};
const valueTypes = {
	[i18n.t('Step')]: WorkflowStepOptionTypes.STEP,
	[i18n.t('Value')]: WorkflowStepOptionTypes.VALUE
};

export default connect(mapStateToProps)(WorkflowStepOptionModal);
