import React, { Component } from 'react';
import { Link } from 'react-router';
import { isFunction } from '../../Utils';

/**
 * Componente que muestra los detalles del paso en la lista de pasos agregados al flujo actual.
 *
 * @export
 * @class WorkflowStepItem
 * @extends {Component}
 */
export default class WorkflowStepItem extends Component {

	render() {
		const { step, workflow, canDelete, onDelete } = this.props;
		const showDelete = isFunction(onDelete) && canDelete !== false;
		return (
			<div className="col-xs-12">
				<div
					className="panel"
					style={{
						display: 'flex',
						cursor: 'pointer',
						marginBottom: 2,
						backgroundColor: '#C3E6FF',
						marginTop: 5
					}}
				>
					<Link
						to={`/data/workflows/${workflow.id}/step/${step.id}`}
						style={{ flex: 'auto', padding: 5, }}
					>{step.name}</Link>
					{(showDelete) &&
						<div style={{ padding: 5, }}>
							<a href="#deleteRow" onClick={onDelete}>
								{/* <i className="fa text-muted fa-trash-o" /> */}
								<span className="text-muted">Eliminar</span>
							</a>
						</div>
					}
				</div>
			</div>
		);
	}
}
