import React, { Component } from 'react';
import { connect } from 'react-redux';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Input,
	DataTable,
	Pager,
	TagInput,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { WorkflowModal } from '.';

const actions = ReduxOutlet('workflows', 'workflow').actions;
/**
 * Componente que permite al usuario filtrar un flujo y seleccionarlo.
 *
 * @class WorkflowSearch
 * @extends {Component}
 */
class WorkflowSearch extends Component {
	state = {
		item: {},
		selected: null,
		disabled: false,
		search: '',
		errormessage: '',
	}

	componentWillMount() {
		const { dispatch } = this.props;
		this.setState({ item: {} });
		dispatch(actions.setProp('temp', []));
		dispatch(actions.setProp('tempRows', 0));
		dispatch(actions.setProp('tempNumber', 1));
	}

	handleChange(search, newPageNumber) {
		this.setState({ search });
		const {
			dispatch, token,
			workflows: { item, tempSize: pageSize, },
		} = this.props;
		const pageNumber = newPageNumber || 1;

		const value = `${search}%`;
		const criteria = 'like';
		const separator = 'or';
		const where = [
			{ name: 'id', criteria: '!=', value: item.id },
			{
				group: [
					{ name: 'id', value, criteria, separator, },
					{ name: 'name', value, criteria, separator, },
					{ name: 'tags', value, criteria, separator, },
				],
			},
			{ name: 'deleted', value: '0' },
		];
		const success = (resp) => {
			// console.log(resp);
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const { items, totalRows } = data;
				if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
					const maxPage = Pager.getMaxPage(totalRows, pageSize);
					this.handleChange(search, maxPage);
				}
			}
		};
		dispatch(actions.fetchPage({
			token, where, pageNumber, pageSize, temp: true, success,
		}));
	}

	selectItem(row) {
		this.setState({ selected: row });
		if (this.props.onSelect) {
			this.props.onSelect(row);
		}
	}

	handleNewItem(item) {
		const { dispatch, token, } = this.props;
		const success = (data) => {
			this.setState({ item: {}, disabled: false });
			if (data.data && data.data.code === 200) {
				const newItem = data.data.items[0];
				this.selectItem(newItem);
				this.handleChange(newItem.name);
				$('[href="#TabHome"]').click();
			}
		};
		const failure = () => this.setState({ disabled: false });
		this.setState({ disabled: true });
		dispatch(actions.create(token, item, success, failure));
	}

	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}

	render() {
		const {
			workflows: {
				temp,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
			},
		} = this.props;
		const { item, search, } = this.state;
		const selected = this.props.selected || this.props.state;
		const rows = temp || [];
		const onPageChange = ({ newPage }) => {
			this.handleChange(search, newPage);
		};
		return (
			<section style={{ marginBottom: 0 }}>
				<ul className="nav nav-tabs">
					<li className="active"><a data-toggle="tab" href="#TabHome">{i18n.t('ToSearch')}</a></li>
					<li><a data-toggle="tab" href="#TabNew">{i18n.t('WorkflowNewTab')}</a></li>
				</ul>

				<div className="tab-content">
					<div id="TabHome" className="tab-pane fade in active">
						<section style={{ marginBottom: 0 }} >
							<div className="panel-body m-b-none">
								<div className="_form-group">
									<label className="block" htmlFor="search">{i18n.t('Search')}:</label>
									<Input
										type="text" className="form-control"
										value={search} placeholder={i18n.t('Name')}
										onFieldChange={(e) => this.handleChange(e.target.value, pageNumber)}
										style={{ borderColor: '#ccc', boxShadow: 'unset', }}
									/>
								</div>
								<DataTable
									hover
									schema={{
										fields: {
											tags: {
												label: i18n.t('Tags'),
												format: (row) => (<TagInput inline value={row.tags} />)
											},
											name: i18n.t('Name'),
											id: i18n.t('ID'),
										}
									}}
									link={(row) => this.selectItem(row)}
									{...{ selected, rows, totalRows, pageNumber, pageSize, onPageChange, }}
								/>
							</div>
						</section>
					</div>
					<div id="TabNew" className="tab-pane fade" style={{ paddingTop: 10 }}>
						<WorkflowModal
							item={item}
							onCreate={(newItem) => this.handleNewItem(newItem)}
							disabled={this.isDisabled()}
						/>
					</div>
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(WorkflowSearch);
