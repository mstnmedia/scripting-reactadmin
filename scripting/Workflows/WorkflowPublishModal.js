import React, { Component } from 'react';
import {
	FormGroup,
	TextArea,
	NumericInput,
} from '../../reactadmin/components/';
import i18n from '../../i18n';

/**
 * Componente que controla el formulario de publicación de flujos.
 *
 * @export
 * @class WorkflowPublishModal
 * @extends {Component}
 */
export default class WorkflowPublishModal extends Component {
	state = {}

	getCurrentVersion() {
		const { current } = this.props;
		if (current) {
			return current.version_id_version;
		}
		return undefined;
	}

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}
	isValid() {
		const { item, } = this.props;
		return item && item.version
			&& item.id_workflow && item.id_workflow_version;
	}

	saveItem() {
		if (this.isValid()) {
			this.props.onCreate(this.props.item);
		}
	}

	render() {
		const item = this.props.item || {};
		return (
			<div className="panel-body m-b-none">
				<div role="form">
					<FormGroup>
						<label htmlFor="activeversion">{i18n.t('ActiveVersion')}:</label>
						<span className="form-control" disabled>
							{this.getCurrentVersion() || i18n.t('NotApplyShort')}
						</span>
					</FormGroup>
					<FormGroup isValid={!!item.version}>
						<label htmlFor="newversion" className="control-label">{i18n.t('NewVersion')}:</label>
						<NumericInput
							placeholder={i18n.t('NewVersionNoDescription')}
							value={item.version}
							onFieldChange={(e) => this.handleChange('version', e.target.value)}
							disabled={!item.isItemWorkflow || this.isDisabled()}
							required
						/>
					</FormGroup>
					<FormGroup>
						<label htmlFor="notes">{i18n.t('Description')}:</label>
						<TextArea
							value={item.notes}
							placeholder={i18n.t('NewVersionNoteDescription')}
							onFieldChange={(e) => {
								this.handleChange('notes', e.target.value);
							}}
							disabled={this.isDisabled()}
						/>
					</FormGroup>
				</div>

				<div className="">
					<button
						type="button" onClick={(e) => this.saveItem(e)}
						className="btn btn-info"
						disabled={this.isDisabled() || !this.isValid()}
					>{i18n.t('Publish')}</button>
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}
