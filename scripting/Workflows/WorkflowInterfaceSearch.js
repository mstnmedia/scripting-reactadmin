import React from 'react';
// import { connect } from 'react-redux';

// import { ReduxOutlet, mapStateToProps } from '../../outlets/';

// import DataTable from '../../reactadmin/components/data/DataTable';
// import { Input } from '../../reactadmin/components/ui/';

// const interfaceActions = ReduxOutlet('interfaces', 'interface').actions;

// const dataSchema = {
// 	name: 'interfaces',
// 	description: '',
// 	fields: {
// 		label: {
// 			type: 'String',
// 			reference: '',
// 			label: 'Titulo'
// 		},
// 	}
// };

/**
 * Componente en desuso.
 *
 * @export
 * @class WorkflowInterfaceSearch
 * @extends {React.Component}
 * @deprecated
 */
export default class WorkflowInterfaceSearch extends React.Component {
// 	constructor(props) {
// 		super(props);
// 		this.state = {
// 			name: '',
// 			errormessage: ''
// 		};
// 	}

// 	handleChange(e) {
// 		const { value } = e.target;
// 		this.setState({ name: value });
// 		const { dispatch, token } = this.props;
// 		const query = [];
// 		query.push({ name: 'name', value: `${value}%`, criteria: 'like', });
// 		query.push({ name: 'label', value: `${value}%`, criteria: 'like', separator: 'or' });
// 		// query.push({ name: 'main', value: '0', separator: 'and' });
// 		dispatch(interfaceActions.fetch(token, query, '', true));
// 	}

// 	selectInterface(row) {
// 		this.props.onSelect(row);
// 	}

// 	render() {
// 		const { interfaces } = this.props;
// 		return (
// 			<section style={{ marginBottom: 0 }}>
// 				<ul className="nav nav-tabs">
// 					<li className="active"><a data-toggle="tab" href="#IFHome">Buscar</a></li>
// 				</ul>

// 				<div className="tab-content">
// 					<div id="IFHome" className="tab-pane fade in active">
// 						<section>
// 							<form role="form">
// 								<div className="form-group">
// 									<Input
// 										type="text" className="form-control has-error" value={this.state.name}
// 										placeholder={'Nombre'}
// 										onFieldChange={(e) => this.handleChange(e)}
// 									/>
// 									<DataTable
// 										schema={dataSchema} 
// 										rows={interfaces.temp}
// 										link={(row) => this.selectInterface(row)}
// 									/>
// 								</div>
// 							</form>
// 						</section>
// 					</div>
// 				</div>
// 			</section>
// 		);
// 	}
}

// export default connect(mapStateToProps)(WorkflowInterfaceSearch);
