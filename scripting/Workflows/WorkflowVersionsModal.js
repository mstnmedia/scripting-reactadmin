import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps, } from '../../outlets/ReduxOutlet';
import {
    Col,
    DataTable,
    Pager,
    ModalFactory,
    Button,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

/**
 * Componente que muestra al usuario la lista de versiones que ha tenido un flujo.
 *
 * @class WorkflowVersionsModal
 * @extends {Component}
 */
class WorkflowVersionsModal extends Component {
    state = {
        pageSize: 10,
        pageNumber: 1,
    }

    goToPreviousWorkflow(version) {
        const workflowID = version.id_workflow;
        const versionID = version.id_version;
        ModalFactory.hide(this.props.modalref);
        this.props.router.push(`/data/workflows/${workflowID}/version/${versionID}`);
    }

    openPublishModal({ e, ...rest }) {
        e.stopPropagation();
        if (this.canPublish()) {
            this.props.openPublishModal({ e, ...rest });
        }
    }

    canPublish() {
        const { user, openPublishModal, } = this.props;
        return user.hasPermissions('workflows.publish')
            && isFunction(openPublishModal);
    }

    tableSchema() {
        const { disabled, publish, } = this.props;
        const canPublish = this.canPublish();
        return {
            fields: {
                publish: canPublish && {
                    label: ' ',
                    cellStyle: { width: 60, },
                    format: (item) => (item.active !== 1 &&
                        <Button
                            className="btn-success btn-xs inline-block"
                            icon={disabled && publish.id_workflow_version === item.id
                                ? 'fa-spinner fa-spin'
                                : 'fa-plus-square'
                            }
                            label={i18n.t('Publish')}
                            onClick={(e) => this.openPublishModal({ e, item })}
                            disabled={disabled}
                        />
                    ),
                },
                notes: {
                    label: i18n.t('Notes'),
                    format: (row) => <div style={{ whiteSpace: 'pre' }}>{row.notes}</div>
                },
                docdate: {
                    type: 'DateTimeZ',
                    label: i18n.t('PublishedDate'),
                },
                active: {
                    type: 'Boolean',
                    label: i18n.t('ActiveVersion'),
                    format: (row) => row.active === 1,
                },
                id_version: i18n.t('Version'),
                id: i18n.t('ID'),
            },
        };
    }

    render() {
        // window.Scripting_WorkflowVersionsModal = this;
        const { versions: { temp } } = this.props;
        const { pageNumber, pageSize, } = this.state;
        const items = temp || [];

        const onPageChange = ({ newPage }) => {
            this.setState({ pageNumber: newPage });
        };
        const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
        const totalRows = items.length;
        return (
            <div>
                <DataTable
                    schema={this.tableSchema()}
                    emptyRow
                    {...{ rows, totalRows, pageSize, pageNumber, onPageChange, }}
                    link={(row) => this.goToPreviousWorkflow(row)}
                />
                <Col>
                    <hr />
                    <button
                        type="button" className="btn btn-danger"
                        data-dismiss="modal" aria-hidden="true"
                    >{i18n.t('Close')}</button>
                </Col>
            </div >
        );
    }
}

export default connect(mapStateToProps)(WorkflowVersionsModal);
