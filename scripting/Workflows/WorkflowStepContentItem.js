import React, { PureComponent } from 'react';
import { isFunction } from '../../Utils';

/**
 * Componente que muestra los detalles del contenido en la lista de contenidos agregados a un 
 * paso del flujo.
 *
 * @export
 * @class WorkflowStepContentItem
 * @extends {Component}
 */
export default class WorkflowStepContentItem extends PureComponent {
	render() {
		const { item, onClick, canDelete, onDelete, } = this.props;
		const showDelete = isFunction(onDelete) && canDelete !== false;
		return (
			<div
				style={{
					display: 'flex',
					cursor: 'pointer',
					marginBottom: 2,
					borderBottom: '1px solid #eee',
				}}
			>
				<div onClick={onClick} style={{ flex: 'auto', padding: 5, }}>
					{item.content.name}
				</div>
				{showDelete &&
					<div style={{ padding: 5, }}>
						<a href="#deleteRow" onClick={onDelete}>
							<i className="fa text-muted fa-trash-o" />
						</a>
					</div>
				}
			</div>
		);
	}
}
