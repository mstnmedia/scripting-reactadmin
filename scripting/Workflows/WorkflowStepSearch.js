import React, { Component } from 'react';
import { connect } from 'react-redux';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import DataTable from '../../reactadmin/components/data/DataTable';
import {
	Input, Pager,
} from '../../reactadmin/components/';

import WorkflowStepModal from './WorkflowStepModal';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
const actions = ReduxOutlet('workflows', 'workflowstep').actions;
/**
 * Componente que permite al usuario filtrar un paso del flujo actual y seleccionarlo.
 *
 * @class WorkflowStepSearch
 * @extends {Component}
 */
class WorkflowStepSearch extends Component {
	static fetchStepSearch(that, searchValue, page) {
		const {
			dispatch, token,
			workflows: { item: workflow },
			steps: { tempSize: pageSize, },
		} = that.props;
		const pageNumber = page || 1;
		const criteria = 'like';
		const separator = 'or';
		const value = `${searchValue}%`;
		const where = [];
		where.push({ name: 'id_workflow', value: workflow.id });
		where.push({ name: 'id_workflow_version', value: workflow.version_id });
		where.push({
			group: [
				{ name: 'id', criteria, value, separator, },
				{ name: 'name', criteria, value, separator, },
			]
		});
		const success = (resp) => {
			// console.log(resp);
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const { items, totalRows } = data;
				if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
					const maxPage = Pager.getMaxPage(totalRows, pageSize);
					that.handleChange(searchValue, maxPage);
				}
			}
		};
		dispatch(actions.fetchPage({
			token, where, pageNumber, pageSize, temp: true, success,
		}));
	}
	state = {
		step: {},
		value: '',
		selected: null,
		disabled: false,
		errormessage: '',
	}

	componentWillMount() {
		this.setState({ step: {} });
		const { dispatch } = this.props;
		dispatch(actions.setProp('temp', []));
		dispatch(actions.setProp('tempNumber', 1));
		dispatch(actions.setProp('tempRows', 0));
		WorkflowStepSearch.fetchStepSearch(this, '', 1);
	}
	handleChange(value, pageNumber) {
		this.setState({ value });
		WorkflowStepSearch.fetchStepSearch(this, value, pageNumber);
	}

	selectStep(selected) {
		this.setState({ selected });
		if (isFunction(this.props.onSelect)) {
			this.props.onSelect(selected);
		}
	}

	handleNewStep(step) {
		const { dispatch, token, workflows } = this.props;
		const workflow = workflows.item;
		this.setState({ disabled: true });
		dispatch(actions.createChild(
			token, step, workflowActions, workflow.id,
			(data) => {
				// console.log('WorkflowStepSearch > created step data: ', data);
				this.setState({ step: {}, disabled: false });
				if (data && data.code === 200) {
					const item = data.items[0];
					this.selectStep(item);
					this.handleChange(item.name);
					$('[href="#WSSHome"]').click();
				}
			}, () => this.setState({ disabled: false }),
		));
	}

	isDisable() {
		return this.props.disabled || this.state.disabled;
	}

	render() {
		const {
			steps: {
				temp,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
			},
			workflows: { item: workflow },
		} = this.props;
		const { value, selected, step, } = this.state;
		const rows = temp || [];
		const onPageChange = ({ newPage }) => {
			this.handleChange(value, newPage);
		};
		return (
			<section style={{ marginBottom: 0 }}>
				<ul className="nav nav-tabs">
					<li className="active"><a data-toggle="tab" href="#WSSHome">{i18n.t('ToSearch')}</a></li>
					<li><a data-toggle="tab" href="#WSSNew">{i18n.t('StepNewTab')}</a></li>
				</ul>

				<div className="tab-content">
					<div id="WSSHome" className="tab-pane fade in active">
						<section>
							<div className="form-group">
								<Input
									type="text" className="form-control has-error"
									placeholder={i18n.t('ToSearch')}
									value={value}
									onFieldChange={(e) => this.handleChange(e.target.value)}
								/>
								<DataTable
									hover
									schema={{
										fields: {
											name: i18n.t('Name'),
											id: i18n.t('ID'),
										}
									}}
									link={(row) => this.selectStep(row)}
									{...{ selected, rows, totalRows, pageNumber, pageSize, onPageChange, }}
								/>
							</div>
						</section>
					</div>
					<div id="WSSNew" className="tab-pane fade">
						<WorkflowStepModal
							item={step} workflow={workflow}
							onCreate={(newStep) => this.handleNewStep(newStep)}
							disabled={this.isDisable()}
						/>
					</div>
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(WorkflowStepSearch);
