import React, { Component } from 'react';
import { connect } from 'react-redux';

import CriteriaModal from './CriteriaModal';
import { DataTable, Input, Pager, } from '../../reactadmin/components/';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import i18n from '../../i18n';

const actions = ReduxOutlet('workflows', 'criteria').actions;
/**
 * Componente que permite al usuario filtrar un criterio y seleccionarlo.
 *
 * @class CriteriaSearch
 * @extends {Component}
 */
class CriteriaSearch extends Component {
	state = {
		item: {},
		selected: null,
		disabled: false,
		search: '',
		errormessage: '',
	}

	componentWillMount() {
		const { dispatch, } = this.props;
		dispatch(actions.setProp('temp', []));
		dispatch(actions.setProp('tempNumber', 1));
		dispatch(actions.setProp('tempRows', 0));
		this.resetItem(this.props);
		// window.Scripting_CriteriaSearch = this;
	}
	componentWillReceiveProps(props) {
		if (props.workflow && props.workflow !== this.props.workflow) {
			this.resetItem(props);
		}
	}

	handleChange(searchValue, pageNumber = 1) {
		this.setState({ search: searchValue });
		const {
			dispatch, token,
			workflows: { item: workflow, },
			criterias: { tempSize: pageSize, },
		} = this.props;
		const criteria = 'like';
		const separator = 'or';
		const value = `${searchValue}%`;
		const where = [];
		where.push({ name: 'id_center', value: workflow.id_center });
		where.push({
			group: [
				{ name: 'id', criteria, value, separator, },
				{ name: 'name', criteria, value, separator, },
			]
		});
		dispatch(actions.fetch(token, where, '', true));
		const success = (resp) => {
			// console.log(resp);
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const { items, totalRows } = data;
				if (totalRows > 0 && items.length === 0 && pageNumber > 1) {
					const maxPage = Pager.getMaxPage(totalRows, pageSize);
					this.handleChange(searchValue, maxPage);
				}
			}
		};
		dispatch(actions.fetchPage({
			token, where, pageNumber, pageSize, temp: true, success,
		}));
	}

	selectRow(selected) {
		this.setState({ selected });
		if (this.props.onSelect) {
			this.props.onSelect(selected);
		}
	}

	resetItem(props) {
		if (props.workflow) {
			this.setState({
				item: {
					id_center: props.workflow.id_center,
					center_name: props.workflow.center_name,
				},
				disabled: false,
			});
		}
	}
	handleNewItem(newItem) {
		const { dispatch, token, } = this.props;
		const success = (data) => {
			// console.log('CriteriaSearch > created criteria data: ', data);
			if (data.data && data.data.code === 200) {
				this.resetItem(this.props);
				const item = data.data.items[0];
				this.selectRow(item);
				this.handleChange(item.name);
				$('[href="#CrHome"]').click();
			}
		};
		const failure = () => this.setState({ disabled: false });
		this.setState({ disabled: true });
		dispatch(actions.create(token, newItem, success, failure));
	}

	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}

	render() {
		const {
			criterias: {
				temp,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
			},
		} = this.props;
		const { search, selected, item, } = this.state;
		const items = temp || [];
		const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
		const onPageChange = ({ newPage }) => {
			this.setState({ pageNumber: newPage });
		};
		return (
			<section style={{ marginBottom: 0 }}>
				<ul className="nav nav-tabs">
					<li className="active"><a data-toggle="tab" href="#CrHome">{i18n.t('ToSearch')}</a></li>
					<li><a data-toggle="tab" href="#CrNew">{i18n.t('CriteriaNewTab')}</a></li>
				</ul>

				<div className="tab-content">
					<div id="CrHome" className="tab-pane fade in active">
						<section style={{ marginBottom: 0 }} >
							<div className="panel-body m-b-none">
								<div className="form-group">
									<Input
										type="text" className="form-control"
										value={search} placeholder={i18n.t('Name')}
										onFieldChange={(e) => this.handleChange(e.target.value)}
									/>
									<DataTable
										hover
										schema={{
											fields: {
												name: i18n.t('Name'),
												// center_name: user.master ? i18n.t('Center') : undefined,
												id: i18n.t('ID'),
											}
										}}
										link={(row) => this.selectRow(row)}
										{...{ selected, rows, totalRows, pageNumber, pageSize, onPageChange, }}
									/>
								</div>
							</div>
						</section>
					</div>
					<div id="CrNew" className="tab-pane fade" style={{ paddingTop: 10 }}>
						<CriteriaModal
							item={item} hiddenClose centerDisabled
							onCreate={(newItem) => this.handleNewItem(newItem)}
							disabled={this.isDisabled()}
						/>
					</div>
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(CriteriaSearch);
