import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	ButtonGroup,
	Input,
	DropDown,
	AnimatedSpinner,
	FormGroup,
	Button,
} from '../../reactadmin/components/';

import { WorkflowSearch, } from './';
import {
	WorkflowStepTypes,
	WorkflowStepTypeNames,
	MapInterfaceFieldFrom,
	Interfaces,
	InterfaceFieldTypes,
	TransactionResultListTemplate,
} from '../../store/enums';
import i18n from '../../i18n';
import * as Utils from '../../reactadmin/Utils';
import { TransactionInterfaceFieldControl } from '../Transactions';

const interfaceActions = ReduxOutlet('interfaces', 'interfaceBase').actions;
/**
 * Componente que controla el formulario de los detalles de un paso.
 *
 * @class WorkflowStepModal
 * @extends {PureComponent}
 */
class WorkflowStepModal extends PureComponent {
	state = {
		fetchingInterface: {},
		fetchedInterface: {},
		updaterProp: new Date(),
	}
	componentWillMount() {
		if (this.props.setDisable) this.props.setDisable(false);
		this.fetchInterfaces();
		// window.Scripting_WorkflowStepModal = this;
	}

	getFetchingInterface(id) {
		return this.state.fetchingInterface[id];
	}
	getFetchedInterface(id, fetchIfAbsent) {
		const inter = this.state.fetchedInterface[id];
		if (fetchIfAbsent && !inter) {
			this.fetchInterface(id);
		}
		return inter;
	}
	setFetchedInterface(id, inter) {
		this.setState({
			fetchedInterface: Object.assign({}, this.state.fetchedInterface, { [id]: inter }),
		});
		this.setFetchingInterface(id, false);
	}
	setFetchingInterface(id, state) {
		this.setState({
			fetchingInterface: Object.assign({}, this.state.fetchingInterface, { [id]: state }),
		});
	}

	fetchInterfaces() {
		const { dispatch, token, } = this.props;
		dispatch(interfaceActions.fetch(token));
	}
	fetchInterface(id) {
		if (!id || id === MapInterfaceFieldFrom.FIXED || id === MapInterfaceFieldFrom.USER) {
			return;
		}
		if (this.getFetchingInterface(id) || this.getFetchedInterface(id)) {
			return;
		}
		const { dispatch, token, } = this.props;
		this.setFetchingInterface(id, true);
		dispatch(interfaceActions.fetchOne(token, id, (resp) => {
			if (resp.data.code === 200) {
				const inter = resp.data.items[0];
				this.setFetchedInterface(id, inter);
			}
		}, () => this.setFetchingInterface(id, false)));
	}

	saveItem() {
		const { item, workflow } = this.props;
		if (this.isValid()) {
			const newItem = Object.assign({}, item);
			newItem.preType = undefined;
			newItem.workflow = null;
			newItem.interface = null;
			newItem.workflow_step_interface_field = JSON.parse(
				JSON.stringify(item.workflow_step_interface_field)
			);
			//Removing old WSIFs of other interfaces 
			const wsifs = newItem.workflow_step_interface_field;
			for (let i = 0; i < wsifs.length; i++) {
				const wsif = wsifs[i];
				if (wsif.id_interface !== newItem.id_workflow_external) {
					wsifs.splice(i, 1);
					i--;
				}
				if (wsif.id_interface_source === MapInterfaceFieldFrom.FIXED
					&& Array.isArray(wsif.id_interface_field_source)) {
					wsif.id_interface_field_source = JSON.stringify(wsif.id_interface_field_source);
				}
			}
			if (!newItem.id) {
				newItem.id = 0;
				newItem.id_center = workflow.id_center;
				newItem.id_workflow = workflow.id;
				newItem.id_workflow_version = workflow.version_id;
				newItem.initial_step = false;
				newItem.guid = '';
				newItem.order_index = newItem.order_index || 0;
				newItem.workflow_step_content = [];
				newItem.workflow_step_option = [];
				this.props.onCreate(newItem);
			} else {
				this.props.onUpdate(newItem);
			}
		}
	}
	selectInterface(id) {
		this.handleChange('id_workflow_external', id);
		this.handleChange('id_interface_service', 0);
		this.fetchInterface(id);
	}
	selectInterfaceService(id) {
		this.handleChange('id_interface_service', id);
	}

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}
	changedMapInterface(id, map) {
		this.handleMapChange(map, 'id_interface_source', id);
		this.handleMapChange(map, 'id_interface_field_source', '');
		this.fetchInterface(id);
	}
	handleMapChange(map, prop, value) {
		const item = map;
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('worflows.add') || user.hasPermissions('workflows.edit');
	}
	isFieldDisabled(iff) {
		return this.isDisabled(iff) || !this.isAllowedUser();
	}
	isDisabled(iff) {
		return !!iff || this.props.disabled || this.state.disabled;
	}
	isValid() {
		const { item } = this.props;
		return item && item.name && item.id_type
			&& this.validateType(item);
	}
	isValidField(pField, form, fields, wsif) {
		const field = pField;
		field.isValid = [true];

		if (wsif && wsif.id_interface_source === MapInterfaceFieldFrom.USER) {
			return field;
		} else if (wsif && wsif.id_interface_source !== MapInterfaceFieldFrom.FIXED) {
			field.isValid[0] = !!wsif.id_interface_field_source;
			return field;
		}

		if (field.id_type === InterfaceFieldTypes.HIDDEN) {
			return field;
		}
		const value = field.name === TransactionResultListTemplate ? form : form[field.name];
		if (field.id_type === InterfaceFieldTypes.TABLE) {
			if (field.required && (!Array.isArray(value) || !value.length)) {
				field.isValid[0] = false;
			} else {
				const children = (field.children || []);
				// const allResults = [];
				for (let iR = 0; iR < value.length; iR++) {
					const row = value[iR];
					field.isValid[iR + 1] = [];
					for (let iC = 0; iC < children.length; iC++) {
						const child = children[iC];
						this.isValidField(child, row, fields);
						field.isValid[iR + 1][iC] = child.isValid;
					}
				}
				// field.isValid = allResults.filter(i => !i).length === 0;
			}
			return field;
		}
		if (!field.required && (!value)) {
			return field;
		}
		if (!field.Validate) {
			if (typeof field.onvalidate === 'string' && !!field.onvalidate.trim()) {
				field.Validate = new Function('value', 'form', 'field', 'fields', 'InterfaceFieldTypes', 'Utils', 'screen', field.onvalidate);  //eslint-disable-line max-len, no-new-func
			} else {
				field.Validate = () => {
					if (field.required) {
						return (!!value || value === 0 || value === false);
					}
					return true;
				};
			}
		}
		field.isValid[0] = field.Validate(value, form, field, fields, InterfaceFieldTypes, Utils, 'step'); // eslint-disable-line max-len
		return field;
	}
	validateType(item) {
		switch (item.id_type) {
			case WorkflowStepTypes.REGULAR:
				return true;
			case WorkflowStepTypes.WORKFLOW:
				return !!item.id_workflow_external;
			case WorkflowStepTypes.EXTERNAL: {
				if (!item.id_workflow_external || !item.id_interface_service) {
					return false;
				}
				const inter = this.getFetchedInterface(item.id_workflow_external);
				if (!inter) return false;
				const fields = ((inter && inter.interface_field) || []);
				const wsifs = item.workflow_step_interface_field;
				const form = {};
				const mapWSIF = {};
				fields.forEach((field) => {
					mapWSIF[field.name] = wsifs.find(w => w.id_interface_field === field.name);
					form[field.name] = mapWSIF[field.name].id_interface_field_source;
				});

				const isInvalid = (results) => {
					const filtered = results
						.filter(value => {
							if (Array.isArray(value)) {
								return isInvalid(value);
							}
							return !value;
						});
					return filtered.length > 0;
				};
				const results = fields
					.map(field => this.isValidField(field, form, fields, mapWSIF[field.name]))
					.filter(field => isInvalid(field.isValid));

				return results.length === 0;
			}
			default:
				return false;
		}
	}

	renderSubTypeFixedInput(map, field) {
		return (
			<TransactionInterfaceFieldControl
				field={field}
				form={{ [field.name]: map.id_interface_field_source || '' }}
				onChange={(name, value) => this.handleMapChange(
					map, 'id_interface_field_source', value
				)}
				disabled={this.isFieldDisabled()}
			/>
		);
	}
	renderSubType(map, field) {
		if (map && map.id_interface_source) {
			if (map.id_interface_source === MapInterfaceFieldFrom.FIXED) {
				return this.renderSubTypeFixedInput(map, field);
			} else if (map.id_interface_source !== MapInterfaceFieldFrom.USER) {
				const mapInterface = this.getFetchedInterface(map.id_interface_source, true);
				const mapInterfaceResults = (mapInterface && mapInterface.interface_results) || [];
				return (
					<DropDown
						editable
						items={mapInterfaceResults
							.slice()
							.sort((a, b) => (a.label > b.label ? 1 : -1))
							.map(i => ({ value: i.name, label: i.label, }))
						}
						value={map.id_interface_field_source}
						onChange={(e) => this.handleMapChange(
							map, 'id_interface_field_source', e.target.value
						)}
						emptyOption="SelectDisabled"
						disabled={this.isFieldDisabled()}
					/>
				);
			}
		}
		return null;
	}
	renderTypeControl() {
		const { item, interfaceBases: { list } } = this.props;
		const { fetchingInterface } = this.state;
		const {
			id_type: type,
			id_workflow_external: value,
			id_interface_service: serviceID,
		} = item;
		if (type === WorkflowStepTypes.WORKFLOW) {
			const workflow = item.workflow;
			return (
				<FormGroup isValid={!!workflow}>
					<label htmlFor="workflow" className="control-label">
						{i18n.t('Workflow')}:
						{workflow && <Button
							className="btn-info btn-xs"
							icon="fa-pencil"
							label={i18n.t('ToChange')}
							onClick={() => {
								this.handleChange('workflow', null);
								this.handleChange('id_workflow_external', null);
							}}
						/>}
					</label>
					{workflow ?
						<Input value={workflow.name || ''} disabled />
						:
						<WorkflowSearch
							onSelect={(selected) => {
								this.handleChange('workflow', selected);
								this.handleChange('id_workflow_external', selected.id);
							}}
						/>
					}
				</FormGroup>
			);
		} else if (type === WorkflowStepTypes.EXTERNAL) {
			const interfaces = (list || [])
				.map(i => ({
					id: i.id,
					value: i.id,
					label: i.label,
					hidden: i.delete,
					disabled: [
						Interfaces.SCRIPTING.TransactionInfoBase,
						Interfaces.SCRIPTING.CustomerInfoBase,
						Interfaces.SCRIPTING.SubscriptionInfoBase
					].indexOf(i.id) > -1,
				}))
				.sort(Utils.sortFunction('label'));
			const inter = this.getFetchedInterface(value, true);
			const fields = ((inter && inter.interface_field) || []);
			const stepFieldMaps = item.workflow_step_interface_field;
			if (fields.length) {
				for (const field of fields) {
					let map = stepFieldMaps.find(i => (i.id_interface_field === field.name));
					if (!map) {
						map = {
							id: 0,
							id_center: (item.id_center || 0),
							id_workflow_step: item.id,
							id_interface: inter.id,
							id_interface_field: field.name,
							id_interface_source: MapInterfaceFieldFrom.USER,
							id_interface_field_source: '',
						};
						stepFieldMaps.push(map);
					}
					field.map = map;
				}
			}
			const services = (inter && inter.interface_services) || [];
			return (
				<div>
					<FormGroup isValid={!!value}>
						<label htmlFor="inter" className="control-label">{i18n.t('Interface')}: </label>
						<DropDown
							style={{ width: 'unset', maxWidth: '100%', display: 'inline-block' }}
							items={interfaces}
							value={value}
							onChange={(e) => this.selectInterface(e.target.value * 1)}
							emptyOption={<option disabled={!!value}>{i18n.t('Select')}</option>}
							disabled={this.isFieldDisabled()}
						/>
					</FormGroup>
					<FormGroup isValid={!!serviceID}>
						<label htmlFor="service" className="control-label">
							{i18n.t('Service')}: <AnimatedSpinner show={fetchingInterface[value]} />
						</label>
						<DropDown
							style={{ width: 'unset', maxWidth: '100%', display: 'inline-block' }}
							items={services.map(i => ({ value: i.id, label: i.name }))}
							value={serviceID}
							onChange={(e) => this.selectInterfaceService(e.target.value * 1)}
							emptyOption={<option disabled={!!value}>{i18n.t('Select')}</option>}
							disabled={this.isFieldDisabled()}
						/>
					</FormGroup>
					{fields.length ?
						<div className="table-responsive">
							<table className="table">
								<thead>
									<tr><th>{i18n.t('DataSource')}</th></tr>
								</thead>
								<tbody>
									{fields.map(field => {
										const map = field.map;
										const isValid = !field.required
											|| map.id_interface_source === MapInterfaceFieldFrom.USER
											|| !!map.id_interface_field_source;
										return (
											<tr key={field.name}>
												<td>
													<FormGroup isValid={isValid}>
														<label
															htmlFor={field.name}
															className="control-label"
														>{field.required && '* '}{field.label}:</label>
														<DropDown
															items={[
																{ value: MapInterfaceFieldFrom.FIXED, label: i18n.t('FixedValue') },
																{ value: MapInterfaceFieldFrom.USER, label: i18n.t('Salesman') },
																...interfaces.map(i => ({
																	value: i.id,
																	label: i18n.t('Interface_Name', { name: i.label })
																})),
															]}
															value={map.id_interface_source}
															onChange={(e) => this.changedMapInterface(e.target.value, map)}
															emptyOption={<option disabled={!!value}>{i18n.t('Select')}</option>}
															disabled={this.isFieldDisabled()}
														/>
														{this.renderSubType(map, field)}
													</FormGroup>
												</td>
											</tr>
										);
									})}
								</tbody>
							</table>
						</div>
						: <div>
							{this.getFetchingInterface(value) ? [
								<AnimatedSpinner key="1" show={fetchingInterface[value]} />,
								i18n.t('Loading')
							] : ''}
						</div>
					}
				</div>
			);
		}
		return <div />;
	}
	render() {
		const { item } = this.props;
		const {
			id, name,
			id_type: type,
			order_index: orderIndex,
			initial_step: isInitial,
		} = item;
		const preType = item.preType || type;
		const disableType = !!id && !(isInitial && preType === WorkflowStepTypes.UNDEFINED);

		const activeTypes = {
			[WorkflowStepTypes.REGULAR]: WorkflowStepTypeNames()[WorkflowStepTypes.REGULAR],
			[WorkflowStepTypes.WORKFLOW]: WorkflowStepTypeNames()[WorkflowStepTypes.WORKFLOW],
			[WorkflowStepTypes.EXTERNAL]: WorkflowStepTypeNames()[WorkflowStepTypes.EXTERNAL],
		};
		const valuesTypes = {
			[WorkflowStepTypeNames()[WorkflowStepTypes.REGULAR]]: WorkflowStepTypes.REGULAR,
			[WorkflowStepTypeNames()[WorkflowStepTypes.WORKFLOW]]: WorkflowStepTypes.WORKFLOW,
			[WorkflowStepTypeNames()[WorkflowStepTypes.EXTERNAL]]: WorkflowStepTypes.EXTERNAL,
		};
		return (
			<div role="form">
				{id && <FormGroup>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input className="form-control" value={id} disabled />
				</FormGroup>}
				<FormGroup isValid={!!name}>
					<label htmlFor="name" className="control-label">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						placeholder={i18n.t('Name')} value={name}
						onFieldChange={(e) => this.handleChange('name', e.target.value)}
						disabled={this.isFieldDisabled()}
						required
					/>
				</FormGroup>
				<FormGroup>
					<label htmlFor="order" className="control-label">{i18n.t('OrderNo')}:</label>
					<Input
						type="number" className="form-control"
						placeholder={i18n.t('OrderNo')} value={orderIndex || 0}
						onFieldChange={(e) => this.handleChange('order_index', e.target.value)}
						disabled={this.isFieldDisabled()}
						required
					/>
				</FormGroup>
				<FormGroup isValid={!!type}>
					<label htmlFor="type" className="control-label block">{i18n.t('Type')}:</label>
					<ButtonGroup
						items={{ active: activeTypes[type], values: valuesTypes, }}
						onChange={(newType) => {
							if (!item.preType) {
								item.preType = type;
							}
							this.handleChange('id_type', newType);
							this.handleChange('id_workflow_external', 0);
							this.handleChange('id_interface_service', 0);
							this.handleChange('workflow_step_interface_field', []);
							this.handleChange('workflow', null);
							this.handleChange('interface', null);
						}}
						disabled={this.isFieldDisabled(disableType)}
					/>
				</FormGroup>

				{this.renderTypeControl()}

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button"
							onClick={(e) => this.saveItem(e)}
							className="btn btn-info"
							disabled={!this.isValid() || this.isDisabled()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(WorkflowStepModal);
