import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	// Row,
	Col, Page,
	Panel, Button,
	ModalFactory, Factory, AnimatedSpinner, DataTable,
} from '../../reactadmin/components/';

import { WorkflowStepTypes, ContentTypes, WorkflowStepTypeNames, } from '../../store/enums';
import {
	WorkflowStepContentItem,
	WorkflowStepOptionItem,
	WorkflowStepModal,
	WorkflowStepOptionModal,
	WorkflowStepContentModal,
} from './';
import i18n from '../../i18n';
import { ContentModal, } from '../Contents';
import ContentSearch from '../Contents/ContentSearch';
import { ContentDetailModal } from '../Transactions';
import { DialogModal } from '../DialogModal';
import { respErrorDetails, isFunction } from '../../reactadmin/Utils';

const { UNDEFINED, REGULAR, WORKFLOW, EXTERNAL } = WorkflowStepTypes;

const actions = ReduxOutlet('workflows', 'workflowstep').actions;
const contentActions = ReduxOutlet('contents', 'content').actions;
const interfaceActions = ReduxOutlet('interfaces', 'interfaceBase').actions;
const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
const workflowStepContentActions = ReduxOutlet('workflows', 'workflowStepContent').actions;
const workflowStepOptionActions = ReduxOutlet('workflows', 'workflowStepOption').actions;

/**
 * Componente que muestra la configuración de un paso del flujo actual para la versión editable.
 *
 * @class WorkflowStep
 * @extends {PureComponent}
 */
class WorkflowStep extends PureComponent {
	state = {
		step: {},
		option: {},
		stepContent: {},
		stepDisabled: false,
		optionDisabled: false,
		stepContentDisabled: false,
		content: {},
		contentDisabled: false,
		confirmModalProps: {},
		stepDeleting: {},
	}
	componentWillMount() {
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
		this.fetchData(this.props);
		window.Scripting_WorkflowStep = this;
	}
	componentWillReceiveProps(props) {
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
		if (props.params.id_workflow !== this.props.params.id_workflow) {
			this.fetchCurrentWorkflow(props);
		}
		if (props.params.id !== this.props.params.id) {
			this.fetchCurrentStep(props);
			this.fetchDependentSteps(props);
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	getBreadcrumbItems() {
		const { id_workflow: workflowID, id: stepID } = this.props.params;
		const { steps: { item: step }, } = this.props;
		const workflowName = (step.workflow_name || `#${workflowID}`);
		const stepName = (step.name || `#${stepID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Workflows'), to: '/data/workflows' },
			{ label: `${i18n.t('Workflow')} ${workflowName}`, to: `/data/workflows/${workflowID}`, },
			{ label: `${i18n.t('Step')} ${stepName}`, },
		];
	}
	getSystem(id) {
		const { interfaceBases: { list }, } = this.props;
		const inter = (list || []).find(i => i.id === id);
		return inter || {};
	}
	getSystemName(item) {
		if (item.id_type === WorkflowStepTypes.EXTERNAL) {
			const inter = this.getSystem(item.id_workflow_external);
			return inter.label;
		}
	}
	getDependentSteps() {
		const { steps: { dependentSteps, }, } = this.props;
		return dependentSteps || [];
	}

	goToWorkflowStep({ value: stepID }) {
		const {
			id_workflow: workflowID,
		} = this.props.params;
		this.props.router.push(
			`/data/workflows/${workflowID}/step/${stepID}`
		);
	}

	fetchData(props) {
		this.fetchCurrentWorkflow(props);
		this.fetchCurrentStep(props);
		this.fetchDependentSteps(props);
	}
	fetchDataS(props) {
		this.fetchCurrentWorkflow(props, () => {
			this.fetchCurrentStep(props, () => {
				this.fetchDependentSteps(props);
			});
		});
	}
	fetchCurrentWorkflow(props, callback) {
		const { dispatch, token, params, } = (props || this.props);
		dispatch(workflowActions.setProp('item', {}));
		dispatch(workflowActions.fetchOne(token, params.id_workflow, callback));
	}
	fetchCurrentStep(props, callback) {
		const {
			dispatch, token, params,
			interfaceBases: { list, },
		} = (props || this.props);
		dispatch(actions.setProp('item', {}));
		const success = (resp) => {
			dispatch(actions.setProp('isFetching', false));
			if (isFunction(callback)) callback();
			const { status, data } = resp;
			if (status === 200 && data.code === 200) {
				const step = (data.items || [])[0];
				if (step && step.id_type === EXTERNAL && !list.length) {
					this.fetchInterfaces();
				}
			}
		};
		const failure = () => dispatch(actions.setProp('isFetching', false));
		dispatch(actions.setProp('isFetching', true));
		dispatch(actions.fetchOne(token, params.id, success, failure));
	}
	fetchDependentSteps(props) {
		const { token, dispatch, params } = (props || this.props);
		dispatch(actions.setProp('fetchingDependentSteps', true));
		dispatch(actions.setProp('dependentSteps', []));
		dispatch(actions.postToURL({
			token,
			route: `workflowstep/${params.id}/getDependentSteps`,
			success: (resp) => {
				dispatch(actions.setProp('fetchingDependentSteps', false));
				dispatch(actions.setProp('dependentSteps', resp.data));
			},
			failure: (error) => {
				dispatch(actions.setProp('fetchingDependentSteps', false));
				window.message.error(i18n.t('ErrorFetchingDependentSteps'), 0, respErrorDetails(error));
				console.error(error);
			},
		}));
	}
	fetchInterfaces() {
		const { dispatch, token, } = this.props;
		dispatch(interfaceActions.fetch(token));
	}

	newOptionClick() {
		const { steps: { item: step } } = this.props;
		this.setState({
			option: {
				order_index: step.workflow_step_option && step.workflow_step_option.length,
			}
		});
		ModalFactory.show('wsOptionModal');
	}
	newStepContentClick() {
		const { steps: { item: step } } = this.props;
		this.setState({
			stepContent: {
				order_index: step.workflow_step_content && step.workflow_step_content.length,
			}
		});
		ContentSearch.fetchContentSearch(this, '', 1);
		ModalFactory.show('wsStepContentModal');
	}
	openOptionDetail(option, e) {
		e.preventDefault();
		this.setState({ option: Object.assign({}, option) }, () => ModalFactory.show('wsOptionModal'));
	}
	openStepContentDetail(stepContent, e) {
		e.preventDefault();
		this.setState(
			{ stepContent: JSON.parse(JSON.stringify(stepContent)) },
			() => ModalFactory.show('wsStepContentModal')
		);
	}
	openContentModal(content) {
		// console.log('content clicked: ', content);
		const newItem = JSON.parse(JSON.stringify(content));
		newItem.oldType = newItem.id_type;
		newItem.oldValue = newItem.value;
		this.setState({ item: newItem });
		ModalFactory.show('addContentModal');
		this.setState({ content: newItem }, () => ModalFactory.show('wsContentModal'));
	}

	handleOptionChange(option) {
		const { dispatch, token, steps: { item } } = this.props;
		this.setState({ optionDisabled: true });
		const success = () => {
			ModalFactory.hide('wsOptionModal');
			this.setState({ optionDisabled: false });
		};
		const failure = () => this.setState({ optionDisabled: false });
		if (!option.id) {
			dispatch(workflowStepOptionActions.createChild(
				token, option, actions,
				item.id, success, failure,
			));
		} else {
			dispatch(workflowStepOptionActions.updateChild(
				token, option, actions, item.id,
				success, failure,
			));
		}
	}
	handleStepContentChange(stepContent) {
		const { dispatch, token, steps: { item } } = this.props;
		this.setState({ stepContentDisabled: true });
		const success = () => {
			ModalFactory.hide('wsStepContentModal');
			this.setState({ stepContentDisabled: false });
		};
		const failure = () => this.setState({ stepContentDisabled: false });
		if (!stepContent.id) {
			dispatch(workflowStepContentActions.createChild(
				token, stepContent, actions,
				item.id, success, failure,
			));
		} else {
			dispatch(workflowStepContentActions.updateChild(
				token, stepContent, actions, item.id,
				success, failure,
			));
		}
	}
	handleContentChange(newItem) {
		const item = Object.assign({}, newItem, { oldValue: undefined, oldType: undefined, });
		const { dispatch, token, } = this.props;
		const { stepContent, } = this.state;
		const success = (resp) => {
			// console.log('updated content', resp);
			const content = resp.data.items[0];
			this.setState({
				stepContent: Object.assign({}, stepContent, { content }),
				contentDisabled: false,
			});
			ModalFactory.hide('wsContentModal');
			this.fetchCurrentStep();
		};
		const failure = () => this.setState({ contentDisabled: false });
		this.setState({ contentDisabled: true });
		if (!item.id) {
			dispatch(contentActions.create(token, item, success, failure));
		} else {
			dispatch(contentActions.update({ token, item, success, failure }));
		}
	}

	handleOptionDelete(option, e) {
		e.preventDefault();
		const okClick = () => {
			this.setState({ optionDisabled: true });
			const success = () => {
				ModalFactory.hide('confirmModal');
				this.setState({ optionDisabled: false });
			};
			const failure = () => this.setState({ optionDisabled: false });
			const { dispatch, token, steps: { item } } = this.props;
			dispatch(workflowStepOptionActions.deleteChild(
				token, option, actions, item.id, success, failure
			));
		};
		const closeClick = () => {
			ModalFactory.hide('confirmModal');
		};

		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteStepOption'),
				description: i18n.t('WishDeleteStepOptionDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: okClick,
						className: 'btn-danger',
						disabled: () => this.state.optionDisabled,
					},
					{ label: i18n.t('Close'), onClick: closeClick, className: 'btn-default', },
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleStepContentDelete(stepContent, e) {
		e.preventDefault();
		const okClick = () => {
			this.setState({ stepContentDisabled: true });
			const success = () => {
				ModalFactory.hide('confirmModal');
				this.setState({ stepContentDisabled: false });
			};
			const failure = () => this.setState({ stepContentDisabled: false });
			const { dispatch, token, steps: { item } } = this.props;
			dispatch(workflowStepContentActions.deleteChild(
				token, stepContent, actions, item.id, success, failure
			));
		};
		const closeClick = () => ModalFactory.hide('confirmModal');
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteStepOption'),
				description: i18n.t('WishDeleteStepOptionDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: okClick,
						className: 'btn-danger',
						disabled: () => this.state.optionDisabled,
					},
					{ label: i18n.t('Close'), onClick: closeClick, className: 'btn-default', },
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	markAsInitial(step) {
		const { dispatch, token } = this.props;
		if (step.initial_step) return;
		this.setState({ stepDisabled: true });
		dispatch(actions.postToURL({
			token,
			route: 'workflowstep/markAsInitial',
			item: { id: step.id },
			success: (resp) => {
				this.setState({ stepDisabled: false });
				const item = resp.data;
				dispatch(actions.setSelected(item));
			},
			failure: () => this.setState({ stepDisabled: false }),
		}));
	}

	modStepClick(e) {
		e.preventDefault();
		const { steps: { item } } = this.props;
		this.setState({ step: JSON.parse(JSON.stringify(item)) });
		ModalFactory.show('wsStepModal');
	}
	handleStepChange(step) {
		const { dispatch, token, workflows: { item } } = this.props;
		const success = () => {
			this.setState({ stepDisabled: false });
			ModalFactory.hide('wsStepModal');
		};
		const failure = () => this.setState({ stepDisabled: false });
		this.setState({ stepDisabled: true });
		if (!step.id) {
			const params = {};
			if (this.state.replaceInitial) {
				params.replaceInitial = true;
			}
			dispatch(actions.createChild(
				token, step, workflowActions, item.id,
				(data) => {
					success();
					ModalFactory.hide('confirmModal');
					const newItem = data.items[0];
					this.props.router.replace(`/data/workflows/${newItem.id_workflow}/step/${newItem.id}`);
				}, failure, true, params
			));
		} else {
			dispatch(actions.updateChild(
				token, step, workflowActions, item.id,
				() => {
					success();
					this.fetchCurrentStep();
				}, failure
			));
		}
	}
	handleStepDelete(item, e) {
		e.preventDefault();
		let title = i18n.t('WishDeleteStep', item);
		let description = i18n.t('WishDeleteStepDescription', item);
		let buttons = [
			{
				className: 'btn-danger',
				label: i18n.t('Ok'),
				onClick: () => {
					const { dispatch, token, } = this.props;
					const deleting = (v) => this.setState({
						stepDeleting: Object.assign({}, this.state.stepDeleting, { [item.id]: v }),
					});
					if (item.initial_step) {
						this.setState({
							replaceInitial: true,
							step: {
								initial_step: true,
								order_index: 0,
							}
						});
						ModalFactory.show('wsStepModal');
					} else {
						deleting(true);
						dispatch(actions.delete({
							token,
							item,
							success: () => {
								ModalFactory.hide('confirmModal');
								this.props.router.replace(`/data/workflows/${item.id_workflow}`);
								// deleting(false);
							},
							failure: () => deleting(false),
						}));
					}
				},
				disabled: () => this.state.stepDeleting[item.id],
			},
			{
				className: 'btn-default',
				label: i18n.t('Close'),
				onClick: () => ModalFactory.hide('confirmModal'),
			},
		];

		const dependents = this.getDependentSteps();
		if (dependents.length) {
			title = i18n.t('CantDeleteStepByDependents', item);
			description = i18n.t('CantDeleteStepByDependentsDescription', item);
			buttons = [
				{
					className: 'btn-default',
					label: i18n.t('Ok'),
					onClick: () => ModalFactory.hide('confirmModal'),
				}
			];
		}
		this.setState({
			confirmModalProps: { title, description, buttons, },
		}, () => ModalFactory.show('confirmModal'));
	}

	renderSpinner() {
		const { steps: { isFetching }, } = this.props;
		return (
			<AnimatedSpinner key="spinner" show={isFetching} />
		);
	}
	renderContentList(canDelete) {
		const { steps: { item, isFetching, }, } = this.props;
		if (!item) return null;

		let stepContents = item.workflow_step_content || [];
		if (stepContents && stepContents.length) {
			stepContents = stepContents.map(sC => (
				<WorkflowStepContentItem
					key={sC.id} item={sC} step={item}
					onClick={(e) => this.openStepContentDetail(sC, e)}
					canDelete={canDelete}
					onDelete={(e) => this.handleStepContentDelete(sC, e)}
				/>
			));
		} else if (isFetching) {
			stepContents = i18n.t('Loading');
		} else if (item.id_type === REGULAR || item.id_type === EXTERNAL) {
			stepContents = i18n.t('NoContents');
		} else {
			stepContents = i18n.t('CantAddContentsToStep');
		}

		return (
			<Panel title={[this.renderSpinner(), i18n.t('Contents')]}>
				{stepContents}
			</Panel>
		);
	}
	renderOptionList(canDelete) {
		const { steps: { item, isFetching, }, } = this.props;
		if (!item) return null;

		let options = item.workflow_step_option || [];
		if (options && options.length) {
			options = options.map(option => (
				<WorkflowStepOptionItem
					key={option.id} item={option} step={item}
					onClick={(e) => this.openOptionDetail(option, e)}
					onStepClick={(data) => this.goToWorkflowStep(data)}
					canDelete={canDelete}
					onDelete={(e) => this.handleOptionDelete(option, e)}
				/>
			));
		} else if (isFetching) {
			options = i18n.t('Loading');
		} else if (item.id_type === REGULAR) {
			options = `${i18n.t('NoOptions')}.`;
		} else {
			options = `${i18n.t('CantAddOptionsToStep')}.`;
		}
		return options;
	}
	renderDependentSteps() {
		const { steps: { fetchingDependentSteps: isFetching, }, } = this.props;
		const rows = this.getDependentSteps();
		return (
			<Panel
				title={[
					<AnimatedSpinner key="spin" show={isFetching} />,
					i18n.t('DependentWorkflowSteps')
				]}
			>
				<DataTable
					emptyRow loading={isFetching}
					{...{ rows, totalRows: rows.length, }}
					schema={{
						fields: {
							order_index: i18n.t('Order'),
							id_type: {
								label: i18n.t('Type'),
								format: (row) => WorkflowStepTypeNames()[row.id_type],
							},
							name: i18n.t('Name'),
							id: i18n.t('ID'),
						}
					}}
					// link={(row) => {
					// 	this.props.router.push(`/data/workflows/${row.id_workflow}/step/${row.id}`);
					// }}
					link={({ id: value }) => this.goToWorkflowStep({ value })}
				/>
			</Panel>
		);
	}
	render() {
		const {
			steps: { item, fetchingDependentSteps: isFetching },
			workflows: { item: workflow },
			routeParams: { id },
			user,
		} = this.props;
		const {
			step,
			stepDisabled,
			option,
			optionDisabled,
			stepContent,
			stepContentDisabled,
			content,
			contentDisabled,
			confirmModalProps,
		} = this.state;
		const canAdd = user.hasPermissions('workflows.add');
		const canEdit = user.hasPermissions('workflows.edit');
		const canDelete = user.hasPermissions('workflows.delete');
		return (
			<Page>
				<Factory
					modalref="wsStepModal" title={i18n.t('StepInfo', step)}
					factory={WorkflowStepModal} large
					item={step} workflow={workflow}
					disabled={stepDisabled} setDisabled={(value) => this.setState({ stepDisabled: value })}
					onCreate={(newStep) => this.handleStepChange(newStep)}
					onUpdate={(newStep) => this.handleStepChange(newStep)}
				/>
				<Factory
					modalref="wsOptionModal" title={i18n.t('OptionInfo', { ...option, step: item, workflow })}
					factory={WorkflowStepOptionModal}
					item={option} step={item}
					onCreate={(newOption) => this.handleOptionChange(newOption)}
					onUpdate={(newOption) => this.handleOptionChange(newOption)}
					disabled={optionDisabled}
				/>
				<Factory
					modalref="wsStepContentModal" factory={WorkflowStepContentModal} large
					title={i18n.t('StepContentInfo', { ...stepContent, step: item, workflow })}
					item={stepContent} step={item}
					onCreate={(newStepContent) => this.handleStepContentChange(newStepContent)}
					onUpdate={(newStepContent) => this.handleStepContentChange(newStepContent)}
					openContentModal={(newContent) => this.openContentModal(newContent)}
					disabled={stepContentDisabled}
				/>
				<Factory
					modalref="wsContentModal" factory={ContentModal} large hideDependents
					title={i18n.t('ContentInfo', { ...content, step: item, workflow })}
					item={content} disabled={contentDisabled}
					onCreate={(newItem) => this.handleContentChange(newItem)}
					onUpdate={(newItem) => this.handleContentChange(newItem)}
					openContentDetail={() => ModalFactory.show('wsContentDetailModal')}
				/>
				<Factory
					modalref="wsContentDetailModal" large
					factory={ContentDetailModal}
					title={(content.id_type !== ContentTypes.HTML && content.oldValue) || content.name}
					content={content}
					results={[]}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col size={7}>
					{this.renderContentList(canDelete)}
					{this.renderDependentSteps()}
				</Col>
				<Col size={5}>
					<Panel>
						<div style={{ textAlign: 'center' }}>
							<Button
								label={i18n.t('Info')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={(e) => this.modStepClick(e)}
								disabled={item.id !== (id * 1)}
							/>
							{canAdd && <Button
								label={i18n.t('Content')} size="btn-sm"
								icon="fa-plus-square"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={(e) => this.newStepContentClick(e)}
								disabled={stepDisabled || (item.id_type !== REGULAR && item.id_type !== EXTERNAL)}
							/>}
							{canAdd && <Button
								label={i18n.t('Option')} size="btn-sm"
								icon="fa-plus-square"
								color="btn-info" rounded className={'m-r-sm m-b-sm'}
								onClick={(e) => this.newOptionClick(e)}
								disabled={stepDisabled || item.id_type !== REGULAR}
							/>}
							{canEdit && !item.initial_step && <Button
								label={i18n.t('MarkAsInitial')}
								className="btn-info btn-sm m-r-sm m-b-sm"
								icon="fa-tag" rounded
								onClick={() => this.markAsInitial(item)}
								disabled={stepDisabled || item.id !== (id * 1)}
							/>}
							{canDelete && <Button
								label={i18n.t('Delete')}
								className="btn-danger btn-sm m-r-sm m-b-sm"
								icon="fa-trash-o" rounded
								onClick={(e) => this.handleStepDelete(item, e)}
								disabled={isFetching}
							/>}
						</div>

						<div>
							<h4>{this.renderSpinner()}{i18n.t('Data')}:</h4>
							<div><strong>{i18n.t('ID')}: </strong>{item.id}</div>
							<div><strong>{i18n.t('Workflow')}: </strong>{item.workflow_name}</div>
							<div><strong>{i18n.t('Name')}: </strong>{item.name}</div>
							<div><strong>{i18n.t('OrderNo')}: </strong>{item.order_index}</div>
							<div>
								<strong>{i18n.t('InitialStep')}: </strong>{i18n.t(`${!!item.initial_step}`)}
							</div>
							<div>
								<strong>{i18n.t('Type')}: </strong>{(() => {
									if (item.id_type === REGULAR) {
										return i18n.t('Regular');
									} else if (item.id_type === WORKFLOW) {
										return i18n.t('Workflow');
									} else if (item.id_type === EXTERNAL) {
										return i18n.t('ExternalSystem');
									} else if (item.id_type === UNDEFINED) {
										return i18n.t('Undefined');
									}
								})()}
							</div>

							{(item.id_type === WORKFLOW) &&
								<div>
									<strong>{i18n.t('Workflow')}: </strong>
									<Link to={item.workflow && `/data/workflows/${item.id_workflow_external}`} >
										{item.workflow && item.workflow.name} ({item.id_workflow_external})
									</Link>
								</div>
							}
							{(item.id_type === EXTERNAL) &&
								<div>
									<strong>{i18n.t('SystemName')}: </strong>
									<Link to={`/data/interfaces/${item.id_workflow_external}`} >
										{this.getSystemName(item)} ({item.id_workflow_external})
									</Link>
								</div>
							}
						</div>
						<hr />
						<h4>{this.renderSpinner()}{i18n.t('Options')}:</h4>
						<div>{this.renderOptionList(canDelete)}</div>
					</Panel>
				</Col>
			</Page >
		);
	}
}

export default connect(mapStateToProps)(WorkflowStep);
