/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Workflow
 */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	DataTable,
	Row, Col, Page,
	Panel, Button,
	ModalFactory, Factory,
	Input, TagInput,
	AnimatedSpinner,
} from '../../reactadmin/components/';

import {
	DependentWorkflowsModal,
	WorkflowCriteriasModal,
	WorkflowModal,
	// WorkflowStepItem,
	WorkflowStepModal,
	WorkflowPublishModal,
	WorkflowVersionsModal,
} from './';
import { DialogModal } from '../DialogModal';
import i18n from '../../i18n';
import { containsIgnoringCaseAndAccents, respErrorDetails } from '../../reactadmin/Utils';
import { WorkflowStepTypes, WorkflowStepTypeNames, DateFormats } from '../../store/enums';
import WorkflowPublishResultModal from './WorkflowPublishResultModal';
import WorkflowDependenciesModal from './WorkflowDependenciesModal';

const actions = ReduxOutlet('workflows', 'workflow').actions;
const workflowStepActions = ReduxOutlet('workflows', 'workflowstep').actions;
const workflowVersionActions = ReduxOutlet('workflows', 'workflowVersion').actions;
const workflowCriteriaActions = ReduxOutlet('workflows', 'workflowCriteria').actions;
/**
 * Componente que muestra la configuración de un flujo para la versión editable.
 *
 * @class Workflow
 * @extends {PureComponent}
 */
class Workflow extends PureComponent {
	state = {
		publish: {},
		workflow: {},
		step: {},
		workflowDependencies: [],
		replaceInitial: false,
		publishDisabled: false,
		workflowDisabled: false,
		stepDisabled: false,
		textFilter: '',
		loading: false,
		loadingDependencies: false,
		loadingCurrentVersion: false,
		criteriaDeleting: {},
		stepDeleting: {},
	}
	componentWillMount() {
		this.init(this.props);
		// window.Scripting_Workflow = this;
	}
	componentWillReceiveProps(props) {
		if (props.params.id !== this.props.params.id) {
			this.init(props);
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	componentWillUnmount() {
		this.unmounting = true;
	}

	getBreadcrumbItems() {
		const workflowID = this.props.params.id;
		const { workflows: { item: workflow }, } = this.props;
		const workflowName = (workflow.name || `#${workflowID}`);
		return [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Workflows'), to: '/data/workflows' },
			{ label: `${i18n.t('Workflow')} ${workflowName}`, },
		];
	}
	getCurrentVersion() {
		const { workflows, params } = this.props;
		const current = workflows[`workflow_${params.id}_active`];
		if (current && current.items) {
			return current.items[0];
		}
		return {};
	}

	setPublishDisabled(value) {
		this.changeState({ publishDisabled: value });
	}
	setItemDisabled(value) {
		this.changeState({ workflowDisabled: value });
	}

	unmounting = false
	changeState(state, callback) {
		if (!this.unmounting) {
			this.setState(state, callback);
		}
	}

	init(props) {
		global.getBreadcrumbItems = () => this.getBreadcrumbItems();
		this.fetchCurrentWorkflow(props);
		this.fetchCurrentVersion(props);
		this.fetchVersions(props);
	}
	fetchCurrentWorkflow(props) {
		const { dispatch, token, params, } = (props || this.props);
		const callback = () => dispatch(actions.setProp('isFetching', false));
		dispatch(actions.setProp('isFetching', true));
		dispatch(actions.setProp('item', {}));
		dispatch(actions.fetchOne(token, params.id, callback, callback));
	}
	fetchCurrentVersion(props) {
		const { token, dispatch, params } = (props || this.props);
		this.changeState({ loadingCurrentVersion: true });
		dispatch(actions.postToURL({
			token,
			route: `workflow/${params.id}/active`,
			success: () => this.changeState({ loadingCurrentVersion: false }),
			failure: () => this.changeState({ loadingCurrentVersion: false }),
		}));
	}
	fetchWorkflowDependentSteps(pageNumber) {
		const {
			dispatch, token,
			workflows: { item, },
		} = this.props;
		const where = [
			{ name: 'id_type', value: WorkflowStepTypes.WORKFLOW },
			{ name: 'id_workflow_external', value: item.id },
		];
		const orderBy = 'id_workflow, id_workflow_version, initial_step, order_index';
		const success = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const failure = () => dispatch(workflowStepActions.setProp('fetchingDependents', false));
		const newState = {
			modalTitle: i18n.t('DependentWorkflowsFromWorkflow', item),
			fetchingDependents: true,
			temp: [],
		};
		if (pageNumber === 1) {
			newState.tempNumber = pageNumber;
			newState.tempRows = 0;
			newState.fetchDependentSteps = (page) => this.fetchWorkflowDependentSteps(page);
		}
		dispatch(workflowStepActions.updateStateObj(newState));
		dispatch(workflowStepActions.fetchPage({
			token,
			temp: true,
			where,
			orderBy,
			pageNumber,
			success,
			failure,
		}));
	}
	fetchWorkflowDependencies() {
		const {
			token, dispatch,
			workflows: { item, },
		} = this.props;

		this.changeState({ loadingDependencies: true });
		dispatch(actions.postToURL({
			token,
			route: 'workflowstep/workflowDependencies',
			item: { id: item.version_id, },
			success: (resp) => {
				this.changeState({ loadingDependencies: false });
				if (resp.status === 200) {
					this.changeState({ workflowDependencies: resp.data });
				}
			},
			failure: (resp) => {
				this.changeState({ loadingDependencies: false });
				window.message.error(i18n.t('ErrorInWorkflowDependencies'), 0, respErrorDetails(resp));
			},
		}));
	}
	fetchVersions() {
		const { dispatch, token, params } = this.props;
		const where = [
			{ name: 'id_workflow', value: params.id },
		];
		dispatch(workflowVersionActions.setProp('list', []));
		dispatch(workflowVersionActions.fetch(token, where, '', true));
	}

	openWorkflowModal(e) {
		e.preventDefault();

		const { workflows: { item: workflow } } = this.props;
		this.changeState({ workflow: Object.assign({}, workflow) });
		ModalFactory.show('wfWorkflowModal', e);
	}
	handleItemChange(item) {
		const { dispatch, token } = this.props;
		this.setItemDisabled(true);
		dispatch(actions.update({
			token,
			item,
			success: () => this.workflowChanged(),
			failure: () => this.setItemDisabled(false),
		}));
	}
	workflowChanged() {
		this.setItemDisabled(false);
		ModalFactory.hide('wfWorkflowModal');
		this.fetchCurrentWorkflow();
	}

	newStepClick(e, initial = false) {
		e.preventDefault();
		const { workflows: { item: workflow } } = this.props;
		this.changeState({
			replaceInitial: !!initial,
			step: {
				initial_step: !!initial,
				order_index: initial ? 0 : workflow.workflow_step && workflow.workflow_step.length,
			}
		});
		ModalFactory.hide('confirmModal');
		ModalFactory.show('stepModal');
	}
	openStepDetail(step, e) {
		e.preventDefault();
		this.changeState({ step: Object.assign({}, step) });
		ModalFactory.show('stepModal');
	}
	handleStepChange(step) {
		const { dispatch, token, workflows: { item } } = this.props;
		const success = () => {
			this.changeState({
				step: {},
				stepDisabled: false,
			});
			ModalFactory.hide('stepModal');
		};
		const failure = () => this.changeState({ stepDisabled: false });
		this.changeState({ stepDisabled: true });
		if (!step.id) {
			const params = {};
			if (this.state.replaceInitial) {
				params.replaceInitial = true;
			}
			dispatch(workflowStepActions.createChild(
				token, step, actions, item.id,
				success, failure, true, params,
			));
		} else {
			dispatch(workflowStepActions.updateChild(
				token, step, actions, item.id,
				success, failure
			));
		}
	}
	handleStepDelete(step, e) {
		e.preventDefault();
		this.changeState({
			confirmModalProps: {
				title: i18n.t('WishDeleteStep', step),
				description: i18n.t('WishDeleteStepDescription', step),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const deleting = (v) => this.changeState({
								stepDeleting: Object.assign({}, this.state.stepDeleting, { [step.id]: v }),
							});

							if (step.initial_step) {
								this.newStepClick(e, true);
							} else {
								const { dispatch, token, workflows: { item } } = this.props;
								deleting(true);
								dispatch(workflowStepActions.deleteChild(token, step, actions, item.id, () => {
									ModalFactory.hide('confirmModal');
									// deleting(false);
								}, () => deleting(false)));
							}
						},
						disabled: () => this.state.stepDeleting[step.id],
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}
	handleWorkflowCriteriaDelete(wc) {
		this.changeState({
			confirmModalProps: {
				title: i18n.t('WishDeleteWorkflowCriteria', wc),
				description: i18n.t('WishDeleteWorkflowCriteriaDescription', wc),
				buttons: [
					{
						className: 'btn-danger',
						label: i18n.t('Ok'),
						onClick: () => {
							const deleting = (v) => this.changeState({
								criteriaDeleting: Object.assign({}, this.state.criteriaDeleting, { [wc.id]: v }),
							});
							const { dispatch, token, workflows: { item } } = this.props;
							deleting(true);
							dispatch(workflowCriteriaActions.deleteChild(
								token, wc, actions, item.id, () => {
									ModalFactory.hide('confirmModal');
									// deleting(false);
								}, () => deleting(false),
							));
						},
						disabled: () => this.state.criteriaDeleting[wc.id],
					},
					{
						className: 'btn-default',
						label: i18n.t('Close'),
						onClick: () => ModalFactory.hide('confirmModal'),
					},
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	selectStep(row) {
		this.props.router.push(`/data/workflows/${row.id_workflow}/step/${row.id}`);
	}
	deleteItem(id) {
		const { token, dispatch } = this.props;
		dispatch(actions.delete(token, { _id: id }));
	}

	openPublishModal({ e, item }) {
		this.fetchCurrentVersion();
		const isItemWorkflow = !item.workflow_name;
		this.changeState({
			publish: {
				id_workflow: isItemWorkflow ? item.id : item.id_workflow,
				id_workflow_version: isItemWorkflow ? item.version_id : item.id,
				version: item.active === 0 ? '' : item.id_version,
				notes: item.active === 0 ? '' : item.notes,
				isItemWorkflow: isItemWorkflow || item.active === 0,
			}
		});
		ModalFactory.show('publishModal', e);
	}
	publishWorkflow(config) {
		const { dispatch, token } = this.props;
		// TODO: Retornar el flujo con la nueva versión para actualizar esta pantalla
		//		 evitando llamar fetchCurrentVersion.
		if (!config.version) {
			ModalFactory.show('publishErrorModal');
		} else {
			const item = Object.assign({}, config, { isItemWorkflow: undefined, });
			this.setPublishDisabled(true);
			dispatch(actions.postToURL({
				token,
				route: 'workflow/publish',
				item,
				success: (resp) => {
					this.setPublishDisabled(false);
					if (resp.data.items && resp.data.items.result < 3) {
						ModalFactory.hide('publishModal');
					}
					ModalFactory.show('publishResultModal');
					this.fetchCurrentVersion();
					this.fetchVersions();
				},
				failure: () => this.setPublishDisabled(false),
			}));
		}
	}

	renderPublishErrorModal() {
		const modalref = 'publishErrorModal';
		const title = i18n.t('IncompleteInfo');
		const description = i18n.t('PublishMissingVersion');
		const buttons = [{
			label: i18n.t('Ok'),
			onClick: () => ModalFactory.hide(modalref),
			className: 'btn-info',
		}];
		return (
			<DialogModal {...{ modalref, title, description, buttons }} />
		);
	}
	renderStepsColumn() {
		const { workflows: { item, isFetching }, user, } = this.props;
		const { textFilter } = this.state;
		const onFilterChange = (filter = '') => {
			this.changeState({ textFilter: filter });
		};

		let steps = [];
		if (item && item.workflow_step) {
			steps = item.workflow_step;
		}
		const totalRows = steps.length;
		const rows = steps.filter(i => !textFilter || (
			containsIgnoringCaseAndAccents(i.id, textFilter) ||
			containsIgnoringCaseAndAccents(i.name, textFilter) ||
			containsIgnoringCaseAndAccents(WorkflowStepTypeNames()[i.id_type], textFilter)
		));
		const canAdd = user.hasPermissions('workflows.add');
		const canDelete = user.hasPermissions('workflows.delete');
		return (
			<Col size="7" className="col-md-pull-5">
				<Panel
					title={[
						<AnimatedSpinner key="spin" show={isFetching} />,
						i18n.t('Steps'),
					]}
					style={{ backgroundColor: 'grey' }}
				>
					{canAdd && <Button
						label={i18n.t('Step')} size="btn-sm"
						icon="fa-plus-square"
						color="btn-info" rounded className={'m-r-sm  m-b-sm'}
						style={{ top: 9, right: 50, position: 'absolute' }}
						onClick={(e) => this.newStepClick(e)}
						disabled={!item || !item.id}
					/>}
					<Row>
						<Col>
							<header className="panel-heading">
								<div className="form-group col-sm-4 pull-right">
									<Input
										clearable
										placeholder={i18n.t('Name')}
										onFieldChange={(e) => onFilterChange(e.target.value)}
										value={textFilter || ''}
									/>
								</div>
							</header>
						</Col>
					</Row>
					<Row style={{ position: 'relative', minHeight: 25, }}>
						<Col>
							{/* {steps.map((step, idx) => (
								<WorkflowStepItem
									key={idx}
									step={step} workflow={item}
									canDelete={canDelete}
									onDelete={(e) => this.handleStepDelete(step, e)}
									onSelect={(index) => this.selectItem(index)}
								/>
							))} */}
							<DataTable
								emptyRow loading={isFetching}
								{...{ rows, totalRows, canDelete, }}
								schema={{
									fields: {
										order_index: i18n.t('Order'),
										// initial_step: {
										// 	type: 'Boolean',
										// 	label: i18n.t('InitialStep'),
										// },
										id_type: {
											label: i18n.t('Type'),
											format: (row) => WorkflowStepTypeNames()[row.id_type],
										},
										name: i18n.t('Name'),
										id: i18n.t('ID'),
									}
								}}
								onDelete={(id, { row, e }) => this.handleStepDelete(row, e)}
								link={(row) => this.selectStep(row)}
							/>
						</Col>
					</Row>
				</Panel>
			</Col>
		);
	}
	render() {
		const {
			publish, workflow, step,
			publishDisabled, workflowDisabled, stepDisabled,
			workflowDependencies, loadingDependencies,
			confirmModalProps, loadingCurrentVersion,
		} = this.state;
		const {
			workflows: { item, isFetching },
			steps: {
				temp: dependentSteps,
				tempNumber: pageNumber,
				tempSize: pageSize,
				tempRows: totalRows,
				fetchingDependents: loading,
				modalTitle = i18n.t('DependentWorkflows'),
				fetchDependentSteps,
			},
			user,
			router,
		} = this.props;
		const current = this.getCurrentVersion();

		const canPublish = user.hasPermissions('workflows.publish');
		return (
			<Page>
				{canPublish && this.renderPublishErrorModal()}
				<Factory
					modalref="wfWorkflowModal" title={i18n.t('WorkflowInfo', item)}
					item={workflow} factory={WorkflowModal}
					disabled={workflowDisabled}
					onUpdate={(newWorkflow) => this.handleItemChange(newWorkflow)}
				/>
				<Factory
					modalref="stepModal" title={i18n.t('AddStep')}
					factory={WorkflowStepModal} large
					item={step} workflow={item}
					disabled={stepDisabled} setDisabled={(value) => this.changeState({ stepDisabled: value })}
					onCreate={(newStep) => this.handleStepChange(newStep)}
					onUpdate={(newStep) => this.handleStepChange(newStep)}
				/>
				<Factory
					modalref="workflowCriteriasModal"
					item={item} canBeClose
					title={i18n.t('WorkflowsCriterias')}
					factory={WorkflowCriteriasModal}
					onDelete={(...args) => this.handleWorkflowCriteriaDelete(...args)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Factory
					modalref="dependentWorkflowsModal" factory={DependentWorkflowsModal} large
					title={modalTitle} canBeClose backdrop="true" keyboard="true"
					{...{ dependentSteps, pageNumber, pageSize, totalRows, loading, router, }}
					fetchDependentSteps={(newPage) => fetchDependentSteps(newPage)}
				/>
				<Factory
					modalref="WorkflowDependenciesModal"
					factory={WorkflowDependenciesModal} large
					title={i18n.t('WorkflowDependencies')}
					backdrop="true" keyboard="true"
					dependencies={workflowDependencies}
					loading={loadingDependencies}
					router={router}
				/>
				<Factory
					modalref="workflowVersionsModal" large
					factory={WorkflowVersionsModal}
					backdrop="true" keyboard="true"
					title={i18n.t('WorkflowVersions', { workflowName: item.name || '' })}
					openPublishModal={(...args) => this.openPublishModal(...args)}
					publish={publish}
					router={router}
					disabled={publishDisabled}
				/>
				{canPublish && [
					<Factory
						key="1" modalref="publishModal" title={i18n.t('PublishDescription')}
						item={publish} factory={WorkflowPublishModal}
						onCreate={(config) => this.publishWorkflow(config)}
						workflow={item} current={current}
						disabled={publishDisabled}
					/>,
					<Factory
						key="2" modalref="publishResultModal" title={i18n.t('WorkflowPublishResult')}
						factory={WorkflowPublishResultModal}
					/>
				]}
				<Col size="5" className="col-md-push-7">
					<Panel title={`${i18n.t('Workflow')} ${item.name || ''}`}>
						<Row style={{ textAlign: 'center' }}>
							<Button
								label={i18n.t('Info')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={(e) => this.openWorkflowModal(e)}
								disabled={!item || !item.id}
							/>
							<Button
								label={i18n.t('Criterias')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={() => ModalFactory.show('workflowCriteriasModal')}
								disabled={!item || !item.id}
							/>
							<Button
								label={i18n.t('DependentWorkflows')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={() => {
									this.fetchWorkflowDependentSteps(1);
									ModalFactory.show('dependentWorkflowsModal');
								}}
								disabled={!item || !item.id}
							/>
							<Button
								label={i18n.t('WorkflowDependencies')} size="btn-sm"
								icon="fa-search"
								color="btn-info" rounded className={'m-r-sm  m-b-sm'}
								onClick={() => {
									this.fetchWorkflowDependencies();
									ModalFactory.show('WorkflowDependenciesModal');
								}}
								disabled={!item || !item.id}
							/>
							{canPublish && <Button
								className={'m-r-sm  m-b-sm'}
								size="btn-sm"
								label={i18n.t('Publish')}
								icon={publishDisabled ? 'fa-spinner fa-spin' : 'fa-plus-square'}
								color="btn-info" rounded
								onClick={(e) => this.openPublishModal({ e, item })}
								disabled={!item || !item.id || publishDisabled}
							/>}
						</Row>
						<div>
							<h4>
								<AnimatedSpinner show={isFetching} />
								{i18n.t('Data')}:
							</h4>
							<strong>{i18n.t('ID')}: </strong> {item.id}<br />
							<strong>{i18n.t('Center')}: </strong> {item.center_name}<br />
							<strong>{i18n.t('Name')}: </strong> {item.name}<br />
							<strong>{i18n.t('Tags')}: </strong> <TagInput inline value={item.tags} /><br />
							<strong>{i18n.t('Main')}: </strong>
							{item.id && i18n.t((!!item.main).toString())}<br />
							<strong>{i18n.t('Disabled')}: </strong>
							{item.id && i18n.t((!!item.deleted).toString())}<br />
						</div>
						<div>
							<Row>
								<Col size={{ xs: 7 }}>
									<h4>
										<AnimatedSpinner show={loadingCurrentVersion} />
										{i18n.t('CurrentVersion')}:
									</h4>
								</Col>
								<Col size={{ xs: 5 }}>
									<Button
										label={i18n.t('OtherWorkflowVersions')} size="btn-sm"
										icon="fa-search"
										color="btn-info" rounded className={'m-r-sm  m-b-sm'}
										onClick={() => ModalFactory.show('workflowVersionsModal')}
									/>
								</Col>
							</Row>
							<strong>{i18n.t('Number')}: </strong> {
								current.version_id_version || i18n.t('N_A')
							}<br />
							<strong>{i18n.t('PublishedDate')}: </strong> {
								current.version_docdate
									? moment(current.version_docdate).format(DateFormats.DATE_TIME_Z)
									: i18n.t('N_A')
							}<br />
							<strong>{i18n.t('Notes')}: </strong> {current.version_notes}<br />
						</div>
					</Panel>
				</Col>
				{this.renderStepsColumn()}
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Workflow);
