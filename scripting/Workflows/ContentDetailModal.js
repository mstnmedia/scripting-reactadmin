import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import /* ReduxOutlet,  */{ mapStateToProps } from '../../outlets/ReduxOutlet';
import { Iframe } from '../../reactadmin/components';

/**
 * Componente en desuso.
 *
 * @class ContentDetailModal
 * @extends {Component}
 */
class ContentDetailModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	render() {
		const { content } = this.props;
		return (
			<section
				style={{
					marginBottom: 0
				}}
			>
				<div className="panel-body">
					<Iframe urlSource={content.url} htmlCode={content.html} />
				</div>
			</section>
		);
	}
}

export default connect(mapStateToProps)(ContentDetailModal);
