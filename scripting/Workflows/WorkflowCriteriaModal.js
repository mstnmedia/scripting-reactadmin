import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import CriteriaSearch from './CriteriaSearch';

/**
 * Componente que controlaba el mantenimiento de la relación flujos-criterios.
 *
 * @class WorkflowCriteriaModal
 * @extends {Component}
 * @deprecated
 */
class WorkflowCriteriaModal extends Component {

	state = {
		updaterProp: null,
	}

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}

	saveItem() {
		const { item, workflow, } = this.props;
		if (item) {
			if (!item.id) {
				item.order_index = item.order_index || 0;
				item.id_workflow = workflow.id;

				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	render() {
		const { item: { criteria_name: name }, } = this.props;
		return (
			<div role="form">
				<div>
					<label htmlFor="selected">{i18n.t('SelectedCriteria')}</label>
					<span>{name}</span>
				</div>

				<CriteriaSearch
					onSelect={(selected) => {
						this.handleChange('id_criteria', selected.id);
						this.handleChange('criteria_name', selected.name);
					}}
				/>

				<div>
					<hr />
					<button type="button" onClick={(e) => this.saveItem(e)} className="btn btn-info">
						{i18n.t('Add')}
					</button>
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"// btn-block w-pad
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(WorkflowCriteriaModal);
