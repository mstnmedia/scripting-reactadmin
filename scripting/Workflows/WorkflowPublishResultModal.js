import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps, } from '../../outlets/ReduxOutlet';
import {
	Col,
	DataTable,
} from '../../reactadmin/components/';
import i18n from '../../i18n';

const noteSchema = {
	fields: {
		value: i18n.t('Results'),
	},
};
const dependentSchema = {
	fields: {
		name: i18n.t('DependentWorkflows'),
		id: i18n.t('ID'),
	},
};

/**
 * Componente que muestra al usuario los datos resultantes de una publicación de flujo.
 *
 * @class WorkflowPublishResultModal
 * @extends {Component}
 */
class WorkflowPublishResultModal extends Component {
	componentWillMount() {
		// window.Scripting_WorkflowPublishResultModal = this;
	}

	render() {
		const { workflows: { workflow_publish: result, } } = this.props;
		const items = result && result.items;
		if (!items) return null;
		// const tempNotes = items.notes || [];
		// const groupedNotes = { 1: [], 2: [], 3: [] };
		// tempNotes.forEach(note => groupedNotes[note.id].push(note));
		// const notes = Array.concat(Object.keys(tempNotes).map(key => tempNotes[key]));
		const notes = items.notes || [];

		const workflowsMap = (items.referers || [])
			.reduce((map, step) => {
				const obj = map;
				obj[step.id_workflow] = step.workflow_name;
				return map;
			}, {});
		const dependents = Object.keys(workflowsMap)
			.map(key => ({ id: key, name: workflowsMap[key] }))
			.sort((a, b) => (a.name < b.name && -1) || (a.name > b.name && 1) || 0);

		return (
			<div>
				<DataTable
					schema={noteSchema}
					rowKey={(row, i) => i}
					rowClass={(row) =>
						(row.id === 1 && 'success') ||
						(row.id === 2 && 'warning') ||
						(row.id === 3 && 'danger')
					}
					rows={notes}
				/>
				{dependents.length ?
					<DataTable
						schema={dependentSchema}
						rows={dependents}
					/>
					: null
				}
				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}

export default connect(mapStateToProps)(WorkflowPublishResultModal);
