import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Input, Fa, FormGroup,
} from '../../reactadmin/components/';
import ContentSearch from '../Contents/ContentSearch';
import i18n from '../../i18n';
import { isFunction } from '../../reactadmin/Utils';

/**
 * Componente que permite al usuario agregar contenidos al paso actual.
 *
 * @class WorkflowStepContentModal
 * @extends {PureComponent}
 */
class WorkflowStepContentModal extends PureComponent {

	state = {
		updaterProp: null,
	}
	componentWillMount() {
		// window.Scripting_WorkflowStepContentModal = this;
	}

	handleChange(property, value) {
		if (this.props.item) {
			this.props.item[property] = value;
			this.setState({ updaterProp: new Date() });
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('worflows.add') || user.hasPermissions('workflows.edit');
	}
	isFieldDisabled(iff) {
		return this.isDisabled(iff) || !this.isAllowedUser();
	}
	isDisabled(iff) {
		return !!iff || this.props.disabled || this.state.disabled;
	}
	isValid() {
		const { item } = this.props;
		if (item && item.id_content) {
			return true;
		}
		return false;
	}

	saveItem() {
		if (this.isValid()) {
			const { item, step } = this.props;
			if (!item.id) {
				item.id_center = step.id_center;
				item.id_workflow = step.id_workflow;
				item.id_workflow_version = step.id_workflow_version;
				item.id_workflow_step = step.id;
				item.order_index = item.order_index || 0;
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	render() {
		const {
			workflows: { item: workflow, },
			item: { id, order_index: orderIndex, content },
			openContentModal,
		} = this.props;
		return (
			<div role="form">
				<FormGroup>
					<label className="control-label" htmlFor="name">{i18n.t('OrderNo')}:</label>
					<Input
						type="number" className="form-control"
						placeholder={i18n.t('OrderNo')} value={orderIndex || 0}
						onFieldChange={(e) => this.handleChange('order_index', e.target.value * 1)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<FormGroup isValid={!!content}>
					<label className="control-label" htmlFor="content">{i18n.t('Content')}: </label>
					{content && content.name &&
						<span>{` ${content.name} (${content.id})`}
							{!this.isDisabled() && (
								<Fa
									icon="remove" color="red"
									style={{ padding: 5 }}
									onClick={() => {
										this.handleChange('id_content', null);
										this.handleChange('content', null);
									}}
								/>
							)}
							{isFunction(openContentModal) &&
								<Fa
									className="m-l-sm"
									title={i18n.t('Open')} icon={'folder-open-o'}
									onClick={() => openContentModal(content)}
								/>
							}
						</span>
					}
				</FormGroup>
				{!this.isFieldDisabled() &&
					<ContentSearch
						workflow={workflow}
						onSelect={(selectedContent) => {
							this.handleChange('id_content', selectedContent.id);
							this.handleChange('content', selectedContent);
						}}
					/>
				}

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button"
							onClick={(e) => this.saveItem(e)}
							className="btn btn-info"
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(WorkflowStepContentModal);
