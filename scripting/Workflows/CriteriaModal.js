import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	Row, Col,
	DataTable,
	FormGroup,
	Input,
	DropDown,
	Tooltip,
} from '../../reactadmin/components/';

import { mapStateToProps, } from '../../outlets/ReduxOutlet';
import i18n from '../../i18n';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de un criterio.
 *
 * @class CriteriaModal
 * @extends {Component}
 */
class CriteriaModal extends Component {

	state = {
		name: '',
		operator: '',
		value: '',
	}

	getItem(props) {
		return (props || this.props).item || {};
	}
	getConditions() {
		return this.getItem().conditions || [];
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}
	handleConditionChange(prop, value) {
		this.setState({ [prop]: value });
	}

	addCondition() {
		const { name, operator, value } = this.state;
		const { id_center, id, } = this.getItem();
		if (name && operator /* && value */) {
			const condition = {
				id_center,
				id_criteria: id,
				name,
				operator,
				value,
			};
			let conditions = this.getItem().conditions;
			if (!conditions) {
				conditions = this.getItem().conditions = [];
			}
			conditions.push(condition);
			this.setState({ name: '', operator: '', value: '' });
		}
	}
	deleteCondition(idx) {
		if (idx === -1) return;
		const { conditions, } = this.getItem();
		conditions.splice(idx, 1);
		this.setState({});
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			for (let i = 0; i < item.conditions.length; i++) {
				const aux = item.conditions[i];
				delete aux.idx;
			}
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('criterias.add') || user.hasPermissions('criterias.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		const conditions = this.getConditions();
		return item.name
			&& (!this.props.user.master || item.id_center)
			&& conditions && conditions.length;
	}

	renderAddCondition() {
		const { name, operator, value, } = this.state;
		return (
			<div className={`m-t-md ${this.isFieldDisabled() ? 'hidden' : ''}`}>
				<Row>
					<Col size={{ sm: 5 }}>
						<label className="control-label" htmlFor="name">
							{i18n.t('CriteriaConditionName')}:
						</label>
						<Input
							type="text" className="form-control"
							value={name}
							onFieldChange={(e) => {
								this.handleConditionChange('name', e.target.value);
							}}
						/>
					</Col>
					<FormGroup isValid={!name || !!operator} className="col-sm-2 p-l-none p-r-none">
						<label className="control-label" htmlFor="operator">
							{i18n.t('CriteriaOperator')}:
						</label>
						<DropDown
							items={['==', '>', '<', '>=', '<=', '!=']}
							value={operator}
							emptyOption
							onChange={(e) => this.handleConditionChange('operator', e.target.value)}
						/>
					</FormGroup>
					<Col size={{ sm: 5 }}>
						<label className="control-label" htmlFor="value">{i18n.t('Value')}:</label>
						<Input
							type="text" className="form-control"
							value={value || ''}
							onFieldChange={(e) => {
								this.handleConditionChange('value', e.target.value);
							}}
						/>
					</Col>
				</Row>
				<Row>
					<Col className="text-right">
						<button
							type="button" className="btn btn-info m-t-sm"
							onClick={(e) => this.addCondition(e)}
							disabled={!name || !operator || this.isDisabled()}
						>{i18n.t('Add')}</button>
					</Col>
				</Row>
			</div>
		);
	}
	render() {
		const { id,
			id_center: centerID,
			center_name: centerName,
			name,
		} = this.getItem();
		const conditions = this.getConditions()
			.map((i, idx) => Object.assign({}, i, { idx }));
		const { user, hiddenClose, centerDisabled, } = this.props;
		const canDelete = user.hasPermissions('criterias.delete');
		return (
			<div>
				{id && <FormGroup>
					<label className="control-label" htmlFor="id">{i18n.t('ID')}:</label>
					<Input type="text" className="form-control" value={id || ''} disabled />
				</FormGroup>}
				{user.master && <FormGroup isValid={!!centerID}>
					<label className="control-label" htmlFor="center">{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							getItemValue={(item) => NameIDText(item)}
							value={centerName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={id || this.isFieldDisabled() || centerDisabled}
						/>
					</Tooltip>
				</FormGroup>}
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={name} placeholder={i18n.t('Name')}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>

				<FormGroup isValid={conditions && conditions.length}>
					<label className="control-label" htmlFor="conditions" style={{ marginBottom: -10 }}>
						{i18n.t('CriteriaConditions')}:
					</label>
					<DataTable
						schema={{
							fields: {
								value: i18n.t('Value'),
								operator: i18n.t('CriteriaOperator'),
								name: i18n.t('Name'),
								// id: i18n.t('ID'),
							},
						}}
						rows={conditions}
						rowKey={(row) => row.idx}
						canDelete={canDelete}
						onDelete={(idx) => this.deleteCondition(idx)}
					/>
				</FormGroup>
				{this.renderAddCondition()}
				<div>
					<hr />
					{this.isAllowedUser() && <button
						type="button" className="btn btn-info"
						onClick={(e) => this.saveItem(e)}
						disabled={this.isDisabled() || !this.isValid()}
					>
						{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
					</button>}
					{!hiddenClose && <button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>}
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(CriteriaModal);
