/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import i18n from '../../i18n';
import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import WorkflowModal from './WorkflowModal';
import DataTable from '../../reactadmin/components/data/DataTable';
import {
	Panel,
	// Input,
	Col,
	Page,
	PanelCollapsible,
	ModalFactory,
	ButtonGroup,
	Input,
	DropDown,
	AdvancedFilters,
	TagInput,
	Tooltip,
} from '../../reactadmin/components/';
import { NumericBooleanOptions, filterAdvActions } from '../../store/enums';
import { setFilterHandles, NameIDText } from '../../Utils';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { DialogModal } from '../DialogModal';

const Factory = ModalFactory.modalFromFactory;

const workflowActions = ReduxOutlet('workflows', 'workflow').actions;

/**
 * Componente que controla la pantalla del mantenimiento de flujos.
 *
 * @class Workflows
 * @extends {Component}
 */
class Workflows extends Component {
	state = {
		workflow: {},
		disabled: false,
	}
	componentWillMount() {
		setFilterHandles(this, workflowActions, 'workflows', true)
			.initSamePage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Workflows'), },
		];
		// window.Scripting_Workflows = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	handleNewWorkflow(newWorkflow) {
		const { dispatch, token, workflows: { pageNumber, } } = this.props;
		this.setState({ disabled: true });
		dispatch(workflowActions.create(
			token, { id: 0, ...newWorkflow },
			() => {
				ModalFactory.hide('addWorkflowModal');
				this.fetchPage(pageNumber);
				this.setState({ disabled: false });
			},
			() => this.setState({ disabled: false })
		));
	}

	newWorkflowClick(e) {
		e.preventDefault();
		this.setState({ workflow: {} });
		ModalFactory.show('addWorkflowModal');
	}

	handleItemDelete(id) {
		const okClick = () => {
			const { dispatch, token, workflows: { pageNumber, }, } = this.props;
			this.setState({ disabled: true });
			dispatch(workflowActions.delete({
				token,
				item: { id },
				success: () => {
					ModalFactory.hide('confirmModal');
					this.setState({ disabled: false });
					this.fetchPage(pageNumber);
				},
				failure: () => this.setState({ disabled: false }),
			}));
		};
		const closeClick = () => ModalFactory.hide('confirmModal');
		this.setState({
			confirmModalProps: {
				title: i18n.t('WishDeleteWorkflow'),
				description: i18n.t('WishDeleteWorkflowDescription'),
				buttons: [
					{
						label: i18n.t('Ok'),
						onClick: okClick,
						className: 'btn-danger',
						disabled: () => this.state.disabled,
					},
					{ label: i18n.t('Close'), onClick: closeClick, className: 'btn-default', },
				],
			},
		}, () => ModalFactory.show('confirmModal'));
	}

	selectWorkflow(row) {
		this.props.router.push(`/data/workflows/${row.id}`);
	}

	tableSchema() {
		const isMaster = this.props.user.isMaster();
		return {
			name: 'workflows',
			description: 'A simple messaging schema',
			fields: {
				deleted: {
					type: 'Boolean',
					label: i18n.t('Disabled'),
				},
				version_docdate: {
					type: 'DateTimeZ',
					label: i18n.t('VersionDate'),
					coalesce: 'N/A',
				},
				version_id_version: {
					type: 'Integer',
					label: i18n.t('CurrentVersion'),
					coalesce: 'N/A',
				},
				main: {
					type: 'Boolean',
					label: i18n.t('Main'),
				},
				tags: {
					label: i18n.t('Tags'),
					format: (row) => (<TagInput inline value={row.tags} />)
				},
				name: i18n.t('Name'),
				center_name: isMaster ? i18n.t('Center') : undefined,
				id: i18n.t('ID'),
			}
		};
	}

	renderFilters() {
		const { where, whereObj, advancedSearch } = this.state;
		const isMaster = this.props.user.isMaster();
		const centerID = this.filterValue('id_center');
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				{!advancedSearch ?
					<div>
						<div className="form-group col-md-3">
							<label htmlFor="id">{i18n.t('ID')}:</label>
							<Input
								type="number" className="form-control"
								value={this.filterValue('id', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'id',
									value: e.target.value,
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						{isMaster && <div className="form-group col-md-3">
							<label htmlFor="center">{i18n.t('Center')}:</label>
							{centerID ? <span> {centerID}</span> : null}
							<RemoteAutocomplete
								url={`${window.config.backend}/centers/center`}
								getItemValue={(item) => NameIDText(item)}
								value={this.filterText('center_name') || ''}
								onFieldChange={(e, text) => {
									this.filterTextChange('center_name', text);
									this.filterChange({ name: 'id_center', value: undefined });
								}}
								onSelect={(text, item) => {
									this.filterTextChange('center_name', item.name);
									this.filterChange({ name: 'id_center', value: item.id });
								}}
								onBlur={() => {
									if (!this.filterValue('id_center')) {
										this.filterTextChange('center_name', '');
									}
								}}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>}
						<div className="form-group col-md-3">
							<label htmlFor="name">{i18n.t('Name')}:</label>
							<Input
								type="text" className="form-control"
								value={this.filterValue('name', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'name',
									value: e.target.value,
									criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="tags">{i18n.t('Tags')}:</label>
							<TagInput
								value={this.filterValue('tags', '')} placeholder={i18n.t('Tags')}
								onChange={({ tags, value }) => this.filterChange({
									label: 'tags',
									value: tags.length ? value : undefined,
									group: tags.map(i => ({ name: 'tags', criteria: 'like', value: i }))
								})}
								onKeyDown={this.onKeyDownMixed}
							/>
						</div>

						<div className="form-group col-md-3">
							<label htmlFor="version">{i18n.t('Version')}:</label>
							<Input
								type="text" className="form-control"
								value={this.filterValue('version_id_version', '')}
								onFieldChange={(e) => this.filterChange({
									name: 'version_id_version',
									value: e.target.value,
									// criteria: 'like',
								})}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>

						<div className="form-group col-md-3">
							<label htmlFor="main">{i18n.t('Main')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('main', '')}
								items={NumericBooleanOptions()}
								onChange={(e) => this.filterChange({ name: 'main', value: e.target.value })}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
						<div className="form-group col-md-3">
							<label htmlFor="deleted">{i18n.t('Disabled')}:</label>
							<DropDown
								emptyOption
								value={this.filterValue('deleted', '')}
								items={NumericBooleanOptions()}
								onChange={(e) => this.filterChange({ name: 'deleted', value: e.target.value })}
								onKeyDown={this.onKeyDownSimple}
							/>
						</div>
					</div>
					:
					<AdvancedFilters
						{...{ where, whereObj }}
						fields={{/* eslint-disable max-len */
							id: { type: 'number', label: i18n.t('ID'), onKeyDown: this.onKeyDownSimple, },
							id_center: { type: 'autocomplete', label: i18n.t('Center'), url: `${window.config.backend}/centers/center`, onKeyDown: this.onKeyDownMixed, },
							name: { type: 'text', label: i18n.t('Name'), onKeyDown: this.onKeyDownSimple, },
							tags: { type: 'tags', label: i18n.t('Tags'), onKeyDown: this.onKeyDownMixed, },
							version_id_version: { type: 'text', label: i18n.t('Version'), onKeyDown: this.onKeyDownSimple, },
							main: { type: 'dropdown', label: i18n.t('Main'), items: NumericBooleanOptions(), onKeyDown: this.onKeyDownSimple, },
							deleted: { type: 'dropdown', label: i18n.t('Disabled'), items: NumericBooleanOptions(), onKeyDown: this.onKeyDownSimple, },
						}}/* eslint-enable max-len */
					/>
				}

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterAdvActions(advancedSearch), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const {
			workflows: { list, pageNumber, pageSize, totalRows, },
			user,
		} = this.props;
		const {
			confirmModalProps,
			disabled,
		} = this.state;
		const rows = list || [];

		const canAdd = user.hasPermissions('workflows.add');
		const canDelete = user.hasPermissions('workflows.delete');
		return (
			<Page>
				<Factory
					modalref="addWorkflowModal" title={i18n.t('AddWorkflow')} factory={WorkflowModal}
					item={this.state.workflow}
					onCreate={(newWorkflow) => this.handleNewWorkflow(newWorkflow)}
					disabled={disabled}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ pageSize, pageNumber, totalRows, rows, }}
							onPageChange={(data) => this.pageChange(data)}
							schema={this.tableSchema()}
							addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Workflows') })}
							onAdd={canAdd && ((e) => this.newWorkflowClick(e))}
							canDelete={canDelete}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectWorkflow(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Workflows);
