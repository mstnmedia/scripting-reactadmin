import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps, } from '../../outlets/ReduxOutlet';
import {
    Col,
    DataTable,
    Pager,
    ModalFactory,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import { WorkflowStepTypes } from '../../store/enums';

/**
 * Componente que lista todos los pasos del flujo seleccionado que invocan cualquier subflujo.
 *
 * @class WorkflowDependenciesModal
 * @extends {Component}
 */
class WorkflowDependenciesModal extends Component {
    state = {
        pageSize: 10,
        pageNumber: 1,
    }

    componentWillMount() {
        // window.Scripting_WorkflowDependenciesModal = this;
    }

    goToWorkflow(row) {
        if (row.id_type === WorkflowStepTypes.WORKFLOW) {
            ModalFactory.hide(this.props.modalref);
            this.props.router.push(`/data/workflows/${row.workflow.id}`);
        }
    }

    render() {
        const { dependencies, loading, } = this.props;
        const { pageNumber, pageSize, } = this.state;
        const items = (dependencies || [])
            .filter(row => row.id_type === WorkflowStepTypes.WORKFLOW);

        const onPageChange = ({ newPage }) => {
            this.setState({ pageNumber: newPage });
        };
        const rows = Pager.getPageItems({ items, pageSize, pageNumber, });
        const totalRows = items.length;
        return (
            <div>
                <DataTable
                    emptyRow
                    schema={{
                        fields: {
                            workflow: {
                                label: i18n.t('Workflow'),
                                format: (row) => row.workflow
                                    && `${row.workflow.id} | ${row.workflow.name}`,
                            },
                            name: {
                                label: i18n.t('Step'),
                                format: (row) => `${row.id} | ${row.name}`
                            },
                        },
                    }}
                    {...{ rows, totalRows, pageSize, pageNumber, onPageChange, loading, }}
                    link={(row) => this.goToWorkflow(row)}
                />
                <Col>
                    <hr />
                    <button
                        type="button" className="btn btn-danger"
                        data-dismiss="modal" aria-hidden="true"
                    >{i18n.t('Close')}</button>
                </Col>
            </div >
        );
    }
}

export default connect(mapStateToProps)(WorkflowDependenciesModal);
