import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps, ReduxOutlet } from '../../outlets/ReduxOutlet';
import {
	DataTable,
	Col,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import CriteriaSearch from './CriteriaSearch';
import { isFunction } from '../../reactadmin/Utils';

const actions = ReduxOutlet('workflows', 'workflowCriteria').actions;
const workflowActions = ReduxOutlet('workflows', 'workflow').actions;
/**
 * Componente que controla el mantenimiento de la relación flujos-criterios.
 *
 * @class WorkflowCriteriasModal
 * @extends {Component}
 */
class WorkflowCriteriasModal extends Component {

	state = {
		item: {},
		itemDisabled: false,
		deleting: {},
	}

	getItem(props) {
		return (props || this.props).item || {};
	}
	getWCriterias() {
		return this.getItem().workflow_criteria || [];
	}

	saveItem() {
		const { dispatch, token, workflows: { item } } = this.props;
		const { item: wc } = this.state;
		const success = () => this.setState({ itemDisabled: false });
		const failure = () => this.setState({ itemDisabled: false });
		this.setState({ itemDisabled: true });
		dispatch(actions.createChild(token, wc, workflowActions, item.id, success, failure));
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('worflows.add') || user.hasPermissions('workflows.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}

	render() {
		const { user, workflows: { item: workflow } } = this.props;
		const {
			item: {
				id_criteria: criteriaID,
				criteria_name: criteriaName,
			},
			itemDisabled,
		} = this.state;
		const rows = this.getWCriterias();
		const canDelete = user.hasPermissions('workflows.delete');
		const canAdd = () => criteriaName && !rows.find(i => i.id_criteria === criteriaID);
		return (
			<div>
				<DataTable
					schema={{
						fields: {
							criteria_name: i18n.t('Name'),
							// center_name: user.master ? {
							// 	label: i18n.t('Center'),
							// 	format: (row) => row.criteria && row.criteria.center_name,
							// } : undefined,
							id_criteria: i18n.t('CriteriaID'),
							id: i18n.t('ID'),
						},
					}}
					rows={rows} emptyRow
					canDelete={canDelete}
					onDelete={(id) => isFunction(this.props.onDelete) && this.props.onDelete({ id })}
				/>
				<div className="m-t-md">
					<span className="m-r-sm">
						<label htmlFor="selected">
							{i18n.t('SelectedCriteria')}:
						</label> {criteriaName}
					</span>
					{canAdd() && <button
						type="button"
						className="btn btn-info btn-xs"
						onClick={(e) => this.saveItem(e)}
						disabled={itemDisabled}
					>{i18n.t('Add')}</button>}
				</div>
				<div>
					<CriteriaSearch
						workflow={workflow}
						onSelect={(selected) => {
							this.setState({
								item: {
									id_center: workflow.id_center,
									// center_name: workflow.center_name,
									id_workflow: workflow.id,
									id_criteria: selected.id,
									criteria_name: selected.name,
								},
							});
						}}
					/>
				</div>
				<Col>
					<hr />
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</Col>
			</div >
		);
	}
}

export default connect(mapStateToProps)(WorkflowCriteriasModal);
