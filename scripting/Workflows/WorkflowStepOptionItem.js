import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { WorkflowStepOptionTypes, WorkflowStepTypes } from '../../store/enums';
import { mapStateToProps } from '../../outlets/';
import { isFunction } from '../../reactadmin/Utils';
import i18n from '../../i18n';

/**
 * Componente que muestra los detalles de una opción en la lista de opciones agregadas a un 
 * paso del flujo.
 *
 * @class WorkflowStepOptionItem
 * @extends {PureComponent}
 */
class WorkflowStepOptionItem extends PureComponent {
	stepClick(e) {
		const { item, onStepClick, } = this.props;
		e.preventDefault();
		e.stopPropagation();
		if (isFunction(onStepClick)) {
			onStepClick(item, e);
		}
	}
	render() {
		const { item, step, onClick, canDelete, onDelete, } = this.props;
		let typeText = '';
		if (item.id_type === WorkflowStepOptionTypes.STEP) {
			const rStep = item.referedWorkflowStep;
			const stepName = rStep ? rStep.name : '';
			const stepID = `(${item.value})`;
			typeText = (
				<Link
					onClick={rStep ? ((e) => this.stepClick(e)) : undefined}
					style={{ cursor: rStep ? 'pointer' : undefined, }}
				>
					<span>{rStep
						? i18n.t('GoToStep_Name', { stepName, stepID })
						: i18n.t('DeletedStep', { stepName, stepID })}
					</span>
				</Link >
			);
		} else if (item.id_type === WorkflowStepOptionTypes.VALUE) {
			typeText += i18n.t('ReturnValue_Value', { value: item.value });
		} else {
			typeText += i18n.t('NoConfigured');
		}

		const showDelete = step.id_type === WorkflowStepTypes.REGULAR &&
			isFunction(onDelete) && canDelete !== false;
		return (
			<div
				style={{
					display: 'flex',
					width: '100%',
					marginBottom: 2,
					borderBottom: '1px solid #eee',
				}}
			>
				<div style={{ flex: '1 1 auto', overflow: 'auto', padding: 5, wordBreak: 'break-all', }}>
					{item.name} - {typeText}
				</div>
				<div style={{ display: 'flex', flex: 'none', flexDirection: 'row', overflow: 'auto' }}>
					<div style={{ padding: 5, }}>
						<a href="#editRow" onClick={onClick}>
							<i className="fa text-muted fa-edit" />
						</a>
					</div>
					{showDelete &&
						<div style={{ padding: 5, }}>
							<a href="#deleteRow" onClick={onDelete}>
								<i className="fa text-muted fa-trash-o" />
							</a>
						</div>
					}
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(WorkflowStepOptionItem);
