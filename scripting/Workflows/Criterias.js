import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';

import {
	Panel,
	Col,
	Page,
	ModalFactory,
	DataTable,
	Factory,
	ButtonGroup,
	Input,
	PanelCollapsible,
	Tooltip,
} from '../../reactadmin/components/';
import { CriteriaModal } from './';
import i18n from '../../i18n';
import { filterActions } from '../../store/enums';
import { setFilterHandles, setItemHandles, } from '../../Utils';
import { DialogModal } from '../DialogModal';

const actions = ReduxOutlet('criterias', 'criteria').actions;
/**
 * Componente que controla la pantalla del mantenimiento de criterios.
 *
 * @class Criterias
 * @extends {Component}
 */
class Criterias extends Component {
	state = {
		item: {
			conditions: [],
		},
	}
	componentWillMount() {
		setItemHandles({ pageComponent: this, actions, itemName: 'Criteria' });
		setFilterHandles(this, actions, 'criterias', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Criterias'), },
		];
		// window.Scripting_Criterias = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	newItemClick(e) {
		e.preventDefault();
		this.setState({ item: { conditions: [] }, itemDisabled: false, });
		ModalFactory.show(this.modalNameRef);
	}

	renderFilters() {
		return (
			<PanelCollapsible 
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>} 
				contentStyle={{ padding: 5 }}
			>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="id">{i18n.t('ID')}:</label>
					<Input
						type="number" className="form-control"
						value={this.filterValue('id', '')}
						onFieldChange={(e) => this.filterChange({
							name: 'id',
							value: e.target.value,
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group input-group-sm col-md-3">
					<label htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={this.filterValue('name', '')}
						onFieldChange={(e) => this.filterChange({
							name: 'name',
							value: e.target.value,
							criteria: 'like',
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { criterias: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
		const { item, itemDisabled, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('criterias.add');
		const canDelete = user.hasPermissions('criterias.delete');
		return (
			<Page>
				<Factory
					modalref={this.modalNameRef} factory={CriteriaModal} 
					title={i18n.t('CriteriaInfo', item)}
					item={item} disabled={itemDisabled}
					onCreate={(newItem) => this.saveItem(newItem)}
					onUpdate={(newItem) => this.saveItem(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							schema={{
								fields: {
									name: i18n.t('Name'),
									center_name: user.master ? i18n.t('Center') : undefined,
									id: i18n.t('ID'),
								}
							}}
							{...{ pageSize, pageNumber, totalRows, rows, canDelete, }}
							onPageChange={(data) => this.pageChange(data)}
                            addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Criterias') })}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}

export default connect(mapStateToProps)(Criterias);
