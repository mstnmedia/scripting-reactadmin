import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Input,
	ButtonGroup,
	TagInput,
	FormGroup,
	Tooltip,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de un flujo.
 *
 * @class WorkflowModal
 * @extends {Component}
 */
class WorkflowModal extends Component {

	state = {}

	getItem(props) {
		return (props || this.props).item || {};
	}

	handleChange(property, value) {
		const item = this.getItem();
		item[property] = value;
		this.setState({ updaterProp: new Date() });
	}

	isAllowedUser() {
		const { user, } = this.props;
		return user.hasPermissions('worflows.add') || user.hasPermissions('workflows.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.state.disabled || this.props.disabled;
	}
	isValid() {
		const isMaster = this.props.user.isMaster();
		const item = this.getItem();
		return item.name && (!isMaster || item.id_center);
	}

	saveItem() {
		if (this.isValid()) {
			const item = this.getItem();
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}

	render() {
		const { id,
			id_center: centerID,
			center_name: centerName,
			name, tags, main, deleted,
		} = this.getItem();
		const isMaster = this.props.user.isMaster();
		return (
			<div role="form">
				{id && <FormGroup>
					<label className="block" htmlFor="name">{i18n.t('ID')}</label>
					<Input type="number" className="form-control" value={id} disabled />
				</FormGroup>}
				{isMaster && <FormGroup isValid={!!centerID}>
					<label className="control-label" htmlFor="center">{i18n.t('Center')}:</label>
					{centerID ? <span> {centerID}</span> : null}
					<Tooltip className="balloon" tag="TooltipCenterField">
						<RemoteAutocomplete
							url={`${window.config.backend}/centers/center`}
							getItemValue={(item) => NameIDText(item)}
							value={centerName || ''}
							onFieldChange={(e, text) => {
								this.handleChange('id_center', 0);
								this.handleChange('center_name', text);
							}}
							onSelect={(text, item) => {
								this.handleChange('id_center', item.id);
								this.handleChange('center_name', item.name);
							}}
							onBlur={() => {
								if (!this.getItem().id_center) {
									this.handleChange('center_name', '');
								}
							}}
							disabled={id || this.isFieldDisabled()} required
						/>
					</Tooltip>
				</FormGroup>}

				<FormGroup isValid={!!name}>
					<label htmlFor="name">{i18n.t('Name')}</label>
					<Input
						type="text" className="form-control has-error"
						placeholder={i18n.t('Name')} value={name || ''}
						onFieldChange={(e) => this.handleChange('name', e.target.value)}
						disabled={this.isFieldDisabled()}
						required
					/>
					<span className="text-muted help-block m-b-none">{this.state.errormessage}</span>
				</FormGroup>
				<FormGroup>
					<label htmlFor="tags">{i18n.t('Tags')}</label>
					<Tooltip className="balloon" tag="TooltipTagField">
						<TagInput
							value={tags} placeholder={i18n.t('Tags')}
							onChange={({ value }) => this.handleChange('tags', value)}
							disabled={this.isFieldDisabled()} required
						/>
					</Tooltip>
					<span className="text-muted help-block m-b-none">{this.state.errormessage}</span>
				</FormGroup>
				<FormGroup>
					<label className="block" htmlFor="main">{i18n.t('Main')}</label>
					<ButtonGroup
						items={{
							active: activeBoolean[!!main],
							values: valuesBoolean,
						}}
						onChange={(value) => this.handleChange('main', value ? 1 : 0)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<FormGroup>
					<label htmlFor="deleted">{i18n.t('Disabled')}</label>
					<Input
						type="checkbox"
						className="inline" checked={deleted}
						containerStyle={{ margin: 5, }}
						onFieldChange={(e) => this.handleChange('deleted', e.target.checked ? 1 : 0)}
						disabled={this.isFieldDisabled()}
					/>
				</FormGroup>
				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button"
							onClick={(e) => this.saveItem(e)}
							className="btn btn-info"
							disabled={this.isDisabled() || !this.isValid()}
						>{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button"
						data-dismiss="modal"
						aria-hidden="true"
						className="btn btn-danger"
						disabled={this.isDisabled()}
					>{i18n.t('Close')}</button>
				</div>
			</div>
		);
	}
}
const activeBoolean = {
	true: i18n.t('Yes'),
	false: i18n.t('No'),
};
const valuesBoolean = {
	[i18n.t('Yes')]: true,
	[i18n.t('No')]: false,
};

export default connect(mapStateToProps)(WorkflowModal);
