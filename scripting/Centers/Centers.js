/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule DataGrid
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { ReduxOutlet, mapStateToProps } from '../../outlets/';
import {
	Page,
	Col,
	Panel, PanelCollapsible,
	DataTable,
	Factory,
	Input,
	ButtonGroup,
	Tooltip,
} from '../../reactadmin/components/';
import { DialogModal } from '../DialogModal';
import { CenterModal } from './';
import i18n from '../../i18n';
import { setFilterHandles, setItemHandles, } from '../../Utils';
import { filterActions } from '../../store/enums';

const actions = ReduxOutlet('centers', 'center').actions;
/**
 * Componente que controla la pantalla del mantenimiento de centros.
 *
 * @class Centers
 * @extends {Component}
 */
class Centers extends Component {
	state = {}
	componentWillMount() {
		setItemHandles({ pageComponent: this, actions, itemName: 'Center' });
		setFilterHandles(this, actions, 'centers', true)
			.initFirstPage();
		global.getBreadcrumbItems = () => [
			{ label: i18n.t('Home'), to: '/' },
			{ label: i18n.t('Centers'), },
		];
		// window.Scripting_Centers = this;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	renderFilters() {
		return (
			<PanelCollapsible
				title={<Tooltip position="right" tag="TooltipFilters">{i18n.t('Filters')}</Tooltip>}
				contentStyle={{ padding: 5 }}
			>
				<div className="form-group col-md-3">
					<label htmlFor="id">{i18n.t('ID')}:</label>
					<Input
						type="number" className="form-control"
						value={this.filterValue('id', '')}
						onFieldChange={(e) => this.filterChange({
							name: 'id',
							value: e.target.value,
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>
				<div className="form-group col-md-3">
					<label htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={this.filterValue('name', '')}
						onFieldChange={(e) => this.filterChange({
							name: 'name',
							value: e.target.value,
							criteria: 'like',
						})}
						onKeyDown={this.onKeyDownSimple}
					/>
				</div>

				<div className="col-xs-12 inline-block">
					<hr style={{ marginTop: 5, marginBottom: 5 }} />
					<ButtonGroup
						items={{ values: filterActions(), }}
						onChange={(action) => this[`${action}Click`]()}
					/>
				</div>
			</PanelCollapsible>
		);
	}
	render() {
		const { centers: { list, pageNumber, pageSize, totalRows, }, user, } = this.props;
		const { item, itemDisabled, confirmModalProps, } = this.state;
		const rows = list || [];
		const canAdd = user.hasPermissions('centers.add');
		const canDelete = user.hasPermissions('centers.delete');
		return (
			<Page>
				<Factory
					modalref={this.modalNameRef}
					factory={CenterModal} large
					title={i18n.t('CenterDetails', item)}
					item={item} disabled={itemDisabled}
					onCreate={(newItem) => this.saveItem(newItem)}
					onUpdate={(newItem) => this.saveItem(newItem)}
				/>
				<DialogModal
					modalref="confirmModal"
					{...confirmModalProps}
				/>
				<Col>
					{this.renderFilters()}
					<Panel>
						<DataTable
							{...{ rows, pageSize, pageNumber, totalRows }}
							onPageChange={(data) => this.pageChange(data)}
							schema={{
								fields: {
									name: i18n.t('Name'),
									id: i18n.t('ID'),
								}
							}}
                            addTooltip={i18n.t('TooltipTableAdd', { screen: i18n.t('Centers') })}
							onAdd={canAdd && ((e) => this.newItemClick(e))}
							canDelete={canDelete}
							onDelete={(id) => this.handleItemDelete(id)}
							link={(row) => this.selectItem(row)}
						/>
					</Panel>
				</Col>
			</Page>
		);
	}
}
export default connect(mapStateToProps)(Centers);
