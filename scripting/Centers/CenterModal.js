import React, { Component } from 'react';
import { connect } from 'react-redux';

import { mapStateToProps } from '../../outlets/ReduxOutlet';
import {
	Row,
	FormGroup,
	DataTable,
	Input, NumericInput, Tooltip,
} from '../../reactadmin/components/';
import i18n from '../../i18n';
import RemoteAutocomplete from '../RemoteAutocomplete';
import { NameIDText } from '../../Utils';

/**
 * Componente que controla el formulario de los detalles de un centro.
 *
 * @class CenterModal
 * @extends {Component}
 */
class CenterModal extends Component {

	state = {}

	componentWillReceiveProps(props) {
		if (this.props.item !== props.item) {
			this.resetState();
		}
	}

	getItem(props) {
		return (props || this.props).item || {};
	}
	getManagers() {
		let managers = this.getItem().managers;
		if (!this.getItem().managers) {
			this.getItem().managers = managers = [];
		}
		return managers;
	}

	resetState() {
		this.setState({
			managerID: '',
			managerName: '',
			goalSeconds: '',
			marginPercentage: '',
		});
	}

	handleChange(prop, value) {
		const item = this.getItem();
		item[prop] = value;
		this.setState({ updaterProp: new Date() });
	}
	handleManagerChange(prop, value) {
		this.setState({ [prop]: value });
	}

	addManager() {
		const { managerID, managerName, goalSeconds, marginPercentage, } = this.state;
		const { id, name, } = this.getItem();
		if (this.isValidManager()) {
			const manager = {
				id_center: id,
				id_manager: managerID,
				center_name: name,
				manager_name: managerName,
				goal_seconds: goalSeconds,
				margin_percentage: marginPercentage,
			};
			const managers = this.getManagers();
			managers.push(manager);
			this.resetState();
		}
	}
	deleteManager(idx) {
		if (idx === -1) return;
		const managers = this.getManagers();
		managers.splice(idx, 1);
		this.setState({});
	}

	saveItem(e) {
		e.preventDefault();
		if (this.isValid()) {
			const item = this.getItem();
			const managers = this.getManagers();
			for (let i = 0; i < managers.length; i++) {
				const aux = managers[i];
				delete aux.idx;
			}
			if (!item.id) {
				this.props.onCreate(item);
			} else {
				this.props.onUpdate(item);
			}
		}
	}
	isAllowedUser() {
		return this.props.user.hasPermissions('centers.add', 'centers.edit');
	}
	isFieldDisabled() {
		return this.isDisabled() || !this.isAllowedUser();
	}
	isDisabled() {
		return this.props.disabled || this.state.disabled;
	}
	isValid() {
		const item = this.getItem();
		return item.name;
	}
	isValidManager() {
		const { managerID, managerName, goalSeconds, marginPercentage, } = this.state;
		return managerID && managerName && goalSeconds && marginPercentage;
	}

	renderAddManager() {
		const { managerID, managerName, goalSeconds, marginPercentage, } = this.state;
		return (
			<div className={`m-t-md ${this.isFieldDisabled() ? 'hidden' : ''}`}>
				<Row>
					<FormGroup className="form-group col-sm-6 col-md-5">
						<label className="control-label inline-block" htmlFor="manager">
							{i18n.t('Manager')}:
						</label>
						{managerID ? <span> {managerID}</span> : null}
						<Tooltip className="balloon" tag="TooltipCenterManagerField">
							<RemoteAutocomplete
								wrapperStyle={{ display: 'block' }}
								value={managerName || ''}
								url={`${window.config.backend}/employees/employee`}
								getItemValue={(item) => NameIDText(item)}
								onFieldChange={(e, text) => {
									this.handleManagerChange('managerID', 0);
									this.handleManagerChange('managerName', text);
								}}
								onSelect={(text, item) => {
									// console.log('autocomplete selected: ', text, item);
									this.handleManagerChange('managerID', item.id);
									this.handleManagerChange('managerName', item.name);
								}}
								onBlur={() => {
									if (!this.state.managerID) {
										this.handleManagerChange('managerName', '');
									}
								}}
								disabled={this.isFieldDisabled()} required
							/>
						</Tooltip>
					</FormGroup>
					<FormGroup isValid={!managerID || !!goalSeconds} className="col-sm-3 col-md-2">
						<label className="control-label" htmlFor="goal">{i18n.t('GoalSeconds')}:</label>
						<Tooltip className="balloon" tag="TooltipCenterGoalField">
							<NumericInput
								natural
								value={goalSeconds || ''}
								onFieldChange={(e) => {
									this.handleManagerChange('goalSeconds', e.target.value * 1);
								}}
							/>
						</Tooltip>
					</FormGroup>
					<FormGroup isValid={!managerID || !!marginPercentage} className="col-sm-3 col-md-2">
						<label className="control-label" htmlFor="margin">{i18n.t('MarginPercentage')}:</label>
						<Tooltip className="balloon" tag="TooltipCenterMarginField">
							<NumericInput
								natural max={100}
								value={marginPercentage || ''}
								onFieldChange={(e) => {
									this.handleManagerChange('marginPercentage', e.target.value * 1);
								}}
							/>
						</Tooltip>
					</FormGroup>
					<FormGroup className="form-group col-sm-6 col-md-3">
						<button
							type="button" className="btn btn-info m-t-md"
							onClick={(e) => this.addManager(e)}
							disabled={!this.isValidManager() || this.isDisabled()}
						>{i18n.t('AddManager')}</button>
					</FormGroup>
				</Row>
			</div>
		);
	}
	render() {
		const { id, name, } = this.getItem();
		const managers = this.getManagers()
			.map((i, idx) => Object.assign({}, i, { idx }));
		const canDelete = this.props.user.hasPermissions('centers.delete');
		return (
			<form>
				{id &&
					<FormGroup isValid={!!id}>
						<label className="control-label" htmlFor="name">{i18n.t('ID')}</label>
						<Input type="number" className="form-control" value={id} disabled />
					</FormGroup>
				}
				<FormGroup isValid={!!name}>
					<label className="control-label" htmlFor="name">{i18n.t('Name')}:</label>
					<Input
						type="text" className="form-control"
						value={name || ''} placeholder={i18n.t('Name')}
						onFieldChange={(e) => {
							this.handleChange('name', e.target.value);
						}}
						disabled={this.isFieldDisabled()} required
					/>
				</FormGroup>
				<FormGroup>
					<label className="block" htmlFor="managers" style={{ marginBottom: -10 }}>
						{i18n.t('Managers')}:
					</label>
					<DataTable
						schema={{
							fields: {
								margin_percentage: i18n.t('MarginPercentage'),
								goal_seconds: i18n.t('GoalSeconds'),
								manager_name: i18n.t('ManagerName'),
								id_manager: i18n.t('ManagerID'),
								id: i18n.t('ID'),
							},
						}}
						rows={managers}
						rowKey={(row) => row.idx}
						canDelete={canDelete}
						onDelete={(idx) => this.deleteManager(idx)}
					/>
					{this.renderAddManager()}
				</FormGroup>

				<div className="">
					<hr />
					{this.isAllowedUser() &&
						<button
							type="button" className="btn btn-info"
							onClick={(e) => this.saveItem(e)}
							disabled={this.isDisabled() || !this.isValid()}
						>
							{!id ? i18n.t('Add') : i18n.t('SaveChanges')}
						</button>
					}
					<button
						type="button" className="btn btn-danger"
						data-dismiss="modal" aria-hidden="true"
					>{i18n.t('Close')}</button>
				</div>
			</form>
		);
	}
}

export default connect(mapStateToProps)(CenterModal);
