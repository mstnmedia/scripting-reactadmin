import React, { Component } from 'react';
import { connect } from 'react-redux';

/**
 * Componente en desuso.
 *
 * @class Workflows
 * @extends {Component}
 * @deprecated
 */
class Workflows extends Component {
	render() {
		return (
			<div>{this.props.user.email}</div> // from redux
		);
	}
}

function mapStateToProps(state) {
	return {
		token: state.app.token,
		user: state.user
	};
}

export default connect(mapStateToProps)(Workflows);
