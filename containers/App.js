/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule App
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import { App } from '../reactadmin/containers/';
import { mapStateToProps, ReduxOutlet, } from '../outlets/';
import i18n from '../i18n';

import userActions from '../actions/';
import { LanguagePrefix } from '../store/enums';
import { respErrorDetails } from '../reactadmin/Utils';

const transactionActions = ReduxOutlet('transactions', 'transaction').actions;
/**
 * Componente que envuelve a todos los componentes que controlan las pantallas.
 * Se encarga principalmente de consultar el perfil del usuario logueado y redirigirlo
 * desde cualquier pantalla cuando tiene una transacción abierta. También remueve todos
 * los fondos de modales cuando cambia la navegación.
 *
 * @class LocalApp
 * @extends {Component}
 */
class LocalApp extends Component {
	componentWillMount() {
		const { dispatch, token, } = this.props;
		dispatch(userActions.fetchMe({
			token,
			success: (resp) => {
				const user = resp.data;
				const lang = (user.language || '').replace(LanguagePrefix, '');
				i18n.changeLanguage(lang, (error) => {
					if (error) {
						// console.log('Error setting user language', error);
					}
				});
				const isMaster = user.permissions.indexOf('scripting.master') !== -1;
				const canAdd = user.permissions.indexOf('transactions.add') !== -1;
				if (!isMaster && canAdd) {
					if (window.config.ignoreActiveTransactions) return;
					const where = [
						{ name: 'state', value: '1', },
						{ name: 'id_employee', value: user.id, },
					];
					dispatch(transactionActions.fetch(token, where, '', true, (response) => {
						// console.log('Active transactions resp: ', response);
						const items = (response.data && response.data.items) || [];
						if (items.length) {
							this.props.router.replace(`/data/transactions/${items[0].id}`);
						}
					}, (error) => {
						console.error('Error fetching active transactions: ', error);
						window.message.error(i18n.t('ErrorFetchingUserActiveTransactions'), 0,
							respErrorDetails(error));
					}, false));
				}
			},
			failure: (resp) => window.message.error(i18n.t('ErrorFetchingUserInfo'), 0,
				respErrorDetails(resp)),
		}));
		this.props.router.listen((/* location, action */) => {
			// console.log(location, action);
			$('.modal-backdrop').remove();
		});
		window.Scripting_LocalApp = this;
	}
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	render() {
		return (
			<App {...this.props} />
		);
	}
}

export default connect(mapStateToProps)(LocalApp);
