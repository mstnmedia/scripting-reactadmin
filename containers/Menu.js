/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Menu
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import { mapStateToProps, } from '../outlets/';

import i18n from '../i18n';
import { Menu } from '../reactadmin/containers/';
import { MenuItem, SubMenuItem } from '../reactadmin/components/widgets/';

/**
 * Componente que muestra el componente base Menu con las diferentes opciones a las que tiene 
 * acceso.
 *
 * @class LocalMenu
 * @extends {Component}
 */
class LocalMenu extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}
	renderAlerts(position) {
		const { user } = this.props;
		if (!user) return null;
		const alerts = user.hasPermissions('alerts.show');
		const alertTriggereds = user.hasPermissions('alerts.history');
		if (!alerts && !alertTriggereds) return null;
		return (
			<MenuItem
				link={'/data/alerts'}
				icon='fa-exclamation-triangle'
				color='bg-danger'
				linkText={i18n.t('Alerts')}
				currentPage={this.props.currentPage}
				// tooltip={{ tag: 'TooltipMenuIconAlerts', position, }}
			>
				{alerts && <SubMenuItem
					link={'/data/alerts'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Alerts')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuAlerts', position, }}
				/>}
				{alertTriggereds && <SubMenuItem
					link={'/data/alerts/history'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('AlertTriggereds')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuAlertTriggereds', position, }}
				/>}
			</MenuItem>
		);
	}
	renderLanguage(position) {
		const { user } = this.props;
		if (!user) return null;
		const language = user.hasPermissions('language.show');
		if (!language) return null;
		return (
			<MenuItem
				link={'/data/languages'}
				icon='fa-language'
				color='bg-danger'
				linkText={i18n.t('LanguagesMenu')}
				currentPage={this.props.currentPage}
				// tooltip={{ tag: 'TooltipMenuIconLanguage', position, }}
			>
				<SubMenuItem
					link={'/data/languages'}
					icon='fa-language'
					color='bg-danger'
					linkText={i18n.t('LanguagesByUser')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuLanguagesByUser', position, }}
				/>
				<SubMenuItem
					link={'/data/languages/systemTags'}
					icon='fa-language'
					color='bg-danger'
					linkText={i18n.t('SystemTags')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuSystemTags', position, }}
				/>
			</MenuItem>
		);
	}
	renderPersons(position) {
		const { user } = this.props;
		if (!user) return null;
		const centers = user.hasPermissions('centers.show');
		const delegations = user.hasPermissions('delegations.show');
		if (!centers && !delegations) return null;
		return (
			<MenuItem
				link={'/config'}
				icon='fa-users'
				color='bg-success'
				linkText={i18n.t('MenuPersons')}
				currentPage={this.props.currentPage}
				// tooltip={{ tag: 'TooltipMenuIconPersons', position, }}
			>
				{centers && <SubMenuItem
					link={'/data/centers'}
					// icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Centers')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuCenters', position, }}
				/>}
				{delegations && <SubMenuItem
					link={'/data/delegations'}
					// icon='fa-users'
					color='bg-danger'
					linkText={i18n.t('Delegations')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuDelegations', position, }}
				/>}
			</MenuItem>
		);
	}
	renderTransactions(position) {
		const { user } = this.props;
		if (!user) return null;
		const transactions = user.hasPermissions('transactions.show');
		const active = user.hasPermissions('transactions.active');
		const categories = user.hasPermissions('categories.show');
		const endCall = user.hasPermissions('endcall.show');
		if (!transactions && !active && !categories && !endCall) return null;
		return (
			<MenuItem
				link={'/data/transactions'}
				icon='fa-phone'
				color='bg-danger'
				linkText={i18n.t('Transactions')}
				currentPage={this.props.currentPage}
				// tooltip={{ tag: 'TooltipMenuIconTransactions', position, }}
			>
				{transactions && <SubMenuItem
					link={'/data/transactions'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Transactions')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuTransactions', position, }}
				/>}
				{active && <SubMenuItem
					link={'/data/transactions/active'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('ActiveTransactions')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuActiveTransactions', position, }}
				/>}
				{categories && <SubMenuItem
					link={'/data/categories'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Categories')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuCategories', position, }}
				/>}
				{endCall && <SubMenuItem
					link={'/data/endCall'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('EndCall')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuEndCall', position, }}
				/>}
			</MenuItem>
		);
	}
	renderWorkflows(position) {
		const { user } = this.props;
		if (!user) return null;
		const contents = user.hasPermissions('contents.show');
		const workflows = user.hasPermissions('workflows.show');
		const criterias = user.hasPermissions('criterias.show');
		const interfaces = user.hasPermissions('interfaces.show');
		if (!contents && !workflows && !criterias && !interfaces) return null;
		return (
			<MenuItem
				link={'/data/workflows'}
				// icon='fa-code-fork'
				icon='fa-sitemap'
				color='bg-danger'
				linkText={i18n.t('Workflows')}
				currentPage={this.props.currentPage}
				// tooltip={{ tag: 'TooltipMenuIconWorkflows', position, }}
			>
				{workflows && <SubMenuItem
					link={'/data/workflows'}
					icon='fa-sitemap'
					color='bg-success'
					linkText={i18n.t('Workflows')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuWorkflows', position, }}
				/>}
				{criterias && <SubMenuItem
					link={'/data/criterias'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Criterias')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuCriterias', position, }}
				/>}
				{contents && <SubMenuItem
					link={'/data/contents'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Contents')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuContents', position, }}
				/>}
				{interfaces && <SubMenuItem
					link={'/data/interfaces'}
					icon='fa-plug'
					color='bg-success'
					linkText={i18n.t('Interfaces')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuInterfaces', position, }}
				/>}
			</MenuItem>
		);
	}
	render() {
		const { user } = this.props;
		if (!user) return null;
		const dashboard = user.hasPermissions('dashboard.show');
		const reports = user.hasPermissions('reports.show');
		const docs = user.hasPermissions('docs.show');
		const screen = global.getBootstrapViewport();
		const position = screen.code === 'xs' ? 'down' : 'right';
		return (
			<Menu {...this.props}>
				{dashboard && <MenuItem
					link={'/'}
					icon='fa-home'
					color='bg-danger'
					linkText={i18n.t('Dashboard')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuIconDashboard', position, }}
				/>}
				{this.renderTransactions(position)}
				{this.renderAlerts(position)}
				{this.renderWorkflows(position)}
				{this.renderPersons(position)}
				{this.renderLanguage(position)}
				{reports && <MenuItem
					link={'/data/reports'}
					icon='fa-th-list'
					color='bg-success'
					linkText={i18n.t('Reports')}
					currentPage={this.props.currentPage}
					tooltip={{ tag: 'TooltipMenuIconReports', position, }}
				/>}
				{docs && <MenuItem
					link={'/data/docs'}
					icon='fa-book'
					color='bg-success'
					linkText={i18n.t('Documentations')}
					currentPage={this.props.currentPage}
					tooltip={{
						tag: 'TooltipMenuIconDocumentations',
						position: screen.code === 'xs' ? 'up' : 'right',
					}}
				/>}
			</Menu>
		);
	}
}

export default connect(mapStateToProps)(LocalMenu);
