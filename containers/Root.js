/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule Root
 */

import React, { Component } from 'react';
import { Route, IndexRoute } from 'react-router';
import shallowCompare from 'react-addons-shallow-compare';
import i18n from '../i18n';

import { App } from './';
import '../store/enums';
import {
	Root,
	// App,
	Analytics,
	Account,
	Boards,
	Bootstrap,
	Buttons,
	Charts,
	Components,
	Dashboard,
	DataForms,
	DataGrid,
	Docs,
	Email,
	FontAwesome,
	Forms,
	Landing,
	Maps,
	MaterialIcons,
	Modals,
	Note,
	Notes,
	Pins,
	Profile,
	Settings,
	Signup,
	Tables,
	Widgets,
} from '../reactadmin/containers/';

import { Workflows, Workflow, WorkflowStep, Criterias, } from '../scripting/Workflows/';

import {
	ActiveTransactions,
	Categories,
	Transactions,
	Transaction,
	TransactionCreate,
	EndCalls,
} from '../scripting/Transactions/';

import { Interfaces, Interface } from '../scripting/Interfaces/';
import { Contents, } from '../scripting/Contents/';

import Test from '../scripting/Test';
import { Centers, } from '../scripting/Centers';
import { PreviousWorkflow, PreviousWorkflowStep } from '../scripting/PreviousWorkflows/';
import { Delegations } from '../scripting/Delegations';
import { Languages, SystemTags, } from '../scripting/Languages';
import { Alerts, AlertTriggereds } from '../scripting/Alerts';
// import Test1 from '../scripting/Test1/Test1';
import { Documentations, Documentation } from '../scripting/Documentations';
import Reports from '../scripting/Reports/Reports';

/**
 * Componente carga al componente base Root que controla que componente controla la pantalla cuando 
 * el usuario navega y registra las rutas disponibles.
 *
 * @export
 * @class LocalRoot
 * @extends {Component}
 */
export default class LocalRoot extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	}

	render() {
		return (/* eslint-disable max-len */
			<Root>
				<Route path='/' component={App} pageName={i18n.t('Home')}>
					<IndexRoute component={Dashboard} pageName={i18n.t('Dashboard')} pageDescription={i18n.t('DashboardDescription')} />

					<Route path='/analytics' component={Analytics} pageName='Analytics' pageDescription='Simple analytics example.' />

					<Route path='/account' component={Account} pageName='Account' pageDescription='Manage your account.' />
					<Route path='/settings' component={Settings} pageName='Settings' pageDescription='Application Settings.' />
					<Route path='/signup' component={Signup} pageName='' pageDescription='' />
					<Route path='/profile' component={Profile} pageName='Profile' pageDescription='Manage your profile' />
					<Route path='/charts' component={Charts} pageName='Charts' pageDescription='Chartist Charts' />

					<Route path='/ui/general' component={Bootstrap} pageName='General' pageDescription='Bootstrap 3' />
					<Route path='/ui/buttons' component={Buttons} pageName='Buttons' pageDescription='UI Buttons.' />
					<Route path='/ui/modals' component={Modals} pageName='Modals' pageDescription='Modal dialogs' />
					<Route path='/ui/fontawesome' component={FontAwesome} pageName='Fontawesome Icons' pageDescription='React stateless component.' />
					<Route path='/ui/materialicons' component={MaterialIcons} pageName='Material Design Icons' pageDescription='React stateless component.' />
					<Route path='/ui/tables' component={Tables} pageName='Tables' pageDescription='Simple Tables' />

					<Route path='/apps/email' component={Email} pageName='Email' pageDescription='react powered email.' />
					<Route path='/apps/maps' component={Maps} pageName='Maps' pageDescription='react powered google maps.' />
					<Route path='/apps/boards' component={Boards} pageName='Boards' pageDescription='Boards page' />
					<Route path='/apps/boards/:id' component={Pins} pageName='Pins' pageDescription='' />
					<Route path='/apps/notes' component={Notes} pageName='Notes' pageDescription='Notes page' />
					<Route path='/apps/notes/:id' component={Note} pageName='Notes' pageDescription='A simple notes app.' />

					<Route path='/forms' component={Forms} pageName='Forms' pageDescription='Layout and Elements' />
					<Route path='/docs' component={Docs} pageName='Docs' pageDescription='Sample Docs' />
					<Route path='/components' component={Components} pageName='Components' pageDescription='Custom React components.' />
					<Route path='/widgets' component={Widgets} pageName='Widgets' pageDescription='React Admin Widgets' />

					<Route path='/data/forms' component={DataForms} pageName='Data Forms' pageDescription='Data driven forms.' />
					<Route path='/data/grid' component={DataGrid} pageName='Data Grid' pageDescription='Data driven data grid.' />

					<Route path='/data/docs' component={Documentations} pageName={i18n.t('Documentations')} pageDescription={i18n.t('DocumentationsDescription')} />
					<Route path='/data/docs/:id' component={Documentation} pageName={i18n.t('Documentation')} pageDescription={i18n.t('DocumentationDescription')} />

					<Route path='/data/workflows' component={Workflows} pageName={i18n.t('Workflows')} pageDescription={i18n.t('WorkflowsDescription')} />
					<Route path='/data/workflows/:id' component={Workflow} pageName={i18n.t('Workflow')} pageDescription={i18n.t('WorkflowDescription')} />
					<Route path='/data/workflows/:id_workflow/step/:id' component={WorkflowStep} pageName={i18n.t('Step')} pageDescription={i18n.t('StepDescription')} />
					<Route path='/data/workflows/:id_workflow/version/:id_version' component={PreviousWorkflow} pageName={i18n.t('PreviousWorkflow')} pageDescription={i18n.t('PreviousWorkflowDescription')} />
					<Route path='/data/workflows/:id_workflow/version/:id_version/step/:id_step' component={PreviousWorkflowStep} pageName={i18n.t('PreviousWorkflowStep')} pageDescription={i18n.t('PreviousWorkflowStepDescription')} />

					<Route path='/data/transactions' component={Transactions} pageName={i18n.t('Transactions')} pageDescription={i18n.t('TransactionsDescription')} />
					<Route path='/data/transactions/active' component={ActiveTransactions} pageName={i18n.t('ActiveTransactions')} pageDescription={i18n.t('ActiveTransactionsDescription')} />
					<Route path='/data/transactions/create' component={TransactionCreate} pageName={i18n.t('NewTransaction')} pageDescription={i18n.t('NewTransactionDescription')} />
					<Route path='/data/transactions/:id' component={Transaction} pageName={i18n.t('Transaction')} pageDescription={i18n.t('TransactionDescription')} />

					<Route path='/data/categories' component={Categories} pageName={i18n.t('Categories')} pageDescription={i18n.t('CategoriesDescription')} />
					<Route path='/data/delegations' component={Delegations} pageName={i18n.t('Delegations')} pageDescription={i18n.t('DelegationsDescription')} />
					<Route path='/data/endCall' component={EndCalls} pageName={i18n.t('EndCallReasons')} pageDescription={i18n.t('EndCallReasonsDescription')} />
					<Route path='/data/languages' component={Languages} pageName={i18n.t('LanguagesByUser')} pageDescription={i18n.t('LanguagesDescription')} />
					<Route path='/data/languages/systemTags' component={SystemTags} pageName={i18n.t('SystemTags')} pageDescription={i18n.t('SystemTagsDescription')} />

					<Route path='/data/centers' component={Centers} pageName={i18n.t('Centers')} pageDescription={i18n.t('CentersDescription')} />
					<Route path='/data/contents' component={Contents} pageName={i18n.t('Contents')} pageDescription={i18n.t('ContentsDescription')} />
					<Route path='/data/criterias' component={Criterias} pageName={i18n.t('Criterias')} pageDescription={i18n.t('CriteriasDescription')} />

					<Route path='/data/interfaces' component={Interfaces} pageName={i18n.t('Interfaces')} pageDescription={i18n.t('InterfacesDescription')} />
					<Route path='/data/interfaces/:id' component={Interface} pageName={i18n.t('Interface')} pageDescription={i18n.t('InterfaceDescription')} />

					<Route path='/data/alerts' component={Alerts} pageName={i18n.t('Alerts')} pageDescription={i18n.t('AlertTriggeredsDescription')} />
					<Route path='/data/alerts/history' component={AlertTriggereds} pageName={i18n.t('AlertTriggereds')} pageDescription={i18n.t('AlertTriggeredsDescription')} />
					<Route path='/data/reports' component={Reports} pageName={i18n.t('Reports')} pageDescription={i18n.t('ReportsDescription')} />
				</Route>
				<Route path='/landing' component={Landing} />
				<Route path='/test' component={Test} />
			</Root>
		);/* eslint-enable max-len */
	}
}
